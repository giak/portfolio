/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./src/.vuepress/**/*.{vue,js,ts,jsx,tsx,md}",
    "./src/**/*.{vue,js,ts,jsx,tsx,md}",
  ],
  // Important: désactiver le preflight pour éviter les conflits avec theme-hope
  corePlugins: {
    preflight: false,
  },
  // Important: prefix pour éviter les conflits de noms
  prefix: 'tw-',
  theme: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
  // Important: désactiver le mode important pour éviter les conflits
  important: false,
} 