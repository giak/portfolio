---
title: A propos
description: "Une brève présentation de mon environnement de travail et de mes centres d'intérêt"
index: true
date: 2024-03-01
sticky: false
star: 3
icon: circle-info
category:
    - information
---

<div class="article-content">

## Un site vitrine technique

Capitalisant sur mon expérience en documentation technique et support de formation avec [VuePress](https://v2.vuepress.vuejs.org), j'ai naturellement évolué vers la création d'un portfolio.  
Pour accélérer le développement, j'ai choisi d'utiliser [VuePress Theme Hope](https://theme-hope.vuejs.press), un thème robuste découvert via [awesome-vuepress](https://github.com/vuepress/awesome-vuepress/blob/main/v2.md).  
Cette solution offre une excellente base technique, facilement personnalisable et déployable sur diverses plateformes.

## Environnement de développement professionnel

### Infrastructure matérielle optimisée

-   **Poste de travail ergonomique** : Bureau sur mesure équipé de 3 écrans 27" sur bras articulés
-   **Périphériques haute qualité** : Clavier mécanique, trackball, webcam Full HD 1080p, système audio professionnel (Edifier + Sennheiser)
-   **Station de travail** : [MinisForum UM780-XT](https://store.minisforum.com/products/minisforum-um780-xtx) sous Linux Mint
-   **Infrastructure réseau** :
    -   NAS délocalisé sous Debian/OpenMediaVault pour la sécurisation des données
    -   Protection électrique : 2 onduleurs APC 900 VA (régulation tension, protection réseau)
    -   Connectivité : Fibre optique + infrastructure réseau redondante (switch + CPL)
-   **Station mobile** : Laptop Windows pour formations et présentations clients

<img src="./work.jpg" alt="Espace de travail" title="Setup de développement"/>

### Stack logicielle moderne

-   **Système d'exploitation** : [Linux Mint 22 Cinnamon](https://www.linuxmint.com) - Une distribution stable et performante
-   **Environnement de développement** :
    -   [VSCode](https://code.visualstudio.com/) : IDE principal pour JavaScript/TypeScript
    -   [Cursor IDE](https://www.cursor.com/) : Extension de VSCode avec IA intégrée
    -   [GitKraken](https://www.gitkraken.com/) : Interface graphique Git
    -   [Bruno](https://www.usebruno.com/) : Tests API et requêtes réseau
    -   [Docker](https://www.docker.com/) + [Portainer](https://www.portainer.io/) : Conteneurisation
    -
-   **Outils de productivité** :
    -   [Figma](https://www.figma.com/) : Design UI/UX
    -   [Zsh/Oh My Zsh](https://ohmyz.sh) : Terminal avancé
    -   [Keepassxc](https://keepassxc.org/) : Gestion sécurisée des accès
    -   [Obsidian](https://obsidian.md/) : Prises de notes transférables à Notion.so
    -

### Sécurité et sauvegarde

-   **Stratégie de backup** :
    -   Sauvegarde système hebdomadaire
    -   Sauvegarde données quotidienne
    -   Réplication sur NAS délocalisé

### Services cloud sélectionnés

-   **Développement** : [Github](https://github.com/giak) & [GitLab](https://gitlab.com/giak)
-   **Documentation** : [Notion.so](https://www.notion.so) avec sauvegarde via API
-   **Communication** : [Proton](https://proton.me/) (Mail, VPN, Cloud)
-   **Gestion de projet** :
    -   [Grimp.io](https://www.grimp.io/) : Suivi commercial
    -   [Wrike](https://www.wrike.com) : Gestion de projet agile

## Au-delà du code : une approche holistique

### Vision professionnelle

Je mets l'accent sur le partenariat et la confiance plutôt que sur la pure technique.
Cependant, dans notre domaine, la démonstration des compétences est essentielle - d'où l'importance d'un portfolio et d'un historique Git bien documenté.

### Centres d'intérêt diversifiés

#### Parcours technologique

De mes débuts sur TO7 et Amstrad jusqu'aux architectures modernes, ma passion pour l'informatique n'a cessé de croître, me poussant à en faire ma profession.

#### Développement durable et résilience

L'autonomie énergétique, la gestion des ressources et la collaboration communautaire sont des principes que j'applique tant dans ma vie personnelle que dans mes approches de développement.

#### Agriculture et artisanat

-   Jardinage et permaculture
-   Transformation et conservation alimentaire
-   Bricolage et solutions DIY (pompe solaire, séchoir solaire, électricité, etc.)
-   Électricité solaire avec batteries

<img src="./home.jpg" alt="Environnement personnel" title="Espace de vie"/>

#### Production musicale

**Setup DJ & Production** :

-   [Traktor Kontrol S8](https://fr.audiofanzine.com/surface-de-controle-dj/native-instruments/traktor-kontrol-s8/) : Surface de contrôle DJ
-   [Akai Professional Force](https://fr.audiofanzine.com/sequenceur-sampleur/akai/apc-live/) : Séquenceur/sampleur
-   [Korg Kaoss Pad 3 KP3+](https://fr.audiofanzine.com/effet-dj/korg/kaoss-pad-3-kp3plus/) : Processeur d'effets
-   **Monitoring** :
    -   [Mackie Big Knob Studio+](https://fr.audiofanzine.com/controleur-monitoring/mackie/big-knob-studioplus/)
    -   [Behringer Nekkst K8](https://fr.audiofanzine.com/enceinte-active/behringer/nekkstk8/)
    -   [Sennheiser HD 215-II](https://fr.audiofanzine.com/casque-dj/sennheiser/hd-215-ii/)

<img src="./music.jpg" alt="Setup musical" title="Configuration audio"/>
</div>
