---
title: BBOI
description: Révolutionner la gestion des chantiers avec une solution tout-en-un
icon: building
category:
    - Réalisations
tag:
    - PHP
    - MySQL
    - JavaScript
---

# BBOI - Gestion Intelligente des Chantiers

<div class="hero-section">
  <div class="hero-image-container">
    <div class="browser-frame">
      <div class="browser-header">
        <div class="browser-actions">
          <span></span><span></span><span></span>
        </div>
        <div class="browser-title">BBOI - Synthèse de Gestion</div>
      </div>
      <div class="browser-content">
        <img src="/assets/img/BBOI_Synthese_de_Gestion_de_l_Affaire_2014-03-07.png" alt="Interface BBOI" />
      </div>
    </div>
  </div>
</div>

::: tip Impact Business
✨ **40%** de réduction du temps de gestion  
🎯 **60%** moins d'erreurs administratives  
📈 **35%** d'amélioration de l'efficacité  
💰 ROI positif en moins de 6 mois
:::

## Le Défi

Le Groupe Philippe Marraud, leader dans le secteur de la construction, faisait face à des défis majeurs :

-   Gestion complexe des chantiers avec différents types d'interventions (Viabilisation, Construction)
-   Suivi des ressources humaines (Permanents, Stagiaires, Intérimaires)
-   Gestion des absences et planning (Congés, RTT, Formations, etc.)
-   Administration des documents techniques et financiers

## Notre Solution

### Vision

Créer une plateforme unifiée qui transforme la gestion des chantiers en un processus fluide, intelligent et connecté, adaptée aux différents profils utilisateurs (Conducteurs de travaux, Directeurs, Service matériel).

### Innovation Technique

::: echarts Répartition des Fonctionnalités

```json
{
    "tooltip": {
        "trigger": "item",
        "formatter": "{b}: {c}%"
    },
    "legend": {
        "orient": "horizontal",
        "bottom": "bottom"
    },
    "series": [
        {
            "name": "Fonctionnalités",
            "type": "pie",
            "radius": ["40%", "70%"],
            "center": ["50%", "45%"],
            "avoidLabelOverlap": true,
            "itemStyle": {
                "borderRadius": 10,
                "borderColor": "#fff",
                "borderWidth": 2
            },
            "label": {
                "show": true,
                "position": "outside",
                "formatter": "{b}\n{c}%"
            },
            "emphasis": {
                "label": {
                    "show": true,
                    "fontSize": "18",
                    "fontWeight": "bold"
                }
            },
            "data": [
                {
                    "value": 40,
                    "name": "Gestion Chantiers",
                    "itemStyle": { "color": "#42b883" }
                },
                {
                    "value": 35,
                    "name": "Gestion Matériel",
                    "itemStyle": { "color": "#33a06f" }
                },
                {
                    "value": 25,
                    "name": "Suivi Budgétaire",
                    "itemStyle": { "color": "#2d8f5b" }
                }
            ]
        }
    ]
}
```

:::

## Fonctionnalités Clés

<div class="features-grid">
  <div class="feature-card">
    <h3>🏗️ Gestion des Chantiers</h3>
    <ul>
      <li>Suivi des affaires par type (Viabilisation/Construction)</li>
      <li>Planning des interventions</li>
      <li>Gestion des équipes (Permanents/Intérimaires)</li>
      <li>Suivi des absences et congés</li>
    </ul>
  </div>

  <div class="feature-card">
    <h3>📊 Ressources</h3>
    <ul>
      <li>Gestion du matériel vertical/horizontal</li>
      <li>Suivi des sous-traitants GO/CES</li>
      <li>Allocation des ressources</li>
      <li>Transferts et mouvements</li>
    </ul>
  </div>

  <div class="feature-card">
    <h3>📑 Administration</h3>
    <ul>
      <li>Gestion documentaire multi-formats</li>
      <li>Extraction données paie/compta</li>
      <li>Rapports personnalisés</li>
      <li>Archivage automatique</li>
    </ul>
  </div>
</div>

## Stack Technique

<div class="tech-stack">
  <div class="tech-category">
    <h3>🔧 Backend</h3>
    <ul>
      <li>PHP 5.5</li>
      <li>MySQL 5.5</li>
      <li>AJAX</li>
    </ul>
  </div>

  <div class="tech-category">
    <h3>🎨 Frontend</h3>
    <ul>
      <li>JavaScript</li>
      <li>HTML5/CSS3</li>
      <li>Interface responsive</li>
    </ul>
  </div>

  <div class="tech-category">
    <h3>⚡ Sécurité</h3>
    <ul>
      <li>Authentification avancée</li>
      <li>Gestion des rôles</li>
      <li>Traçabilité complète</li>
    </ul>
  </div>
</div>

## Témoignage Client

::: info Témoignage
"BBOI a transformé notre façon de gérer les chantiers. La solution nous permet d'avoir une vision claire et en temps réel de toutes nos opérations."

_— Directeur des Opérations, Groupe Philippe Marraud_
:::

## Architecture & Interfaces

### Modules Principaux

<div class="modules-grid">
  <div class="module-card high">
    <h3>🏗️ Gestion des Affaires</h3>
    <div class="complexity">Complexité: Haute</div>
    <ul>
      <li>Gestion des chantiers</li>
      <li>Suivi budgétaire </li>
      <li>Affectation des ressources</li>
    </ul>
    <div class="relations">Relations:  gestion des états multiples</div>
  </div>

  <div class="module-card medium">
    <h3>📦 Gestion du Matériel</h3>
    <div class="complexity">Complexité: Moyenne</div>
    <ul>
      <li>Matériel vertical (coffrage, consoles)</li>
      <li>Matériel horizontal (étaiement)</li>
      <li>Équipements de sécurité</li>
    </ul>
    <div class="relations">Relations: Hiérarchie à 3 niveaux, 14500+ références</div>
  </div>

  <div class="module-card high">
    <h3>👥 Gestion RH</h3>
    <div class="complexity">Complexité: Haute</div>
    <ul>
      <li>Gestion des absences (10 types)</li>
      <li>Planning des équipes</li>
      <li>Suivi des compétences</li>
    </ul>
    <div class="relations">Relations: Multi-tables avec historisation</div>
  </div>
</div>

## Métriques Techniques

<div class="metrics-grid">
  <div class="metric-card">
    <h3>📊 Volume de Code</h3>
    <ul>
      <li>38 classes PHP spécialisées</li>
      <li>35+ modules métier</li>
      <li>~260K lignes de code total</li>
      <li>14,500+ références matériel</li>
    </ul>
  </div>

  <div class="metric-card">
    <h3>🔄 Architecture</h3>
    <ul>
      <li>20+ gestionnaires spécialisés</li>
      <li>15 workflows métier</li>
      <li>8 niveaux de validation</li>
      <li>25+ tables principales</li>
    </ul>
  </div>

  <div class="metric-card">
    <h3>📈 Modules Clés</h3>
    <ul>
      <li>Gestion Analytique</li>
      <li>Suivi Facturation</li>
      <li>Bible Budget</li>
      <li>Synthèse Gestion</li>
    </ul>
  </div>
</div>

<style scoped>
.features-grid {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
  gap: 1.5rem;
  margin: 2rem 0;
}

.feature-card {
  background: var(--bg-color-float);
  padding: 1.5rem;
  border-radius: 8px;
  box-shadow: 0 2px 4px rgba(0,0,0,0.1);
}

.feature-card h3 {
  margin-top: 0;
  color: var(--theme-color);
}

.feature-card ul {
  list-style-type: none;
  padding: 0;
}

.feature-card li {
  margin: 0.5rem 0;
  padding-left: 1.5rem;
  position: relative;
}

.feature-card li::before {
  content: "→";
  position: absolute;
  left: 0;
  color: var(--theme-color);
}

.tech-stack {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
  gap: 1.5rem;
  margin: 2rem 0;
}

.tech-category {
  background: var(--bg-color-float);
  padding: 1.5rem;
  border-radius: 8px;
  box-shadow: 0 2px 4px rgba(0,0,0,0.1);
}

img {
  max-width: 100%;
  height: auto;
  display: block;
  margin: 2rem auto;
  border-radius: 8px;
  box-shadow: 0 4px 12px rgba(0, 0, 0, 0.15);
}

.info.custom-block {
  background: linear-gradient(135deg, var(--bg-color-float) 0%, var(--bg-color) 100%);
  border: none;
  border-radius: 16px;
  padding: 2rem;
  margin: 2.5rem 0;
  position: relative;
  box-shadow: 0 10px 30px -10px rgba(0, 0, 0, 0.1);
  transform: translateY(0);
  transition: all 0.3s ease;
}

.info.custom-block:hover {
  transform: translateY(-5px);
  box-shadow: 0 15px 35px -10px rgba(0, 0, 0, 0.15);
}

.info.custom-block p {
  font-size: 1.2rem;
  line-height: 1.6;
  color: var(--text-color);
  font-style: italic;
  margin: 0;
}

.info.custom-block p:first-of-type {
  margin-bottom: 1.5rem;
}

.info.custom-block p:last-of-type {
  font-size: 1rem;
  color: var(--theme-color);
  font-weight: 600;
  text-align: right;
  margin-top: 1rem;
}

.info.custom-block::before {
  content: '"';
  position: absolute;
  top: -0.5rem;
  left: 1rem;
  font-size: 5rem;
  color: var(--theme-color);
  opacity: 0.1;
  font-family: serif;
  line-height: 1;
}

.modules-grid {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
  gap: 1.5rem;
  margin: 2rem 0;
}

.module-card {
  background: var(--bg-color-float);
  padding: 1.5rem;
  border-radius: 8px;
  box-shadow: 0 2px 4px rgba(0,0,0,0.1);
  position: relative;
}

.module-card.high {
  border-left: 4px solid #ff6b6b;
}

.module-card.medium {
  border-left: 4px solid #ffd93d;
}

.module-card.low {
  border-left: 4px solid #6bff6b;
}

.complexity {
  position: absolute;
  top: 1rem;
  right: 1rem;
  font-size: 0.8rem;
  padding: 0.2rem 0.5rem;
  border-radius: 4px;
  background: var(--theme-color);
  color: white;
}

.relations {
  margin-top: 1rem;
  font-size: 0.9rem;
  color: var(--text-color-light);
  border-top: 1px solid var(--border-color);
  padding-top: 0.5rem;
}

.metrics-grid {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
  gap: 1.5rem;
  margin: 2rem 0;
}

.metric-card {
  background: var(--bg-color-float);
  padding: 1.5rem;
  border-radius: 8px;
  box-shadow: 0 4px 6px rgba(0,0,0,0.1);
  transition: transform 0.3s ease;
}

.metric-card:hover {
  transform: translateY(-5px);
}

.metric-card h3 {
  color: var(--theme-color);
  margin-top: 0;
  display: flex;
  align-items: center;
  gap: 0.5rem;
}

.metric-card ul {
  list-style: none;
  padding: 0;
  margin: 1rem 0 0;
}

.metric-card li {
  padding: 0.5rem 0;
  border-bottom: 1px solid var(--border-color);
  display: flex;
  justify-content: space-between;
  align-items: center;
}

.metric-card li:last-child {
  border-bottom: none;
}

.hero-section {
  margin: 3rem auto;
  max-width: 1100px;
  padding: 0 1rem;
}

.hero-image-container {
  background: var(--bg-color-float);
  border-radius: 12px;
  padding: 2rem;
  box-shadow: 0 8px 24px rgba(0,0,0,0.12);
}

.browser-frame {
  background: var(--bg-color);
  border-radius: 8px;
  overflow: hidden;
  box-shadow: 0 4px 12px rgba(0,0,0,0.08);
}

.browser-header {
  background: var(--bg-color-light);
  padding: 0.75rem 1.25rem;
  border-bottom: 1px solid var(--border-color);
  display: flex;
  align-items: center;
  gap: 1rem;
}

.browser-actions {
  display: flex;
  gap: 0.5rem;
}

.browser-actions span {
  width: 12px;
  height: 12px;
  border-radius: 50%;
  display: block;
}

.browser-actions span:nth-child(1) { background: #ff5f57; }
.browser-actions span:nth-child(2) { background: #ffbd2e; }
.browser-actions span:nth-child(3) { background: #28c940; }

.browser-title {
  color: var(--text-color-light);
  font-size: 0.9rem;
  flex: 1;
  text-align: center;
}

.browser-content {
  max-height: 700px;
  overflow-y: auto;
  position: relative;
  scrollbar-width: thin;
  scrollbar-color: var(--theme-color) var(--bg-color-light);
}

.browser-content::-webkit-scrollbar {
  width: 8px;
}

.browser-content::-webkit-scrollbar-track {
  background: var(--bg-color-light);
}

.browser-content::-webkit-scrollbar-thumb {
  background-color: var(--theme-color);
  border-radius: 4px;
}

.browser-content img {
  width: 100%;
  height: auto;
  display: block;
  margin: 0;
  border-radius: 0;
  box-shadow: none;
}
</style>
