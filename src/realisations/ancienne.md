---
title: Anciennes Réalisations
description: Portfolio des projets historiques (2000-2010)
icon: history
category:
    - Réalisations
tag:
    - Web
    - Design
    - Multimédia
---

# Anciennes Réalisations (2000-2010)

<div class="project-timeline">
  <div class="timeline-marker">2003-2006</div>
</div>

## France Télécom / Orange

### Plateforme Pékin Express

<div class="gallery-grid">
  <div class="gallery-item wide">
    <img src="/assets/img/50_pekin_turbo_mailing.jpg" alt="Pékin Express - Interface Turbo" />
    <div class="gallery-caption">
      <h4>Pékin Express</h4>
      <p>Interface Turbo</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/47_pekin_ft_espace_promotion.jpg" alt="Espace Promotion" />
    <div class="gallery-caption">
      <h4>Espace Promotion</h4>
      <p>Plateforme marketing</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/49_pekin_tracking_md.jpg" alt="Système de Tracking" />
    <div class="gallery-caption">
      <h4>Tracking System</h4>
      <p>Suivi des campagnes</p>
    </div>
  </div>
</div>

### Autres Services France Télécom

<div class="gallery-grid">
  <div class="gallery-item">
    <img src="/assets/img/46_pekin_cetelem.jpg" alt="Cetelem" />
    <div class="gallery-caption">
      <h4>Cetelem</h4>
      <p>Services financiers</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/48_pekin_nos_offres_home.jpg" alt="Nos Offres" />
    <div class="gallery-caption">
      <h4>Nos Offres</h4>
      <p>Catalogue services</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/51_pekin_turbo_v2.jpg" alt="Turbo V2" />
    <div class="gallery-caption">
      <h4>Turbo V2</h4>
      <p>Interface v2</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/52_pekin_turbo_v3.jpg" alt="Turbo V3" />
    <div class="gallery-caption">
      <h4>Turbo V3</h4>
      <p>Interface v3</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/53_pekin_wanadoo_sei.jpg" alt="Wanadoo SEI" />
    <div class="gallery-caption">
      <h4>Wanadoo SEI</h4>
      <p>Service entreprises</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/54_pekin_wanadoo_tableau_comp.jpg" alt="Tableau Comparatif" />
    <div class="gallery-caption">
      <h4>Tableau Comparatif</h4>
      <p>Comparateur offres</p>
    </div>
  </div>
</div>

## Projets Corporate & Institutionnels (2003-2006)

<div class="gallery-grid">
  <div class="gallery-item">
    <img src="/assets/img/08_credit_agricole_uni_editions.jpg" alt="Uni-éditions" />
    <div class="gallery-caption">
      <h4>Uni-éditions</h4>
      <p>Plateforme éditoriale</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/07_credit_agricole_i_comme_infos.jpg" alt="i comme infos" />
    <div class="gallery-caption">
      <h4>i comme infos</h4>
      <p>Newsletter corporate</p>
    </div>
  </div>
</div>

<div class="gallery-grid">
  <div class="gallery-item">
    <img src="/assets/img/53_espace_proactif.jpg" alt="Espace Proactif" />
    <div class="gallery-caption">
      <h4>Espace Proactif</h4>
      <p>Plateforme collaborative</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/56_societe_broceliande_serigraphie.jpg" alt="Brocéliande Sérigraphie" />
    <div class="gallery-caption">
      <h4>Brocéliande Sérigraphie</h4>
      <p>Site vitrine</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/57_test_devaluation.jpg" alt="Test d'évaluation" />
    <div class="gallery-caption">
      <h4>Test d'évaluation</h4>
      <p>Plateforme RH</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/58_mail_opboutik.jpg" alt="OpBoutik" />
    <div class="gallery-caption">
      <h4>OpBoutik</h4>
      <p>E-mailing</p>
    </div>
  </div>
</div>

<div class="project-timeline">
  <div class="timeline-marker">2000-2003</div>
</div>

## MCM International

### Projets Majeurs

<div class="gallery-grid">
  <div class="gallery-item">
    <img src="/assets/img/20_mcm_daft.jpg" alt="Daft Punk - Site Spécial" />
    <div class="gallery-caption">
      <h4>Daft Punk - Site Événementiel</h4>
      <p>Site promotionnel créé pour la sortie de l'album "Discovery". Interface interactive avec animations Flash et expérience utilisateur immersive.</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/16_mcm_blairwitch.jpg" alt="Blair Witch Project" />
    <div class="gallery-caption">
      <h4>Blair Witch Project</h4>
      <p>Site promotionnel pour la sortie française du film culte. Design mystérieux et atmosphérique reflétant l'ambiance du film.</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/18_mcm_chatericramzi.jpg" alt="Chatéri Cramzi" />
    <div class="gallery-caption">
      <h4>Chatéri Cramzi</h4>
      <p>Plateforme communautaire</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/25_mcm_pactesdesloups.jpg" alt="Le Pacte des Loups" />
    <div class="gallery-caption">
      <h4>Le Pacte des Loups</h4>
      <p>Site événementiel</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/21_mcm_eurocks2000.jpg" alt="Eurockéennes 2000" />
    <div class="gallery-caption">
      <h4>Eurockéennes 2000</h4>
      <p>Couverture festival</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/30_mcm_u2.jpg" alt="U2 - Site Spécial" />
    <div class="gallery-caption">
      <h4>U2</h4>
      <p>Site spécial artiste</p>
    </div>
  </div>
</div>

### Productions Spéciales

<div class="gallery-grid">
  <div class="gallery-item">
    <img src="/assets/img/15_mcm_australie.jpg" alt="MCM Australie" />
    <div class="gallery-caption">
      <h4>MCM Australie</h4>
      <p>Site thématique</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/17_mcm_cafe.jpg" alt="MCM Café" />
    <div class="gallery-caption">
      <h4>MCM Café</h4>
      <p>Site événementiel</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/19_mcm_dabox.jpg" alt="Da Box" />
    <div class="gallery-caption">
      <h4>Da Box</h4>
      <p>Émission interactive</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/22_mcm_lascars.jpg" alt="Les Lascars" />
    <div class="gallery-caption">
      <h4>Les Lascars</h4>
      <p>Série animée</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/23_mcm_moby.jpg" alt="Moby" />
    <div class="gallery-caption">
      <h4>Moby</h4>
      <p>Site artiste</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/24_mcm_morrisey.jpg" alt="Morrissey" />
    <div class="gallery-caption">
      <h4>Morrissey</h4>
      <p>Site artiste</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/26_mcm_smashing.jpg" alt="Smashing Pumpkins" />
    <div class="gallery-caption">
      <h4>Smashing Pumpkins</h4>
      <p>Site artiste</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/27_mcm_subculture.jpg" alt="Subculture" />
    <div class="gallery-caption">
      <h4>Subculture</h4>
      <p>Magazine en ligne</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/28_mcm_thecell.jpg" alt="The Cell" />
    <div class="gallery-caption">
      <h4>The Cell</h4>
      <p>Site film</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/29_mcm_titan.jpg" alt="Titan AE" />
    <div class="gallery-caption">
      <h4>Titan AE</h4>
      <p>Site film</p>
    </div>
  </div>
</div>

## Projets Web & Digital (2000-2003)

### Plateformes Médias

<div class="gallery-grid">
  <div class="gallery-item">
    <img src="/assets/img/01_1genia_ca_portail.jpg" alt="Crédit Agricole Portail" />
    <div class="gallery-caption">
      <h4>Crédit Agricole</h4>
      <p>Portail bancaire</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/02_1genia_canal_plus_en_net.jpg" alt="Canal+ en Net" />
    <div class="gallery-caption">
      <h4>Canal+ en Net</h4>
      <p>Plateforme TV</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/03_1genia_cfdt.jpg" alt="CFDT" />
    <div class="gallery-caption">
      <h4>CFDT</h4>
      <p>Portail syndical</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/04_1genia_domicilio.jpg" alt="Domicilio" />
    <div class="gallery-caption">
      <h4>Domicilio</h4>
      <p>Services immobiliers</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/05_1genia_fncofor.jpg" alt="FNCOFOR" />
    <div class="gallery-caption">
      <h4>FNCOFOR</h4>
      <p>Fédération forestière</p>
    </div>
  </div>
</div>

### Services & E-commerce

<div class="gallery-grid">
  <div class="gallery-item">
    <img src="/assets/img/06_cacharel.jpg" alt="Cacharel" />
    <div class="gallery-caption">
      <h4>Cacharel</h4>
      <p>Site e-commerce</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/09_d8.jpg" alt="D8" />
    <div class="gallery-caption">
      <h4>D8</h4>
      <p>Site commercial</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/10_ddu.jpg" alt="DDU" />
    <div class="gallery-caption">
      <h4>DDU</h4>
      <p>Service en ligne</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/11_dj_regie.jpg" alt="DJ Régie" />
    <div class="gallery-caption">
      <h4>DJ Régie</h4>
      <p>Plateforme publicitaire</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/12_espacepanoramique.jpg" alt="Espace Panoramique" />
    <div class="gallery-caption">
      <h4>Espace Panoramique</h4>
      <p>Services photo</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/13_flobrasseries.jpg" alt="FLO Brasseries" />
    <div class="gallery-caption">
      <h4>FLO Brasseries</h4>
      <p>Réseau de restaurants</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/14_leparticulier.jpg" alt="Le Particulier" />
    <div class="gallery-caption">
      <h4>Le Particulier</h4>
      <p>Service juridique</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/36_neo05.jpg" alt="Neo05" />
    <div class="gallery-caption">
      <h4>Neo05</h4>
      <p>Service digital</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/37_over23.jpg" alt="Over23" />
    <div class="gallery-caption">
      <h4>Over23</h4>
      <p>E-commerce</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/55_publicsenat.jpg" alt="Public Sénat" />
    <div class="gallery-caption">
      <h4>Public Sénat</h4>
      <p>Site institutionnel</p>
    </div>
  </div>
</div>

### Projets Musicaux

<div class="gallery-grid">
  <div class="gallery-item wide">
    <img src="/assets/img/32_mptrois_v5.jpg" alt="MP3 v5" />
    <div class="gallery-caption">
      <h4>MP3 v5</h4>
      <p>Plateforme de distribution musicale</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/31_mptrois_v4.jpg" alt="MP3 v4" />
    <div class="gallery-caption">
      <h4>MP3 v4</h4>
      <p>Plateforme musicale</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/33_musik_2003_05_31_ttc_woxo_cd01.jpg" alt="WOXO CD" />
    <div class="gallery-caption">
      <h4>WOXO CD</h4>
      <p>Production musicale</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/34_musik_2003_06_14_ttc_flyer_dos.jpg" alt="TTC Flyer" />
    <div class="gallery-caption">
      <h4>TTC Flyer</h4>
      <p>Design événementiel</p>
    </div>
  </div>
  <div class="gallery-item">
    <img src="/assets/img/35_musik_boitier_cristal_cd.jpg" alt="Boîtier Cristal CD" />
    <div class="gallery-caption">
      <h4>Boîtier Cristal</h4>
      <p>Design packaging</p>
    </div>
  </div>
</div>

<style scoped>
/* Base Typography */
h1 {
  font-size: clamp(2rem, 5vw, 3.5rem);
  font-weight: 900;
  letter-spacing: -0.03em;
  background: linear-gradient(135deg, var(--theme-color), #2a9d8f);
  -webkit-background-clip: text;
  color: transparent;
  margin: 6rem auto 4rem;
  text-align: center;
  max-width: 800px;
  padding: 0 2rem;
}

h2 {
  font-size: clamp(1.5rem, 4vw, 2.5rem);
  font-weight: 800;
  color: var(--theme-color);
  margin: 6rem 0 3rem;
  text-align: center;
  position: relative;
}

h2::after {
  content: '';
  position: absolute;
  bottom: -1rem;
  left: 50%;
  transform: translateX(-50%);
  width: 60px;
  height: 4px;
  background: var(--theme-color);
  border-radius: 2px;
}

h3 {
  font-size: clamp(1.2rem, 3vw, 1.8rem);
  font-weight: 700;
  color: var(--text-color);
  margin: 4rem 0 2rem;
  text-align: center;
}

/* Timeline Design */
.project-timeline {
  margin: 8rem 0;
  text-align: center;
  position: relative;
}

.timeline-marker {
  display: inline-flex;
  align-items: center;
  font-size: 1.1rem;
  font-weight: 700;
  color: var(--theme-color);
  background: var(--bg-color);
  padding: 1rem 2.5rem;
  border-radius: 100px;
  position: relative;
  z-index: 2;
  box-shadow: 
    0 2px 10px rgba(0, 0, 0, 0.1),
    0 10px 30px rgba(var(--theme-color-rgb), 0.2);
  transition: transform 0.3s ease;
}

.timeline-marker::before,
.timeline-marker::after {
  content: '';
  position: absolute;
  top: 50%;
  width: 100px;
  height: 1px;
  background: linear-gradient(90deg, var(--theme-color), transparent);
}

.timeline-marker::before {
  right: 100%;
  margin-right: 1rem;
}

.timeline-marker::after {
  left: 100%;
  margin-left: 1rem;
  transform: rotate(180deg);
}

/* Updated Gallery Styles */
.gallery-grid {
  display: grid;
  grid-template-columns: repeat(auto-fill, 200px);
  gap: 2.5rem;
  justify-content: center;
  padding: 2rem;
  margin: 2rem auto;
  max-width: 1400px;
}

.gallery-item {
  position: relative;
  border-radius: 12px;
  overflow: visible;
  background: var(--bg-color);
  transition: all 0.4s cubic-bezier(0.165, 0.84, 0.44, 1);
}

.gallery-item img {
  width: 200px;
  height: 150px;
  object-fit: cover;
  display: block;
  border-radius: 8px;
  transition: all 0.5s cubic-bezier(0.165, 0.84, 0.44, 1);
}

.gallery-caption {
  position: static;
  padding: 1rem 0.5rem;
  background: none;
  opacity: 1;
}

.gallery-caption h4 {
  font-size: 1rem;
  font-weight: 600;
  color: var(--text-color);
  margin: 0;
  transform: none;
}

.gallery-caption p {
  font-size: 0.9rem;
  color: var(--text-color-light);
  margin: 0.5rem 0 0;
  transform: none;
  line-height: 1.4;
}

.gallery-item:hover {
  transform: translateY(-4px);
}

.gallery-item:hover img {
  transform: scale(1.05);
  box-shadow: 
    0 10px 20px rgba(0, 0, 0, 0.1),
    0 20px 40px rgba(0, 0, 0, 0.1);
}

/* Responsive adjustments */
@media (max-width: 768px) {
  .gallery-grid {
    grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
    gap: 2rem;
  }
  
  .gallery-item img {
    width: 100%;
  }
}

/* Dark mode adjustments */
@media (prefers-color-scheme: dark) {
  .gallery-caption h4 {
    color: var(--text-color-dark);
  }
  
  .gallery-caption p {
    color: var(--text-color-light-dark);
  }
}
</style>
