---
title: BBOIHeures
description: Révolutionner la gestion du temps avec une solution intelligente
icon: clock
category:
    - Réalisations
tag:
    - PHP
    - MySQL
    - JavaScript
    - jQuery
date: 2013-09-01
---

# BBOIHeures - Gestion Intelligente du Temps

<div class="hero-section">
  <div class="hero-image-container">
    <div class="browser-frame">
      <div class="browser-header">
        <div class="browser-actions">
          <span></span><span></span><span></span>
        </div>
        <div class="browser-title">BBOIHeures - Tableau de Bord</div>
      </div>
      <div class="browser-content">
        <img src="/assets/img/BBOI_Heures_tableau_de_bord_2014-03-07.png" alt="Interface BBOIHeures" />
      </div>
    </div>
  </div>
</div>

<style scoped>
.features-grid {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
  gap: 1.5rem;
  margin: 2rem 0;
}

.feature-card {
  background: var(--bg-color-float);
  padding: 1.5rem;
  border-radius: 8px;
  box-shadow: 0 2px 4px rgba(0,0,0,0.1);
}

.feature-card h3 {
  margin-top: 0;
  color: var(--theme-color);
}

.feature-card ul {
  list-style-type: none;
  padding: 0;
}

.feature-card li {
  margin: 0.5rem 0;
  padding-left: 1.5rem;
  position: relative;
}

.feature-card li::before {
  content: "→";
  position: absolute;
  left: 0;
  color: var(--theme-color);
}

.tech-stack {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
  gap: 1.5rem;
  margin: 2rem 0;
}

.tech-category {
  background: var(--bg-color-float);
  padding: 1.5rem;
  border-radius: 8px;
  box-shadow: 0 2px 4px rgba(0,0,0,0.1);
}

img {
  max-width: 100%;
  height: auto;
  display: block;
  margin: 2rem auto;
  border-radius: 8px;
  box-shadow: 0 4px 12px rgba(0, 0, 0, 0.15);
}

.info.custom-block {
  background: linear-gradient(135deg, var(--bg-color-float) 0%, var(--bg-color) 100%);
  border: none;
  border-radius: 16px;
  padding: 2rem;
  margin: 2.5rem 0;
  position: relative;
  box-shadow: 0 10px 30px -10px rgba(0, 0, 0, 0.1);
  transform: translateY(0);
  transition: all 0.3s ease;
}

.info.custom-block:hover {
  transform: translateY(-5px);
  box-shadow: 0 15px 35px -10px rgba(0, 0, 0, 0.15);
}

.info.custom-block p {
  font-size: 1.2rem;
  line-height: 1.6;
  color: var(--text-color);
  font-style: italic;
  margin: 0;
}

.info.custom-block p:first-of-type {
  margin-bottom: 1.5rem;
}

.info.custom-block p:last-of-type {
  font-size: 1rem;
  color: var(--theme-color);
  font-weight: 600;
  text-align: right;
  margin-top: 1rem;
}

.info.custom-block::before {
  content: '"';
  position: absolute;
  top: -0.5rem;
  left: 1rem;
  font-size: 5rem;
  color: var(--theme-color);
  opacity: 0.1;
  font-family: serif;
  line-height: 1;
}

.modules-grid {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
  gap: 1.5rem;
  margin: 2rem 0;
}

.module-card {
  background: var(--bg-color-float);
  padding: 1.5rem;
  border-radius: 8px;
  box-shadow: 0 2px 4px rgba(0,0,0,0.1);
  position: relative;
}

.module-card.high {
  border-left: 4px solid #ff6b6b;
}

.module-card.medium {
  border-left: 4px solid #ffd93d;
}

.module-card.low {
  border-left: 4px solid #6bff6b;
}

.complexity {
  position: absolute;
  top: 1rem;
  right: 1rem;
  font-size: 0.8rem;
  padding: 0.2rem 0.5rem;
  border-radius: 4px;
  background: var(--theme-color);
  color: white;
}

.relations {
  margin-top: 1rem;
  font-size: 0.9rem;
  color: var(--text-color-light);
  border-top: 1px solid var(--border-color);
  padding-top: 0.5rem;
}

.metrics-grid {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
  gap: 1.5rem;
  margin: 2rem 0;
}

.metric-card {
  background: var(--bg-color-float);
  padding: 1.5rem;
  border-radius: 8px;
  box-shadow: 0 4px 6px rgba(0,0,0,0.1);
  transition: transform 0.3s ease;
}

.metric-card:hover {
  transform: translateY(-5px);
}

.metric-card h3 {
  color: var(--theme-color);
  margin-top: 0;
  display: flex;
  align-items: center;
  gap: 0.5rem;
}

.metric-card ul {
  list-style: none;
  padding: 0;
  margin: 1rem 0 0;
}

.metric-card li {
  padding: 0.5rem 0;
  border-bottom: 1px solid var(--border-color);
  display: flex;
  justify-content: space-between;
  align-items: center;
}

.metric-card li:last-child {
  border-bottom: none;
}

.hero-section {
  margin: 3rem auto;
  max-width: 1100px;
  padding: 0 1rem;
}

.hero-image-container {
  background: var(--bg-color-float);
  border-radius: 12px;
  padding: 2rem;
  box-shadow: 0 8px 24px rgba(0,0,0,0.12);
}

.browser-frame {
  background: var(--bg-color);
  border-radius: 8px;
  overflow: hidden;
  box-shadow: 0 4px 12px rgba(0,0,0,0.08);
}

.browser-header {
  background: var(--bg-color-light);
  padding: 0.75rem 1.25rem;
  border-bottom: 1px solid var(--border-color);
  display: flex;
  align-items: center;
  gap: 1rem;
}

.browser-actions {
  display: flex;
  gap: 0.5rem;
}

.browser-actions span {
  width: 12px;
  height: 12px;
  border-radius: 50%;
  background: #dee2e6;
}

.browser-actions span:nth-child(1) { background: #ff5f56; }
.browser-actions span:nth-child(2) { background: #ffbd2e; }
.browser-actions span:nth-child(3) { background: #27c93f; }

.browser-title {
  color: var(--text-color-light);
  font-size: 0.9rem;
  flex: 1;
  text-align: center;
}

.browser-content {
  max-height: 700px;
  overflow-y: auto;
  position: relative;
  scrollbar-width: thin;
  scrollbar-color: var(--theme-color) var(--bg-color-light);
}

.browser-content::-webkit-scrollbar {
  width: 8px;
}

.browser-content::-webkit-scrollbar-track {
  background: var(--bg-color-light);
}

.browser-content::-webkit-scrollbar-thumb {
  background-color: var(--theme-color);
  border-radius: 4px;
}

.browser-content img {
  width: 100%;
  height: auto;
  display: block;
  margin: 0;
  border-radius: 0;
  box-shadow: none;
}

.browser-content img {
  width: 100%;
  height: auto;
  display: block;
  margin: 0;
  border-radius: 0;
}
</style>

::: tip Impact Business
✨ **70%** de réduction des erreurs de saisie  
⏱️ **4h/jour** gagnées sur le traitement administratif  
💰 **15%** d'économie sur les coûts RH  
🎯 **95%** de satisfaction utilisateur
:::

## Le Défi

Le Groupe Philippe Marraud était confronté à des défis majeurs dans la gestion du temps :

-   Suivi complexe des heures sur plus de 35 chantiers simultanés
-   Gestion des absences avec 8 types différents (congés, maladie, formation...)
-   Calcul des primes et indemnités selon les zones géographiques
-   Validation multi-niveaux par les chefs de chantier et conducteurs

## Notre Solution

### Vision

Créer une plateforme unifiée qui transforme la gestion du temps en un processus fluide et précis, adaptée aux différents profils utilisateurs (Chefs de chantier, RH, Direction).

### Innovation Technique

::: echarts Répartition des Fonctionnalités

```json
{
    "tooltip": {
        "trigger": "item",
        "formatter": "{b}: {c}%"
    },
    "legend": {
        "orient": "horizontal",
        "bottom": "bottom"
    },
    "series": [
        {
            "name": "Fonctionnalités",
            "type": "pie",
            "radius": ["40%", "70%"],
            "center": ["50%", "45%"],
            "avoidLabelOverlap": true,
            "itemStyle": {
                "borderRadius": 10,
                "borderColor": "#fff",
                "borderWidth": 2
            },
            "label": {
                "show": true,
                "position": "outside",
                "formatter": "{b}\n{c}%"
            },
            "emphasis": {
                "label": {
                    "show": true,
                    "fontSize": "18",
                    "fontWeight": "bold"
                }
            },
            "data": [
                {
                    "value": 45,
                    "name": "Gestion Temps",
                    "itemStyle": { "color": "#42b883" }
                },
                {
                    "value": 30,
                    "name": "Gestion Absences",
                    "itemStyle": { "color": "#33a06f" }
                },
                {
                    "value": 25,
                    "name": "Reporting RH",
                    "itemStyle": { "color": "#2d8f5b" }
                }
            ]
        }
    ]
}
```

:::

## Fonctionnalités Clés

<div class="features-grid">
  <div class="feature-card">
    <h3>⏱️ Gestion du Temps</h3>
    <ul>
      <li>Saisie des heures par chantier</li>
      <li>Validation multi-niveaux</li>
      <li>Calcul des heures supplémentaires</li>
      <li>Suivi temps réel</li>
    </ul>
  </div>

  <div class="feature-card">
    <h3>📅 Gestion des Absences</h3>
    <ul>
      <li>8 types d'absences différents</li>
      <li>Workflow de validation</li>
      <li>Calcul automatique des soldes</li>
      <li>Planning des équipes</li>
    </ul>
  </div>

  <div class="feature-card">
    <h3>📊 Reporting RH</h3>
    <ul>
      <li>Tableaux de bord personnalisés</li>
      <li>Export pour la paie</li>
      <li>Statistiques de présence</li>
      <li>Analyse des coûts</li>
    </ul>
  </div>
</div>

## Stack Technique

<div class="tech-stack">
  <div class="tech-category">
    <h3>🔧 Backend</h3>
    <ul>
      <li>PHP 5.5</li>
      <li>MySQL 5.5</li>
      <li>Architecture MVC</li>
    </ul>
  </div>

  <div class="tech-category">
    <h3>🎨 Frontend</h3>
    <ul>
      <li>JavaScript</li>
      <li>jQuery</li>
      <li>Interface responsive</li>
    </ul>
  </div>

  <div class="tech-category">
    <h3>⚡ Performance</h3>
    <ul>
      <li>Cache système</li>
      <li>Optimisation requêtes</li>
      <li>+500 utilisateurs actifs</li>
    </ul>
  </div>
</div>

## Témoignage Client

::: info Témoignage
"BBOIHeures a révolutionné notre gestion du temps. Nous avons gagné en précision et en efficacité, avec une réduction significative des erreurs de saisie."

_— Responsable RH, Groupe Philippe Marraud_
:::

## Architecture & Interfaces

### Modules Principaux

<div class="modules-grid">
  <div class="module-card high">
    <h3>⏱️ Gestion du Temps</h3>
    <div class="complexity">Complexité: Haute</div>
    <ul>
      <li>Saisie et validation des heures</li>
      <li>Calcul des heures supplémentaires</li>
      <li>Gestion des plannings</li>
    </ul>
    <div class="relations">Relations: Multi-tables avec historisation</div>
  </div>

  <div class="module-card high">
    <h3>📅 Gestion des Absences</h3>
    <div class="complexity">Complexité: Haute</div>
    <ul>
      <li>8 types d'absences</li>
      <li>Workflow de validation</li>
      <li>Calcul des soldes</li>
    </ul>
    <div class="relations">Relations: Gestion complexe des états</div>
  </div>

  <div class="module-card medium">
    <h3>📊 Reporting</h3>
    <div class="complexity">Complexité: Moyenne</div>
    <ul>
      <li>Tableaux de bord</li>
      <li>Export paie</li>
      <li>Statistiques</li>
    </ul>
    <div class="relations">Relations: Agrégation multi-sources</div>
  </div>
</div>

## Métriques Techniques

<div class="metrics-grid">
  <div class="metric-card">
    <h3>📊 Volume de Code</h3>
    <ul>
      <li>20 tables principales</li>
      <li>25+ modules métier</li>
      <li>~180K lignes de code</li>
      <li>8 types d'absences gérés</li>
    </ul>
  </div>

  <div class="metric-card">
    <h3>🔄 Architecture</h3>
    <ul>
      <li>3 niveaux de validation</li>
      <li>10 workflows métier</li>
      <li>4 profils utilisateurs</li>
      <li>15+ tables principales</li>
    </ul>
  </div>

  <div class="metric-card">
    <h3>📈 Performance</h3>
    <ul>
      <li>+500 utilisateurs actifs</li>
      <li>+35 chantiers simultanés</li>
      <li>8 ans d'historique</li>
      <li>Temps de réponse < 1s</li>
    </ul>
  </div>
</div>
