---
title: VGA - VDGI
description: Plateforme de Gestion Administrative Avancée
icon: building-columns
category:
    - Réalisations
tag:
    - PHP
    - MySQL
    - JavaScript
    - jQuery
---

# VGA - Gestion Administrative Intelligente

<div class="hero-section">
  <div class="hero-image-container">
    <div class="browser-frame">
      <div class="browser-header">
        <div class="browser-actions">
          <span></span><span></span><span></span>
        </div>
        <div class="browser-title">VGA - Gestion des Dossiers Habitat</div>
      </div>
      <div class="browser-content">
        <img src="/assets/img/VDGI_dossier_occupant_2014-06-01.png" alt="Interface VGA" />
      </div>
    </div>
  </div>
</div>

::: tip Impact Business
📊 **40%** de gain en productivité administrative  
💼 **60%** de réduction des erreurs de saisie  
⚡ **30%** d'amélioration du temps de traitement  
🏠 **+2000** dossiers habitat traités par an
:::

## Le Défi

La gestion des aides à l'amélioration de l'habitat présente des défis majeurs :

-   Multiplicité des dispositifs d'aide (ANAH, MaPrimeRénov, Éco-PTZ...)
-   Complexité des critères d'éligibilité
-   Suivi rigoureux des dossiers et des délais
-   Coordination entre les différents acteurs (propriétaires, artisans, conseillers)
-   Gestion des pièces justificatives et des validations

## Notre Solution

### Vision

Créer une plateforme unifiée qui simplifie la gestion des dossiers d'amélioration de l'habitat, de l'instruction à la validation finale, en intégrant tous les dispositifs d'aide disponibles.

### Types de Dossiers Gérés

-   Réhabilitation des logements dégradés
-   Amélioration du confort thermique
-   Adaptation pour l'autonomie des personnes
-   Rénovation des façades
-   Création de logements locatifs

### Innovation Technique

::: echarts Répartition des Fonctionnalités

```json
{
    "tooltip": {
        "trigger": "item",
        "formatter": "{b}: {c}%"
    },
    "legend": {
        "orient": "horizontal",
        "bottom": "bottom"
    },
    "series": [
        {
            "name": "Fonctionnalités",
            "type": "pie",
            "radius": ["40%", "70%"],
            "center": ["50%", "45%"],
            "avoidLabelOverlap": true,
            "itemStyle": {
                "borderRadius": 10,
                "borderColor": "#fff",
                "borderWidth": 2
            },
            "label": {
                "show": true,
                "position": "outside",
                "formatter": "{b}\n{c}%"
            },
            "emphasis": {
                "label": {
                    "show": true,
                    "fontSize": "18",
                    "fontWeight": "bold"
                }
            },
            "data": [
                {
                    "value": 35,
                    "name": "Gestion des Dossiers",
                    "itemStyle": { "color": "#42b883" }
                },
                {
                    "value": 30,
                    "name": "Calcul d'Aides",
                    "itemStyle": { "color": "#33a06f" }
                },
                {
                    "value": 35,
                    "name": "Suivi & Reporting",
                    "itemStyle": { "color": "#2d8f5b" }
                }
            ]
        }
    ]
}
```

:::

## Fonctionnalités Clés

<div class="features-grid">
  <div class="feature-card">
    <h3>📋 Gestion des Dossiers</h3>
    <ul>
      <li>Formulaires dynamiques par type d'aide</li>
      <li>Gestion des pièces justificatives</li>
      <li>Workflow de validation multi-niveaux</li>
      <li>Suivi des délais et relances</li>
    </ul>
  </div>

  <div class="feature-card">
    <h3>💰 Calcul des Aides</h3>
    <ul>
      <li>Simulation en temps réel</li>
      <li>Critères d'éligibilité dynamiques</li>
      <li>Cumul des dispositifs</li>
      <li>Historique des calculs</li>
    </ul>
  </div>

  <div class="feature-card">
    <h3>📊 Suivi & Reporting</h3>
    <ul>
      <li>Tableaux de bord par dispositif</li>
      <li>Statistiques territoriales</li>
      <li>Exports réglementaires</li>
      <li>Indicateurs de performance</li>
    </ul>
  </div>
</div>

## Architecture & Interfaces

### Modules Principaux

<div class="modules-grid">
  <div class="module-card high">
    <h3>📋 Gestion des Dossiers</h3>
    <div class="complexity">Complexité: Haute</div>
    <ul>
      <li>Formulaires adaptatifs</li>
      <li>Gestion documentaire</li>
      <li>Workflow paramétrable</li>
    </ul>
    <div class="relations">Relations: Intégration multi-dispositifs</div>
  </div>

  <div class="module-card high">
    <h3>🔄 Moteur de Calcul</h3>
    <div class="complexity">Complexité: Haute</div>
    <ul>
      <li>Règles d'éligibilité</li>
      <li>Calcul des montants</li>
      <li>Plafonnements</li>
    </ul>
    <div class="relations">Relations: Base réglementaire</div>
  </div>

  <div class="module-card medium">
    <h3>📊 Pilotage</h3>
    <div class="complexity">Complexité: Moyenne</div>
    <ul>
      <li>Tableaux de bord</li>
      <li>Statistiques</li>
      <li>Exports ANAH</li>
    </ul>
    <div class="relations">Relations: Reporting institutionnel</div>
  </div>
</div>

## Métriques Techniques

<div class="metrics-grid">
  <div class="metric-card">
    <h3>📊 Volume</h3>
    <ul>
      <li>12 tables principales</li>
      <li>35+ modules métier</li>
      <li>~250K lignes de code</li>
      <li>5 types de charges</li>
    </ul>
  </div>

  <div class="metric-card">
    <h3>🔄 Architecture</h3>
    <ul>
      <li>Architecture MVC</li>
      <li>3 profils utilisateurs</li>
      <li>Multi-identités juridiques</li>
      <li>Plan comptable complet</li>
    </ul>
  </div>

  <div class="metric-card">
    <h3>📈 Performance</h3>
    <ul>
      <li>+1500 dossiers/an</li>
      <li>+50 utilisateurs actifs</li>
      <li>Temps de réponse < 1s</li>
      <li>99.9% disponibilité</li>
    </ul>
  </div>
</div>

## Témoignage Client

::: info Témoignage
"VDGI a transformé notre gestion des dossiers d'amélioration de l'habitat. La plateforme nous permet de traiter plus efficacement les demandes tout en assurant un meilleur suivi pour les propriétaires."

_— Responsable Service Habitat, Val de Garonne Agglomération_
:::

<style scoped>
.features-grid {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
  gap: 1.5rem;
  margin: 2rem 0;
}

.feature-card {
  background: var(--bg-color-float);
  padding: 1.5rem;
  border-radius: 8px;
  box-shadow: 0 2px 4px rgba(0,0,0,0.1);
}

.feature-card h3 {
  margin-top: 0;
  color: var(--theme-color);
}

.feature-card ul {
  list-style-type: none;
  padding: 0;
}

.feature-card li {
  margin: 0.5rem 0;
  padding-left: 1.5rem;
  position: relative;
}

.feature-card li::before {
  content: "→";
  position: absolute;
  left: 0;
  color: var(--theme-color);
}

.tech-stack {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
  gap: 1.5rem;
  margin: 2rem 0;
}

.tech-category {
  background: var(--bg-color-float);
  padding: 1.5rem;
  border-radius: 8px;
  box-shadow: 0 2px 4px rgba(0,0,0,0.1);
}

img {
  max-width: 100%;
  height: auto;
  display: block;
  margin: 2rem auto;
  border-radius: 8px;
  box-shadow: 0 4px 12px rgba(0, 0, 0, 0.15);
}

.info.custom-block {
  background: linear-gradient(135deg, var(--bg-color-float) 0%, var(--bg-color) 100%);
  border: none;
  border-radius: 16px;
  padding: 2rem;
  margin: 2.5rem 0;
  position: relative;
  box-shadow: 0 10px 30px -10px rgba(0, 0, 0, 0.1);
  transform: translateY(0);
  transition: all 0.3s ease;
}

.info.custom-block:hover {
  transform: translateY(-5px);
  box-shadow: 0 15px 35px -10px rgba(0, 0, 0, 0.15);
}

.info.custom-block p {
  font-size: 1.2rem;
  line-height: 1.6;
  color: var(--text-color);
  font-style: italic;
  margin: 0;
}

.info.custom-block p:first-of-type {
  margin-bottom: 1.5rem;
}

.info.custom-block p:last-of-type {
  font-size: 1rem;
  color: var(--theme-color);
  font-weight: 600;
  text-align: right;
  margin-top: 1rem;
}

.info.custom-block::before {
  content: '"';
  position: absolute;
  top: -0.5rem;
  left: 1rem;
  font-size: 5rem;
  color: var(--theme-color);
  opacity: 0.1;
  font-family: serif;
  line-height: 1;
}

.modules-grid {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
  gap: 1.5rem;
  margin: 2rem 0;
}

.module-card {
  background: var(--bg-color-float);
  padding: 1.5rem;
  border-radius: 8px;
  box-shadow: 0 2px 4px rgba(0,0,0,0.1);
  position: relative;
}

.module-card.high {
  border-left: 4px solid #ff6b6b;
}

.module-card.medium {
  border-left: 4px solid #ffd93d;
}

.module-card.low {
  border-left: 4px solid #6bff6b;
}

.complexity {
  position: absolute;
  top: 1rem;
  right: 1rem;
  font-size: 0.8rem;
  padding: 0.2rem 0.5rem;
  border-radius: 4px;
  background: var(--theme-color);
  color: white;
}

.relations {
  margin-top: 1rem;
  font-size: 0.9rem;
  color: var(--text-color-light);
  border-top: 1px solid var(--border-color);
  padding-top: 0.5rem;
}

.metrics-grid {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
  gap: 1.5rem;
  margin: 2rem 0;
}

.metric-card {
  background: var(--bg-color-float);
  padding: 1.5rem;
  border-radius: 8px;
  box-shadow: 0 4px 6px rgba(0,0,0,0.1);
  transition: transform 0.3s ease;
}

.metric-card:hover {
  transform: translateY(-5px);
}

.metric-card h3 {
  color: var(--theme-color);
  margin-top: 0;
  display: flex;
  align-items: center;
  gap: 0.5rem;
}

.metric-card ul {
  list-style: none;
  padding: 0;
  margin: 1rem 0 0;
}

.metric-card li {
  padding: 0.5rem 0;
  border-bottom: 1px solid var(--border-color);
  display: flex;
  justify-content: space-between;
  align-items: center;
}

.metric-card li:last-child {
  border-bottom: none;
}

.hero-section {
  margin: 3rem auto;
  max-width: 1100px;
  padding: 0 1rem;
}

.hero-image-container {
  background: var(--bg-color-float);
  border-radius: 12px;
  padding: 2rem;
  box-shadow: 0 8px 24px rgba(0,0,0,0.12);
}

.browser-frame {
  background: var(--bg-color);
  border-radius: 8px;
  overflow: hidden;
  box-shadow: 0 4px 12px rgba(0,0,0,0.08);
}

.browser-header {
  background: var(--bg-color-light);
  padding: 0.75rem 1.25rem;
  border-bottom: 1px solid var(--border-color);
  display: flex;
  align-items: center;
  gap: 1rem;
}

.browser-actions {
  display: flex;
  gap: 0.5rem;
}

.browser-actions span {
  width: 12px;
  height: 12px;
  border-radius: 50%;
  background: #dee2e6;
}

.browser-actions span:nth-child(1) { background: #ff5f56; }
.browser-actions span:nth-child(2) { background: #ffbd2e; }
.browser-actions span:nth-child(3) { background: #27c93f; }

.browser-title {
  color: var(--text-color-light);
  font-size: 0.9rem;
}

.browser-content {
  padding: 0;
}

.browser-content img {
  width: 100%;
  height: auto;
  display: block;
  margin: 0;
  border-radius: 0;
}
</style>
