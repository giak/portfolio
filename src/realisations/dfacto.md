---
title: DFacto
description: Solution innovante de gestion comptable et financière
icon: chart-line
category:
    - Réalisations
tag:
    - PHP
    - MySQL
    - JavaScript
    - jQuery
---

# DFacto - Pilotage Financier Intelligent

<div class="hero-section">
  <div class="hero-image-container">
    <div class="browser-frame">
      <div class="browser-header">
        <div class="browser-actions">
          <span></span><span></span><span></span>
        </div>
        <div class="browser-title">DFacto - Tableau de Bord Financier</div>
      </div>
      <div class="browser-content">
        <img src="/assets/img/mon_entreprise_2015_sc_charge_2015.png" alt="Interface DFacto" />
      </div>
    </div>
  </div>
</div>

::: tip Impact Business
📊 **45%** de gain en productivité comptable  
💰 **30%** de meilleur suivi des charges  
📈 **25%** d'amélioration du pilotage  
⚡ ROI positif en 4 mois
:::

## Le Défi

Les entreprises font face à des défis majeurs dans leur gestion financière :

-   Complexité du suivi des charges fixes et variables
-   Difficulté de pilotage des indicateurs financiers
-   Gestion complexe des différents types juridiques (SARL, SAS, SA...)
-   Besoin d'analyses financières en temps réel
-   Conformité avec le plan comptable général

## Notre Solution

### Vision

Créer une plateforme unifiée qui transforme la gestion comptable en un outil de pilotage intelligent, adaptée aux différents profils utilisateurs (Comptables, Dirigeants, Contrôleurs de gestion).

### Innovation Technique

::: echarts Répartition des Fonctionnalités

```json
{
    "tooltip": {
        "trigger": "item",
        "formatter": "{b}: {c}%"
    },
    "legend": {
        "orient": "horizontal",
        "bottom": "bottom"
    },
    "series": [
        {
            "name": "Fonctionnalités",
            "type": "pie",
            "radius": ["40%", "70%"],
            "center": ["50%", "45%"],
            "avoidLabelOverlap": true,
            "itemStyle": {
                "borderRadius": 10,
                "borderColor": "#fff",
                "borderWidth": 2
            },
            "label": {
                "show": true,
                "position": "outside",
                "formatter": "{b}\n{c}%"
            },
            "emphasis": {
                "label": {
                    "show": true,
                    "fontSize": "18",
                    "fontWeight": "bold"
                }
            },
            "data": [
                {
                    "value": 40,
                    "name": "Gestion Comptable",
                    "itemStyle": { "color": "#42b883" }
                },
                {
                    "value": 35,
                    "name": "Analyse Financière",
                    "itemStyle": { "color": "#33a06f" }
                },
                {
                    "value": 25,
                    "name": "Reporting",
                    "itemStyle": { "color": "#2d8f5b" }
                }
            ]
        }
    ]
}
```

:::

## Interfaces Clés

<div class="interfaces-gallery">
  <div class="interface-card">
    <div class="browser-frame">
      <div class="browser-header">
        <div class="browser-actions">
          <span></span><span></span><span></span>
        </div>
        <div class="browser-title">Pilotage et Analyse</div>
      </div>
      <div class="browser-content">
        <img src="/assets/img/dfacto_pilotage_analyse.png" alt="Interface de pilotage et analyse" />
      </div>
    </div>
    <div class="interface-description">
      <h4>📊 Tableau de Bord</h4>
      <p>Vue consolidée des indicateurs clés de performance avec graphiques interactifs et filtres dynamiques.</p>
    </div>
  </div>

  <div class="interface-card">
    <div class="browser-frame">
      <div class="browser-header">
        <div class="browser-actions">
          <span></span><span></span><span></span>
        </div>
        <div class="browser-title">Suivi de Facturation</div>
      </div>
      <div class="browser-content">
        <img src="/assets/img/dfacto_suivi_facturation.png" alt="Interface de suivi de facturation" />
      </div>
    </div>
    <div class="interface-description">
      <h4>💰 Facturation</h4>
      <p>Gestion complète du cycle de facturation avec suivi des statuts et relances automatisées.</p>
    </div>
  </div>

  <div class="interface-card">
    <div class="browser-frame">
      <div class="browser-header">
        <div class="browser-actions">
          <span></span><span></span><span></span>
        </div>
        <div class="browser-title">Export Comptable</div>
      </div>
      <div class="browser-content">
        <img src="/assets/img/dfacto_export_comptable_vente.png" alt="Interface d'export comptable" />
      </div>
    </div>
    <div class="interface-description">
      <h4>📑 Exports</h4>
      <p>Génération automatisée des écritures comptables avec contrôle de cohérence intégré.</p>
    </div>
  </div>

  <div class="interface-card">
    <div class="browser-frame">
      <div class="browser-header">
        <div class="browser-actions">
          <span></span><span></span><span></span>
        </div>
        <div class="browser-title">Gestion des Devis</div>
      </div>
      <div class="browser-content">
        <img src="/assets/img/dfacto_devis.png" alt="Interface de gestion des devis" />
      </div>
    </div>
    <div class="interface-description">
      <h4>📝 Devis</h4>
      <p>Création et suivi des devis avec workflow de validation et conversion en factures.</p>
    </div>
  </div>

  <div class="interface-card">
    <div class="browser-frame">
      <div class="browser-header">
        <div class="browser-actions">
          <span></span><span></span><span></span>
        </div>
        <div class="browser-title">Suivi des Charges</div>
      </div>
      <div class="browser-content">
        <img src="/assets/img/dfacto_charge.png" alt="Interface de suivi des charges" />
      </div>
    </div>
    <div class="interface-description">
      <h4>💹 Charges</h4>
      <p>Analyse détaillée des charges avec catégorisation et ventilation analytique.</p>
    </div>
  </div>
</div>

## Fonctionnalités Clés

<div class="features-grid">
  <div class="feature-card">
    <h3>📊 Gestion Comptable</h3>
    <ul>
      <li>Plan comptable personnalisé</li>
      <li>Suivi des charges fixes/variables</li>
      <li>Gestion multi-entreprises</li>
      <li>Import/Export données</li>
    </ul>
  </div>

  <div class="feature-card">
    <h3>💰 Analyse Financière</h3>
    <ul>
      <li>Tableaux de bord dynamiques</li>
      <li>Indicateurs personnalisables</li>
      <li>Analyse des écarts</li>
      <li>Prévisions budgétaires</li>
    </ul>
  </div>

  <div class="feature-card">
    <h3>📈 Reporting</h3>
    <ul>
      <li>Rapports automatisés</li>
      <li>Export multi-formats</li>
      <li>Graphiques interactifs</li>
      <li>Alertes personnalisées</li>
    </ul>
  </div>
</div>

## Stack Technique

<div class="tech-stack">
  <div class="tech-category">
    <h3>🔧 Backend</h3>
    <ul>
      <li>PHP 5.5</li>
      <li>MySQL 5.5</li>
      <li>Architecture MVC</li>
    </ul>
  </div>

  <div class="tech-category">
    <h3>🎨 Frontend</h3>
    <ul>
      <li>JavaScript</li>
      <li>jQuery</li>
      <li>Interface responsive</li>
    </ul>
  </div>

  <div class="tech-category">
    <h3>⚡ Performance</h3>
    <ul>
      <li>Cache système</li>
      <li>Optimisation SQL</li>
      <li>Indexation avancée</li>
    </ul>
  </div>
</div>

## Témoignage Client

::: info Témoignage
"DFacto nous a permis de gagner un temps considérable dans notre gestion comptable. L'interface intuitive et les analyses en temps réel nous donnent une vision claire de notre situation financière."

_— Directeur Financier, Libre Sens_
:::

## Architecture & Interfaces

### Modules Principaux

<div class="modules-grid">
  <div class="module-card high">
    <h3>📊 Gestion Comptable</h3>
    <div class="complexity">Complexité: Haute</div>
    <ul>
      <li>Plan comptable dynamique</li>
      <li>Gestion des écritures</li>
      <li>Suivi des charges</li>
    </ul>
    <div class="relations">Relations: Multi-tables avec historisation</div>
  </div>

  <div class="module-card high">
    <h3>💰 Analyse Financière</h3>
    <div class="complexity">Complexité: Haute</div>
    <ul>
      <li>Calculs complexes</li>
      <li>Agrégation données</li>
      <li>Indicateurs temps réel</li>
    </ul>
    <div class="relations">Relations: Analyses croisées multiples</div>
  </div>

  <div class="module-card medium">
    <h3>📈 Reporting</h3>
    <div class="complexity">Complexité: Moyenne</div>
    <ul>
      <li>Génération rapports</li>
      <li>Export données</li>
      <li>Visualisations</li>
    </ul>
    <div class="relations">Relations: Agrégation multi-sources</div>
  </div>
</div>

## Métriques Techniques

<div class="metrics-grid">
  <div class="metric-card">
    <h3>📊 Volume de Code</h3>
    <ul>
      <li>12 tables principales</li>
      <li>35+ modules métier</li>
      <li>~250K lignes de code</li>
      <li>12 types juridiques gérés</li>
    </ul>
  </div>

  <div class="metric-card">
    <h3>🔄 Architecture</h3>
    <ul>
      <li>5 types de charges</li>
      <li>4 niveaux de validation</li>
      <li>3 profils utilisateurs</li>
      <li>Plan comptable complet</li>
    </ul>
  </div>

  <div class="metric-card">
    <h3>📈 Performance</h3>
    <ul>
      <li>+1000 écritures/jour</li>
      <li>+50 entreprises gérées</li>
      <li>Multi-identités juridiques</li>
      <li>Temps de réponse < 1s</li>
    </ul>
  </div>
</div>

<style scoped>
.features-grid {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
  gap: 1.5rem;
  margin: 2rem 0;
}

.feature-card {
  background: var(--bg-color-float);
  padding: 1.5rem;
  border-radius: 8px;
  box-shadow: 0 2px 4px rgba(0,0,0,0.1);
}

.feature-card h3 {
  margin-top: 0;
  color: var(--theme-color);
}

.feature-card ul {
  list-style-type: none;
  padding: 0;
}

.feature-card li {
  margin: 0.5rem 0;
  padding-left: 1.5rem;
  position: relative;
}

.feature-card li::before {
  content: "→";
  position: absolute;
  left: 0;
  color: var(--theme-color);
}

.tech-stack {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
  gap: 1.5rem;
  margin: 2rem 0;
}

.tech-category {
  background: var(--bg-color-float);
  padding: 1.5rem;
  border-radius: 8px;
  box-shadow: 0 2px 4px rgba(0,0,0,0.1);
}

img {
  max-width: 100%;
  height: auto;
  display: block;
  margin: 2rem auto;
  border-radius: 8px;
  box-shadow: 0 4px 12px rgba(0, 0, 0, 0.15);
}

.info.custom-block {
  background: linear-gradient(135deg, var(--bg-color-float) 0%, var(--bg-color) 100%);
  border: none;
  border-radius: 16px;
  padding: 2rem;
  margin: 2.5rem 0;
  position: relative;
  box-shadow: 0 10px 30px -10px rgba(0, 0, 0, 0.1);
  transform: translateY(0);
  transition: all 0.3s ease;
}

.info.custom-block:hover {
  transform: translateY(-5px);
  box-shadow: 0 15px 35px -10px rgba(0, 0, 0, 0.15);
}

.info.custom-block p {
  font-size: 1.2rem;
  line-height: 1.6;
  color: var(--text-color);
  font-style: italic;
  margin: 0;
}

.info.custom-block p:first-of-type {
  margin-bottom: 1.5rem;
}

.info.custom-block p:last-of-type {
  font-size: 1rem;
  color: var(--theme-color);
  font-weight: 600;
  text-align: right;
  margin-top: 1rem;
}

.info.custom-block::before {
  content: '"';
  position: absolute;
  top: -0.5rem;
  left: 1rem;
  font-size: 5rem;
  color: var(--theme-color);
  opacity: 0.1;
  font-family: serif;
  line-height: 1;
}

.modules-grid {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
  gap: 1.5rem;
  margin: 2rem 0;
}

.module-card {
  background: var(--bg-color-float);
  padding: 1.5rem;
  border-radius: 8px;
  box-shadow: 0 2px 4px rgba(0,0,0,0.1);
  position: relative;
}

.module-card.high {
  border-left: 4px solid #ff6b6b;
}

.module-card.medium {
  border-left: 4px solid #ffd93d;
}

.module-card.low {
  border-left: 4px solid #6bff6b;
}

.complexity {
  position: absolute;
  top: 1rem;
  right: 1rem;
  font-size: 0.8rem;
  padding: 0.2rem 0.5rem;
  border-radius: 4px;
  background: var(--theme-color);
  color: white;
}

.relations {
  margin-top: 1rem;
  font-size: 0.9rem;
  color: var(--text-color-light);
  border-top: 1px solid var(--border-color);
  padding-top: 0.5rem;
}

.metrics-grid {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
  gap: 1.5rem;
  margin: 2rem 0;
}

.metric-card {
  background: var(--bg-color-float);
  padding: 1.5rem;
  border-radius: 8px;
  box-shadow: 0 4px 6px rgba(0,0,0,0.1);
  transition: transform 0.3s ease;
}

.metric-card:hover {
  transform: translateY(-5px);
}

.metric-card h3 {
  color: var(--theme-color);
  margin-top: 0;
  display: flex;
  align-items: center;
  gap: 0.5rem;
}

.metric-card ul {
  list-style: none;
  padding: 0;
  margin: 1rem 0 0;
}

.metric-card li {
  padding: 0.5rem 0;
  border-bottom: 1px solid var(--border-color);
  display: flex;
  justify-content: space-between;
  align-items: center;
}

.metric-card li:last-child {
  border-bottom: none;
}

.hero-section {
  margin: 3rem auto;
  max-width: 1100px;
  padding: 0 1rem;
}

.hero-image-container {
  background: var(--bg-color-float);
  border-radius: 12px;
  padding: 2rem;
  box-shadow: 0 8px 24px rgba(0,0,0,0.12);
}

.browser-frame {
  background: var(--bg-color);
  border-radius: 8px;
  overflow: hidden;
  box-shadow: 0 4px 12px rgba(0,0,0,0.08);
}

.browser-header {
  background: var(--bg-color-light);
  padding: 0.75rem 1.25rem;
  border-bottom: 1px solid var(--border-color);
  display: flex;
  align-items: center;
  gap: 1rem;
}

.browser-actions {
  display: flex;
  gap: 0.5rem;
}

.browser-actions span {
  width: 12px;
  height: 12px;
  border-radius: 50%;
  background: #dee2e6;
}

.browser-actions span:nth-child(1) { background: #ff5f56; }
.browser-actions span:nth-child(2) { background: #ffbd2e; }
.browser-actions span:nth-child(3) { background: #27c93f; }

.browser-title {
  color: var(--text-color-light);
  font-size: 0.9rem;
}

.browser-content {
  padding: 0;
}

.browser-content img {
  width: 100%;
  height: auto;
  display: block;
  margin: 0;
  border-radius: 0;
}

.interfaces-gallery {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
  gap: 2rem;
  margin: 2rem 0;
}

.interface-card {
  background: var(--bg-color-float);
  border-radius: 12px;
  overflow: hidden;
  box-shadow: 0 8px 24px rgba(0,0,0,0.12);
  transition: transform 0.3s ease;
}

.interface-card:hover {
  transform: translateY(-5px);
}

.interface-description {
  padding: 1.5rem;
}

.interface-description h4 {
  margin: 0;
  color: var(--theme-color);
  display: flex;
  align-items: center;
  gap: 0.5rem;
}

.interface-description p {
  margin: 0.5rem 0 0;
  font-size: 0.9rem;
  color: var(--text-color-light);
  line-height: 1.4;
}
</style>
