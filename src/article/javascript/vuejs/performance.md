---
title: Vue.js 3 - Optimisation des Performances et Profiling
description: Guide complet sur l'optimisation des performances des applications Vue.js 3, avec conseils, outils et techniques avancées pour améliorer le chargement, les mises à jour et le profiling.
icon: fa-solid fa-tachometer-alt
lang: fr
category:
    - Vue.js
    - JavaScript
    - Development
    - Performance
tags:
    - Vue.js
    - Performance
    - Profiling
    - Best Practices
    - Optimization
    - SSR
    - Lazy Loading
    - Virtual Scrolling
dir:
    text: Performance
    icon: fa-solid fa-tachometer-alt
    order: 3
    collapsible: false
    link: true
---

![Vue.js Performance](./performance.jpg)

# Vue.js 3 : Optimisation des Performances et Profiling

La performance est un enjeu majeur dans le développement d'applications web modernes. Dans cet article, nous aborderons les meilleures pratiques pour optimiser les performances de vos applications Vue.js 3, ainsi que les outils et techniques de profiling indispensables pour identifier et résoudre les goulots d'étranglement.

Nous explorerons notamment :

-   Les optimisations de chargement initial (Page Load Performance),
-   L'amélioration des performances lors des mises à jour (Update Performance),
-   Des techniques avancées telles que le lazy loading, le code splitting, la virtualisation des listes, et le batching des mises à jour.

Ces conseils s'appuient sur les recommandations officielles de Vue.js ([Vue.js Performance Best Practices](https://vuejs.org/guide/best-practices/performance)) ainsi que sur des ressources comme [ce guide avancé sur DEV Community](https://dev.to/delia_code/how-to-optimize-performance-in-vuejs-applications-beginner-to-advanced-guide-53db) et le [Vue.js Performance Guide de MadewithVueJS](https://madewithvuejs.com/blog/vuejs-performance-guide).

---

## 1. Introduction : Pourquoi la Performance est Cruciale ?

La performance de votre application influe directement sur l'expérience utilisateur, le SEO et même la rétention d'utilisateurs. Deux aspects se distinguent :

-   **Page Load Performance** : Vitesse de chargement initial et interactivité dès la première visite (LCP, FID).
-   **Update Performance** : Temps de réponse lors d'interactions, par exemple quand une liste se met à jour ou qu'une navigation interne se produit.

Le choix de l'architecture et des techniques d'optimisation est déterminant pour atteindre ces objectifs.

---

## 2. Outils de Profiling et Mesures

### 2.1 Vue Devtools

Vue Devtools est indispensable pour inspecter l'état réactif, suivre les mises à jour de composants et analyser les performances en temps réel.

### 2.2 Lighthouse

Lighthouse permet d'auditer la performance, l'accessibilité et le SEO de votre application. Il fournit des métriques essentielles comme le LCP et le FID.

### 2.3 Webpack Bundle Analyzer

Cet outil visualise la taille de vos bundles et aide à identifier les modules lourds à optimiser.

### 2.4 Outils de Monitoring

Des solutions comme Sentry permettent de monitorer les performances et d'identifier rapidement les problèmes en production.

---

## 3. Optimisations du Chargement (Page Load Optimizations)

### 3.1 Code Splitting et Lazy Loading

Divisez votre code en petits bundles et chargez-les à la demande. Avec Vite, le code splitting est automatique pour les imports dynamiques :

```typescript
// ✅ Vite gère automatiquement le code splitting
const LazyComponent = () => import('./LazyComponent.vue');
```

### 3.2 Bundle Size et Tree Shaking

Vite utilise Rollup en production et offre un excellent tree-shaking par défaut. Configurez votre `vite.config.ts` pour optimiser la taille du bundle :

```typescript
// vite.config.ts
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';

export default defineConfig({
    build: {
        // Réduire la taille du chunk
        chunkSizeWarningLimit: 600,
        rollupOptions: {
            output: {
                manualChunks: {
                    vendor: ['vue', 'vue-router', 'pinia'],
                    ui: ['./src/components/ui/**/*.vue'],
                },
            },
        },
    },
    // Optimisations pour la production
    optimizeDeps: {
        include: ['vue', 'vue-router', 'pinia'],
    },
});
```

---

## 4. Optimisations des Mises à Jour (Update Optimizations)

### 4.1 Stabilité des Props et Utilisation de v-once

Utilisez des props stables pour éviter des re-rendus inutiles. La directive `v-once` permet de rendre statique une partie du template.

```vue
<p v-once>{{ staticContent }}</p>
```

### 4.2 Optimisations avec v-memo et Computed Stability

La directive `v-memo` (à venir dans certaines versions ou via plugins) et le recours aux computed properties stables améliorent la réactivité en évitant les recalculations inutiles.

### 4.3 Virtual Scrolling pour les Grandes Listes

Pour gérer de longues listes, le virtual scrolling ne rend que les éléments visibles. Par exemple, avec `vue-virtual-scroll-list` :

```vue
<template>
    <virtual-list :size="50" :remain="10" :items="items">
        <template v-slot="{ item }">
            <div class="item">{{ item }}</div>
        </template>
    </virtual-list>
</template>

<script>
import { ref } from 'vue';
import VirtualList from 'vue-virtual-scroll-list';

export default {
    components: { VirtualList },
    setup() {
        const items = ref([...Array(1000).keys()]);
        return { items };
    },
};
</script>
```

---

## 5. Techniques Avancées de Profiling et Optimisation

### 5.1 Optimisations Réactives

#### Réduction de la Surcharge Réactive

Pour les structures de données immuables volumineuses, utilisez `shallowRef` ou `shallowReactive` :

```typescript
// ❌ Surcharge réactive inutile pour les données immuables
const data = ref(largeImmutableData);

// ✅ Meilleure performance avec shallowRef
const data = shallowRef(largeImmutableData);
```

#### Stabilité des Props

Évitez de créer de nouvelles références à chaque rendu :

```typescript
// ❌ Nouvelle référence à chaque rendu
<MyComponent :options="['A', 'B', 'C'].map(x => x.toLowerCase())" />

// ✅ Référence stable
const options = computed(() => ['A', 'B', 'C'].map(x => x.toLowerCase()))
<MyComponent :options="options" />
```

### 5.2 Optimisations de Rendu

#### Utilisation de v-memo

La directive `v-memo` permet d'éviter les re-rendus inutiles :

```vue
<template>
    <div v-memo="[item.id, item.selected]">
        <!-- Contenu complexe -->
        <ExpensiveComponent :item="item" />
    </div>
</template>
```

#### Virtualisation des Listes

Pour les listes volumineuses, utilisez la virtualisation avec `vue-virtual-scroller` :

```vue
<template>
    <RecycleScroller class="scroller" :items="items" :item-size="32" key-field="id" v-slot="{ item }">
        <div class="user">
            {{ item.name }}
        </div>
    </RecycleScroller>
</template>

<script setup>
import { RecycleScroller } from 'vue-virtual-scroller';
import 'vue-virtual-scroller/dist/vue-virtual-scroller.css';

const items = ref(
    Array.from({ length: 10000 }, (_, i) => ({
        id: i,
        name: `User ${i}`,
    })),
);
</script>
```

### 5.3 Optimisations de Build

#### Configuration Avancée de Vite

Optimisez votre build Vite pour la production :

```typescript
// vite.config.ts
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';

export default defineConfig({
    build: {
        // Minification avancée
        minify: 'terser',
        terserOptions: {
            compress: {
                drop_console: true,
                drop_debugger: true,
            },
        },
        // Pré-bundling des dépendances
        commonjsOptions: {
            include: [/node_modules/],
        },
        // Génération de sourcemaps
        sourcemap: false,
        // Optimisation des assets
        assetsInlineLimit: 4096,
        // Compression Brotli
        brotliSize: true,
    },
});
```

#### Mode Bibliothèque

Pour le développement de bibliothèques Vue.js :

```typescript
// vite.config.ts
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import { resolve } from 'path';

export default defineConfig({
    build: {
        lib: {
            entry: resolve(__dirname, 'src/index.ts'),
            name: 'MyLib',
            fileName: (format) => `my-lib.${format}.js`,
        },
        rollupOptions: {
            external: ['vue'],
            output: {
                globals: {
                    vue: 'Vue',
                },
            },
        },
    },
});
```

### 5.4 Monitoring et Analytics

#### Performance Metrics

Implémentez le monitoring des Web Vitals :

```typescript
import { onMounted } from 'vue';
import { getCLS, getFID, getLCP } from 'web-vitals';

export function usePerformanceMonitoring() {
    onMounted(() => {
        getCLS(console.log);
        getFID(console.log);
        getLCP(console.log);
    });
}
```

#### Error Tracking

Intégrez un système de suivi des erreurs :

```typescript
app.config.errorHandler = (err, instance, info) => {
    // Envoi à votre service de monitoring
    trackError({
        error: err,
        componentName: instance?.$.type.name,
        info,
    });
};
```

## 6. Nouvelles Fonctionnalités de Performance (Vue 3.3+)

### 6.1 Compiler Macros

Utilisez les nouvelles macros de compilation pour une meilleure performance :

```vue
<script setup>
// Définition de props avec validation à la compilation
defineProps<{
  items: { id: number; name: string }[]
}>()

// Modèle réactif optimisé
defineModel<string>({ default: '' })
</script>
```

### 6.2 Suspense et Async Setup

Gérez le chargement asynchrone efficacement :

```vue
<template>
    <Suspense>
        <template #default>
            <AsyncComponent />
        </template>
        <template #fallback>
            <LoadingSpinner />
        </template>
    </Suspense>
</template>

<script setup>
const AsyncComponent = defineAsyncComponent(() => import('./HeavyComponent.vue'));
</script>
```

## 7. Meilleures Pratiques de Déploiement

### 7.1 CDN et Edge Caching

Utilisez un CDN avec des règles de cache optimisées :

```nginx
# nginx.conf
location /assets/ {
    expires 1y;
    add_header Cache-Control "public, no-transform";
}
```

### 7.2 Compression et Optimisation

Activez la compression et les optimisations modernes :

```nginx
# nginx.conf
gzip on;
gzip_types text/plain text/css application/javascript application/json;
brotli on;
brotli_types text/plain text/css application/javascript application/json;
```

## 8. Ressources et Outils 🛠️

### 8.1 Outils de Performance

-   [Vue DevTools Performance Tab](https://devtools.vuejs.org/guide/performance.html)
-   [Lighthouse CI](https://github.com/GoogleChrome/lighthouse-ci)
-   [WebPageTest](https://www.webpagetest.org/)
-   [Chrome DevTools Performance Panel](https://developer.chrome.com/docs/devtools/performance)

### 8.2 Ressources d'Apprentissage

-   [Vue.js Performance Documentation](https://vuejs.org/guide/best-practices/performance.html)
-   [Web Vitals](https://web.dev/vitals/)
-   [Performance Budgets](https://web.dev/performance-budgets-101/)
-   [Vue Performance Series](https://www.vuemastery.com/courses/vue-3-performance)

## 9. Conclusion

L'optimisation des performances dans Vue.js 3 est un processus continu qui nécessite une approche holistique. En suivant ces bonnes pratiques et en utilisant les outils appropriés, vous pouvez créer des applications performantes qui offrent une excellente expérience utilisateur.

N'oubliez pas de :

-   Mesurer avant d'optimiser
-   Se concentrer sur les métriques qui comptent pour vos utilisateurs
-   Maintenir un équilibre entre performance et maintenabilité
-   Surveiller régulièrement les performances en production

_Citations_ :

-   [Vue.js Performance Guide](https://madewithvuejs.com/blog/vuejs-performance-guide)
-   [Vue.js Official Performance Best Practices](https://vuejs.org/guide/best-practices/performance.html)
-   [How to Optimize Performance in Vue.js Applications](https://dev.to/delia_code/how-to-optimize-performance-in-vuejs-applications-beginner-to-advanced-guide-53db)

## 10. Configuration Vite Recommandée

### 10.1 Structure du Projet

```bash
.
├── index.html
├── package.json
├── vite.config.ts
├── tsconfig.json
├── public/
└── src/
    ├── assets/
    ├── components/
    ├── composables/
    ├── pages/
    └── main.ts
```

### 10.2 Configuration TypeScript

```typescript
// tsconfig.json
{
  "compilerOptions": {
    "target": "ESNext",
    "useDefineForClassFields": true,
    "module": "ESNext",
    "moduleResolution": "Node",
    "strict": true,
    "jsx": "preserve",
    "sourceMap": true,
    "resolveJsonModule": true,
    "isolatedModules": true,
    "esModuleInterop": true,
    "lib": ["ESNext", "DOM"],
    "skipLibCheck": true,
    "paths": {
      "@/*": ["./src/*"]
    }
  },
  "include": ["src/**/*.ts", "src/**/*.d.ts", "src/**/*.tsx", "src/**/*.vue"],
  "references": [{ "path": "./tsconfig.node.json" }]
}
```

### 10.3 Configuration Vite Complète

```typescript
// vite.config.ts
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import { resolve } from 'path';

export default defineConfig({
    plugins: [vue()],
    resolve: {
        alias: {
            '@': resolve(__dirname, './src'),
        },
    },
    server: {
        port: 3000,
        open: true,
        cors: true,
    },
    build: {
        target: 'esnext',
        minify: 'terser',
        cssCodeSplit: true,
        rollupOptions: {
            output: {
                manualChunks: {
                    vendor: ['vue', 'vue-router', 'pinia'],
                    styles: ['./src/assets/styles/main.css'],
                    utils: ['./src/utils/**/*.ts'],
                },
            },
        },
    },
    optimizeDeps: {
        include: ['vue', 'vue-router', 'pinia'],
        exclude: ['your-package-name'],
    },
});
```

Cette configuration inclut :

-   Support TypeScript optimisé
-   Alias de chemins
-   Optimisation des dépendances
-   Découpage intelligent du code
-   Configuration du serveur de développement
-   Optimisations de production
