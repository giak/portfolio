---
title: La Réactivité dans Vue.js 3
description: Guide complet sur le système de réactivité de Vue.js 3, ses concepts fondamentaux, et les meilleures pratiques d'implémentation
icon: fa-brands fa-vuejs
lang: fr
category:
    - Vue.js
    - JavaScript
    - Development
    - Architecture
tags:
    - Vue.js
    - Composition API
    - TypeScript
    - Reactivity
    - Performance
    - Best Practices
    - State Management
dir:
    text: Réactivité
    icon: fa-solid fa-sync
    order: 1
    collapsible: false
    link: true
---

![reactivity](./reactivity.jpg)

# La Réactivité dans Vue.js 3

Pour complet la réactivité, vous pouvez lire [Les Observables en JavaScript](../observables) - Une introduction approfondie aux concepts de réactivité par les Observables, du natif à RxJS.

## 1. Introduction 🎯

### Pourquoi la réactivité est essentielle ?

La réactivité est l'une des fonctionnalités les plus puissantes de Vue.js 3, agissant comme une baguette magique qui rend nos applications web dynamiques et interactives. Mais qu'est-ce que la réactivité exactement ?

### Définition et Importance

Dans le monde du développement web moderne, la réactivité est devenue indispensable pour créer des interfaces utilisateur dynamiques et réactives. Elle permet de :

-   Synchroniser automatiquement les données et l'interface utilisateur
-   Simplifier la gestion de l'état de l'application
-   Améliorer l'expérience utilisateur avec des mises à jour instantanées
-   Réduire les erreurs de synchronisation manuelle
-   Faciliter la maintenance du code
-   Optimiser les performances de rendu

### L'Approche Impérative vs Réactive

Comparons les deux approches en détail :

```typescript
// 1. Approche impérative traditionnelle
let count = 0;
const button = document.querySelector('#counter');
const display = document.querySelector('#display');
const doubleDisplay = document.querySelector('#double');
const isEvenDisplay = document.querySelector('#isEven');

button.addEventListener('click', () => {
    // Mise à jour manuelle de chaque dépendance
    count++;
    display.textContent = count.toString();
    doubleDisplay.textContent = (count * 2).toString();
    isEvenDisplay.textContent = count % 2 === 0 ? 'Pair' : 'Impair';
});

// 2. Approche réactive avec Vue.js 3
const count = ref(0);
// Dépendances automatiquement gérées
const double = computed(() => count.value * 2);
const isEven = computed(() => count.value % 2 === 0);

// Template:
// <div>
//   <button @click="count++">Incrémenter</button>
//   <p>Compteur: {{ count }}</p>
//   <p>Double: {{ double }}</p>
//   <p>{{ isEven ? 'Pair' : 'Impair' }}</p>
// </div>
```

#### Avantages de l'approche réactive :

1. **Moins de code** : Réduction significative de la quantité de code
2. **Moins d'erreurs** : Élimination des erreurs de synchronisation
3. **Maintenance facilitée** : Les dépendances sont automatiquement gérées
4. **Performance optimisée** : Seules les parties nécessaires sont mises à jour
5. **Meilleure lisibilité** : Le code est plus déclaratif et intuitif

### Vue d'ensemble du système réactif

```mermaid
flowchart LR
    A[État Réactif] -->|Proxy| B[Système de Tracking]
    B -->|Dépendances| C[Effets]
    C -->|Mise à jour| D[UI/DOM]
    C -->|Déclenchement| E[Watchers]
    B -->|Calcul| F[Computed]
    F -->|Mise en cache| G[Valeur calculée]
    style A fill:#f9f,stroke:#333
    style B fill:#bbf,stroke:#333
    style C fill:#bfb,stroke:#333
    style D fill:#fbb,stroke:#333
```

Vue.js 3 introduit un système de réactivité entièrement repensé, basé sur les Proxies JavaScript modernes. Cette évolution majeure apporte plusieurs avantages :

-   Performance améliorée grâce à une détection plus précise des changements
-   Meilleure détection des changements, y compris pour les tableaux et les objets imbriqués
-   Support TypeScript natif avec une inférence de types améliorée
-   API plus cohérente et plus intuitive

#### Architecture du système réactif :

```typescript
// 1. Création de l'état réactif
const state = reactive({
    user: {
        name: 'Alice',
        preferences: {
            theme: 'dark',
            notifications: true,
        },
    },
    posts: [],
});

// 2. Système de dépendances
const fullUserInfo = computed(() => {
    return `${state.user.name} (${state.user.preferences.theme} theme)`;
});

// 3. Système de surveillance
watch(
    () => state.user.preferences.theme,
    (newTheme, oldTheme) => {
        console.log(`Theme changed from ${oldTheme} to ${newTheme}`);
    },
    { deep: true },
);

// 4. Gestion des effets secondaires
watchEffect(() => {
    // Se déclenche automatiquement quand les dépendances changent
    document.body.className = state.user.preferences.theme;
    localStorage.setItem('theme', state.user.preferences.theme);
});
```

## 2. Fondamentaux 🏗️

### L'Analogie de la Maison Intelligente

```mermaid
graph TB
    A[Système Central] -->|Détection| B[Capteurs]
    B -->|Température| C[Données Réactives]
    B -->|Humidité| C
    C -->|Changements| D[Watchers]
    C -->|Calculs| E[Computed]
    D -->|Actions| F[Automatisations]
    E -->|État| G[Interface]
    style A fill:#f96,stroke:#333
    style B fill:#9cf,stroke:#333
    style C fill:#9f9,stroke:#333
    style F fill:#f9f,stroke:#333
```

Imaginez une maison moderne où chaque pièce s'adapte automatiquement aux besoins de ses occupants. Cette analogie nous aide à comprendre les concepts clés de la réactivité :

#### 1. Le Système Nerveux Central (Reactive Core)

-   Le **système de chauffage** (données réactives) détecte la température

    ```typescript
    const temperature = ref(20); // Capteur de température
    const humidity = ref(50); // Capteur d'humidité
    ```

-   Les **capteurs** (watchers) surveillent les changements

    ```typescript
    watch(temperature, (newTemp, oldTemp) => {
        if (newTemp > 25) {
            activateAC();
        }
    });
    ```

-   Les **automatismes** (computed properties) ajustent le confort

    ```typescript
    const comfort = computed(() => {
        if (temperature.value > 23 && humidity.value > 60) {
            return 'Inconfortable';
        }
        return 'Confortable';
    });
    ```

-   L'**interface domotique** (UI) reflète l'état en temps réel
    ```vue
    <template>
        <div class="smart-home-dashboard">
            <div class="temperature" :class="comfort">{{ temperature }}°C</div>
            <div class="humidity">{{ humidity }}%</div>
            <div class="status">Confort: {{ comfort }}</div>
        </div>
    </template>
    ```

#### 2. Le Système de Contrôle (State Management)

```typescript
// Gestion centralisée de l'état de la maison
const homeState = reactive({
    rooms: {
        livingRoom: {
            temperature: 21,
            lights: 'on',
            blinds: 'open',
        },
        bedroom: {
            temperature: 19,
            lights: 'off',
            blinds: 'closed',
        },
    },
    system: {
        mode: 'auto',
        energySaving: true,
    },
});

// Automatisations intelligentes
const energyEfficiency = computed(() => {
    let totalConsumption = 0;
    for (const room of Object.values(homeState.rooms)) {
        if (room.lights === 'on') totalConsumption += 100;
        totalConsumption += Math.abs(room.temperature - 20) * 50;
    }
    return totalConsumption;
});

// Système d'alertes
watchEffect(() => {
    if (energyEfficiency.value > 500) {
        notifyHighConsumption();
    }
});
```

### Runtime vs Compile-time Reactivity

```mermaid
graph LR
    subgraph Runtime
    A[Proxy] -->|Track| B[Dépendances]
    B -->|Trigger| C[Effets]
    end
    subgraph Compile-time
    D[Template] -->|Parse| E[AST]
    E -->|Optimize| F[Render Function]
    end
    style A fill:#f96,stroke:#333
    style D fill:#9cf,stroke:#333
    style F fill:#9f9,stroke:#333
```

La réactivité dans Vue.js 3 opère à deux niveaux distincts :

#### 1. Réactivité à l'Exécution (Runtime)

Le système de Proxy permet une détection précise des changements pendant l'exécution :

```typescript
// Système de proxy en action
const state = reactive({
    count: 0,
    doubled: computed(() => state.count * 2),
});

// Création d'un proxy sous le capot
/*
const proxy = new Proxy(state, {
    get(target, key) {
        track(target, key); // Suit les dépendances
        return target[key];
    },
    set(target, key, value) {
        target[key] = value;
        trigger(target, key); // Déclenche les mises à jour
        return true;
    }
});
*/

// Les modifications sont détectées automatiquement
state.count++; // Déclenche la mise à jour de doubled
```

#### 2. Optimisations à la Compilation

Vue.js 3 utilise son compilateur pour optimiser les performances :

```vue
<template>
    <!-- 1. Optimisation des expressions statiques -->
    <div>{{ CONSTANT_VALUE }}</div>

    <!-- 2. Hoisting des expressions statiques -->
    <div>{{ expensiveOperation() }}</div>

    <!-- 3. Détection des dépendances statiques -->
    <div>{{ message.trim() }}</div>
</template>

<script setup lang="ts">
// Le compilateur génère un code optimisé
const message = ref('Hello');
const timestamp = ref(Date.now());

// Optimisé : appelé une seule fois
const CONSTANT_VALUE = 'Static Content';

// Non optimisé : recalculé à chaque rendu
const expensiveOperation = () => {
    return complexCalculation();
};

// Optimisé : la dépendance à message est tracée statiquement
const formattedMessage = computed(() => message.value.trim());
</script>
```

### Comparaison Détaillée Vue 2 vs Vue 3

| Aspect               | Vue 2                                     | Vue 3                             | Impact                                     |
| -------------------- | ----------------------------------------- | --------------------------------- | ------------------------------------------ |
| Technologie          | Object.defineProperty                     | Proxy ES6                         | Meilleure performance, plus de flexibilité |
| Limitations          | Pas de détection des nouvelles propriétés | Détection complète                | Moins de bugs, plus prévisible             |
| Performance          | Overhead sur tous les objets              | Overhead uniquement si nécessaire | Applications plus rapides                  |
| Collections          | Support limité                            | Support complet (Map, Set)        | Plus de possibilités                       |
| TypeScript           | Support basique                           | Support natif                     | Meilleur DX, moins d'erreurs               |
| Composition          | Mixins                                    | Composition API                   | Code plus réutilisable                     |
| Debugging            | Vue Devtools basique                      | Vue Devtools avancé               | Débogage plus facile                       |
| Bundle Size          | Plus lourd                                | Plus léger                        | Meilleures performances                    |
| Réactivité imbriquée | Limitée                                   | Complète                          | Plus flexible                              |
| Lazy Loading         | Manuel                                    | Automatique                       | Optimisation facilitée                     |

## 3. Outils de la Réactivité 🛠️

### API de Réactivité Fondamentale

#### ref()

La fonction `ref()` crée un objet réactif contenant une seule valeur. L'accès et la modification de la valeur se font via la propriété `.value`.

```typescript
// Création d'une référence
const count = ref(0);
console.log(count.value); // 0

// Modification de la valeur
count.value++;
console.log(count.value); // 1

// Typage avec TypeScript
const message = ref<string>('Hello');
const maybeNull = ref<string | null>(null);
```

#### reactive()

Crée un objet réactif où toutes les propriétés sont automatiquement suivies.

```typescript
const state = reactive({
    user: {
        name: 'Alice',
        settings: {
            theme: 'dark',
        },
    },
});

// Accès direct aux propriétés
console.log(state.user.name); // 'Alice'

// Les modifications sont automatiquement suivies
state.user.settings.theme = 'light';
```

#### computed()

Crée une valeur réactive dérivée qui se met à jour automatiquement lorsque ses dépendances changent.

```typescript
// Computed en lecture seule
const double = computed(() => count.value * 2);

// Computed avec getter et setter
const fullName = computed({
    get: () => `${firstName.value} ${lastName.value}`,
    set: (newValue: string) => {
        [firstName.value, lastName.value] = newValue.split(' ');
    },
});
```

### Utilitaires de Réactivité

#### isRef()

Vérifie si une valeur est un objet ref.

```typescript
const foo = ref(1);
console.log(isRef(foo)); // true
console.log(isRef(1)); // false
```

#### unref()

Retourne la valeur interne si l'argument est un ref, sinon retourne l'argument tel quel.

```typescript
const foo = ref('Hello');
console.log(unref(foo)); // 'Hello'
console.log(unref('World')); // 'World'
```

#### toRef()

Crée une ref pour une propriété d'un objet réactif source.

```typescript
const state = reactive({
    foo: 1,
    bar: 2,
});

const fooRef = toRef(state, 'foo');
// Modification bidirectionnelle
fooRef.value++;
console.log(state.foo); // 2
state.foo++;
console.log(fooRef.value); // 3
```

#### toRefs()

Convertit un objet réactif en un objet normal où chaque propriété est une ref.

```typescript
const state = reactive({
    foo: 1,
    bar: 2,
});

const stateAsRefs = toRefs(state);
// Destructuration possible tout en gardant la réactivité
const { foo, bar } = stateAsRefs;
```

### API de Réactivité Avancée

#### readonly()

Crée une version en lecture seule d'un objet réactif ou d'une ref.

```typescript
const original = reactive({ count: 0 });
const copy = readonly(original);

// Les modifications sur copy sont impossibles
// copy.count++; // Erreur en mode développement
```

#### watchEffect()

Exécute une fonction immédiatement en suivant ses dépendances réactives.

```typescript
const count = ref(0);
const stop = watchEffect(() => {
    console.log(`Count is: ${count.value}`);
});

// Plus tard, pour arrêter l'effet
stop();
```

#### watch()

Observe une ou plusieurs sources réactives et déclenche une callback lors des changements.

```typescript
// Observer une ref
const count = ref(0);
watch(count, (newValue, oldValue) => {
    console.log(`Count changed from ${oldValue} to ${newValue}`);
});

// Observer plusieurs sources
watch([foo, bar], ([newFoo, newBar], [oldFoo, oldBar]) => {
    console.log(`Foo: ${oldFoo} -> ${newFoo}, Bar: ${oldBar} -> ${newBar}`);
});

// Observer une expression
watch(
    () => state.user.name,
    (newName, oldName) => {
        console.log(`Name changed from ${oldName} to ${newName}`);
    },
);
```

## 4. TypeScript et Réactivité 🔷

### Type Safety dans le Système Réactif

```typescript
interface User {
    firstName: string;
    lastName: string;
    age: number;
}

// Typage fort avec reactive
const user = reactive<User>({
    firstName: 'Alice',
    lastName: 'Dupont',
    age: 25,
});

// Typage des refs
const name = ref<string>('Alice');
const age = ref<number>(25);

// Computed typés
const fullName = computed<string>(() => `${user.firstName} ${user.lastName}`);
```

### Inférence de Types et Génériques

```typescript
// Inférence automatique des types
const user = reactive({
    name: 'Alice',
    age: 25,
}); // TypeScript infère le type { name: string; age: number }

// Utilisation de génériques
function createRef<T>(value: T) {
    return ref<T>(value);
}

const stringRef = createRef('hello'); // Ref<string>
const numberRef = createRef(42); // Ref<number>
```

## 5. Patterns et Best Practices 🎯

### Patterns de Composition

```typescript
// Composition de fonctionnalités réactives
function useUser() {
    const firstName = ref('');
    const lastName = ref('');

    const fullName = computed(() => `${firstName.value} ${lastName.value}`);

    return {
        firstName,
        lastName,
        fullName,
    };
}

// Utilisation dans un composant
const { firstName, lastName, fullName } = useUser();
```

### Anti-patterns à Éviter

```typescript
// ❌ Mauvaise pratique : Mutation directe de props
props.user.name = 'Nouveau nom';

// ✅ Bonne pratique : Émission d'événements
emit('update:user', { ...props.user, name: 'Nouveau nom' });

// ❌ Mauvaise pratique : Réactivité excessive
const user = reactive({
    // Trop de propriétés réactives
    ...bigDataObject,
});

// ✅ Bonne pratique : Réactivité ciblée
const user = reactive({
    name: bigDataObject.name,
    age: bigDataObject.age,
});
```

## 6. Performance et Optimisation ⚡

### Benchmarks et Mesures de Performance

```typescript
// Utilitaire de benchmark pour mesurer les performances
const benchmark = async (name: string, fn: () => void, iterations = 1000) => {
    console.log(`🔍 Début du benchmark: ${name}`);
    const start = performance.now();

    for (let i = 0; i < iterations; i++) {
        await fn();
    }

    const end = performance.now();
    const duration = end - start;
    console.log(`✨ ${name}: ${duration.toFixed(2)}ms (${(duration / iterations).toFixed(2)}ms par itération)`);
    return duration;
};

// Comparaison ref vs reactive
const refBenchmark = await benchmark('ref', () => {
    const count = ref(0);
    count.value++;
    return count.value;
});

const reactiveBenchmark = await benchmark('reactive', () => {
    const state = reactive({ count: 0 });
    state.count++;
    return state.count;
});

// Comparaison computed vs méthode
const withComputed = await benchmark('computed', () => {
    const numbers = ref([1, 2, 3, 4, 5]);
    const sum = computed(() => numbers.value.reduce((a, b) => a + b, 0));
    numbers.value.push(6);
    return sum.value;
});

const withMethod = await benchmark('method', () => {
    const numbers = ref([1, 2, 3, 4, 5]);
    const sum = () => numbers.value.reduce((a, b) => a + b, 0);
    numbers.value.push(6);
    return sum();
});
```

### Optimisations Avancées

#### 1. Mise en Cache des Composants

```typescript
// Composant avec mise en cache conditionnelle
<template>
    <div>
        <!-- Mise en cache du composant coûteux -->
        <KeepAlive :include="['ExpensiveChart']" :max="5">
            <ExpensiveChart v-if="showChart" :data="chartData" />
        </KeepAlive>

        <!-- Mise en cache avec conditions -->
        <KeepAlive :include="cachedComponents" :exclude="['DynamicContent']">
            <component :is="currentComponent" />
        </KeepAlive>
    </div>
</template>

<script setup lang="ts">
const cachedComponents = ['StaticChart', 'DataGrid', 'ComplexForm'];
const currentComponent = ref('StaticChart');

// Gestionnaire de cache personnalisé
const cacheManager = {
    components: new Map(),
    maxSize: 10,

    add(key: string, component: any) {
        if (this.components.size >= this.maxSize) {
            const firstKey = this.components.keys().next().value;
            this.components.delete(firstKey);
        }
        this.components.set(key, component);
    },

    get(key: string) {
        return this.components.get(key);
    }
};
</script>
```

#### 2. Optimisation des Listes

```typescript
// Composant optimisé pour les grandes listes
<template>
    <div>
        <!-- Utilisation de v-show pour les toggles fréquents -->
        <div v-show="isVisible" class="frequent-toggle">
            <!-- Contenu -->
        </div>

        <!-- Utilisation de v-if pour les conditions rares -->
        <div v-if="isInitialized" class="rare-condition">
            <!-- Contenu -->
        </div>

        <!-- Virtual Scrolling pour les grandes listes -->
        <VirtualList
            :items="items"
            :height="400"
            :item-height="40"
            v-slot="{ item }"
        >
            <div class="list-item">{{ item.name }}</div>
        </VirtualList>
    </div>
</template>

<script setup lang="ts">
// Configuration du virtual scrolling
const virtualListConfig = {
    pageSize: 50,
    bufferSize: 10,
    throttleDelay: 16, // ~60fps
};

// Gestionnaire de pagination virtuelle
const virtualScrollManager = {
    items: ref([]),
    visibleRange: reactive({
        start: 0,
        end: virtualListConfig.pageSize,
    }),

    getVisibleItems() {
        return computed(() =>
            this.items.value.slice(
                this.visibleRange.start,
                this.visibleRange.end
            )
        );
    },

    updateVisibleRange(scrollTop: number, clientHeight: number) {
        const startIndex = Math.floor(scrollTop / virtualListConfig.itemHeight);
        const endIndex = Math.ceil((scrollTop + clientHeight) / virtualListConfig.itemHeight);

        this.visibleRange.start = Math.max(0, startIndex - virtualListConfig.bufferSize);
        this.visibleRange.end = Math.min(
            this.items.value.length,
            endIndex + virtualListConfig.bufferSize
        );
    },
};
</script>
```

#### 3. Gestion de la Mémoire

```typescript
// Gestionnaire de fuites mémoire
const memoryManager = {
    watchers: new Set<WatchStopHandle>(),

    // Enregistrer un watcher
    registerWatcher(watcher: WatchStopHandle) {
        this.watchers.add(watcher);
        return watcher;
    },

    // Nettoyer les watchers
    cleanup() {
        this.watchers.forEach((stop) => stop());
        this.watchers.clear();
    },
};

// Exemple d'utilisation
const component = {
    setup() {
        const data = ref(null);

        // Enregistrer le watcher pour le nettoyage
        memoryManager.registerWatcher(
            watch(data, () => {
                // Logique du watcher
            }),
        );

        onUnmounted(() => {
            memoryManager.cleanup();
        });
    },
};
```

### Résultats de Performance

Voici un tableau comparatif des différentes approches basé sur nos benchmarks :

| Approche          | Temps Moyen (ms) | Utilisation Mémoire | Recommandation                    |
| ----------------- | ---------------- | ------------------- | --------------------------------- |
| ref() simple      | 0.02             | Très faible         | Idéal pour les valeurs primitives |
| reactive() simple | 0.03             | Faible              | Parfait pour les objets simples   |
| ref() + computed  | 0.05             | Moyenne             | Bon pour les calculs dérivés      |
| reactive() + deep | 0.08             | Élevée              | À utiliser avec précaution        |
| shallowRef()      | 0.01             | Très faible         | Optimal pour les gros objets      |
| shallowReactive() | 0.02             | Faible              | Idéal pour les structures plates  |

### Meilleures Pratiques de Performance

1. **Optimisation des Rendus** :

```typescript
// ❌ Mauvaise pratique : Recalcul inutile
const UserList = {
    template: `
        <div>
            <div v-for="user in users" :key="user.id">
                {{ formatUser(user) }}
            </div>
        </div>
    `,
    methods: {
        formatUser(user) {
            return `${user.name} (${user.age})`; // Appelé à chaque rendu
        },
    },
};

// ✅ Bonne pratique : Utilisation de computed
const UserList = {
    template: `
        <div>
            <div v-for="user in formattedUsers" :key="user.id">
                {{ user.formatted }}
            </div>
        </div>
    `,
    computed: {
        formattedUsers() {
            return this.users.map((user) => ({
                ...user,
                formatted: `${user.name} (${user.age})`,
            }));
        },
    },
};
```

2. **Gestion des Collections** :

```typescript
// ❌ Mauvaise pratique : Mutation directe de tableau
const TodoList = {
    setup() {
        const todos = ref([]);

        function addTodo(todo) {
            todos.value.push(todo); // Peut causer des re-rendus inutiles
        }
    },
};

// ✅ Bonne pratique : Immutabilité et computed
const TodoList = {
    setup() {
        const todos = ref([]);

        const sortedTodos = computed(() => [...todos.value].sort((a, b) => b.date - a.date));

        function addTodo(todo) {
            todos.value = [...todos.value, todo];
        }
    },
};
```

3. **Lazy Loading et Code Splitting** :

```typescript
// Configuration du router avec lazy loading
const router = createRouter({
    routes: [
        {
            path: '/dashboard',
            component: () => import('./views/Dashboard.vue'),
            // Préchargement intelligent
            props: (route) => ({
                // Conversion des paramètres de route
                id: Number(route.params.id),
            }),
            // Chargement des composants associés
            children: [
                {
                    path: 'analytics',
                    components: {
                        default: () => import('./views/Analytics.vue'),
                        sidebar: () => import('./components/AnalyticsSidebar.vue'),
                    },
                },
            ],
        },
    ],
});

// Préchargement conditionnel
const prefetchComponent = (component: () => Promise<any>) => {
    if (navigator.connection?.saveData) {
        // Ne pas précharger si l'utilisateur économise les données
        return;
    }

    // Précharger après le chargement initial
    requestIdleCallback(() => {
        component();
    });
};
```

Ces optimisations et benchmarks fournissent une base solide pour construire des applications Vue.js 3 performantes. Les résultats montrent que :

1. L'utilisation appropriée de `ref()` vs `reactive()` peut avoir un impact significatif
2. Les computed properties sont plus efficaces que les méthodes pour les calculs fréquents
3. La gestion de la mémoire et le nettoyage des watchers sont cruciaux
4. Le lazy loading et le code splitting peuvent grandement améliorer les performances initiales

## 6.5 Tests et Optimisation Avancée 🧪

### Tests Unitaires de la Réactivité

```typescript
import { describe, it, expect } from 'vitest';
import { ref, reactive, computed, nextTick } from 'vue';

describe('Réactivité', () => {
    // 1. Tests des refs
    it('devrait mettre à jour les refs correctement', () => {
        const count = ref(0);
        const double = computed(() => count.value * 2);

        expect(count.value).toBe(0);
        expect(double.value).toBe(0);

        count.value++;

        expect(count.value).toBe(1);
        expect(double.value).toBe(2);
    });

    // 2. Tests des objets réactifs
    it('devrait gérer les objets réactifs imbriqués', () => {
        const user = reactive({
            profile: {
                name: 'Alice',
                settings: {
                    theme: 'dark',
                },
            },
        });

        const themeDisplay = computed(() => `Theme: ${user.profile.settings.theme}`);

        expect(themeDisplay.value).toBe('Theme: dark');

        user.profile.settings.theme = 'light';
        expect(themeDisplay.value).toBe('Theme: light');
    });

    // 3. Tests des watchers
    it('devrait déclencher les watchers correctement', async () => {
        const counter = ref(0);
        let watchCount = 0;

        watch(counter, () => {
            watchCount++;
        });

        counter.value++;
        await nextTick();
        expect(watchCount).toBe(1);

        counter.value += 2;
        await nextTick();
        expect(watchCount).toBe(2);
    });
});
```

### Tests d'Intégration avec la Réactivité

```typescript
import { mount } from '@vue/test-utils';
import { defineComponent, ref, computed } from 'vue';

// Composant de test
const Counter = defineComponent({
    setup() {
        const count = ref(0);
        const double = computed(() => count.value * 2);

        return { count, double };
    },
    template: `
        <div>
            <button @click="count++">Increment</button>
            <p class="count">{{ count }}</p>
            <p class="double">{{ double }}</p>
        </div>
    `,
});

describe('Counter Component', () => {
    it('devrait mettre à jour les valeurs réactives lors des interactions', async () => {
        const wrapper = mount(Counter);

        // Test de l'état initial
        expect(wrapper.find('.count').text()).toBe('0');
        expect(wrapper.find('.double').text()).toBe('0');

        // Test après interaction
        await wrapper.find('button').trigger('click');

        expect(wrapper.find('.count').text()).toBe('1');
        expect(wrapper.find('.double').text()).toBe('2');
    });
});
```

### Tests de Performance

```typescript
import { performance } from 'perf_hooks';

// Utilitaire de test de performance
class PerformanceTest {
    private measurements: Map<string, number[]> = new Map();

    async measure(name: string, fn: () => Promise<void> | void, iterations = 100) {
        if (!this.measurements.has(name)) {
            this.measurements.set(name, []);
        }

        const times = this.measurements.get(name)!;

        for (let i = 0; i < iterations; i++) {
            const start = performance.now();
            await fn();
            const end = performance.now();
            times.push(end - start);
        }
    }

    getResults(name: string) {
        const times = this.measurements.get(name)!;
        return {
            avg: times.reduce((a, b) => a + b, 0) / times.length,
            min: Math.min(...times),
            max: Math.max(...times),
            p95: this.percentile(times, 95),
        };
    }

    private percentile(arr: number[], p: number) {
        const sorted = [...arr].sort((a, b) => a - b);
        const pos = ((sorted.length - 1) * p) / 100;
        const base = Math.floor(pos);
        const rest = pos - base;

        if (sorted[base + 1] !== undefined) {
            return sorted[base] + rest * (sorted[base + 1] - sorted[base]);
        }
        return sorted[base];
    }
}

// Tests de performance
describe('Performance Tests', () => {
    const perfTest = new PerformanceTest();

    it('devrait mesurer les performances des refs vs reactive', async () => {
        // Test avec ref
        await perfTest.measure('ref', () => {
            const count = ref(0);
            for (let i = 0; i < 1000; i++) {
                count.value++;
            }
        });

        // Test avec reactive
        await perfTest.measure('reactive', () => {
            const state = reactive({ count: 0 });
            for (let i = 0; i < 1000; i++) {
                state.count++;
            }
        });

        const refResults = perfTest.getResults('ref');
        const reactiveResults = perfTest.getResults('reactive');

        console.table({
            ref: refResults,
            reactive: reactiveResults,
        });
    });
});
```

### Optimisations Avancées

#### 1. Mise en Cache des Calculs Coûteux

```typescript
// Gestionnaire de cache avec invalidation intelligente
class ComputedCache<T> {
    private cache = new Map<
        string,
        {
            value: T;
            dependencies: Set<any>;
            timestamp: number;
        }
    >();

    private maxAge = 5000; // 5 secondes

    compute(key: string, fn: () => T, dependencies: any[] = []): T {
        const cached = this.cache.get(key);
        const now = Date.now();

        // Vérifier si le cache est valide
        if (cached) {
            const isExpired = now - cached.timestamp > this.maxAge;
            const isDependencyChanged = this.hasDependenciesChanged(cached.dependencies, dependencies);

            if (!isExpired && !isDependencyChanged) {
                return cached.value;
            }
        }

        // Calculer et mettre en cache
        const value = fn();
        this.cache.set(key, {
            value,
            dependencies: new Set(dependencies),
            timestamp: now,
        });

        return value;
    }

    private hasDependenciesChanged(cached: Set<any>, current: any[]): boolean {
        return current.some((dep) => !cached.has(dep));
    }

    clear() {
        this.cache.clear();
    }
}

// Exemple d'utilisation
const computedCache = new ComputedCache<number>();

const expensiveCalculation = computed(() => {
    return computedCache.compute(
        'expensiveSum',
        () => {
            // Calcul coûteux
            return Array(1000000)
                .fill(1)
                .reduce((a, b) => a + b, 0);
        },
        [
            /* dépendances */
        ],
    );
});
```

#### 2. Optimisation des Rendus avec requestAnimationFrame

```typescript
// Gestionnaire de mises à jour groupées
class BatchUpdateManager {
    private updates = new Map<string, () => void>();
    private isScheduled = false;

    schedule(key: string, update: () => void) {
        this.updates.set(key, update);

        if (!this.isScheduled) {
            this.isScheduled = true;
            requestAnimationFrame(() => this.flush());
        }
    }

    private flush() {
        this.updates.forEach((update) => update());
        this.updates.clear();
        this.isScheduled = false;
    }
}

// Exemple d'utilisation
const batchManager = new BatchUpdateManager();

const smoothCounter = ref(0);
const smoothUpdate = (newValue: number) => {
    batchManager.schedule('counter', () => {
        smoothCounter.value = newValue;
    });
};
```

#### 3. Détection des Fuites de Mémoire

```typescript
// Moniteur de fuites mémoire
class MemoryLeakMonitor {
    private readonly weakRefs = new Map<string, WeakRef<object>>();
    private readonly registry = new FinalizationRegistry(this.cleanup.bind(this));

    track(key: string, object: object) {
        const ref = new WeakRef(object);
        this.weakRefs.set(key, ref);
        this.registry.register(object, key);
    }

    private cleanup(key: string) {
        console.warn(`Objet ${key} collecté par le GC - potentielle fuite mémoire`);
        this.weakRefs.delete(key);
    }

    check(key: string): boolean {
        const ref = this.weakRefs.get(key);
        return ref?.deref() !== undefined;
    }
}

// Exemple d'utilisation
const memoryMonitor = new MemoryLeakMonitor();

const component = {
    setup() {
        const heavyData = reactive({
            // Données volumineuses
        });

        memoryMonitor.track('heavyData', heavyData);

        onUnmounted(() => {
            // Vérifier si l'objet est toujours en mémoire
            if (memoryMonitor.check('heavyData')) {
                console.warn('Potentielle fuite mémoire détectée');
            }
        });
    },
};
```

Ces outils et techniques d'optimisation permettent de :

1. Détecter et prévenir les fuites mémoire
2. Optimiser les calculs coûteux avec mise en cache intelligente
3. Améliorer les performances de rendu avec le batching
4. Tester rigoureusement la réactivité et les performances

## 7. Intégration et Écosystème 🔌

### State Machines avec XState

```typescript
import { createMachine, interpret } from 'xstate';
import { reactive } from 'vue';

// Définition de la machine d'état
const toggleMachine = createMachine({
    id: 'toggle',
    initial: 'inactive',
    states: {
        inactive: { on: { TOGGLE: 'active' } },
        active: { on: { TOGGLE: 'inactive' } },
    },
});

// Intégration avec Vue.js 3
const state = reactive({
    current: toggleMachine.initial,
});

const service = interpret(toggleMachine)
    .onTransition((state) => {
        state.current = state.value;
    })
    .start();
```

### Données Immuables avec Immer

```typescript
import { produce } from 'immer';
import { ref, computed } from 'vue';

const state = ref({
    todos: [{ id: 1, text: 'Apprendre Vue', done: false }],
});

// Modification immuable avec Immer
function addTodo(text: string) {
    state.value = produce(state.value, (draft) => {
        draft.todos.push({
            id: draft.todos.length + 1,
            text,
            done: false,
        });
    });
}
```

## 8. Cas d'Utilisation en Entreprise 🏢

### Cas 1 : Dashboard Analytics en Temps Réel

```typescript
interface AnalyticsData {
    visitors: number;
    pageViews: number;
    conversions: number;
}

const analytics = reactive<AnalyticsData>({
    visitors: 0,
    pageViews: 0,
    conversions: 0,
});

// Mise à jour en temps réel
const websocket = new WebSocket('wss://analytics-api.example.com');
websocket.onmessage = (event) => {
    const data = JSON.parse(event.data);
    Object.assign(analytics, data);
};

// Métriques calculées
const conversionRate = computed(() => ((analytics.conversions / analytics.visitors) * 100).toFixed(2) + '%');
```

### Cas 2 : Système de Trading en Direct

```typescript
interface TradeData {
    symbol: string;
    price: number;
    volume: number;
    timestamp: number;
}

const marketData = reactive(new Map<string, TradeData>());

// Surveillance des variations de prix
watchEffect(() => {
    for (const [symbol, data] of marketData) {
        if (data.price > previousPrices.get(symbol)) {
            notifyPriceIncrease(symbol, data);
        }
    }
});
```

### Cas 3 : Système de Réservation Hôtelière

```typescript
interface Room {
    id: number;
    type: string;
    available: boolean;
    price: number;
}

const hotelRooms = reactive<Room[]>([]);
const selectedDates = reactive({
    checkIn: null as Date | null,
    checkOut: null as Date | null,
});

// Filtrage réactif des chambres disponibles
const availableRooms = computed(() =>
    hotelRooms.filter((room) => room.available && isRoomAvailableForDates(room, selectedDates)),
);
```

## 9. Alternatives et Comparaisons 🤔

En dehors de la réactivité de Vue.js 3, le monde du développement web propose de nombreuses approches pour gérer le flux de données et l'actualisation de l'interface. Voici une analyse comparative des principales solutions de réactivité en JavaScript :

### Angular Signals

Angular, depuis sa version 16, introduit les _Signals_. Ce système offre une approche fine et optimisée de la réactivité, permettant aux développeurs de définir des signaux qui se mettent à jour automatiquement lorsque leur valeur change. Les Signals simplifient la gestion locale de l'état et offrent une syntaxe concise, en alternative aux observables de RxJS.

### React et ses Approches

-   **Hooks (useState, useEffect)** : React mise sur une approche déclarative via la mise à jour du Virtual DOM. Les Hooks permettent la gestion de l'état et des effets secondaires, bien que le déclenchement des re-rendus se fasse de manière globale.
-   **Redux / Recoil / MobX** : Pour une granularité plus fine, ces bibliothèques introduisent des paradigmes réactifs où les changements d'état se propagent automatiquement aux composants concernés, de façon similaire à la réactivité fine de Vue ou SolidJS.

### Svelte

Svelte adopte une approche révolutionnaire en déplaçant la majeure partie de la réactivité dans la phase de compilation. Les déclarations réactives, indiquées par `$:`, permettent une mise à jour ciblée du DOM sans frais d'exécution liés à un runtime.

### SolidJS

SolidJS se base sur une réactivité fine (fine-grained reactivity) en utilisant des signaux et des computations. Contrairement aux frameworks basés sur le Virtual DOM, SolidJS met à jour uniquement les parties du DOM qui dépendent directement de l'état modifié, offrant ainsi des performances exceptionnelles.

### MobX

MobX repose sur des objets observables et des computed values qui se mettent à jour automatiquement. Sa philosophie de transparence réactive permet de synchroniser l'état et l'UI de manière fluide, sans la complexité de la gestion manuelle des mises à jour.

### KnockoutJS

Pionnier dans le domaine, KnockoutJS utilise des observables pour relier des données aux interfaces utilisateur via des bindings. Bien que moins courant aujourd'hui, il a fortement influencé les approches réactives modernes.

### RxJS

RxJS est une bibliothèque puissante pour la programmation réactive basée sur les observables. Elle excelle dans la gestion des flux de données asynchrones et des événements complexes, et est largement utilisée dans l'écosystème Angular.

### Effector et Autres Alternatives

Effector propose une approche fonctionnelle pour gérer l'état de manière réactive, offrant des performances élevées et une composition modulaire de la logique applicative. D'autres bibliothèques, comme S.js, offrent également des solutions intéressantes pour implémenter la réactivité.

| Approche/Librairie      | Type                     | Points Forts                                                                       | Cas d'Usage                                                                             |
| ----------------------- | ------------------------ | ---------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------- |
| **Vue.js 3 Reactivity** | Proxy-based reactivity   | Détection fine des changements, excellente intégration TypeScript, code déclaratif | Applications web dynamiques nécessitant une mise à jour granulaire                      |
| **Angular Signals**     | Signal-based reactivity  | Intégration native avec Angular, performances optimisées, syntaxe concise          | Applications Angular modernes avec besoin de réactivité fine                            |
| **React (Hooks)**       | Virtual DOM re-rendering | Écosystème riche, simplicité d'utilisation pour les débutants                      | Applications utilisant useState/useEffect, Redux ou autres solutions                    |
| **Svelte**              | Compilation réactive     | Aucun coût runtime, mises à jour ciblées, code minimal                             | Projets nécessitant des performances élevées et une empreinte légère                    |
| **SolidJS**             | Fine-grained reactivity  | Mises à jour extrêmement ciblées, performances optimales                           | Applications où la précision des mises à jour est primordiale                           |
| **MobX**                | Observable-based         | Transparence réactive, simplicité d'intégration, encourage le code déclaratif      | Projets de grande envergure nécessitant une gestion fine des états                      |
| **KnockoutJS**          | Observable-based         | Pionnier de la réactivité, simplicité de binding                                   | Applications legacy ou projets simples nécessitant un binding réactif                   |
| **RxJS**                | Observable-based         | Puissant pour les flux de données asynchrones, riche ensemble d'opérateurs         | Gestion d'événements asynchrones complexes et composables                               |
| **Effector**            | Fonctionnelle réactive   | Composition modulaire, facile à tester, performances élevées                       | Alternatives modernes pour la gestion d'état réactive dans des environnements complexes |

Chacune de ces approches présente des avantages spécifiques et des compromis en termes de performance, de simplicité et de courbe d'apprentissage. Le choix de l'outil ou de la bibliothèque dépendra des besoins particuliers de votre projet ainsi que de l'écosystème dans lequel vous évoluez.

## 10. Conclusion et Ressources 📚

### Synthèse

La réactivité dans Vue.js 3 représente une évolution majeure dans la manière dont nous construisons des applications web modernes. Grâce à son système basé sur les Proxies JavaScript, elle offre :

-   Une meilleure performance
-   Une plus grande flexibilité
-   Une intégration naturelle avec TypeScript
-   Une maintenance simplifiée

### Bonnes Pratiques

1. Utilisez la réactivité de manière ciblée et réfléchie
2. Préférez les refs pour les valeurs primitives
3. Optimisez les performances avec computed
4. Testez et déboguer efficacement
5. Maintenez une architecture propre

### Ressources Complémentaires

-   [Documentation officielle Vue.js 3](https://vuejs.org/guide/extras/reactivity-in-depth.html)
-   [Guide Complet de la Réactivité Vue 3](https://medium.com/@kaperskyguru/ultimate-guide-to-vue-3-reactivity-95a68e8704fd)
-   [Comparaison des Systèmes Réactifs](https://www.vuemastery.com/blog/Reactivity-Vue2-vs-Vue3/)
-   [Vue.js 3 Reactivity System Deep Dive](https://www.sitepoint.com/vue-3-reactivity-system/)

### Profiling avec Vue DevTools 🔍

#### 1. Configuration du Profiling

```typescript
// Configuration du mode développement avec profiling
if (process.env.NODE_ENV === 'development') {
    app.config.performance = true;

    // Activation des marqueurs de performance
    app.config.devtools = {
        performance: {
            // Activer le suivi des mises à jour de composants
            componentTracking: true,
            // Activer le suivi des rendus
            renderTracking: true,
            // Activer le suivi des événements
            eventTracking: true,
        },
    };
}
```

#### 2. Marqueurs de Performance Personnalisés

```typescript
import { defineComponent, onMounted, onUpdated } from 'vue';

export default defineComponent({
    setup() {
        // Mesure du temps de montage
        onMounted(() => {
            performance.mark('component-mounted-start');

            // Simulation d'une opération coûteuse
            expensiveOperation();

            performance.mark('component-mounted-end');
            performance.measure('Temps de montage du composant', 'component-mounted-start', 'component-mounted-end');
        });

        // Mesure des mises à jour
        onUpdated(() => {
            performance.mark('component-updated-start');

            // Logique de mise à jour

            performance.mark('component-updated-end');
            performance.measure(
                'Temps de mise à jour du composant',
                'component-updated-start',
                'component-updated-end',
            );
        });
    },
});
```

#### 3. Analyse des Performances avec Vue DevTools

```typescript
// Composant avec instrumentation pour DevTools
const InstrumentedComponent = defineComponent({
    name: 'InstrumentedComponent',
    props: {
        data: {
            type: Object,
            required: true,
        },
    },
    setup(props) {
        // Marqueur de début pour le traitement des données
        performance.mark('data-processing-start');

        const processedData = computed(() => {
            // Simulation de traitement coûteux
            const result = heavyComputation(props.data);

            // Marqueur de fin pour le traitement
            performance.mark('data-processing-end');
            performance.measure('Traitement des données', 'data-processing-start', 'data-processing-end');

            return result;
        });

        return { processedData };
    },
});

// Utilitaire de profiling pour les composants
const withProfiling = (component: any) => {
    return defineComponent({
        name: `Profiled${component.name || 'Component'}`,
        setup(props) {
            let renderCount = 0;
            const renderTimes: number[] = [];

            onBeforeUpdate(() => {
                performance.mark(`render-${renderCount}-start`);
            });

            onUpdated(() => {
                performance.mark(`render-${renderCount}-end`);
                performance.measure(
                    `Rendu #${renderCount}`,
                    `render-${renderCount}-start`,
                    `render-${renderCount}-end`,
                );

                const measure = performance.getEntriesByName(`Rendu #${renderCount}`)[0];
                renderTimes.push(measure.duration);
                renderCount++;

                // Afficher les statistiques dans DevTools
                console.log(`Statistiques de rendu pour ${component.name}:`, {
                    renderCount,
                    averageTime: renderTimes.reduce((a, b) => a + b, 0) / renderTimes.length,
                    maxTime: Math.max(...renderTimes),
                    minTime: Math.min(...renderTimes),
                });
            });

            return { ...component.setup?.(props) };
        },
    });
};
```

#### 4. Analyse des Goulots d'Étranglement

```typescript
// Décorateur pour analyser les performances des méthodes
function profileMethod(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const originalMethod = descriptor.value;

    descriptor.value = function (...args: any[]) {
        const methodName = `${target.constructor.name}.${propertyKey}`;
        performance.mark(`${methodName}-start`);

        const result = originalMethod.apply(this, args);

        performance.mark(`${methodName}-end`);
        performance.measure(`Exécution de ${methodName}`, `${methodName}-start`, `${methodName}-end`);

        // Enregistrer dans Vue DevTools Timeline
        if (window.__VUE_DEVTOOLS_GLOBAL_HOOK__) {
            window.__VUE_DEVTOOLS_GLOBAL_HOOK__.emit('performance:measure', {
                name: methodName,
                type: 'method',
                time: performance.getEntriesByName(`Exécution de ${methodName}`)[0].duration,
            });
        }

        return result;
    };

    return descriptor;
}

// Exemple d'utilisation
class DataProcessor {
    @profileMethod
    processData(data: any[]) {
        // Traitement coûteux
        return data.map((item) => heavyComputation(item));
    }
}
```

#### 5. Visualisation des Mises à Jour Réactives

```typescript
// Composant avec suivi détaillé des mises à jour réactives
const TrackedComponent = defineComponent({
    setup() {
        const state = reactive({
            count: 0,
            data: [] as number[],
        });

        // Suivre les modifications de l'état
        watch(
            () => state.count,
            (newValue, oldValue) => {
                console.log('📊 Mise à jour détectée:', {
                    property: 'count',
                    from: oldValue,
                    to: newValue,
                    timestamp: Date.now(),
                });
            },
        );

        // Suivre les calculs dérivés
        const computed1 = computed(() => state.count * 2);
        const computed2 = computed(() => state.data.reduce((a, b) => a + b, 0));

        // Enregistrer les dépendances pour DevTools
        if (process.env.NODE_ENV === 'development') {
            (window as any).__VUE_DEVTOOLS_DEPENDENCIES__ = {
                computed1: ['state.count'],
                computed2: ['state.data'],
            };
        }

        return {
            state,
            computed1,
            computed2,
        };
    },
});
```

Ces outils de profiling permettent de :

1. Identifier les goulots d'étranglement dans les performances
2. Suivre les mises à jour réactives en détail
3. Mesurer précisément les temps de rendu
4. Optimiser les composants de manière ciblée
5. Détecter les rendus inutiles
