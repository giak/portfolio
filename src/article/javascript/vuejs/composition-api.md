---
title: Vue.js Composition API - Organisation et Avantages
description: Approfondissement de la Composition API de Vue.js 3, ses concepts clés, ses avantages par rapport à l'Options API et les meilleures pratiques pour organiser et réutiliser votre logique de composants.
icon: fa-brands fa-vuejs
lang: fr
category:
    - Vue.js
    - JavaScript
    - Development
    - Architecture
tags:
    - Vue.js
    - Composition API
    - TypeScript
    - Reactivity
    - Best Practices
    - State Management
dir:
    text: Composition API
    icon: fa-solid fa-layer-group
    order: 2
    collapsible: false
    link: true
---

![composition api](./composition-api.jpg)

# Vue.js Composition API : Organisation et Avantages

La Composition API, introduite avec Vue.js 3, révolutionne la manière d'organiser et de réutiliser la logique dans vos composants. En regroupant le code par fonction plutôt que par options (data, methods, computed, etc.), elle offre une meilleure lisibilité, une facilité de maintenance et une excellente intégration avec TypeScript.

## 1. Introduction 🎯

Traditionnellement, Vue utilisait l'Options API, où la logique d'un composant était dispersée entre différentes sections. Cela pouvait devenir un frein en cas d'applications complexes ou lorsque plusieurs préoccupations cohabitent dans un même composant.

Avec la Composition API, vous pouvez regrouper la logique associée dans des fonctions dédiées, favorisant ainsi la réutilisation et la modularité. Cette approche s'inspire de concepts semblables à React Hooks, mais avec des différences notables qui simplifient le développement.

## 2. Pourquoi la Composition API ?

### Problématiques de l'Options API

-   **Fragmentation de la logique** : Le code est dispersé entre `data`, `computed`, `methods`, etc., ce qui rend difficile la maintenance lorsqu'un composant grandit.
-   **Réutilisabilité limitée** : Extraire et réutiliser de la logique répartie sur plusieurs options est fastidieux.
-   **Limitations en TypeScript** : Bien que Vue 2 supporte TypeScript, l'Options API ne permet pas une inférence de types aussi robuste que souhaitée.

### Les Apports de la Composition API

-   **Regroupement de la logique** : Organisez le code par fonctionnalité (ex. gestion d'un formulaire, logique de validation), ce qui facilite la compréhension et la réutilisation.
-   **Meilleure intégration TypeScript** : Inférence de types améliorée et code plus sûr.
-   **Flexibilité** : Possibilité d'utiliser des fonctions conditionnelles et de composer facilement différents aspects du composant.
-   **Optimisation des performances** : La composition permet une meilleure gestion des dépendances réactives.

## 3. Concepts Clés de la Composition API

### 3.1. Création d'État Réactif

#### `ref()` et `reactive()`

Utilisez `ref()` pour créer des valeurs primitives réactives et `reactive()` pour des objets complexes.

```typescript
import { ref, reactive } from 'vue';

// Utilisation de ref pour une valeur primitive
const count = ref(0);

// Utilisation de reactive pour un objet complexe
const state = reactive({
    user: {
        name: 'Alice',
        age: 30,
    },
    items: [],
});
```

### 3.2. Propriétés Calculées avec `computed()`

Les propriétés calculées se mettent automatiquement à jour lorsque leurs dépendances changent.

```typescript
import { computed } from 'vue';

const doubleCount = computed(() => count.value * 2);
```

### 3.3. Observateurs avec `watch()` et `watchEffect()`

Pour réagir aux changements, utilisez `watch()` pour surveiller des sources spécifiques et `watchEffect()` pour une surveillance automatique des dépendances réactives.

```typescript
import { watch, watchEffect } from 'vue';

// Exemple avec watch
watch(count, (newVal, oldVal) => {
    console.log(`Count changed from ${oldVal} to ${newVal}`);
});

// Exemple avec watchEffect
watchEffect(() => {
    console.log(`Le double est ${doubleCount.value}`);
});
```

### 3.4. Lifecycle Hooks et Dependency Injection

La Composition API offre également des hooks comme `onMounted()`, `onUnmounted()`, et des fonctions pour le partage de données entre composants via `provide()` et `inject()`.

```typescript
import { onMounted, provide, inject } from 'vue';

onMounted(() => {
    console.log('Le composant est monté');
});

// Exemple de provide/inject
provide('theme', 'dark');
const theme = inject('theme');
```

## 4. Avantages et Comparaison avec l'Options API

| Aspect                      | Options API                          | Composition API                                        |
| --------------------------- | ------------------------------------ | ------------------------------------------------------ |
| **Organisation du code**    | Séparée par sections (data, methods) | Logique regroupée par fonctionnalité                   |
| **Réutilisabilité**         | Extraction complexe                  | Modules et hooks réutilisables                         |
| **Écriture conditionnelle** | Limité                               | Possibilité d'interpoler la logique conditionnellement |
| **TypeScript**              | Support moins explicite              | Excellente inférence des types                         |

La Composition API permet donc une meilleure organisation du code, surtout dans les projets volumineux, en facilitant la réutilisation et l'optimisation de la logique.

## 5. Exemples Pratiques et Patterns

### 5.1. Création d'un Hook Utilisable

Vous pouvez extraire des logiques récurrentes dans des fonctions réutilisables, appelées "composables".

```typescript
function useUser() {
    const firstName = ref('');
    const lastName = ref('');
    const fullName = computed(() => `${firstName.value} ${lastName.value}`);

    return { firstName, lastName, fullName };
}

// Dans un composant
const { firstName, lastName, fullName } = useUser();
```

### 5.2. Gestion des Effets de Bords

Utilisez `watchEffect()` pour gérer les effets de bord automatiquement, comme la mise à jour de données externes ou des effets DOM.

```typescript
watchEffect(() => {
    document.title = `Utilisateur: ${fullName.value}`;
});
```

## 6. Bonnes Pratiques et Optimisations

### 6.1 Organisation du Code

-   **Composables Atomiques** : Créez des composables petits et focalisés qui suivent le principe de responsabilité unique.
-   **Nommage Explicite** : Préfixez vos composables avec `use` (ex: `useUser`, `useForm`).
-   **Séparation des Préoccupations** : Isolez la logique métier, la gestion d'état et les effets secondaires.

```typescript
// ❌ Mauvaise pratique - Trop de responsabilités
function useUserManagement() {
    const user = ref(null);
    const loading = ref(false);
    const error = ref(null);

    async function fetchUser() {
        /* ... */
    }
    function updateUser() {
        /* ... */
    }
    function validateUser() {
        /* ... */
    }

    return { user, loading, error, fetchUser, updateUser, validateUser };
}

// ✅ Bonne pratique - Responsabilités séparées
function useUser() {
    const user = ref(null);
    return { user };
}

function useUserAPI() {
    const { user } = useUser();
    const loading = ref(false);
    const error = ref(null);

    async function fetchUser() {
        /* ... */
    }
    return { loading, error, fetchUser };
}

function useUserValidation() {
    const { user } = useUser();
    function validate() {
        /* ... */
    }
    return { validate };
}
```

### 6.2 Gestion de la Réactivité

-   **Utilisation Appropriée de ref vs reactive** :
    -   `ref` pour les valeurs primitives et les tableaux
    -   `reactive` pour les objets complexes avec des propriétés imbriquées
-   **Évitez la Destructuration** des objets réactifs pour maintenir la réactivité
-   **Utilisez toRefs** pour préserver la réactivité lors de la destructuration

```typescript
// ❌ Perte de réactivité
const state = reactive({ count: 0 });
const { count } = state; // La réactivité est perdue

// ✅ Préservation de la réactivité
const state = reactive({ count: 0 });
const { count } = toRefs(state);
```

### 6.3 Performance et Optimisation

-   **Memoization avec computed** : Utilisez `computed` pour les calculs coûteux
-   **Lazy Loading des Composants** : Importez dynamiquement les composants lourds
-   **Debounce/Throttle** pour les opérations fréquentes

```typescript
// Memoization avec computed
const expensiveValue = computed(() => {
    return someComplexCalculation(props.value);
});

// Lazy Loading
const HeavyComponent = defineAsyncComponent(() => import('./HeavyComponent.vue'));

// Debounce
const debouncedSearch = useDebounceFn((query) => {
    // Logique de recherche
}, 300);
```

### 6.4 TypeScript et Type Safety

-   **Définissez des Types Explicites** pour les props et les émissions
-   **Utilisez defineProps et defineEmits** avec des types génériques
-   **Créez des Interfaces** pour vos états complexes

```typescript
interface User {
    id: number;
    name: string;
    email: string;
}

const props = defineProps<{
    user: User;
    isActive: boolean;
}>();

const emit = defineEmits<{
    (e: 'update', value: User): void;
    (e: 'delete', id: number): void;
}>();
```

### 6.5 Tests et Maintenabilité

-   **Tests Unitaires** pour les composables
-   **Documentation** claire des interfaces et comportements attendus
-   **Error Boundaries** pour la gestion des erreurs

```typescript
// Exemple de test pour un composable
import { useCounter } from './useCounter';

describe('useCounter', () => {
    it('should increment counter', () => {
        const { count, increment } = useCounter();
        expect(count.value).toBe(0);
        increment();
        expect(count.value).toBe(1);
    });
});
```

## 7. Nouveautés Vue 3.3+ et Fonctionnalités Avancées

### 7.1 Macros de Composition Améliorées

Vue 3.3+ introduit des améliorations significatives pour le typage et la syntaxe des composants :

```typescript
// Typage générique des props avec valeurs par défaut
const props = defineProps<{
    title?: string;
    count: number;
}>({
    count: 0, // Valeur par défaut
});

// Typage des refs avec inférence automatique
const title = defineModel<string>();

// Émissions typées avec autocomplétion
const emit = defineEmits<{
    change: [value: string];
    update: [id: number, value: string];
}>();
```

### 7.2 Hooks de Cycle de Vie Avancés

```typescript
// Hook onMounted avec cleanup automatique
onMounted(async () => {
    const cleanup = await setupSomeResource();

    // Nettoyage automatique
    onUnmounted(cleanup);
});

// Gestion des erreurs avec onErrorCaptured
onErrorCaptured((err, instance, info) => {
    console.error(`Erreur capturée dans ${instance.$options.name}:`, err);
    // Empêcher la propagation de l'erreur
    return false;
});
```

### 7.3 Fonctionnalités Expérimentales

-   **defineModel** : Nouvelle API pour la gestion bidirectionnelle des props
-   **Composition API Reactivity Transform** : Syntaxe plus concise pour la réactivité
-   **Suspense et async setup** : Gestion améliorée du chargement asynchrone

```typescript
// Exemple de defineModel avec validation
const model = defineModel({
    default: '',
    required: true,
    validator: (value: string) => value.length > 3,
});

// Reactivity Transform (expérimental)
let count = $ref(0);
const double = $computed(() => count * 2);
```

## 8. Conclusion et Ressources 📚

La Composition API continue d'évoluer avec Vue.js 3, offrant des outils toujours plus puissants pour structurer et maintenir des applications complexes. En suivant ces bonnes pratiques et en utilisant les dernières fonctionnalités, vous pouvez créer des applications plus robustes et maintenables.

### Ressources Complémentaires

-   [Documentation officielle Vue.js 3](https://vuejs.org/guide/introduction.html)
-   [Guide de Migration Vue 2 vers Vue 3](https://v3-migration.vuejs.org/)
-   [Vue 3 Composition API RFC](https://github.com/vuejs/rfcs/blob/master/active-rfcs/0013-composition-api.md)
-   [Vue 3.3 Release Notes](https://blog.vuejs.org/posts/vue-3-3)
-   [VueUse - Collection de composables](https://vueuse.org/)
-   [Vue Mastery - Tutoriels avancés](https://www.vuemastery.com/)
-   [Vue.js Nation - Conférences et présentations](https://vuejsnation.com/)

Ces ressources vous permettront d'approfondir vos connaissances et de rester à jour avec les dernières évolutions de Vue.js et de la Composition API.
