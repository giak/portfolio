---
title: 🛡️ La Gestion d'Erreurs en TypeScript - Guide Complet des Meilleures Pratiques
index: true
date: 2024-03-15
icon: code
sticky: 1
star: 1
category:
    - TypeScript
    - Design Pattern
    - Architecture
tags:
    - TypeScript
    - Error Handling
    - Design Pattern
    - Clean Code
    - Best Practices
    - Exception Handling
    - Domain-Driven Design
    - SOLID Principles
description: Un guide exhaustif sur les meilleures pratiques de gestion d'erreurs en TypeScript, couvrant différentes approches architecturales, patterns et frameworks modernes.
image: ./steampunk.jpg
showPageImage: true
---

![](./steampunk.jpg)

# 🛡️ La Gestion d'Erreurs en TypeScript : Guide Complet des Meilleures Pratiques

## 🎯 Introduction : L'importance de la gestion d'erreurs

Dans le développement web moderne, la gestion d'erreurs est bien plus qu'une simple précaution - c'est un élément fondamental pour créer des applications robustes et maintenables.
En tant que Senior Web Developer spécialisé en TypeScript, j'ai constaté que de nombreux projets souffrent d'une gestion d'erreurs inadéquate, conduisant à des bugs silencieux et des comportements imprévisibles.
Mon expérience s'est faite dans le douleur. J'ai compris que la gestion d'erreur est un calque à appliquer sur le code. C'est long, c'est difficile, c'est fastidieux. Mais c'est nécessaire.
Nous pouvons nous plaindre du TDD, mais sans gestion d'erreur, le TDD est un exercice inutile.

### 💡 Pourquoi une bonne gestion d'erreurs est cruciale ?

-   **Fiabilité** : Des applications plus stables et prévisibles
-   **Maintenance** : Code plus facile à déboguer et à faire évoluer
-   **Expérience utilisateur** : Feedback clair et approprié
-   **Performance** : Identification rapide des goulots d'étranglement
-   **Sécurité** : Meilleure protection contre les comportements malveillants

## 📊 Les différentes approches de gestion d'erreurs

Avant d'explorer les différents moyens de gérer les erreurs, examinons en détail chaque approche disponible en TypeScript.

### Tableau comparatif des approches de gestion d'erreurs

| Approche                  | Description                       | Avantages                                                                       | Inconvénients                                                                                              | Support TypeScript                       | Cas d'utilisation                                                     |
| ------------------------- | --------------------------------- | ------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------- | ---------------------------------------- | --------------------------------------------------------------------- |
| Try/Catch traditionnel    | Blocs try/catch natifs            | • Simple et familier<br>• Natif au langage<br>• Capture automatique des erreurs | • Peut masquer des erreurs<br>• Pas de vérification de type sur les erreurs<br>• Rompt le flux d'exécution | Basique - pas de typage fort des erreurs | • Code legacy<br>• Scripts simples<br>• Prototypes rapides            |
| Result/Option Pattern     | Types encapsulant succès/échec    | • Type-safe<br>• Explicite<br>• Prévisible<br>• Force le traitement des erreurs | • Plus verbeux<br>• Courbe d'apprentissage<br>• Nécessite plus de code initial                             | Excellent - typage complet et inférence  | • Applications critiques<br>• Code complexe<br>• APIs publiques       |
| Either Pattern            | Type union Left/Right             | • Type-safe<br>• Explicite<br>• Composable<br>• Fonctionnel                     | • Verbeux<br>• Concepts FP nécessaires<br>• Courbe d'apprentissage                                         | Excellent - typage discriminé            | • Code fonctionnel<br>• Validation complexe<br>• Chaînes d'opérations |
| Nullable Values           | Utilisation de null/undefined     | • Simple<br>• Direct<br>• Bien supporté                                         | • Pas de contexte d'erreur<br>• Risque de null pointer<br>• Difficile de différencier les types d'erreurs  | Bon - avec strictNullChecks              | • Cas simples<br>• Valeurs optionnelles<br>• Petits projets           |
| Callbacks (style Node.js) | Premier paramètre pour l'erreur   | • Standard Node.js<br>• Explicite<br>• Bon pour l'async                         | • Callback hell<br>• Verbeux<br>• Moins pertinent avec Promises                                            | Moyen - typage des callbacks possible    | • Code Node.js legacy<br>• APIs callback-based                        |
| Décorateurs TypeScript    | Gestion déclarative des erreurs   | • Séparation des préoccupations<br>• Réutilisable<br>• Élégant                  | • Configuration nécessaire<br>• Peut masquer la complexité<br>• Support expérimental                       | Très bon - avec metadata reflection      | • Applications Angular<br>• Code orienté aspect                       |
| DI Error Handler          | Gestionnaire d'erreurs centralisé | • Centralisé<br>• Testable<br>• Flexible<br>• Séparation des responsabilités    | • Setup complexe<br>• Sur-architecturé pour petits projets<br>• Dépendance au DI                           | Excellent - avec decorators et DI        | • Applications enterprise<br>• Grands projets<br>• Code modulaire     |
| Custom Error Classes      | Classes d'erreur personnalisées   | • Hiérarchie claire<br>• Contexte riche<br>• Extensible                         | • Peut devenir complexe<br>• Nécessite maintenance<br>• Possible sur-engineering                           | Très bon - héritage et typage            | • Domaines métier<br>• APIs complexes                                 |
| Error Cause Chain         | Chaînage des causes d'erreurs     | • Traçabilité complète<br>• Context riche<br>• Standard ECMAScript              | • Peut être verbeux<br>• Nécessite ES2022+<br>• Possible sur-utilisation                                   | Bon - avec types natifs                  | • Debug<br>• Logging avancé                                           |
| Codes de retour           | Valeurs numériques/énums          | • Simple<br>• Performant<br>• Explicite                                         | • Pas de contexte<br>• Difficile à maintenir<br>• Propice aux erreurs                                      | Moyen - avec enums                       | • Systèmes embarqués<br>• Performance critique                        |

### **Comparaison et évaluation des approches**

| Approche                  | Lisibilité    | Sécurité du typage | Verbosité  | Performance   | Maintenabilité | Testabilité   | Complexité d'implémentation | Cas d'usage idéal                          |
| ------------------------- | ------------- | ------------------ | ---------- | ------------- | -------------- | ------------- | --------------------------- | ------------------------------------------ |
| Try/Catch traditionnel    | 🟡 Moyenne    | 🔴 Faible          | 🟢 Faible  | 🟡 Moyenne    | 🔴 Faible      | 🔴 Difficile  | 🟢 Simple                   | Scripts simples, prototypes                |
| Result/Option Pattern     | 🟢 Bonne      | 🟢 Excellente      | 🟡 Moyenne | 🟢 Excellente | 🟢 Bonne       | 🟢 Facile     | 🟡 Moyenne                  | Applications critiques                     |
| Either Pattern            | 🟢 Bonne      | 🟢 Excellente      | 🔴 Élevée  | 🟢 Bonne      | 🟢 Excellente  | 🟢 Facile     | 🔴 Complexe                 | Code fonctionnel, validation complexe      |
| Nullable Values           | 🟢 Bonne      | 🟡 Moyenne         | 🟢 Faible  | 🟢 Excellente | 🔴 Faible      | 🟡 Moyenne    | 🟢 Simple                   | Petits projets, cas simples                |
| Callbacks (style Node.js) | 🔴 Faible     | 🟡 Moyenne         | 🔴 Élevée  | 🟡 Moyenne    | 🔴 Faible      | 🔴 Difficile  | 🟡 Moyenne                  | Code legacy Node.js                        |
| Décorateurs TypeScript    | 🟢 Excellente | 🟢 Bonne           | 🟡 Moyenne | 🟡 Moyenne    | 🟢 Bonne       | 🟢 Facile     | 🔴 Complexe                 | Applications Angular, AOP                  |
| DI Error Handler          | 🟢 Excellente | 🟢 Excellente      | 🟡 Moyenne | 🟡 Moyenne    | 🟢 Excellente  | 🟢 Excellente | 🔴 Complexe                 | Applications enterprise                    |
| Custom Error Classes      | 🟢 Bonne      | 🟢 Bonne           | 🟡 Moyenne | 🟢 Bonne      | 🟢 Bonne       | 🟢 Bonne      | 🟡 Moyenne                  | APIs complexes, domaine métier             |
| Error Cause Chain         | 🟢 Bonne      | 🟡 Moyenne         | 🟢 Faible  | 🟢 Excellente | 🟡 Moyenne     | 🟡 Moyenne    | 🟢 Simple                   | Debug, logging avancé                      |
| Codes de retour           | 🟡 Moyenne    | 🟡 Moyenne         | 🔴 Élevée  | 🟢 Excellente | 🔴 Faible      | 🟡 Moyenne    | 🟢 Simple                   | Systèmes embarqués, performances critiques |

**Critères d'évaluation:**

-   **Lisibilité**: Facilité à comprendre le code et son intention
-   **Sécurité du typage**: Niveau de support TypeScript et détection d'erreurs à la compilation
-   **Verbosité**: Quantité de code nécessaire pour implémenter la solution
-   **Performance**: Impact sur les performances de l'application
-   **Maintenabilité**: Facilité à maintenir et faire évoluer le code
-   **Testabilité**: Facilité à écrire des tests unitaires et d'intégration
-   **Complexité d'implémentation**: Effort nécessaire pour mettre en place l'approche
-   **Cas d'usage idéal**: Contexte où l'approche est la plus pertinente

## 🤔 Comment choisir son approche de gestion d'erreurs ?

Le choix d'une approche de gestion d'erreurs dépend de plusieurs facteurs clés. Voici un guide décisionnel pour vous aider à choisir la meilleure approche selon votre contexte.

### 📊 Arbre décisionnel

```mermaid
graph TD
    Start[Projet TypeScript] --> Q1{Criticité?}

    Q1 -->|Critique| C1{Type Système?}
    Q1 -->|Standard| S1{Contexte?}
    Q1 -->|MVP/POC| M1{Contraintes?}

    %% Chemin Critique
    C1 -->|Enterprise| C2{Architecture?}
    C1 -->|Temps-réel| C3{Performance?}

    C2 -->|Monolithique| C4[Either + Domain Errors]
    C2 -->|Microservices| C5[Circuit Breaker + Either]

    C3 -->|Ultra-haute| C6[Result + Error Codes]
    C3 -->|Haute| C7[Result + Custom Errors]

    %% Chemin Standard
    S1 -->|Frontend| S2{Framework?}
    S1 -->|Backend| S3{Complexité?}

    S2 -->|React/Vue| S4[Error Boundaries + Result]
    S2 -->|Angular| S5[ErrorHandler + DI]

    S3 -->|Élevée| S6[Either + Result]
    S3 -->|Moyenne| S7[Result Pattern]

    %% Chemin MVP/POC
    M1 -->|Time-to-market| M2[Result Simple]
    M1 -->|Budget limité| M3[Try/Catch + Custom]
    M1 -->|Équipe junior| M4[Result + Validators]
```

##### Tableau hiérarchisé par niveau de priorité

| Niveau de Priorité | Approche                  | Score Global | Cas d'Usage                                                   | Points Forts                                                                  | Points Faibles                                       | Coût d'Implémentation |
| ------------------ | ------------------------- | ------------ | ------------------------------------------------------------- | ----------------------------------------------------------------------------- | ---------------------------------------------------- | --------------------- |
| 🏆 Tier S          | Either + Domain Errors    | 95/100       | • Enterprise<br>• Systèmes critiques<br>• DDD                 | • Type-safety maximale<br>• Maintenance optimale<br>• Tests robustes          | • Courbe d'apprentissage<br>• Setup initial complexe | $$$                   |
| 🥇 Tier A+         | Circuit Breaker + Either  | 90/100       | • Microservices<br>• Cloud-native<br>• Haute dispo            | • Résilience<br>• Monitoring avancé<br>• Scalabilité                          | • Configuration complexe<br>• Overhead réseau        | $$$                   |
| 🥈 Tier A          | Result Pattern            | 85/100       | • Applications standard<br>• APIs publiques<br>• Services web | • Balance complexité/bénéfices<br>• Adoption facile<br>• Bonne maintenabilité | • Moins expressif qu'Either<br>• Contexte limité     | $$                    |
| 🥉 Tier B+         | Error Boundaries + Result | 80/100       | • React/Vue apps<br>• SPAs<br>• Applications web              | • Isolation des erreurs<br>• UX améliorée<br>• Setup rapide                   | • Spécifique framework<br>• Granularité limitée      | $$                    |
| 🏅 Tier B          | Custom Error Classes      | 75/100       | • Legacy<br>• Migration<br>• Petits projets                   | • Familiarité<br>• Implémentation simple<br>• Flexibilité                     | • Pas de type-safety<br>• Maintenance complexe       | $                     |
| 🎖️ Tier C+         | Try/Catch + Validators    | 70/100       | • MVPs<br>• POCs<br>• Prototypes                              | • Développement rapide<br>• Simple à comprendre<br>• Documentation riche      | • Pas évolutif<br>• Manque de structure              | $                     |

**Score Global basé sur :**

-   Performance (25%)
-   Maintenabilité (25%)
-   Type-safety (20%)
-   Facilité d'adoption (15%)
-   Support tooling (15%)

**Coût d'Implémentation :**

-   $ : Faible (1-2 jours)
-   $$ : Moyen (1-2 semaines)
-   $$$ : Élevé (2+ semaines)

**Critères de Sélection Additionnels :**

**Maturité de l'Équipe**

-   Junior → Result Pattern
-   Senior → Either Pattern
-   Mixte → Result + Custom Errors

**Contraintes Techniques**

-   Performance critique → Result + Codes
-   Mémoire limitée → Custom Error Classes
-   Haute disponibilité → Circuit Breaker

**Budget et Délais**

-   Serré → Try/Catch + Custom
-   Standard → Result Pattern
-   Confortable → Either + Domain

**Maintenance Long Terme**

-   Critique → Either Pattern
-   Standard → Result Pattern
-   Minimale → Custom Errors

##### Tableau de Décision pour la Gestion d'Erreurs

| Contexte             | Approche                  | Cas d'Usage                                                       | Avantages                                                           | Inconvénients                                                         | Prérequis                                                     |
| -------------------- | ------------------------- | ----------------------------------------------------------------- | ------------------------------------------------------------------- | --------------------------------------------------------------------- | ------------------------------------------------------------- |
| Frontend Simple      | Error Boundaries + Result | • Applications React<br>• MVPs<br>• Prototypes                    | • Isolation des erreurs<br>• Setup rapide<br>• Maintenance facile   | • Granularité limitée<br>• Pas de typage fort                         | • Connaissance basique de TS<br>• React 16+                   |
| Frontend Complexe    | Either + State Management | • Apps Enterprise<br>• SPAs complexes<br>• Applications critiques | • Type-safety complète<br>• Gestion d'état robuste<br>• Testabilité | • Courbe d'apprentissage<br>• Setup complexe<br>• Verbosité           | • TS avancé<br>• FP concepts<br>• State management            |
| Backend Monolithique | Either + Domain Errors    | • APIs complexes<br>• Business logic riche<br>• DDD               | • Typage fort<br>• Séparation des concerns<br>• Maintenabilité      | • Complexité initiale<br>• Overhead de code<br>• Formation nécessaire | • DDD<br>• TS avancé<br>• Architecture skills                 |
| Microservices        | Circuit Breaker + Result  | • Systèmes distribués<br>• High availability<br>• Cloud native    | • Résilience<br>• Monitoring facile<br>• Scalabilité                | • Configuration complexe<br>• Overhead réseau<br>• Debug complexe     | • Microservices patterns<br>• Monitoring<br>• Cloud knowledge |
| Serverless           | Result + Error Context    | • AWS Lambda<br>• Azure Functions<br>• Cloud Functions            | • Simplicité<br>• Monitoring intégré<br>• Coût optimisé             | • Contrôle limité<br>• Vendor lock-in<br>• Cold starts                | • Cloud platforms<br>• Serverless concepts                    |
| Librairie Publique   | Result Pattern            | • NPM packages<br>• SDK clients<br>• Framework plugins            | • API claire<br>• Documentation facile<br>• Adoption simple         | • Flexibilité limitée<br>• Pas de context riche                       | • API design<br>• Documentation skills                        |
| Application Legacy   | Custom Error Classes      | • Maintenance<br>• Migration<br>• Refactoring                     | • Intégration facile<br>• Impact minimal<br>• Migration progressive | • Dette technique<br>• Pas de type-safety<br>• Maintenance complexe   | • Legacy codebase<br>• Refactoring skills                     |
| High Performance     | Result + Codes            | • Trading<br>• Gaming<br>• Real-time systems                      | • Performance optimale<br>• Overhead minimal<br>• Prédictibilité    | • Expressivité limitée<br>• Maintenance difficile<br>• Debug complexe | • Performance tuning<br>• Profiling skills                    |

## 🔍 Analyse détaillée de chaque approche

### 1. Try/Catch Traditionnel

Le try/catch est l'approche la plus classique de gestion d'erreurs, héritée directement de JavaScript. C'est souvent le premier réflexe des développeurs, mais il présente des limitations importantes en TypeScript.

#### 📝 Implémentation basique

```typescript
try {
    const data = JSON.parse(invalidJson);
    processData(data);
} catch (error) {
    // TypeScript ne peut pas inférer le type de 'error'
    console.error('Une erreur est survenue:', error);
}
```

#### 🔄 Implémentation améliorée avec typage

```typescript
try {
    const data = JSON.parse(invalidJson);
    processData(data);
} catch (error) {
    if (error instanceof SyntaxError) {
        console.error('Erreur de parsing JSON:', error.message);
    } else if (error instanceof TypeError) {
        console.error('Erreur de type:', error.message);
    } else {
        console.error('Erreur inconnue:', error);
    }
}
```

#### 🎯 Cas d'utilisation idéaux

-   Scripts simples et prototypes rapides
-   Code legacy nécessitant une maintenance minimale
-   Intégration avec des bibliothèques externes utilisant des exceptions

#### ✅ Avantages détaillés

1. **Familiarité**

    - Syntaxe connue de tous les développeurs JavaScript
    - Pas de configuration spéciale requise
    - Documentation abondante

2. **Support natif**

    - Fonctionne dans tous les environnements JavaScript
    - Pas de dépendances externes
    - Performance native du moteur JavaScript

3. **Capture automatique**
    - Capture les erreurs propagées à travers la pile d'appels
    - Fonctionne avec les erreurs asynchrones (avec async/await)
    - Capture même les erreurs non explicitement gérées

#### ❌ Inconvénients détaillés

1. **Typage faible**

    - TypeScript ne peut pas inférer le type de l'erreur automatiquement
    - Nécessite des vérifications de type manuelles
    - Risque d'erreurs silencieuses

2. **Rupture du flux**

    - Interrompt l'exécution normale du code
    - Peut masquer la source réelle de l'erreur
    - Difficile à chaîner avec d'autres opérations

3. **Maintenance difficile**
    - Difficile de garantir que toutes les erreurs sont gérées
    - Peut créer des chemins d'exécution complexes
    - Tests plus difficiles à écrire

#### 🛠️ Bonnes pratiques

1. **Toujours typer les erreurs attendues**

```typescript
try {
    // code susceptible de lever une erreur
} catch (error) {
    if (!(error instanceof Error)) {
        throw error; // Relancer les erreurs non standards
    }
    // Gérer l'erreur typée
}
```

2. **Éviter les blocs try/catch trop larges**

```typescript
// ❌ À éviter
try {
    // Beaucoup de code...
} catch (error) {
    // Gestion générique
}

// ✅ Préférer
try {
    const result = riskyOperation();
} catch (error) {
    // Gestion spécifique
}
```

3. **Documenter les erreurs possibles**

```typescript
/**
 * Traite les données JSON
 * @throws {SyntaxError} Si le JSON est invalide
 * @throws {TypeError} Si la structure ne correspond pas
 */
function processJsonData(json: string): void {
    try {
        // Traitement
    } catch (error) {
        // Gestion typée des erreurs
        throw error;
    }
}
```

#### 📊 Métriques de qualité

-   **Maintenabilité**: 🟡 Moyenne
-   **Testabilité**: 🔴 Faible
-   **Réutilisabilité**: 🟡 Moyenne
-   **Évolutivité**: 🔴 Faible

### 2. Result/Option Pattern

Le Result/Option Pattern est une approche inspirée des langages fonctionnels comme Rust, offrant une gestion d'erreurs type-safe et explicite. Il force le développeur à gérer les cas de succès et d'échec de manière structurée.

#### 📝 Implémentation basique

```typescript
type ResultType<T, E> =
    | {
          success: true;
          value: T;
      }
    | {
          success: false;
          error: E;
      };

// Exemple d'utilisation simple
function divide(a: number, b: number): ResultType<number, string> {
    if (b === 0) {
        return { success: false, error: 'Division par zéro impossible' };
    }
    return { success: true, value: a / b };
}
```

#### 🔄 Implémentation avancée avec utilitaires

```typescript
class Result<T, E> {
    private constructor(private readonly value: T | null, private readonly error: E | null) {}

    static success<T, E>(value: T): Result<T, E> {
        return new Result<T, E>(value, null);
    }

    static failure<T, E>(error: E): Result<T, E> {
        return new Result<T, E>(null, error);
    }

    map<U>(fn: (value: T) => U): Result<U, E> {
        return this.value !== null ? Result.success(fn(this.value)) : Result.failure(this.error!);
    }
}

// Fonction de division avec gestion d'erreurs
function divide(a: number, b: number): Result<number, string> {
    if (b === 0) {
        return Result.failure('Division par zéro impossible');
    }
    return Result.success(a / b);
}

// Exemple d'utilisation avec chaînage
const result = divide(10, 2)
    .map((result) => result * 2)
    .map((result) => `Le résultat est: ${result}`);

// Utilisation avec pattern matching
if (result instanceof Result.success) {
    console.log('Succès:', result.value); // "Le résultat est: 10"
} else {
    console.error('Erreur:', result.error);
}

// Exemple avec division par zéro
const errorResult = divide(10, 0)
    .map((result) => result * 2)
    .map((result) => `Le résultat est: ${result}`);

// Le chaînage s'arrête au premier échec
if (errorResult instanceof Result.failure) {
    console.error('Erreur:', errorResult.error); // "Division par zéro impossible"
}
```

#### 🎯 Cas d'utilisation idéaux

-   Applications critiques nécessitant une gestion d'erreurs rigoureuse
-   APIs publiques avec contrats stricts
-   Opérations complexes avec multiples points de défaillance
-   Validation de données avec feedback détaillé

#### ✅ Avantages détaillés

1. **Sécurité du typage**

    - Vérification statique des types d'erreurs
    - Impossible d'oublier de gérer un cas d'erreur
    - Inférence de type automatique

2. **Explicite et prévisible**

    - Contrat clair sur les valeurs de retour possibles
    - Documentation implicite du comportement
    - Facilite la revue de code

3. **Composable**
    - Chaînage naturel des opérations
    - Transformation des valeurs et erreurs
    - Réutilisation des patterns de traitement

#### ❌ Inconvénients détaillés

1. **Verbosité**

    - Plus de code initial nécessaire
    - Structures de types additionnelles
    - Manipulation explicite des résultats

2. **Courbe d'apprentissage**

    - Concepts nouveaux pour certains développeurs
    - Changement de paradigme nécessaire
    - Documentation parfois limitée

3. **Overhead initial**
    - Setup des types et utilitaires
    - Configuration du projet
    - Formation de l'équipe

#### 🛠️ Bonnes pratiques

1. **Types d'erreurs spécifiques**

```typescript
type ValidationErrorType = {
    field: string;
    message: string;
};

function validateEmail(email: string): Result<string, ValidationErrorType> {
    if (!email.includes('@')) {
        return {
            success: false,
            error: {
                field: 'email',
                message: "Format d'email invalide",
            },
        };
    }
    return { success: true, value: email };
}
```

2. **Utilitaires de manipulation**

```typescript
function combine<T, E>(results: Result<T, E>[]): Result<T[], E> {
    const values: T[] = [];
    for (const result of results) {
        if (!result.success) {
            return result; // Retourne la première erreur
        }
        values.push(result.value);
    }
    return { success: true, value: values };
}
```

3. **Gestion des cas asynchrones**

```typescript
async function fetchUserData(id: string): Promise<Result<User, ApiErrorType>> {
    try {
        const response = await fetch(`/api/users/${id}`);
        if (!response.ok) {
            return {
                success: false,
                error: { code: response.status, message: response.statusText },
            };
        }
        const user = await response.json();
        return { success: true, value: user };
    } catch (error) {
        return {
            success: false,
            error: { code: 500, message: 'Erreur réseau' },
        };
    }
}
```

#### 📊 Métriques de qualité

-   **Maintenabilité**: 🟢 Excellente
-   **Testabilité**: 🟢 Excellente
-   **Réutilisabilité**: 🟢 Excellente
-   **Évolutivité**: 🟢 Excellente

### 3. Either Pattern

Le pattern Either est une approche fonctionnelle sophistiquée qui représente un type pouvant contenir soit une erreur (Left) soit une valeur valide (Right). C'est une évolution du Result Pattern offrant plus de flexibilité et de composabilité.

#### 📝 Implémentation basique

```typescript
type EitherType<L, R> = Left<L> | Right<R>;

class Left<L> {
    readonly _tag = 'Left';
    constructor(readonly value: L) {}
}

class Right<R> {
    readonly _tag = 'Right';
    constructor(readonly value: R) {}
}

// Exemple d'utilisation simple
function validateAge(age: number): EitherType<string, number> {
    return age >= 0 && age <= 150 ? new Right(age) : new Left("L'âge doit être compris entre 0 et 150");
}
```

#### 🔄 Implémentation avancée avec méthodes utilitaires

```typescript
class Either<L, R> {
    private constructor(
        private readonly left: L | null,
        private readonly right: R | null,
        private readonly _tag: 'Left' | 'Right',
    ) {}

    static left<L, R>(value: L): Either<L, R> {
        return new Either<L, R>(value, null, 'Left');
    }

    static right<L, R>(value: R): Either<L, R> {
        return new Either<L, R>(null, value, 'Right');
    }

    isLeft(): boolean {
        return this._tag === 'Left';
    }

    isRight(): boolean {
        return this._tag === 'Right';
    }

    fold<T>(leftFn: (left: L) => T, rightFn: (right: R) => T): T {
        return this.isLeft() ? leftFn(this.left!) : rightFn(this.right!);
    }

    map<T>(fn: (r: R) => T): Either<L, T> {
        return this.isRight() ? Either.right(fn(this.right!)) : Either.left(this.left!);
    }

    flatMap<T>(fn: (r: R) => Either<L, T>): Either<L, T> {
        return this.isRight() ? fn(this.right!) : Either.left(this.left!);
    }
}
```

#### 🎯 Cas d'utilisation idéaux

-   Validation complexe de données
-   Chaînes d'opérations fonctionnelles
-   Gestion d'erreurs avec contexte riche
-   Applications avec traitement de données en pipeline

#### ✅ Avantages détaillés

1. **Sécurité du typage**

    - Discrimination de type complète
    - Gestion exhaustive des cas
    - Inférence de type puissante

2. **Composabilité**

    - Chaînage d'opérations fluide
    - Transformation des erreurs et valeurs
    - Combinaison de multiples Either

3. **Fonctionnel**
    - Immutabilité garantie
    - Pas d'effets de bord
    - Raisonnement équationnel

#### ❌ Inconvénients détaillés

1. **Complexité conceptuelle**

    - Concepts de programmation fonctionnelle requis
    - Apprentissage des opérateurs fonctionnels
    - Paradigme différent du style impératif

2. **Verbosité initiale**

    - Définition des types et classes
    - Setup des utilitaires
    - Boilerplate pour les cas simples

3. **Adoption par l'équipe**
    - Formation nécessaire
    - Changement de mentalité requis
    - Résistance possible

#### 🛠️ Bonnes pratiques

1. **Typage strict des erreurs**

```typescript
type ValidationErrorEitherType = {
    code: string;
    field: string;
    message: string;
};

function validateUser(user: unknown): Either<ValidationErrorEitherType[], User> {
    const errors: ValidationError[] = [];

    if (!isValidEmail(user.email)) {
        errors.push({
            code: 'INVALID_EMAIL',
            field: 'email',
            message: "Format d'email invalide",
        });
    }

    return errors.length > 0 ? Either.left(errors) : Either.right(user as User);
}
```

2. **Composition de validations**

```typescript
const validateUser = (user: unknown): Either<ValidationError[], User> =>
    validateEmail(user.email).flatMap((email) =>
        validatePassword(user.password).map((password) => ({ email, password })),
    );

// Utilisation
const result = validateUser(inputData).flatMap(saveUser).flatMap(sendWelcomeEmail);
```

3. **Gestion des erreurs asynchrones**

```typescript
async function fetchUserData(id: string): Promise<Either<ApiError, User>> {
    try {
        const response = await fetch(`/api/users/${id}`);
        if (!response.ok) {
            return Either.left({
                code: response.status,
                message: await response.text(),
            });
        }
        const user = await response.json();
        return Either.right(user);
    } catch (error) {
        return Either.left({
            code: 500,
            message: error.message,
        });
    }
}
```

#### 📊 Métriques de qualité

-   **Maintenabilité**: 🟢 Excellente
-   **Testabilité**: 🟢 Excellente
-   **Réutilisabilité**: 🟢 Excellente
-   **Évolutivité**: 🟢 Excellente

### 4. Nullable Values

L'approche Nullable Values utilise les valeurs `null` ou `undefined` pour signaler une erreur ou une absence de valeur. C'est une approche simple mais limitée, particulièrement utile pour les cas basiques.

#### 📝 Implémentation basique

```typescript
function findUser(id: string): User | null {
    const user = database.find(id);
    return user || null;
}

// Utilisation
const user = findUser('123');
if (user === null) {
    console.error('Utilisateur non trouvé');
} else {
    console.log('Utilisateur trouvé:', user.name);
}
```

#### 🔄 Implémentation améliorée avec types utilitaires

```typescript
type Optional<T> = T | null;
type Nullable<T> = T | null | undefined;

interface UserRepository {
    findById(id: string): Optional<User>;
    findByEmail(email: string): Optional<User>;
}

class UserService {
    constructor(private repository: UserRepository) {}

    getUserDetails(id: string): Optional<UserDetails> {
        const user = this.repository.findById(id);
        if (!user) {
            return null;
        }

        const details = this.enrichUserData(user);
        return details;
    }

    private enrichUserData(user: User): Optional<UserDetails> {
        try {
            return {
                ...user,
                lastLogin: this.getLastLogin(user.id),
                preferences: this.getUserPreferences(user.id),
            };
        } catch {
            return null;
        }
    }
}
```

#### 🎯 Cas d'utilisation idéaux

-   Cas simples de valeurs optionnelles
-   Petits projets avec peu de logique d'erreur
-   Intégration avec des APIs retournant null
-   Prototypes rapides

#### ✅ Avantages détaillés

1. **Simplicité**

    - Concept facile à comprendre
    - Syntaxe concise
    - Pas de setup nécessaire

2. **Support natif**

    - Intégré au langage
    - Bien supporté par TypeScript
    - Opérateurs dédiés (`?.`, `??`)

3. **Performance**
    - Pas d'overhead
    - Pas d'allocations supplémentaires
    - Optimisations du moteur JS

#### ❌ Inconvénients détaillés

1. **Manque de contexte**

    - Pas d'information sur la cause de l'erreur
    - Impossible de différencier les types d'erreurs
    - Perte de contexte dans les chaînes d'appels

2. **Risques de null**

    - Null pointer exceptions possibles
    - Vérifications constantes nécessaires
    - Propagation silencieuse des nulls

3. **Maintenance difficile**
    - Code défensif nécessaire
    - Tests complexes
    - Debug compliqué

#### 🛠️ Bonnes pratiques

1. **Utilisation de strictNullChecks**

```typescript
// tsconfig.json
{
  "compilerOptions": {
    "strictNullChecks": true
  }
}

// Code plus sûr
function processUser(user: User | null): string {
  if (user === null) {
    return "Utilisateur non disponible";
  }
  return user.name; // TypeScript garantit que user n'est pas null
}
```

2. **Opérateurs de chaînage optionnel**

```typescript
const userName = user?.profile?.name ?? 'Anonyme';

// Plutôt que
const userName = (user && user.profile && user.profile.name) || 'Anonyme';
```

3. **Types utilitaires pour la clarté**

```typescript
type Optional<T> = T | null;
type Nullable<T> = T | null | undefined;
type NullableProps<T> = { [P in keyof T]: Optional<T[P]> };

interface UserProfile extends NullableProps<BaseProfile> {
    id: string; // Non nullable
    createdAt: Date; // Non nullable
}
```

#### 📊 Métriques de qualité

-   **Maintenabilité**: 🟡 Moyenne
-   **Testabilité**: 🟡 Moyenne
-   **Réutilisabilité**: 🟡 Moyenne
-   **Évolutivité**: 🔴 Faible

### 5. Callbacks (style Node.js)

Le style callback de Node.js est une approche traditionnelle où le premier paramètre du callback est réservé pour les erreurs. Bien que moins utilisée avec les Promises modernes, cette approche reste présente dans de nombreuses bibliothèques legacy.

#### 📝 Implémentation basique

```typescript
type CallbackType<T> = (error: Error | null, result?: T) => void;

function readFile(path: string, callback: CallbackType<string>): void {
    fs.readFile(path, 'utf8', (err, data) => {
        if (err) {
            callback(err);
            return;
        }
        callback(null, data);
    });
}

// Utilisation
readFile('config.json', (error, data) => {
    if (error) {
        console.error('Erreur de lecture:', error);
        return;
    }
    console.log('Données:', data);
});
```

#### 🔄 Implémentation améliorée avec types personnalisés

```typescript
interface ErrorCallbackInterface<E = Error> {
    (error: E): void;
}

interface SuccessCallbackInterface<T> {
    (data: T): void;
}

interface AsyncOperationInterface<T, E = Error> {
    (onSuccess: SuccessCallbackInterface<T>, onError: ErrorCallbackInterface<E>): void;
}

class UserService {
    fetchUser: AsyncOperationInterface<User, ApiErrorType> = (onSuccess, onError) => {
        fetch('/api/user')
            .then((response) => response.json())
            .then((user) => onSuccess(user))
            .catch((error) => onError(new ApiError('Erreur réseau', error)));
    };

    // Utilisation
    // this.fetchUser(
    //   user => console.log("Utilisateur:", user),
    //   error => console.error("Erreur:", error)
    // );
}
```

#### 🎯 Cas d'utilisation idéaux

-   Code legacy Node.js
-   APIs basées sur les callbacks
-   Intégration avec des bibliothèques anciennes
-   Migration progressive vers les Promises

#### ✅ Avantages détaillés

1. **Convention établie**

    - Standard bien connu dans Node.js
    - Pattern reconnaissable
    - Documentation abondante

2. **Séparation explicite**

    - Gestion distincte succès/erreur
    - Pas d'ambiguïté sur le type d'erreur
    - Facilité de logging

3. **Flexibilité**
    - Peut être converti en Promises
    - Supporte les opérations asynchrones
    - Adaptable à différents contextes

#### ❌ Inconvénients détaillés

1. **Callback Hell**

    - Imbrication excessive
    - Code difficile à suivre
    - Gestion d'erreurs répétitive

2. **Verbosité**

    - Beaucoup de boilerplate
    - Répétition des patterns
    - Structure lourde

3. **Maintenance complexe**
    - Difficile à tester
    - Difficile à refactorer
    - Difficile à déboguer

#### 🛠️ Bonnes pratiques

1. **Typage strict des callbacks**

```typescript
type ErrorCallbackType<E> = (error: E) => void;
type SuccessCallbackType<T> = (result: T) => void;

function fetchData<T>(onSuccess: SuccessCallbackType<T>, onError: ErrorCallbackType<ApiErrorType>): void {
    // Implémentation...
}
```

2. **Promisification des callbacks**

```typescript
function promisify<T>(callbackFn: (callback: CallbackType<T>) => void): Promise<T> {
    return new Promise((resolve, reject) => {
        callbackFn((error, result) => {
            if (error) {
                reject(error);
                return;
            }
            resolve(result!);
        });
    });
}
```

3. **Gestion d'erreurs typée**

```typescript
class DatabaseError extends Error {
    constructor(message: string, public readonly code: number) {
        super(message);
        this.name = 'DatabaseError';
    }
}

function query(sql: string, onSuccess: (result: QueryResult) => void, onError: (error: DatabaseError) => void): void {
    // Implémentation...
}
```

#### 📊 Métriques de qualité

-   **Maintenabilité**: 🔴 Faible
-   **Testabilité**: 🔴 Faible
-   **Réutilisabilité**: 🟡 Moyenne
-   **Évolutivité**: 🔴 Faible

### 6. Décorateurs TypeScript

Les décorateurs TypeScript offrent une approche élégante et déclarative pour la gestion des erreurs, particulièrement adaptée aux applications orientées aspect et aux frameworks comme Angular.

#### 📝 Implémentation basique

```typescript
function HandleError() {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        const originalMethod = descriptor.value;

        descriptor.value = async function (...args: any[]) {
            try {
                return await originalMethod.apply(this, args);
            } catch (error) {
                console.error(`Erreur dans ${propertyKey}:`, error);
                throw error;
            }
        };

        return descriptor;
    };
}

class UserService {
    @HandleError()
    async fetchUser(id: string): Promise<User> {
        // Implémentation...
    }
}
```

#### 🔄 Implémentation avancée avec configuration

```typescript
interface ErrorHandlerConfig {
    rethrow?: boolean;
    logLevel?: 'error' | 'warn' | 'info';
    transformError?: (error: unknown) => Error;
}

function HandleError(config: ErrorHandlerConfig = {}) {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        const originalMethod = descriptor.value;

        descriptor.value = async function (...args: any[]) {
            try {
                return await originalMethod.apply(this, args);
            } catch (error) {
                const transformedError = config.transformError ? config.transformError(error) : error;

                switch (config.logLevel) {
                    case 'warn':
                        console.warn(`Warning in ${propertyKey}:`, transformedError);
                        break;
                    case 'info':
                        console.info(`Info from ${propertyKey}:`, transformedError);
                        break;
                    default:
                        console.error(`Error in ${propertyKey}:`, transformedError);
                }

                if (config.rethrow) {
                    throw transformedError;
                }
            }
        };

        return descriptor;
    };
}

// Utilisation avancée
class UserService {
    @HandleError({
        rethrow: true,
        logLevel: 'error',
        transformError: (error: unknown) => {
            if (error instanceof NetworkError) {
                return new ApplicationError('Service indisponible', error);
            }
            return error as Error;
        },
    })
    async updateUser(user: User): Promise<User> {
        // Implémentation...
    }
}
```

#### 🎯 Cas d'utilisation idéaux

-   Applications Angular ou NestJS
-   Programmation orientée aspect (AOP)
-   Gestion centralisée des erreurs
-   Logging et monitoring transversal

#### ✅ Avantages détaillés

1. **Séparation des préoccupations**

    - Logique d'erreur isolée
    - Code métier plus propre
    - Réutilisation facilitée

2. **Configuration déclarative**

    - Syntaxe claire et concise
    - Paramétrage flexible
    - Maintenance simplifiée

3. **Aspect transversal**
    - Gestion cohérente des erreurs
    - Logging centralisé
    - Monitoring unifié

#### ❌ Inconvénients détaillés

1. **Configuration nécessaire**

    - Setup TypeScript requis
    - Metadata reflection API
    - Compilation spécifique

2. **Complexité cachée**

    - Logique d'erreur moins visible
    - Debug plus complexe
    - Stack traces modifiées

3. **Support expérimental**
    - Fonctionnalité TypeScript en évolution
    - Compatibilité à surveiller
    - Documentation limitée

#### 🛠️ Bonnes pratiques

1. **Typage des décorateurs**

```typescript
function ValidateInput<T extends object>(validator: (input: T) => ValidationResult) {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        const originalMethod = descriptor.value;

        descriptor.value = function (input: T) {
            const validationResult = validator(input);
            if (!validationResult.isValid) {
                throw new ValidationError(validationResult.errors);
            }
            return originalMethod.call(this, input);
        };

        return descriptor;
    };
}
```

2. **Composition de décorateurs**

```typescript
class UserController {
    @HandleError()
    @ValidateInput(userValidator)
    @Authorize('admin')
    async createUser(userData: UserInput): Promise<User> {
        // Implémentation...
    }
}
```

3. **Gestion du contexte**

```typescript
function WithErrorContext(context: string) {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        const originalMethod = descriptor.value;

        descriptor.value = async function (...args: any[]) {
            try {
                return await originalMethod.apply(this, args);
            } catch (error) {
                error.context = context;
                throw error;
            }
        };

        return descriptor;
    };
}
```

#### 📊 Métriques de qualité

-   **Maintenabilité**: 🟢 Excellente
-   **Testabilité**: 🟢 Bonne
-   **Réutilisabilité**: 🟢 Excellente
-   **Évolutivité**: 🟢 Excellente

### 7. DI Error Handler

Une approche architecturale qui utilise l'injection de dépendances pour centraliser et standardiser la gestion des erreurs à travers l'application.

#### 📝 Implémentation basique

```typescript
// Interface pour le service de gestion d'erreurs
interface ErrorHandler {
    handle(error: Error): void;
    handleWithContext(error: Error, context: unknown): void;
}

// Implémentation concrète du gestionnaire d'erreurs
class GlobalErrorHandler implements ErrorHandler {
    handle(error: Error): void {
        // Logique centralisée de gestion d'erreurs
        if (error instanceof ValidationError) {
            console.error('[Validation]', error.message);
            // Logique spécifique pour les erreurs de validation
        } else if (error instanceof NetworkError) {
            console.error('[Network]', error.message);
            // Logique spécifique pour les erreurs réseau
        } else {
            console.error('[Unexpected]', error);
            // Logique par défaut
        }
    }

    handleWithContext(error: Error, context: unknown): void {
        console.error(`[Context: ${JSON.stringify(context)}]`);
        this.handle(error);
    }
}
```

#### 🔄 Implémentation avancée avec Strategy Pattern

```typescript
// Interface pour les stratégies de gestion d'erreurs
interface ErrorHandlingStrategy {
    canHandle(error: Error): boolean;
    handle(error: Error, context?: unknown): void;
}

// Implémentation des stratégies spécifiques
class ValidationErrorStrategy implements ErrorHandlingStrategy {
    canHandle(error: Error): boolean {
        return error instanceof ValidationError;
    }

    handle(error: ValidationError, context?: unknown): void {
        console.error('[Validation]', {
            message: error.message,
            fields: error.fields,
            context,
        });
        // Logique spécifique aux erreurs de validation
    }
}

// Gestionnaire d'erreurs avancé avec injection de stratégies
@Injectable()
class AdvancedErrorHandler implements ErrorHandler {
    constructor(
        private readonly strategies: ErrorHandlingStrategy[],
        private readonly logger: LoggerService,
        private readonly metrics: MetricsService,
    ) {}

    handle(error: Error, context?: unknown): void {
        const strategy = this.strategies.find((s) => s.canHandle(error));

        if (strategy) {
            strategy.handle(error, context);
        } else {
            this.handleUnknownError(error, context);
        }

        this.metrics.recordError(error);
    }

    private handleUnknownError(error: Error, context?: unknown): void {
        this.logger.error('Erreur non gérée', { error, context });
    }
}
```

#### 🎯 Cas d'utilisation idéaux

-   Applications enterprise
-   Systèmes distribués
-   Microservices
-   Applications avec besoins de monitoring avancé

#### ✅ Avantages détaillés

1. **Centralisation**

    - Gestion cohérente des erreurs
    - Point unique de configuration
    - Facilité de maintenance

2. **Flexibilité**

    - Ajout facile de nouvelles stratégies
    - Configuration par environnement
    - Extensibilité

3. **Séparation des responsabilités**
    - Code métier plus propre
    - Meilleure testabilité
    - Réutilisation facilitée

#### ❌ Inconvénients détaillés

1. **Complexité initiale**

    - Setup d'infrastructure nécessaire
    - Configuration du DI
    - Courbe d'apprentissage

2. **Overhead**

    - Plus de code d'infrastructure
    - Performance légèrement impactée
    - Mémoire supplémentaire

3. **Maintenance**
    - Documentation nécessaire
    - Formation de l'équipe
    - Risque de sur-ingénierie

#### 🛠️ Bonnes pratiques

1. **Configuration modulaire**

```typescript
// Angular
@Module({
    providers: [
        {
            provide: ErrorHandler,
            useClass: AdvancedErrorHandler,
        },
        ValidationErrorStrategy,
        NetworkErrorStrategy,
        // Autres stratégies...
    ],
})
export class ErrorHandlingModule {}
```

2. **Contexte riche**

```typescript
interface ErrorContext {
    userId?: string;
    action: string;
    timestamp: Date;
    metadata?: Record<string, unknown>;
}

class ContextualErrorHandler implements ErrorHandler {
    handle(error: Error, context: ErrorContext): void {
        // Traitement avec contexte
    }
}
```

3. **Monitoring intégré**

```typescript
@Injectable()
class MonitoredErrorHandler implements ErrorHandler {
    constructor(private metrics: MetricsService, private logger: LoggerService) {}

    handle(error: Error): void {
        this.metrics.incrementErrorCount(error.constructor.name);
        this.logger.error(error);
        // Autres traitements...
    }
}
```

#### 📊 Métriques de qualité

-   **Maintenabilité**: 🟢 Excellente
-   **Testabilité**: 🟢 Excellente
-   **Réutilisabilité**: 🟢 Excellente
-   **Évolutivité**: 🟢 Excellente

### 8. Custom Error Classes

Les classes d'erreur personnalisées permettent de créer une hiérarchie d'erreurs riche et typée, facilitant la gestion précise des différents types d'erreurs dans l'application.

#### 📝 Implémentation basique

```typescript
class ApplicationError extends Error {
    constructor(message: string) {
        super(message);
        this.name = 'ApplicationError';
        // Correction de la chaîne de prototype en ES5
        Object.setPrototypeOf(this, ApplicationError.prototype);
    }
}

class ValidationError extends ApplicationError {
    constructor(message: string, public readonly fields: Record<string, string>) {
        super(message);
        this.name = 'ValidationError';
        Object.setPrototypeOf(this, ValidationError.prototype);
    }
}
```

#### 🔄 Implémentation avancée avec hiérarchie complète

```typescript
abstract class BaseError extends Error {
    constructor(message: string, public readonly code: string, public readonly details?: unknown) {
        super(message);
        this.name = this.constructor.name;
        Object.setPrototypeOf(this, new.target.prototype);
    }

    toJSON(): Record<string, unknown> {
        return {
            name: this.name,
            message: this.message,
            code: this.code,
            details: this.details,
            stack: this.stack,
        };
    }
}

class HttpError extends BaseError {
    constructor(message: string, public readonly statusCode: number, details?: unknown) {
        super(message, `HTTP_${statusCode}`, details);
    }
}

class BusinessError extends BaseError {
    constructor(message: string, code: string, public readonly domain: string, details?: unknown) {
        super(message, `BUS_${code}`, details);
    }
}

// Erreurs spécifiques au domaine
class UserError extends BusinessError {
    constructor(message: string, code: string, details?: unknown) {
        super(message, code, 'USER', details);
    }

    static notFound(userId: string): UserError {
        return new UserError(`Utilisateur ${userId} non trouvé`, 'USER_NOT_FOUND', { userId });
    }

    static invalidCredentials(): UserError {
        return new UserError('Identifiants invalides', 'INVALID_CREDENTIALS');
    }
}
```

#### 🎯 Cas d'utilisation idéaux

-   Applications avec domaine métier complexe
-   APIs avec besoins de gestion d'erreurs détaillée
-   Systèmes nécessitant une traçabilité précise
-   Frameworks et bibliothèques

#### ✅ Avantages détaillés

1. **Typage fort**

    - Hiérarchie d'erreurs claire
    - Vérification de type précise
    - Autocomplétion IDE

2. **Contexte riche**

    - Informations détaillées sur l'erreur
    - Métadonnées personnalisées
    - Stack traces améliorées

3. **Maintenabilité**
    - Organisation claire des erreurs
    - Réutilisation facilitée
    - Documentation implicite

#### ❌ Inconvénients détaillés

1. **Complexité**

    - Nécessite une bonne conception
    - Hiérarchie à maintenir
    - Risque de prolifération

2. **Overhead**

    - Classes supplémentaires
    - Mémoire pour les instances
    - Sérialisation/désérialisation

3. **Apprentissage**
    - Convention de nommage
    - Structure à comprendre
    - Documentation nécessaire

#### 🛠️ Bonnes pratiques

1. **Factory methods**

```typescript
class ApiError extends BaseError {
    static notFound(resource: string, id: string): ApiError {
        return new ApiError(`${resource} with id ${id} not found`, 'NOT_FOUND', { resource, id });
    }

    static unauthorized(reason: string): ApiError {
        return new ApiError(`Unauthorized: ${reason}`, 'UNAUTHORIZED', { reason });
    }
}
```

2. **Error Catalogues**

```typescript
const ErrorCodes = {
    VALIDATION: {
        REQUIRED_FIELD: 'VAL_001',
        INVALID_FORMAT: 'VAL_002',
        OUT_OF_RANGE: 'VAL_003',
    },
    BUSINESS: {
        INSUFFICIENT_FUNDS: 'BUS_001',
        ACCOUNT_LOCKED: 'BUS_002',
    },
} as const;

type ErrorCode = (typeof ErrorCodes)[keyof typeof ErrorCodes][keyof (typeof ErrorCodes)[keyof typeof ErrorCodes]];
```

3. **Sérialisation cohérente**

```typescript
class SerializableError extends BaseError {
    toJSON(): Record<string, unknown> {
        return {
            type: this.name,
            message: this.message,
            code: this.code,
            timestamp: new Date().toISOString(),
            details: this.details,
            stack: process.env.NODE_ENV === 'development' ? this.stack : undefined,
        };
    }

    static fromJSON(json: Record<string, unknown>): SerializableError {
        return new SerializableError(json.message as string, json.code as string, json.details);
    }
}
```

#### 📊 Métriques de qualité

-   **Maintenabilité**: 🟢 Excellente
-   **Testabilité**: 🟢 Excellente
-   **Réutilisabilité**: 🟢 Excellente
-   **Évolutivité**: 🟢 Excellente

#### 9. Error Cause Chain

Le chaînage des causes d'erreurs permet de créer une trace détaillée de l'origine et de la propagation des erreurs à travers l'application.

#### 📝 Implémentation basique

```typescript
class ChainableError extends Error {
    constructor(message: string, public readonly cause?: Error) {
        super(message);
        this.name = this.constructor.name;
        Object.setPrototypeOf(this, ChainableError.prototype);
    }

    getCauseChain(): Error[] {
        const chain: Error[] = [this];
        let currentCause = this.cause;

        while (currentCause) {
            chain.push(currentCause);
            currentCause = (currentCause as ChainableError).cause;
        }

        return chain;
    }
}
```

#### 🔄 Implémentation avancée avec contexte enrichi

```typescript
interface ErrorContext {
    timestamp: Date;
    layer: 'api' | 'service' | 'repository';
    operation: string;
    metadata?: Record<string, unknown>;
}

class ContextualError extends Error {
    constructor(message: string, public readonly context: ErrorContext, public readonly cause?: Error) {
        super(message);
        this.name = this.constructor.name;
        Object.setPrototypeOf(this, ContextualError.prototype);
    }

    getFullStack(): string {
        let stack = this.formatError(this);
        let currentCause = this.cause;

        while (currentCause) {
            stack += `\nCaused by: ${this.formatError(currentCause)}`;
            currentCause = (currentCause as ContextualError).cause;
        }

        return stack;
    }

    private formatError(error: Error): string {
        if (error instanceof ContextualError) {
            return `[${error.context.layer}] ${error.message} (${error.context.operation})`;
        }
        return error.message;
    }
}

// Exemple d'utilisation
async function fetchUserData(userId: string): Promise<User> {
    try {
        const response = await fetch(`/api/users/${userId}`);
        if (!response.ok) {
            throw new ContextualError(`HTTP ${response.status}`, {
                timestamp: new Date(),
                layer: 'api',
                operation: 'fetchUserData',
                metadata: { status: response.status },
            });
        }
        return await response.json();
    } catch (error) {
        throw new ContextualError(
            'Échec de récupération des données utilisateur',
            {
                timestamp: new Date(),
                layer: 'service',
                operation: 'getUserDetails',
                metadata: { userId },
            },
            error instanceof Error ? error : new Error(String(error)),
        );
    }
}
```

#### 🎯 Cas d'utilisation idéaux

-   Débogage d'erreurs complexes
-   Systèmes distribués
-   Logging avancé
-   Analyse de cause racine

#### ✅ Avantages détaillés

1. **Traçabilité complète**

    - Historique des erreurs
    - Contexte à chaque niveau
    - Facilite le débogage

2. **Contexte riche**

    - Métadonnées à chaque niveau
    - Information temporelle
    - Données structurées

3. **Standard ECMAScript**
    - Support natif (ES2022+)
    - Interopérabilité
    - Sérialisation JSON

#### ❌ Inconvénients détaillés

1. **Verbosité**

    - Stack traces longues
    - Données redondantes possibles
    - Overhead mémoire

2. **Complexité**

    - Gestion du contexte
    - Sérialisation complexe
    - Performance impactée

3. **Compatibilité**
    - Support ES2022+ requis
    - Polyfills nécessaires
    - Sérialisation limitée

#### 🛠️ Bonnes pratiques

1. **Enrichissement progressif**

```typescript
class ServiceError extends ContextualError {
    static wrap(error: unknown, context: Partial<ErrorContext>): ServiceError {
        const fullContext: ErrorContext = {
            timestamp: new Date(),
            layer: 'service',
            operation: 'unknown',
            ...context,
        };

        return new ServiceError(
            'Erreur de service',
            fullContext,
            error instanceof Error ? error : new Error(String(error)),
        );
    }
}
```

2. **Formatage personnalisé**

```typescript
class FormattedError extends ContextualError {
    toString(): string {
        return [
            `[${this.context.timestamp.toISOString()}]`,
            `[${this.context.layer}]`,
            `[${this.context.operation}]`,
            this.message,
            this.context.metadata && JSON.stringify(this.context.metadata),
        ]
            .filter(Boolean)
            .join(' ');
    }
}
```

3. **Intégration avec le logging**

```typescript
class LoggableError extends ContextualError {
    toLogFormat(): Record<string, unknown> {
        return {
            message: this.message,
            context: this.context,
            timestamp: new Date().toISOString(),
            stack: this.getFullStack(),
            causes: this.getCauseChain().map((error) => ({
                message: error.message,
                name: error.name,
            })),
        };
    }
}
```

#### 📊 Métriques de qualité

-   **Maintenabilité**: 🟢 Excellente
-   **Testabilité**: 🟢 Bonne
-   **Réutilisabilité**: 🟢 Excellente
-   **Évolutivité**: 🟢 Excellente

### 10. Codes de retour

L'approche par codes de retour est une méthode traditionnelle utilisant des valeurs numériques ou des énumérations pour signaler les différents états d'erreur.

#### 📝 Implémentation basique

```typescript
enum StatusCode {
    SUCCESS = 0,
    NOT_FOUND = 1,
    INVALID_INPUT = 2,
    PERMISSION_DENIED = 3,
    INTERNAL_ERROR = 4,
}

function divideNumbers(a: number, b: number): { code: StatusCode; result?: number } {
    if (b === 0) {
        return { code: StatusCode.INVALID_INPUT };
    }
    return { code: StatusCode.SUCCESS, result: a / b };
}
```

#### 🔄 Implémentation avancée avec typage strict

```typescript
// Définition des codes d'erreur par domaine
const ErrorCodes = {
    VALIDATION: {
        INVALID_EMAIL: 1001,
        INVALID_PASSWORD: 1002,
        REQUIRED_FIELD: 1003,
    },
    BUSINESS: {
        USER_NOT_FOUND: 2001,
        INSUFFICIENT_FUNDS: 2002,
        ACCOUNT_LOCKED: 2003,
    },
    SYSTEM: {
        DATABASE_ERROR: 3001,
        NETWORK_ERROR: 3002,
        CONFIGURATION_ERROR: 3003,
    },
} as const;

type ErrorCode = (typeof ErrorCodes)[keyof typeof ErrorCodes][keyof typeof ErrorCodes];

interface OperationResult<T> {
    code: ErrorCode | 0; // 0 représente le succès
    data?: T;
    message?: string;
}

class UserService {
    createUser(userData: UserInput): OperationResult<User> {
        if (!this.validateEmail(userData.email)) {
            return {
                code: ErrorCodes.VALIDATION.INVALID_EMAIL,
                message: "Format d'email invalide",
            };
        }

        try {
            const user = this.userRepository.create(userData);
            return {
                code: 0,
                data: user,
            };
        } catch (error) {
            return {
                code: ErrorCodes.SYSTEM.DATABASE_ERROR,
                message: "Erreur lors de la création de l'utilisateur",
            };
        }
    }
}
```

#### 🎯 Cas d'utilisation idéaux

-   Systèmes embarqués
-   Applications avec contraintes de performance
-   Interfaces C/C++
-   Systèmes legacy

##### ✅ Avantages détaillés

1. **Performance**

    - Pas d'allocation d'objets
    - Overhead minimal
    - Rapide à traiter

2. **Simplicité**

    - Concept facile à comprendre
    - Implémentation directe
    - Pas de dépendances

3. **Portabilité**
    - Compatible avec tous les langages
    - Facile à sérialiser
    - Interopérabilité simple

##### ❌ Inconvénients détaillés

1. **Manque de contexte**

    - Pas d'information détaillée
    - Pas de stack trace
    - Débogage difficile

2. **Maintenance**

    - Codes à maintenir
    - Documentation nécessaire
    - Risque d'incohérence

3. **Type-safety limitée**
    - Pas de vérification automatique
    - Risque d'oubli de vérification
    - Codes magiques

#### 🛠️ Bonnes pratiques

1. **Organisation des codes**

```typescript
// Regroupement par domaine
const DomainErrors = {
    User: {
        NotFound: 404,
        Unauthorized: 401,
        InvalidInput: 400,
    },
    Payment: {
        InsufficientFunds: 4001,
        CardDeclined: 4002,
        InvalidAmount: 4003,
    },
} as const;

type DomainErrorCode = (typeof DomainErrors)[keyof typeof DomainErrors][keyof typeof DomainErrors];
```

2. **Utilitaires de vérification**

```typescript
function isSuccess<T>(result: OperationResult<T>): result is OperationResult<T> & { code: 0; data: T } {
    return result.code === 0;
}

function isError<T>(result: OperationResult<T>): result is OperationResult<T> & { code: ErrorCode; message: string } {
    return result.code !== 0;
}
```

3. **Documentation des codes**

```typescript
/**
 * @enum {number}
 * @readonly
 * @description Codes d'erreur du système
 *
 * 0-999: Erreurs système
 * 1000-1999: Erreurs de validation
 * 2000-2999: Erreurs métier
 * 3000-3999: Erreurs d'infrastructure
 */
enum SystemErrorCodes {
    /** Opération réussie */
    SUCCESS = 0,
    /** Erreur système générique */
    SYSTEM_ERROR = 500,
    /** Erreur de configuration */
    CONFIG_ERROR = 501,
}
```

#### 📊 Métriques de qualité

-   **Maintenabilité**: 🔴 Faible
-   **Testabilité**: 🟡 Moyenne
-   **Réutilisabilité**: 🟡 Moyenne
-   **Évolutivité**: 🔴 Faible

# 🔧 Gestion d'Erreurs dans les Frameworks Modernes

## 1. Angular

Angular propose un système robuste de gestion d'erreurs centralisé via son mécanisme d'ErrorHandler.

### 📝 Implémentation basique

```typescript
@Injectable()
class GlobalErrorHandler implements ErrorHandler {
    constructor(private notifier: NotificationService, private logger: LoggerService) {}

    handleError(error: Error): void {
        this.logger.error('Une erreur est survenue', error);
        this.notifier.showError('Une erreur est survenue');
    }
}

// Configuration
@NgModule({
    providers: [{ provide: ErrorHandler, useClass: GlobalErrorHandler }],
})
export class AppModule {}
```

### 🔄 Implémentation avancée avec zones

```typescript
@Injectable()
class ZoneAwareErrorHandler implements ErrorHandler {
    constructor(@Inject(NgZone) private ngZone: NgZone, private errorService: ErrorService) {}

    handleError(error: unknown): void {
        this.ngZone.run(() => {
            if (error instanceof HttpErrorResponse) {
                this.handleHttpError(error);
            } else {
                this.handleGenericError(error);
            }
        });
    }

    private handleHttpError(error: HttpErrorResponse): void {
        if (error.status === 401) {
            this.errorService.handleUnauthorized();
        } else if (error.status === 404) {
            this.errorService.handleNotFound();
        }
    }
}
```

## 2. Next.js

Next.js offre plusieurs niveaux de gestion d'erreurs, du côté client et serveur.

### 📝 Gestion des erreurs pages

```typescript
// pages/_error.tsx
import { NextPageContext } from 'next';

interface ErrorProps {
    statusCode: number;
}

function Error({ statusCode }: ErrorProps): JSX.Element {
    return (
        <div>
            {statusCode
                ? `Une erreur ${statusCode} est survenue sur le serveur`
                : 'Une erreur est survenue sur le client'}
        </div>
    );
}

Error.getInitialProps = ({ res, err }: NextPageContext) => {
    const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
    return { statusCode };
};

export default Error;
```

### 🔄 API Routes avec gestion d'erreurs

```typescript
// pages/api/users/[id].ts
import { NextApiRequest, NextApiResponse } from 'next';
import { createRouter } from 'next-connect';
import { ApiError } from '@/lib/errors';

const router = createRouter<NextApiRequest, NextApiResponse>();

router
    .use(async (req, res, next) => {
        try {
            await next();
        } catch (error) {
            if (error instanceof ApiError) {
                return res.status(error.statusCode).json({
                    error: error.message,
                });
            }
            res.status(500).json({
                error: 'Erreur interne du serveur',
            });
        }
    })
    .get(async (req, res) => {
        const { id } = req.query;
        const user = await getUserById(id as string);
        if (!user) {
            throw new ApiError(404, 'Utilisateur non trouvé');
        }
        res.json(user);
    });

export default router.handler();
```

## 3. NestJS

NestJS propose un système complet de filtres d'exception et d'intercepteurs.

### 📝 Filtre d'exception global

```typescript
@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
    constructor(private readonly logger: Logger) {}

    catch(exception: unknown, host: ArgumentsHost): void {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();
        const request = ctx.getRequest<Request>();

        const status = exception instanceof HttpException ? exception.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR;

        const errorResponse = {
            statusCode: status,
            timestamp: new Date().toISOString(),
            path: request.url,
            message: this.getErrorMessage(exception),
        };

        this.logger.error(`${request.method} ${request.url}`, exception instanceof Error ? exception.stack : '');

        response.status(status).json(errorResponse);
    }

    private getErrorMessage(exception: unknown): string {
        if (exception instanceof HttpException) {
            return exception.message;
        }
        return 'Erreur interne du serveur';
    }
}
```

### 🔄 Intercepteur avec gestion d'erreurs

```typescript
@Injectable()
export class ErrorsInterceptor implements NestInterceptor {
    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        return next.handle().pipe(
            catchError((error) => {
                if (error instanceof BusinessError) {
                    throw new HttpException(
                        {
                            status: HttpStatus.BAD_REQUEST,
                            error: error.message,
                            code: error.code,
                        },
                        HttpStatus.BAD_REQUEST,
                    );
                }
                throw error;
            }),
        );
    }
}
```

## 4. Vue.js

Vue.js offre plusieurs mécanismes pour la gestion des erreurs, notamment au niveau des composants et du routeur.

### 📝 Gestionnaire d'erreurs global

```typescript
// error-handler.ts
import { App } from 'vue';
import { ErrorTracker } from '@/services/error-tracker';

export function setupErrorHandler(app: App, errorTracker: ErrorTracker): void {
    app.config.errorHandler = (error, instance, info) => {
        // Tracking des erreurs
        errorTracker.captureError(error, {
            componentName: instance?.$options.name,
            errorInfo: info,
            vue: true,
        });

        // Logging
        console.error('[Vue Error]:', error);
        console.error('Component:', instance);
        console.error('Error Info:', info);
    };
}
```

### 🔄 Gestion d'erreurs composant

```typescript
// UserProfile.vue
<script lang="ts">
import { defineComponent } from "vue";
import { useErrorBoundary } from "@/composables/error-boundary";

export default defineComponent({
  name: "UserProfile",

  setup() {
    const { error, resetError } = useErrorBoundary();

    const fetchUserData = async () => {
      try {
        // ... logique de récupération
      } catch (err) {
        error.value = err instanceof Error
          ? err
          : new Error("Erreur inconnue");
      }
    };

    return {
      error,
      resetError,
      fetchUserData
    };
  }
});
</script>

<template>
  <div v-if="error">
    <ErrorDisplay
      :error="error"
      @retry="resetError"
    />
  </div>
  <div v-else>
    <!-- Contenu normal -->
  </div>
</template>
```

## 5. Express.js

Express nécessite une approche structurée de la gestion d'erreurs avec ses middlewares.

### 📝 Middleware de gestion d'erreurs centralisé

```typescript
// error-handling.ts
import { Request, Response, NextFunction } from "express";
import { ValidationError, DatabaseError, AuthError } from "@/errors";

export class ErrorHandler {
  static handle(
    error: Error,
    req: Request,
    res: Response,
    next: NextFunction
  ): void {
    // Log l'erreur
    console.error(error);

    if (error instanceof ValidationError) {
      res.status(400).json({
        type: "ValidationError",
        message: error.message,
        errors: error.details
      });
      return;
    }

    if (error instanceof AuthError) {
      res.status(401).json({
        type: "AuthError",
        message: error.message
      });
      return;
    }

    if (error instanceof DatabaseError) {
      res.status(503).json({
        type: "DatabaseError",
        message: "Service temporairement indisponible"
      });
      return;
    }

    // Erreur par défaut
    res.status(500).json({
      type: "ServerError",
      message: "Erreur interne du serveur"
    });
  }
}

// Middleware async error wrapper
export const asyncHandler =
  (fn: (req: Request, res: Response, next: NextFunction) =>
  (req: Request, res: Response, next: NextFunction): void => {
    Promise.resolve(fn(req, res, next)).catch(next);
  };
```

### 🔄 Utilisation dans les routes

```typescript
// users.routes.ts
import { Router } from 'express';
import { asyncHandler } from '@/middleware/error-handling';
import { UserController } from '@/controllers/user.controller';

const router = new Router();
const userController = new UserController();

router.post(
    '/users',
    asyncHandler(async (req, res) => {
        const user = await userController.createUser(req.body);
        res.status(201).json(user);
    }),
);

export default router;
```

## 6. Fastify

Fastify propose un système de gestion d'erreurs basé sur les schémas et les hooks.

### 📝 Configuration des erreurs

```typescript
// error-handler.ts
import { FastifyError, FastifyReply, FastifyRequest } from 'fastify';
import { ZodError } from 'zod';

export async function errorHandler(error: FastifyError, request: FastifyRequest, reply: FastifyReply): Promise<void> {
    // Erreurs de validation Zod
    if (error instanceof ZodError) {
        await reply.status(400).send({
            statusCode: 400,
            error: 'Validation Error',
            issues: error.issues,
        });
        return;
    }

    // Erreurs Fastify
    if (error.validation) {
        await reply.status(400).send({
            statusCode: 400,
            error: 'Validation Error',
            message: error.message,
        });
        return;
    }

    // Erreurs personnalisées
    if (error instanceof ApplicationError) {
        await reply.status(error.statusCode).send({
            statusCode: error.statusCode,
            error: error.name,
            message: error.message,
        });
        return;
    }

    // Erreur par défaut
    request.log.error(error);
    await reply.status(500).send({
        statusCode: 500,
        error: 'Internal Server Error',
        message: 'Une erreur inattendue est survenue',
    });
}
```

### 🔄 Configuration du serveur avec gestion d'erreurs

```typescript
// server.ts
import Fastify from "fastify";
import { errorHandler } from "./error-handler";

async function buildServer() {
  const fastify = Fastify({
    logger: true,
    ajv: {
      customOptions: {
        removeAdditional: "all",
        coerceTypes: true,
        useDefaults: true
      }
    }
  });

  // Enregistrement du gestionnaire d'erreurs
  fastify.setErrorHandler(errorHandler);

  // Hook pour la gestion des erreurs non attrapées
  fastify.addHook("onError", async (request, reply, error) => {
    request.log.error({
      err: error,
      requestId: request.id,
      url: request.url,
      method: request.method
    });
  }));

  return fastify;
}
```

[...contenu précédent...]

## 7. AdonisJS

AdonisJS fournit un système sophistiqué de gestion d'erreurs avec des fonctionnalités avancées pour le formatage et le traitement des erreurs.

### 📝 Exception Handler Global

```typescript
// app/Exceptions/Handler.ts
import Logger from '@ioc:Adonis/Core/Logger';
import HttpExceptionHandler from '@ioc:Adonis/Core/HttpExceptionHandler';
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';

export default class ExceptionHandler extends HttpExceptionHandler {
    constructor() {
        super(Logger);
    }

    public async handle(error: any, ctx: HttpContextContract): Promise<any> {
        /**
         * Gestion des erreurs de validation
         */
        if (error.code === 'E_VALIDATION_FAILURE') {
            return ctx.response.status(422).send({
                status: 'error',
                message: 'Erreur de validation',
                errors: error.messages,
            });
        }

        /**
         * Gestion des erreurs d'authentification
         */
        if (error.code === 'E_UNAUTHORIZED_ACCESS') {
            return ctx.response.status(401).send({
                status: 'error',
                message: 'Non autorisé',
            });
        }

        /**
         * Gestion des erreurs personnalisées
         */
        if (error.code === 'BUSINESS_ERROR') {
            return ctx.response.status(400).send({
                status: 'error',
                code: error.code,
                message: error.message,
            });
        }

        /**
         * Erreur par défaut
         */
        return super.handle(error, ctx);
    }

    /**
     * Rapport d'erreur
     */
    public async report(error: any, ctx: HttpContextContract): Promise<void> {
        if (error.code === 'BUSINESS_ERROR') {
            // Ne pas logger les erreurs métier
            return;
        }

        // Logger toutes les autres erreurs
        Logger.error(error.message, {
            err: error,
            stack: error.stack,
            url: ctx.request.url(),
            method: ctx.request.method(),
            userId: ctx.auth.user?.id,
        });
    }
}
```

### 🔄 Erreurs Personnalisées

```typescript
// app/Exceptions/BusinessException.ts
import { Exception } from '@adonisjs/core/build/standalone';

export default class BusinessException extends Exception {
    constructor(message: string, code = 'BUSINESS_ERROR', status = 400) {
        super(message, status, code);
    }

    public async handle(error: this, ctx: HttpContextContract): Promise<void> {
        ctx.response.status(error.status).send({
            code: error.code,
            message: error.message,
            status: 'error',
        });
    }
}

// Utilisation dans un contrôleur
@Injectable()
export class UserController {
    public async create({ request, response }: HttpContextContract) {
        try {
            const userData = await request.validate(CreateUserValidator);
            const user = await User.create(userData);

            return response.created(user);
        } catch (error) {
            if (error.code === 'E_VALIDATION_FAILURE') {
                throw error;
            }

            throw new BusinessException("Erreur lors de la création de l'utilisateur", 'USER_CREATION_ERROR');
        }
    }
}
```

## 8. Nuxt.js

Nuxt.js combine la gestion d'erreurs de Vue.js avec des fonctionnalités supplémentaires pour le SSR et le développement universel.

### 📝 Configuration Globale des Erreurs

```typescript
// plugins/error-handler.ts
import { defineNuxtPlugin } from '#app';
import { ErrorTracker } from '@/services/error-tracker';

export default defineNuxtPlugin((nuxtApp) => {
    nuxtApp.vueApp.config.errorHandler = (error, instance, info) => {
        // Tracking des erreurs
        ErrorTracker.capture(error, {
            componentName: instance?.$options.name,
            info,
            environment: process.client ? 'client' : 'server',
        });

        // Logging différencié client/serveur
        if (process.client) {
            console.error('[Client Error]', error);
        } else {
            console.error('[Server Error]', error);
        }
    };

    // Gestion des erreurs de rendu
    nuxtApp.hook('vue:error', (error) => {
        console.error('[Vue Render Error]', error);
    });

    // Gestion des erreurs non attrapées
    if (process.client) {
        window.onerror = function (msg, source, lineNo, columnNo, error) {
            ErrorTracker.capture(error || msg, {
                source,
                lineNo,
                columnNo,
                type: 'uncaught',
            });
            return false;
        };
    }
});
```

### 🔄 Composant d'Erreur Personnalisé

```typescript
// error.vue
<script lang="ts" setup>
import { useError } from '#app'

const props = defineProps<{
  error: Error & {
    statusCode?: number
    url?: string
  }
}>()

const handleError = () => {
  clearError({ redirect: '/' })
}
</script>

<template>
  <div class="error-page">
    <h1>{{ error.statusCode === 404 ? 'Page non trouvée' : 'Une erreur est survenue' }}</h1>

    <div class="error-details" v-if="error.statusCode !== 404">
      <p>{{ error.message }}</p>
      <code v-if="process.dev">{{ error.stack }}</code>
    </div>

    <div class="actions">
      <button @click="handleError">
        Retour à l'accueil
      </button>
    </div>
  </div>
</template>
```

### 🔄 Middleware avec Gestion d'Erreurs

```typescript
// middleware/error-boundary.ts
import { defineNuxtRouteMiddleware } from '#app';
import { ErrorTracker } from '@/services/error-tracker';

export default defineNuxtRouteMiddleware(async (to, from) => {
    try {
        // Vérifications de sécurité ou autres
        if (!canAccess(to)) {
            throw createError({
                statusCode: 403,
                message: 'Accès non autorisé',
            });
        }
    } catch (error) {
        // Tracking de l'erreur
        ErrorTracker.capture(error, {
            route: to.fullPath,
            fromRoute: from.fullPath,
        });

        // Redirection vers la page d'erreur
        return navigateTo('/error', {
            replace: true,
            query: {
                code: error.statusCode || 500,
            },
        });
    }
});
```

### 🔄 API Routes avec Gestion d'Erreurs

```typescript
// server/api/users.post.ts
import { defineEventHandler, createError } from 'h3';
import { validateUser } from '@/validators/user';

export default defineEventHandler(async (event) => {
    try {
        const body = await readBody(event);

        // Validation
        const validation = await validateUser(body);
        if (!validation.success) {
            throw createError({
                statusCode: 400,
                statusMessage: 'Validation Error',
                data: validation.errors,
            });
        }

        // Création de l'utilisateur
        const user = await createUser(body);

        return { user };
    } catch (error) {
        // Gestion spécifique des erreurs
        if (error.code === 'USER_EXISTS') {
            throw createError({
                statusCode: 409,
                statusMessage: 'User already exists',
            });
        }

        // Rethrow des erreurs système
        throw error;
    }
});
```

# 🏗️ Gestion d'Erreurs dans Différentes Architectures

## 1. Architecture MVC

Dans l'architecture MVC, les erreurs sont gérées à différents niveaux de l'application.

### 📝 Structure de gestion d'erreurs MVC

```typescript
// Models
class UserModel {
    async save(): Promise<Either<DatabaseError, User>> {
        try {
            // Logique de sauvegarde
            return Right(savedUser);
        } catch (error) {
            return Left(new DatabaseError('Erreur de sauvegarde', error));
        }
    }
}

// Controllers
class UserController {
    constructor(private userService: UserService) {}

    async createUser(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const result = await this.userService.createUser(req.body);

            result.fold(
                (error) => next(error),
                (user) => res.status(201).json(user),
            );
        } catch (error) {
            next(error);
        }
    }
}

// Views/Middleware
function errorMiddleware(error: Error, req: Request, res: Response, next: NextFunction): void {
    if (error instanceof ValidationError) {
        res.status(400).json({
            type: 'ValidationError',
            message: error.message,
            errors: error.errors,
        });
        return;
    }

    res.status(500).json({
        type: 'ServerError',
        message: 'Erreur interne du serveur',
    });
}
```

## 2. Architecture Hexagonale (Ports & Adapters)

L'architecture hexagonale sépare clairement la gestion d'erreurs par domaine.

### 📝 Structure de gestion d'erreurs hexagonale

```typescript
// Domain (Core)
export class DomainError extends Error {
    constructor(message: string) {
        super(message);
        this.name = 'DomainError';
    }
}

// Application Layer
export class ApplicationError extends Error {
    constructor(message: string, public readonly cause?: Error) {
        super(message);
        this.name = 'ApplicationError';
    }
}

// Infrastructure Layer
export class InfrastructureError extends Error {
    constructor(message: string, public readonly cause?: Error) {
        super(message);
        this.name = 'InfrastructureError';
    }
}

// Ports (Interfaces)
interface UserRepository {
    save(user: User): Promise<Either<RepositoryError, User>>;
}

// Adapters (Implementations)
class PostgresUserRepository implements UserRepository {
    async save(user: User): Promise<Either<RepositoryError, User>> {
        try {
            const result = await this.client.query('INSERT INTO users (name, email) VALUES ($1, $2) RETURNING *', [
                user.name,
                user.email,
            ]);
            return Right(this.toDomain(result.rows[0]));
        } catch (error) {
            return Left(new RepositoryError('Erreur de sauvegarde', error));
        }
    }
}
```

## 3. Clean Architecture

La Clean Architecture nécessite une gestion d'erreurs stricte à chaque niveau.

### 📝 Structure de gestion d'erreurs Clean Architecture

```typescript
// Entities (Enterprise Business Rules)
class UserEntity {
    static create(data: UserData): Either<DomainError, UserEntity> {
        if (!this.isValidEmail(data.email)) {
            return Left(new DomainError('Email invalide'));
        }
        return Right(new UserEntity(data));
    }
}

// Use Cases (Application Business Rules)
class CreateUserUseCase implements UseCase<CreateUserDTO, User> {
    constructor(private userRepository: UserRepository, private emailService: EmailService) {}

    async execute(dto: CreateUserDTO): Promise<Either<ApplicationError, User>> {
        return await pipe(
            UserEntity.create(dto),
            flatMap((user) => this.userRepository.save(user)),
            flatMap((user) => this.emailService.sendWelcomeEmail(user)),
        );
    }
}

// Interface Adapters
class UserController {
    constructor(private createUser: CreateUserUseCase) {}

    async handle(req: Request, res: Response): Promise<void> {
        const result = await this.createUser.execute(req.body);

        result.fold(
            (error) => {
                if (error instanceof ValidationError) {
                    res.status(400).json({ error: error.message });
                } else {
                    res.status(500).json({ error: 'Erreur interne' });
                }
            },
            (user) => res.status(201).json(user),
        );
    }
}
```

## 4. Event-Driven Architecture

Dans une architecture événementielle, la gestion d'erreurs doit tenir compte des événements asynchrones.

### 📝 Structure de gestion d'erreurs événementielle

```typescript
// Event Definitions
interface ErrorEvent {
    type: 'error';
    error: Error;
    context: unknown;
    timestamp: Date;
}

// Error Event Handler
class ErrorEventHandler implements EventHandler<ErrorEvent> {
    constructor(private logger: Logger, private metrics: MetricsService, private notifier: NotificationService) {}

    async handle(event: ErrorEvent): Promise<void> {
        // Log l'erreur
        await this.logger.error(event.error, {
            context: event.context,
            timestamp: event.timestamp,
        });

        // Enregistre les métriques
        await this.metrics.incrementError(event.error.name);

        // Notifie si nécessaire
        if (this.shouldNotify(event.error)) {
            await this.notifier.sendAlert({
                title: `Erreur: ${event.error.name}`,
                message: event.error.message,
                severity: this.getSeverity(event.error),
            });
        }
    }
}
```

## 5. CQRS (Command Query Responsibility Segregation)

CQRS nécessite une gestion d'erreurs distincte pour les commandes et les requêtes.

### 📝 Structure de gestion d'erreurs CQRS

```typescript
// Command Errors
class CommandError extends Error {
    constructor(message: string, public readonly commandName: string, public readonly params: unknown) {
        super(message);
        this.name = 'CommandError';
    }
}

// Query Errors
class QueryError extends Error {
    constructor(message: string, public readonly queryName: string, public readonly params: unknown) {
        super(message);
        this.name = 'QueryError';
    }
}

// Command Handler
class CreateUserCommandHandler implements CommandHandler<CreateUserCommand> {
    async execute(command: CreateUserCommand): Promise<Either<CommandError, void>> {
        try {
            await this.userRepository.save(command.user);
            return Right(undefined);
        } catch (error) {
            return Left(new CommandError("Échec de cr��ation de l'utilisateur", 'CreateUser', command));
        }
    }
}

// Query Handler
class GetUserQueryHandler implements QueryHandler<GetUserQuery, User> {
    async execute(query: GetUserQuery): Promise<Either<QueryError, User>> {
        try {
            const user = await this.userRepository.findById(query.userId);
            if (!user) {
                return Left(new QueryError('Utilisateur non trouvé', 'GetUser', query));
            }
            return Right(user);
        } catch (error) {
            return Left(new QueryError("Erreur de récupération de l'utilisateur", 'GetUser', query));
        }
    }
}
```

## 6. Microservices Architecture

Dans une architecture microservices, la gestion d'erreurs doit prendre en compte la communication inter-services et la résilience.

### 📝 Structure de gestion d'erreurs microservices

```typescript
// Circuit Breaker Pattern
class CircuitBreaker {
    private failures = 0;
    private lastFailure?: Date;
    private readonly threshold = 5;
    private readonly resetTimeout = 60000; // 1 minute

    async execute<T>(operation: () => Promise<T>): Promise<Either<ServiceError, T>> {
        if (this.isOpen()) {
            return Left(new ServiceError('Circuit breaker ouvert'));
        }

        try {
            const result = await operation();
            this.reset();
            return Right(result);
        } catch (error) {
            this.recordFailure();
            return Left(new ServiceError('Erreur service', error instanceof Error ? error : new Error(String(error))));
        }
    }

    private isOpen(): boolean {
        if (this.failures >= this.threshold) {
            const now = new Date();
            if (this.lastFailure && now.getTime() - this.lastFailure.getTime() > this.resetTimeout) {
                this.reset();
                return false;
            }
            return true;
        }
        return false;
    }

    private reset(): void {
        this.failures = 0;
        this.lastFailure = undefined;
    }

    private recordFailure(): void {
        this.failures++;
        this.lastFailure = new Date();
    }
}
```

## 7. Serverless Architecture

La gestion d'erreurs dans une architecture serverless doit être adaptée aux contraintes spécifiques des fonctions cloud.

### 📝 Structure de gestion d'erreurs serverless

```typescript
// AWS Lambda handler avec gestion d'erreurs
import { APIGatewayProxyHandler } from 'aws-lambda';
import { ErrorReporter } from './services/error-reporter';

interface LambdaError extends Error {
    statusCode: number;
    details?: unknown;
}

class LambdaHandlerError extends Error implements LambdaError {
    constructor(message: string, public readonly statusCode: number, public readonly details?: unknown) {
        super(message);
        this.name = 'LambdaHandlerError';
    }
}

export const handler: APIGatewayProxyHandler = async (event, context) => {
    try {
        // Configuration du timeout
        context.callbackWaitsForEmptyEventLoop = false;

        // Traitement de la requête
        const result = await processEvent(event);

        return {
            statusCode: 200,
            body: JSON.stringify(result),
        };
    } catch (error) {
        // Reporting de l'erreur
        await ErrorReporter.captureError(error, {
            requestId: context.awsRequestId,
            event,
        });

        if (error instanceof LambdaHandlerError) {
            return {
                statusCode: error.statusCode,
                body: JSON.stringify({
                    message: error.message,
                    details: error.details,
                }),
            };
        }

        // Erreur générique
        return {
            statusCode: 500,
            body: JSON.stringify({
                message: 'Erreur interne du serveur',
            }),
        };
    }
};
```

## 8. Domain-Driven Design (DDD)

DDD nécessite une gestion d'erreurs qui respecte les limites des contextes et les règles métier.

### 📝 Structure de gestion d'erreurs DDD

```typescript
// Domain Errors
namespace DomainErrors {
    export class InvalidEntityState extends Error {
        constructor(entity: string, reason: string) {
            super(`Invalid ${entity} state: ${reason}`);
            this.name = 'InvalidEntityState';
        }
    }

    export class BusinessRuleViolation extends Error {
        constructor(rule: string, reason: string) {
            super(`Business rule "${rule}" violated: ${reason}`);
            this.name = 'BusinessRuleViolation';
        }
    }

    export class AggregateNotFound extends Error {
        constructor(aggregate: string, id: string) {
            super(`${aggregate} with id ${id} not found`);
            this.name = 'AggregateNotFound';
        }
    }
}

// Domain Service avec gestion d'erreurs
class OrderService {
    constructor(private readonly orderRepository: OrderRepository, private readonly paymentService: PaymentService) {}

    async placeOrder(order: Order): Promise<Either<DomainErrors.BusinessRuleViolation, OrderId>> {
        // Vérification des règles métier
        const validationResult = order.validate();
        if (validationResult.isLeft()) {
            return Left(new DomainErrors.BusinessRuleViolation('OrderValidation', validationResult.value.message));
        }

        // Vérification du stock
        const stockResult = await this.checkStock(order);
        if (stockResult.isLeft()) {
            return Left(new DomainErrors.BusinessRuleViolation('StockAvailability', stockResult.value.message));
        }

        // Sauvegarde et retour
        return Right(await this.orderRepository.save(order));
    }
}
```

## 9. Reactive Architecture

Une architecture réactive nécessite une gestion d'erreurs adaptée aux flux de données et aux événements.

### 📝 Structure de gestion d'erreurs réactive

```typescript
import { Observable, catchError, retry, throwError } from 'rxjs';

// Gestionnaire d'erreurs réactif
class ReactiveErrorHandler {
    static handle<T>(source: Observable<T>, retryCount = 3): Observable<T> {
        return source.pipe(
            retry(retryCount),
            catchError((error) => {
                if (error instanceof NetworkError) {
                    // Retry avec backoff exponentiel
                    return source.pipe(retryWhen((errors) => errors.pipe(delay(1000), take(3))));
                }

                if (error instanceof ValidationError) {
                    // Erreur de validation - pas de retry
                    return throwError(() => error);
                }

                // Autres erreurs - log et retry limité
                console.error('Erreur non gérée:', error);
                return source.pipe(
                    retry(1),
                    catchError(() => throwError(() => error)),
                );
            }),
        );
    }
}

// Utilisation
class UserService {
    getUser(id: string): Observable<User> {
        return this.http.get<User>(`/api/users/${id}`).pipe(ReactiveErrorHandler.handle);
    }
}
```
