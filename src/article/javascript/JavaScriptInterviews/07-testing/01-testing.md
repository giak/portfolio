---
title: Les Tests en JavaScript
date: 2024-10-29
icon: code
category:
    - JavaScript
tags:
    - tests
    - qualité
description: Un guide sur les tests en JavaScript, y compris les tests unitaires et d'intégration.
---

# Les Tests en JavaScript

## Introduction

Les tests sont essentiels pour garantir la qualité et la maintenabilité du code. JavaScript dispose d'un riche écosystème d'outils et de frameworks de test. Nous allons explorer les différents types de tests et les meilleures pratiques.

## Types de Tests

### Tests Unitaires

```javascript
// mathUtils.js
export function add(a, b) {
    return a + b;
}

export function multiply(a, b) {
    return a * b;
}

// mathUtils.test.js
import { add, multiply } from './mathUtils';

describe('Math Functions', () => {
    test('adding two numbers', () => {
        expect(add(2, 3)).toBe(5);
        expect(add(-1, 1)).toBe(0);
        expect(add(0, 0)).toBe(0);
    });

    test('multiplying two numbers', () => {
        expect(multiply(2, 3)).toBe(6);
        expect(multiply(-2, 3)).toBe(-6);
        expect(multiply(0, 5)).toBe(0);
    });
});
```

### Tests d'Intégration

```javascript
// userService.js
export class UserService {
    constructor(database) {
        this.database = database;
    }

    async createUser(userData) {
        const existingUser = await this.database.findByEmail(userData.email);
        if (existingUser) {
            throw new Error('User already exists');
        }
        return this.database.create(userData);
    }
}

// userService.test.js
import { UserService } from './userService';
import { Database } from './database';

describe('UserService - Integration', () => {
    let userService;
    let database;

    beforeEach(() => {
        database = new Database();
        userService = new UserService(database);
    });

    test('creating a new user', async () => {
        const userData = {
            email: 'test@example.com',
            name: 'Test User',
        };

        const user = await userService.createUser(userData);
        expect(user).toMatchObject(userData);

        const savedUser = await database.findByEmail(userData.email);
        expect(savedUser).toMatchObject(userData);
    });
});
```

### Tests End-to-End (E2E)

```javascript
// Example with Cypress
describe('Login Page', () => {
    beforeEach(() => {
        cy.visit('/login');
    });

    it('successful login', () => {
        cy.get('[data-test="email"]').type('user@example.com');
        cy.get('[data-test="password"]').type('password123');
        cy.get('[data-test="submit"]').click();

        cy.url().should('include', '/dashboard');
        cy.get('[data-test="welcome-message"]').should('contain', 'Welcome');
    });

    it('displays error for invalid credentials', () => {
        cy.get('[data-test="email"]').type('invalid@example.com');
        cy.get('[data-test="password"]').type('wrongpassword');
        cy.get('[data-test="submit"]').click();

        cy.get('[data-test="error-message"]').should('be.visible').and('contain', 'Invalid credentials');
    });
});
```

## Mocks et Stubs

### Mocking de Fonctions

```javascript
// paymentService.js
export class PaymentService {
    async processPayment(amount) {
        // Call to external API
        const response = await api.processPayment(amount);
        return response.success;
    }
}

// paymentService.test.js
import { PaymentService } from './paymentService';
import { api } from './api';

jest.mock('./api');

describe('PaymentService', () => {
    let paymentService;

    beforeEach(() => {
        paymentService = new PaymentService();
        jest.clearAllMocks();
    });

    test('successful payment processing', async () => {
        api.processPayment.mockResolvedValue({ success: true });

        const result = await paymentService.processPayment(100);
        expect(result).toBe(true);
        expect(api.processPayment).toHaveBeenCalledWith(100);
    });

    test('failed payment processing', async () => {
        api.processPayment.mockRejectedValue(new Error('Payment rejected'));

        await expect(paymentService.processPayment(100)).rejects.toThrow('Payment rejected');
    });
});
```

### Stubs pour les Tests d'Intégration

```javascript
// database.test.js
describe('Database Integration', () => {
    const dbStub = {
        connect: jest.fn(),
        query: jest.fn(),
        disconnect: jest.fn(),
    };

    beforeEach(() => {
        dbStub.connect.mockResolvedValue(true);
        dbStub.query.mockResolvedValue([]);
        dbStub.disconnect.mockResolvedValue(true);
    });

    test('executing a query', async () => {
        const query = 'SELECT * FROM users';
        await dbStub.query(query);

        expect(dbStub.connect).toHaveBeenCalled();
        expect(dbStub.query).toHaveBeenCalledWith(query);
        expect(dbStub.disconnect).toHaveBeenCalled();
    });
});
```

## Tests Asynchrones

### Promesses et Async/Await

```javascript
// asyncService.test.js
describe('Asynchronous Tests', () => {
    test('with Promise', () => {
        return fetchData().then((data) => {
            expect(data).toBeDefined();
        });
    });

    test('with async/await', async () => {
        const data = await fetchData();
        expect(data).toBeDefined();
    });

    test('error handling', async () => {
        await expect(fetchInvalidData()).rejects.toThrow('Data not found');
    });
});
```

### Timeouts et Intervalles

```javascript
// timerService.test.js
describe('Timer Tests', () => {
    beforeEach(() => {
        jest.useFakeTimers();
    });

    afterEach(() => {
        jest.useRealTimers();
    });

    test('execution after delay', () => {
        const callback = jest.fn();
        setTimeout(callback, 1000);

        expect(callback).not.toBeCalled();
        jest.advanceTimersByTime(1000);
        expect(callback).toBeCalled();
    });

    test('periodic execution', () => {
        const callback = jest.fn();
        setInterval(callback, 1000);

        expect(callback).not.toBeCalled();
        jest.advanceTimersByTime(3000);
        expect(callback).toHaveBeenCalledTimes(3);
    });
});
```

## Couverture de Tests

### Configuration de la Couverture

```javascript
// jest.config.js
module.exports = {
    collectCoverage: true,
    coverageDirectory: 'coverage',
    coverageThreshold: {
        global: {
            branches: 80,
            functions: 80,
            lines: 80,
            statements: 80,
        },
    },
    collectCoverageFrom: ['src/**/*.js', '!src/**/*.test.js', '!src/index.js'],
};
```

### Rapport de Couverture

```javascript
// Exemple de rapport de couverture
/*
----------------------------------|---------|----------|---------|---------|
File                              | % Stmts | % Branch | % Funcs | % Lines |
----------------------------------|---------|----------|---------|---------|
All files                         |   85.71 |    83.33 |   88.89 |   85.71 |
 src/                             |   100.0 |    100.0 |   100.0 |   100.0 |
  index.js                        |   100.0 |    100.0 |   100.0 |   100.0 |
 src/services/                    |   81.82 |     75.0 |   85.71 |   81.82 |
  userService.js                  |   85.71 |     80.0 |   88.89 |   85.71 |
  authService.js                  |   77.78 |     70.0 |   83.33 |   77.78 |
----------------------------------|---------|----------|---------|---------|
*/
```

## Bonnes Pratiques

1. **Organisation des Tests**

    - Un fichier de test par module
    - Regroupement logique des tests
    - Description claire des cas de test
    - Isolation des tests

2. **Nommage et Structure**

    - Noms descriptifs pour les tests
    - Structure AAA (Arrange-Act-Assert)
    - Tests indépendants
    - Éviter la duplication de code

3. **Assertions**

    - Assertions précises et ciblées
    - Vérification des cas limites
    - Tests des cas d'erreur
    - Utilisation appropriée des matchers

4. **Performance**

    - Mocking des dépendances externes
    - Optimisation des tests lents
    - Parallélisation des tests
    - Gestion efficace des ressources

## Exemples Pratiques

### Test d'une API REST

```javascript
// api.test.js
describe('API REST', () => {
    let server;

    beforeAll(async () => {
        server = await startServer();
    });

    afterAll(async () => {
        await server.close();
    });

    test('GET /users', async () => {
        const response = await request(server).get('/api/users').set('Accept', 'application/json');

        expect(response.status).toBe(200);
        expect(response.body).toBeInstanceOf(Array);
    });

    test('POST /users', async () => {
        const userData = {
            name: 'John Doe',
            email: 'john@example.com',
        };

        const response = await request(server).post('/api/users').send(userData).set('Accept', 'application/json');

        expect(response.status).toBe(201);
        expect(response.body).toMatchObject(userData);
    });
});
```

### Test de Composants React

```javascript
// Button.test.js
import { render, fireEvent } from '@testing-library/react';
import Button from './Button';

describe('Button Component', () => {
    test('rendu du bouton', () => {
        const { getByText } = render(<Button>Cliquez-moi</Button>);

        expect(getByText('Cliquez-moi')).toBeInTheDocument();
    });

    test('gestion du clic', () => {
        const handleClick = jest.fn();
        const { getByText } = render(<Button onClick={handleClick}>Cliquez-moi</Button>);

        fireEvent.click(getByText('Cliquez-moi'));
        expect(handleClick).toHaveBeenCalledTimes(1);
    });

    test('état désactivé', () => {
        const { getByText } = render(<Button disabled>Cliquez-moi</Button>);

        const button = getByText('Cliquez-moi');
        expect(button).toBeDisabled();
        expect(button).toHaveClass('disabled');
    });
});
```
