---
title: Déploiement et Optimisation en Production
date: 2024-10-29
icon: code
category:
    - JavaScript
tags:
    - déploiement
    - optimisation
description: Un guide sur le déploiement et l'optimisation des applications JavaScript en production.
---

# Déploiement et Optimisation en Production

## Introduction

Le déploiement et l'optimisation d'applications JavaScript en production sont des aspects critiques du développement web moderne. Cette section couvre les meilleures pratiques, les outils et les stratégies pour assurer des performances optimales en production.

## Build et Bundling

### Configuration Webpack pour la Production

```javascript
// webpack.config.prod.js
const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    mode: 'production',
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].[contenthash].js',
        clean: true,
    },
    optimization: {
        minimizer: [new TerserPlugin(), new CssMinimizerPlugin()],
        splitChunks: {
            chunks: 'all',
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: 'all',
                },
            },
        },
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [['@babel/preset-env', { modules: false }]],
                        plugins: ['@babel/plugin-transform-runtime'],
                    },
                },
            },
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader'],
            },
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'styles.[contenthash].css',
        }),
    ],
};
```

### Configuration Vite pour la Production

```javascript
// vite.config.js
import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import { visualizer } from 'rollup-plugin-visualizer';

export default defineConfig({
    plugins: [react()],
    build: {
        sourcemap: true,
        rollupOptions: {
            output: {
                manualChunks: {
                    vendor: ['react', 'react-dom'],
                    utils: ['lodash', 'date-fns'],
                },
            },
        },
        terserOptions: {
            compress: {
                drop_console: true,
                drop_debugger: true,
            },
        },
    },
    plugins: [
        visualizer({
            filename: './dist/stats.html',
            open: true,
        }),
    ],
});
```

## Optimisation des Performances

### Lazy Loading

```javascript
// Route configuration avec React Router et lazy loading
import { Suspense, lazy } from 'react';
import { Routes, Route } from 'react-router-dom';

const Home = lazy(() => import('./pages/Home'));
const Dashboard = lazy(() => import('./pages/Dashboard'));
const Profile = lazy(() => import('./pages/Profile'));

function App() {
    return (
        <Suspense fallback={<LoadingSpinner />}>
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/dashboard" element={<Dashboard />} />
                <Route path="/profile" element={<Profile />} />
            </Routes>
        </Suspense>
    );
}
```

### Optimisation des Images

```javascript
// Composant d'image optimisé
function OptimizedImage({ src, alt, sizes }) {
    return (
        <picture>
            <source
                type="image/webp"
                srcSet={`
                    ${src.replace(/\.(jpg|png)$/, '.webp')} 1x,
                    ${src.replace(/\.(jpg|png)$/, '@2x.webp')} 2x
                `}
                sizes={sizes}
            />
            <source
                srcSet={`
                    ${src} 1x,
                    ${src.replace(/\.(jpg|png)$/, '@2x.$1')} 2x
                `}
                sizes={sizes}
            />
            <img
                src={src}
                alt={alt}
                loading="lazy"
                decoding="async"
                onLoad={(e) => {
                    e.target.classList.add('loaded');
                }}
            />
        </picture>
    );
}
```

## Déploiement

### Configuration CI/CD avec GitHub Actions

```yaml
# .github/workflows/deploy.yml
name: Deploy

on:
    push:
        branches: [main]

jobs:
    build-and-deploy:
        runs-on: ubuntu-latest
        steps:
            - uses: actions/checkout@v2

            - name: Setup Node.js
              uses: actions/setup-node@v2
              with:
                  node-version: '16'
                  cache: 'npm'

            - name: Install dependencies
              run: npm ci

            - name: Run tests
              run: npm test

            - name: Build
              run: npm run build
              env:
                  VITE_API_URL: ${{ secrets.VITE_API_URL }}

            - name: Deploy to production
              uses: cloudflare/wrangler-action@2.0.0
              with:
                  apiToken: ${{ secrets.CF_API_TOKEN }}
                  environment: production
```

### Configuration Docker

```dockerfile
# Dockerfile
# Étape de build
FROM node:16-alpine as builder
WORKDIR /app
COPY package*.json ./
RUN npm ci
COPY . .
RUN npm run build

# Étape de production
FROM nginx:alpine
COPY --from=builder /app/dist /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

# nginx.conf
server {
    listen 80;
    server_name _;
    root /usr/share/nginx/html;
    index index.html;

    location / {
        try_files $uri $uri/ /index.html;
    }

    location /assets/ {
        expires 1y;
        add_header Cache-Control "public, no-transform";
    }
}
```

## Monitoring et Analytics

### Configuration de la Performance Monitoring

```javascript
// performance-monitoring.js
class PerformanceMonitor {
    constructor() {
        this.metrics = {};
        this.initObservers();
    }

    initObservers() {
        // Core Web Vitals
        new PerformanceObserver((list) => {
            for (const entry of list.getEntries()) {
                this.metrics[entry.name] = entry.value;
                this.reportMetric(entry);
            }
        }).observe({ entryTypes: ['largest-contentful-paint', 'first-input', 'layout-shift'] });

        // Navigation Timing
        window.addEventListener('load', () => {
            setTimeout(() => {
                const navigation = performance.getEntriesByType('navigation')[0];
                this.metrics.ttfb = navigation.responseStart - navigation.requestStart;
                this.metrics.domLoad = navigation.domContentLoadedEventEnd - navigation.requestStart;
                this.reportMetrics();
            }, 0);
        });
    }

    reportMetric(entry) {
        // Envoi à votre service d'analytics
        fetch('/api/metrics', {
            method: 'POST',
            body: JSON.stringify({
                metric: entry.name,
                value: entry.value,
                timestamp: Date.now(),
            }),
        });
    }

    reportMetrics() {
        console.log('Performance Metrics:', this.metrics);
    }
}

// Initialisation
new PerformanceMonitor();
```

## Bonnes Pratiques

1. **Build et Bundling**

    - Utiliser la minification et la compression
    - Implémenter le code splitting
    - Optimiser les dépendances
    - Générer des sourcemaps

2. **Optimisation**

    - Mettre en cache les ressources statiques
    - Utiliser le lazy loading
    - Optimiser les images et médias
    - Implémenter la précharge

3. **Déploiement**

    - Automatiser avec CI/CD
    - Utiliser des conteneurs
    - Implémenter des rollbacks
    - Surveiller les performances

4. **Sécurité**

    - Configurer les en-têtes HTTP
    - Implémenter le CSP
    - Utiliser HTTPS
    - Scanner les vulnérabilités

## Exemples Pratiques

### Script de Déploiement Automatisé

```javascript
// deploy.js
const { exec } = require('child_process');
const fs = require('fs').promises;

async function deploy() {
    try {
        // Vérification de l'environnement
        const branch = await execCommand('git rev-parse --abbrev-ref HEAD');
        if (branch !== 'main') {
            throw new Error('Le déploiement doit être fait depuis la branche main');
        }

        // Tests
        console.log('Exécution des tests...');
        await execCommand('npm test');

        // Build
        console.log('Build de l'application...');
        await execCommand('npm run build');

        // Sauvegarde
        const timestamp = new Date().toISOString().replace(/[:.]/g, '-');
        await execCommand(`tar -czf backup-${timestamp}.tar.gz dist`);

        // Déploiement
        console.log('Déploiement...');
        await execCommand('rsync -avz --delete dist/ user@server:/var/www/app/');

        console.log('Déploiement réussi!');
    } catch (error) {
        console.error('Erreur lors du déploiement:', error);
        process.exit(1);
    }
}

function execCommand(command) {
    return new Promise((resolve, reject) => {
        exec(command, (error, stdout, stderr) => {
            if (error) {
                reject(error);
                return;
            }
            resolve(stdout.trim());
        });
    });
}

deploy();
```

### Configuration de Cache Optimisée

```javascript
// cache-config.js
module.exports = {
    cacheConfig: {
        // Ressources statiques
        static: {
            maxAge: '1y',
            immutable: true,
            patterns: ['assets/**/*', '*.{js,css}', 'favicon.ico'],
        },
        // HTML et API
        dynamic: {
            maxAge: '15m',
            staleWhileRevalidate: '1h',
            patterns: ['/', '/**/*.html', '/api/**/*'],
        },
        // Stratégies spécifiques
        strategies: {
            images: {
                maxAge: '7d',
                patterns: ['**/*.{jpg,png,webp,svg}'],
            },
            fonts: {
                maxAge: '1y',
                patterns: ['**/*.{woff,woff2,ttf,eot}'],
            },
        },
    },
};
```
