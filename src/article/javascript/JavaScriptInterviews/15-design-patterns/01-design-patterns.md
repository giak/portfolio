---
title: Les Design Patterns en JavaScript
date: 2024-10-29
icon: code
category:
    - JavaScript
tags:
    - design patterns
    - bonnes pratiques
description: Un guide sur les design patterns en JavaScript, y compris les patterns de création et de structure.
---

# Les Design Patterns en JavaScript

## Introduction

Les design patterns sont des solutions éprouvées à des problèmes courants en programmation. En JavaScript, ils nous aident à écrire du code plus maintenable, réutilisable et élégant. Cet article explore les principaux patterns et leur implémentation.

## Patterns de Création

### Singleton

```javascript
class Database {
    static #instance;

    constructor() {
        if (Database.#instance) {
            return Database.#instance;
        }
        this.connections = new Map();
        Database.#instance = this;
    }

    connect(url) {
        if (!this.connections.has(url)) {
            this.connections.set(url, new Connection(url));
        }
        return this.connections.get(url);
    }

    static getInstance() {
        if (!Database.#instance) {
            Database.#instance = new Database();
        }
        return Database.#instance;
    }
}

// Utilisation
const db1 = Database.getInstance();
const db2 = Database.getInstance();
console.log(db1 === db2); // true
```

### Factory

```javascript
// Interface commune
class Vehicle {
    constructor(options) {
        this.brand = options.brand;
        this.model = options.model;
    }

    getInfo() {
        return `${this.brand} ${this.model}`;
    }
}

// Types spécifiques
class Car extends Vehicle {
    constructor(options) {
        super(options);
        this.doors = options.doors;
    }
}

class Motorcycle extends Vehicle {
    constructor(options) {
        super(options);
        this.type = options.type;
    }
}

// Factory
class VehicleFactory {
    static createVehicle(type, options) {
        switch (type) {
            case 'car':
                return new Car(options);
            case 'motorcycle':
                return new Motorcycle(options);
            default:
                throw new Error(`Type de véhicule non supporté: ${type}`);
        }
    }
}

// Utilisation
const car = VehicleFactory.createVehicle('car', {
    brand: 'Tesla',
    model: 'Model 3',
    doors: 4,
});
```

### Builder

```javascript
class QueryBuilder {
    constructor() {
        this.query = {
            select: ['*'],
            from: '',
            where: [],
            orderBy: [],
        };
    }

    select(fields) {
        this.query.select = fields;
        return this;
    }

    from(table) {
        this.query.from = table;
        return this;
    }

    where(condition) {
        this.query.where.push(condition);
        return this;
    }

    orderBy(field, direction = 'ASC') {
        this.query.orderBy.push({ field, direction });
        return this;
    }

    build() {
        let sql = `SELECT ${this.query.select.join(', ')} FROM ${this.query.from}`;

        if (this.query.where.length) {
            sql += ` WHERE ${this.query.where.join(' AND ')}`;
        }

        if (this.query.orderBy.length) {
            sql += ` ORDER BY ${this.query.orderBy.map((order) => `${order.field} ${order.direction}`).join(', ')}`;
        }

        return sql;
    }
}

// Utilisation
const query = new QueryBuilder().select(['name', 'email']).from('users').where('age > 18').orderBy('name').build();
```

## Patterns Structurels

### Adapter

```javascript
// Interface existante
class OldAPI {
    getUsers() {
        return [
            { name: 'John', age: 30 },
            { name: 'Jane', age: 25 },
        ];
    }
}

// Nouvelle interface souhaitée
class NewAPI {
    fetchUsers() {
        return Promise.resolve([
            { firstName: 'John', yearOfBirth: 1993 },
            { firstName: 'Jane', yearOfBirth: 1998 },
        ]);
    }
}

// Adapter
class APIAdapter {
    constructor(oldAPI) {
        this.oldAPI = oldAPI;
    }

    async fetchUsers() {
        const users = this.oldAPI.getUsers();
        return users.map((user) => ({
            firstName: user.name,
            yearOfBirth: new Date().getFullYear() - user.age,
        }));
    }
}

// Utilisation
const oldAPI = new OldAPI();
const adapter = new APIAdapter(oldAPI);
adapter.fetchUsers().then(console.log);
```

### Decorator

```javascript
// Composant de base
class Coffee {
    cost() {
        return 3;
    }

    description() {
        return 'Café simple';
    }
}

// Décorateurs
class MilkDecorator {
    constructor(coffee) {
        this.coffee = coffee;
    }

    cost() {
        return this.coffee.cost() + 0.5;
    }

    description() {
        return `${this.coffee.description()} avec lait`;
    }
}

class SugarDecorator {
    constructor(coffee) {
        this.coffee = coffee;
    }

    cost() {
        return this.coffee.cost() + 0.2;
    }

    description() {
        return `${this.coffee.description()} avec sucre`;
    }
}

// Utilisation
let coffee = new Coffee();
coffee = new MilkDecorator(coffee);
coffee = new SugarDecorator(coffee);

console.log(coffee.description()); // "Café simple avec lait avec sucre"
console.log(coffee.cost()); // 3.7
```

## Patterns Comportementaux

### Observer

```javascript
class EventEmitter {
    constructor() {
        this.events = new Map();
    }

    on(event, callback) {
        if (!this.events.has(event)) {
            this.events.set(event, []);
        }
        this.events.get(event).push(callback);
    }

    emit(event, data) {
        if (this.events.has(event)) {
            this.events.get(event).forEach((callback) => callback(data));
        }
    }

    off(event, callback) {
        if (this.events.has(event)) {
            const callbacks = this.events.get(event);
            const index = callbacks.indexOf(callback);
            if (index > -1) {
                callbacks.splice(index, 1);
            }
        }
    }
}

// Utilisation
const emitter = new EventEmitter();

const onUserCreated = (user) => console.log('Nouvel utilisateur:', user);
emitter.on('userCreated', onUserCreated);

emitter.emit('userCreated', { id: 1, name: 'John' });
```

### Strategy

```javascript
// Stratégies de paiement
class PayPalPayment {
    pay(amount) {
        return `Paiement de ${amount}€ via PayPal`;
    }
}

class CreditCardPayment {
    pay(amount) {
        return `Paiement de ${amount}€ par carte de crédit`;
    }
}

class CryptoPayment {
    pay(amount) {
        return `Paiement de ${amount}€ en crypto-monnaie`;
    }
}

// Contexte
class PaymentProcessor {
    constructor(paymentStrategy) {
        this.paymentStrategy = paymentStrategy;
    }

    setStrategy(paymentStrategy) {
        this.paymentStrategy = paymentStrategy;
    }

    processPayment(amount) {
        return this.paymentStrategy.pay(amount);
    }
}

// Utilisation
const processor = new PaymentProcessor(new PayPalPayment());
console.log(processor.processPayment(100));

processor.setStrategy(new CreditCardPayment());
console.log(processor.processPayment(200));
```

## Bonnes Pratiques

1. **Choix du Pattern**

    - Identifier le problème avant de choisir un pattern
    - Ne pas surcharger l'application avec des patterns inutiles
    - Considérer la maintenabilité et la complexité

2. **Implémentation**

    - Garder les implémentations simples
    - Documenter l'utilisation des patterns
    - Suivre les principes SOLID

3. **Performance**

    - Évaluer l'impact sur les performances
    - Optimiser les patterns critiques
    - Éviter la sur-ingénierie

4. **Maintenance**

    - Documenter les raisons du choix du pattern
    - Maintenir la cohérence dans l'utilisation
    - Prévoir l'évolution du code

## Exemples Pratiques

### Gestionnaire de Cache avec Proxy

```javascript
const cacheHandler = {
    cache: new Map(),

    get(target, property) {
        if (this.cache.has(property)) {
            console.log(`Récupération depuis le cache: ${property}`);
            return this.cache.get(property);
        }

        const value = target[property];
        this.cache.set(property, value);
        console.log(`Mise en cache: ${property}`);
        return value;
    },

    set(target, property, value) {
        console.log(`Mise à jour du cache: ${property}`);
        this.cache.set(property, value);
        target[property] = value;
        return true;
    },
};

const data = {
    user: { id: 1, name: 'John' },
    settings: { theme: 'dark' },
};

const proxiedData = new Proxy(data, cacheHandler);
```

### Gestionnaire d'État avec Observer

```javascript
class Store {
    constructor(initialState = {}) {
        this.state = initialState;
        this.observers = new Map();
    }

    subscribe(key, callback) {
        if (!this.observers.has(key)) {
            this.observers.set(key, new Set());
        }
        this.observers.get(key).add(callback);
    }

    setState(key, value) {
        this.state[key] = value;
        if (this.observers.has(key)) {
            this.observers.get(key).forEach((callback) => callback(value));
        }
    }

    getState(key) {
        return this.state[key];
    }
}

// Utilisation
const store = new Store({
    user: null,
    theme: 'light',
});

store.subscribe('user', (user) => {
    console.log('Utilisateur mis à jour:', user);
});

store.subscribe('theme', (theme) => {
    document.body.className = theme;
});

store.setState('user', { id: 1, name: 'John' });
store.setState('theme', 'dark');
```
