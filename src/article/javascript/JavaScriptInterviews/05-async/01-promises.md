---
title: Les Promesses et la Programmation Asynchrone en JavaScript
date: 2024-10-29
icon: code
category:
    - JavaScript
tags:
    - promesses
    - asynchrone
description: Un guide sur les promesses en JavaScript et leur utilisation pour la programmation asynchrone.
---

# Les Promesses et la Programmation Asynchrone en JavaScript

## Introduction

La programmation asynchrone est essentielle en JavaScript pour gérer les opérations qui prennent du temps (requêtes réseau, lectures de fichiers, etc.) sans bloquer l'exécution du programme. Les promesses sont un mécanisme moderne pour gérer l'asynchrone de manière élégante.

## Les Promesses

### Création d'une Promesse

```javascript
// Create a simple promise
const promise = new Promise((resolve, reject) => {
    // Simulating an asynchronous operation
    setTimeout(() => {
        const success = true;
        if (success) {
            resolve('Operation successful');
        } else {
            reject(new Error('Operation failed'));
        }
    }, 1000);
});

// Using the promise
promise.then((result) => console.log(result)).catch((error) => console.error(error));
```

### États d'une Promesse

```javascript
// The three possible states of a promise
const pending = new Promise(() => {}); // pending
const resolved = Promise.resolve('Success'); // fulfilled
const rejected = Promise.reject('Error'); // rejected

// State verification (indirectly)
resolved.then(
    () => console.log('Promise resolved'),
    () => console.log('Promise rejected'),
);
```

## Chaînage de Promesses

```javascript
// Simple chaining
function getUser(id) {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve({ id, name: 'John Smith' });
        }, 1000);
    });
}

function getOrders(user) {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve({
                user,
                orders: [
                    { id: 1, amount: 50 },
                    { id: 2, amount: 30 },
                ],
            });
        }, 1000);
    });
}

// Using chaining
getUser(1)
    .then((user) => getOrders(user))
    .then((result) => console.log(result))
    .catch((error) => console.error(error));
```

## Méthodes de Promise

### Promise.all()

```javascript
// Parallel execution of promises
const promise1 = Promise.resolve(1);
const promise2 = new Promise((resolve) => setTimeout(() => resolve(2), 1000));
const promise3 = Promise.resolve(3);

Promise.all([promise1, promise2, promise3])
    .then(([result1, result2, result3]) => {
        console.log(result1, result2, result3); // 1, 2, 3
    })
    .catch((error) => console.error(error));
```

### Promise.race()

```javascript
// The first resolved promise wins
const fast = new Promise((resolve) => setTimeout(() => resolve('fast'), 100));
const slow = new Promise((resolve) => setTimeout(() => resolve('slow'), 500));

Promise.race([fast, slow])
    .then((winner) => console.log(winner)) // 'fast'
    .catch((error) => console.error(error));
```

### Promise.allSettled()

```javascript
// Wait for all promises to complete
const promises = [Promise.resolve(1), Promise.reject('error'), Promise.resolve(3)];

Promise.allSettled(promises).then((results) => {
    results.forEach((result) => {
        if (result.status === 'fulfilled') {
            console.log('Success:', result.value);
        } else {
            console.log('Failure:', result.reason);
        }
    });
});
```

## Async/Await

### Syntaxe de Base

```javascript
async function getData() {
    try {
        const user = await getUser(1);
        const orders = await getOrders(user);
        return orders;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
}

// Usage
getData()
    .then((result) => console.log(result))
    .catch((error) => console.error(error));
```

### Parallélisation avec Async/Await

```javascript
async function getAll() {
    try {
        // Parallel execution
        const [users, products] = await Promise.all([getUsers(), getProducts()]);

        return {
            users,
            products,
        };
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
}
```

## Gestion des Erreurs

```javascript
async function errorHandling() {
    try {
        // Attempt asynchronous operation
        const result = await riskyOperation();
        return result;
    } catch (error) {
        if (error instanceof NetworkError) {
            // Specific handling of network errors
            console.error('Network error:', error);
            // Attempt to reconnect
            return await reconnectAndRetry();
        } else {
            // Other types of errors
            console.error('Unexpected error:', error);
            throw error;
        }
    } finally {
        // Cleanup, always executed
        await cleanupResources();
    }
}
```

## Bonnes Pratiques

1. **Gestion des Erreurs**

    - Toujours avoir un .catch() ou try/catch
    - Gérer les erreurs de manière spécifique
    - Utiliser des erreurs personnalisées
    - Logger les erreurs de manière appropriée

2. **Performance**

    - Paralléliser quand possible avec Promise.all()
    - Éviter les chaînes de promesses trop longues
    - Utiliser Promise.race() pour les timeouts
    - Gérer la mémoire avec les promesses

3. **Lisibilité**

    - Préférer async/await pour le code synchrone
    - Nommer les fonctions de manière explicite
    - Documenter le comportement asynchrone
    - Structurer le code de manière claire

4. **Maintenance**

    - Centraliser la logique de gestion d'erreurs
    - Créer des helpers pour les opérations communes
    - Utiliser des constantes pour les timeouts
    - Documenter les cas d'échec possibles

## Exemples Pratiques

### Timeout pour les Promesses

```javascript
function withTimeout(promise, delay) {
    const timeout = new Promise((_, reject) => {
        setTimeout(() => reject(new Error('Timeout')), delay);
    });

    return Promise.race([promise, timeout]);
}

// Usage
withTimeout(getData(), 5000)
    .then((result) => console.log(result))
    .catch((error) => console.error('Timeout or error:', error));
```

### Retry Pattern

```javascript
async function avecRetry(operation, maxTentatives = 3, delai = 1000) {
    let dernierErreur;

    for (let tentative = 1; tentative <= maxTentatives; tentative++) {
        try {
            return await operation();
        } catch (erreur) {
            dernierErreur = erreur;
            if (tentative === maxTentatives) break;

            console.log(`Tentative ${tentative} échouée, nouvelle tentative dans ${delai}ms`);
            await new Promise((resolve) => setTimeout(resolve, delai));
        }
    }

    throw new Error(`Échec après ${maxTentatives} tentatives: ${dernierErreur}`);
}

// Utilisation
avecRetry(() => getData())
    .then((result) => console.log(result))
    .catch((erreur) => console.error(erreur));
```
