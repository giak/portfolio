---
title: Tests Avancés et TDD en JavaScript
date: 2024-10-29
icon: code
category:
    - JavaScript
tags:
    - tests
    - TDD
description: Un guide sur les tests avancés et le Test-Driven Development (TDD) en JavaScript.
---

# Tests Avancés et TDD en JavaScript

## Introduction

Les tests avancés et le Test-Driven Development (TDD) sont essentiels pour garantir la qualité et la maintenabilité du code JavaScript. Cet article explore les techniques avancées de test et la méthodologie TDD.

## Test-Driven Development (TDD)

### Cycle Red-Green-Refactor

```javascript
// Exemple de TDD pour une fonction de calcul
// 1. Red: Écrire un test qui échoue
describe('Calculator', () => {
    test('should add two numbers correctly', () => {
        const calculator = new Calculator();
        expect(calculator.add(2, 3)).toBe(5);
    });
});

// 2. Green: Implémenter le minimum pour faire passer le test
class Calculator {
    add(a, b) {
        return a + b;
    }
}

// 3. Refactor: Améliorer le code sans casser les tests
class Calculator {
    add(...numbers) {
        return numbers.reduce((sum, num) => sum + num, 0);
    }
}
```

### Tests Paramétrés

```javascript
// Test paramétré avec Jest
describe('Calculator', () => {
    test.each([
        [2, 3, 5],
        [0, 0, 0],
        [-1, 1, 0],
        [10, -5, 5],
    ])('adds %i + %i to equal %i', (a, b, expected) => {
        const calculator = new Calculator();
        expect(calculator.add(a, b)).toBe(expected);
    });
});
```

## Tests d'Intégration Avancés

### Tests d'API avec Supertest

```javascript
// api.test.js
import request from 'supertest';
import app from './app';
import { createTestDatabase, clearTestDatabase } from './testUtils';

describe('User API', () => {
    let testDb;

    beforeAll(async () => {
        testDb = await createTestDatabase();
    });

    afterEach(async () => {
        await clearTestDatabase(testDb);
    });

    afterAll(async () => {
        await testDb.close();
    });

    test('should create a new user', async () => {
        const userData = {
            name: 'John Doe',
            email: 'john@example.com',
        };

        const response = await request(app).post('/api/users').send(userData).expect(201);

        expect(response.body).toMatchObject({
            id: expect.any(String),
            ...userData,
        });

        // Vérifier en base de données
        const savedUser = await testDb.collection('users').findOne({ email: userData.email });
        expect(savedUser).toMatchObject(userData);
    });

    test('should handle validation errors', async () => {
        const invalidData = {
            name: 'John Doe',
            email: 'invalid-email',
        };

        const response = await request(app).post('/api/users').send(invalidData).expect(400);

        expect(response.body).toEqual({
            error: expect.stringContaining('email'),
        });
    });
});
```

### Tests de Composants React

```javascript
// UserProfile.test.jsx
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { UserProfile } from './UserProfile';
import { UserContext } from './UserContext';

describe('UserProfile', () => {
    const mockUser = {
        id: '1',
        name: 'John Doe',
        email: 'john@example.com',
    };

    const mockUpdateUser = jest.fn();

    const renderWithContext = (component) => {
        return render(
            <UserContext.Provider value={{ user: mockUser, updateUser: mockUpdateUser }}>
                {component}
            </UserContext.Provider>,
        );
    };

    beforeEach(() => {
        mockUpdateUser.mockClear();
    });

    test('displays user information', () => {
        renderWithContext(<UserProfile />);

        expect(screen.getByText(mockUser.name)).toBeInTheDocument();
        expect(screen.getByText(mockUser.email)).toBeInTheDocument();
    });

    test('allows editing user name', async () => {
        renderWithContext(<UserProfile />);

        const editButton = screen.getByRole('button', { name: /edit/i });
        fireEvent.click(editButton);

        const nameInput = screen.getByLabelText(/name/i);
        await userEvent.clear(nameInput);
        await userEvent.type(nameInput, 'Jane Doe');

        const saveButton = screen.getByRole('button', { name: /save/i });
        fireEvent.click(saveButton);

        await waitFor(() => {
            expect(mockUpdateUser).toHaveBeenCalledWith({
                ...mockUser,
                name: 'Jane Doe',
            });
        });
    });
});
```

## Tests de Performance

### Tests de Charge avec k6

```javascript
// load-test.js
import http from 'k6/http';
import { check, sleep } from 'k6';

export const options = {
    stages: [
        { duration: '30s', target: 20 }, // Montée en charge
        { duration: '1m', target: 20 }, // Charge constante
        { duration: '30s', target: 0 }, // Arrêt progressif
    ],
    thresholds: {
        http_req_duration: ['p(95)<500'], // 95% des requêtes sous 500ms
        http_req_failed: ['rate<0.01'], // Moins de 1% d'erreurs
    },
};

export default function () {
    const response = http.get('http://api.example.com/users');

    check(response, {
        'status is 200': (r) => r.status === 200,
        'response time OK': (r) => r.timings.duration < 500,
    });

    sleep(1);
}
```

### Tests de Performance Frontend

```javascript
// performance.test.js
describe('Performance Tests', () => {
    beforeEach(async () => {
        await page.coverage.startJSCoverage();
        await page.coverage.startCSSCoverage();
    });

    afterEach(async () => {
        const [jsCoverage, cssCoverage] = await Promise.all([
            page.coverage.stopJSCoverage(),
            page.coverage.stopCSSCoverage(),
        ]);

        // Analyser la couverture
        analyzeCoverage(jsCoverage, cssCoverage);
    });

    test('page load performance', async () => {
        const metrics = await page.metrics();
        expect(metrics.FirstContentfulPaint).toBeLessThan(1000);
        expect(metrics.DomContentLoaded).toBeLessThan(2000);
    });

    test('scroll performance', async () => {
        const fps = await measureScrollPerformance();
        expect(fps).toBeGreaterThan(30);
    });
});
```

## Mocking Avancé

### Mocks Personnalisés

```javascript
// service.test.js
jest.mock('./database', () => ({
    query: jest.fn(),
    transaction: jest.fn(async (callback) => {
        const transaction = {
            commit: jest.fn(),
            rollback: jest.fn(),
        };
        try {
            await callback(transaction);
            await transaction.commit();
        } catch (error) {
            await transaction.rollback();
            throw error;
        }
    }),
}));

describe('UserService', () => {
    test('should handle transaction rollback', async () => {
        const db = require('./database');
        db.query.mockRejectedValueOnce(new Error('DB Error'));

        const service = new UserService(db);
        await expect(service.createUser({ name: 'John' })).rejects.toThrow('DB Error');

        expect(db.transaction).toHaveBeenCalled();
        const transaction = await db.transaction.mock.results[0].value;
        expect(transaction.rollback).toHaveBeenCalled();
    });
});
```

### Test Doubles

```javascript
// payment.test.js
class PaymentGatewayStub {
    async processPayment(amount) {
        return { success: true, transactionId: 'test-123' };
    }
}

class PaymentGatewaySpy {
    constructor() {
        this.payments = [];
    }

    async processPayment(amount) {
        this.payments.push({ amount, timestamp: Date.now() });
        return { success: true, transactionId: 'test-123' };
    }

    getPayments() {
        return this.payments;
    }
}

describe('PaymentService', () => {
    test('processes payment successfully', async () => {
        const gateway = new PaymentGatewaySpy();
        const service = new PaymentService(gateway);

        await service.pay(100);

        const payments = gateway.getPayments();
        expect(payments).toHaveLength(1);
        expect(payments[0].amount).toBe(100);
    });
});
```

## Bonnes Pratiques

1. **Organisation des Tests**

    - Structure claire des tests
    - Tests isolés et indépendants
    - Setup et teardown appropriés
    - Nommage descriptif des tests

2. **Qualité des Tests**

    - Tests maintenables
    - Couverture de code appropriée
    - Tests déterministes
    - Assertions précises

3. **Performance des Tests**

    - Tests rapides
    - Parallélisation quand possible
    - Mocks efficaces
    - Optimisation des ressources

4. **Maintenance**

    - Documentation des tests
    - Revue régulière
    - Mise à jour des dépendances
    - Nettoyage des tests obsolètes

## Exemples Pratiques

### Test d'une Application Complète

```javascript
// app.test.js
describe('E-commerce Application', () => {
    describe('Product Catalog', () => {
        test('displays products correctly', async () => {
            // Setup
            const products = generateTestProducts(10);
            mockProductAPI(products);

            // Action
            render(<ProductCatalog />);

            // Assertions
            await waitFor(() => {
                products.forEach((product) => {
                    expect(screen.getByText(product.name)).toBeInTheDocument();
                    expect(screen.getByText(`$${product.price}`)).toBeInTheDocument();
                });
            });
        });

        test('handles filtering and sorting', async () => {
            // Setup
            const products = generateTestProducts(20);
            mockProductAPI(products);

            // Action
            render(<ProductCatalog />);

            // Filter
            const filterInput = screen.getByPlaceholderText(/search/i);
            await userEvent.type(filterInput, 'test');

            // Sort
            const sortSelect = screen.getByLabelText(/sort by/i);
            await userEvent.selectOptions(sortSelect, 'price-asc');

            // Assertions
            await waitFor(() => {
                const displayedProducts = screen.getAllByTestId('product-card');
                expect(displayedProducts).toHaveLength(products.filter((p) => p.name.includes('test')).length);
                // Vérifier l'ordre des prix
                const prices = displayedProducts.map((p) =>
                    parseFloat(p.querySelector('[data-testid="price"]').textContent.slice(1)),
                );
                expect(prices).toEqual([...prices].sort((a, b) => a - b));
            });
        });
    });

    describe('Shopping Cart', () => {
        test('manages cart items correctly', async () => {
            // Setup
            const initialCart = [];
            const store = configureStore({ cart: initialCart });

            // Action
            render(
                <Provider store={store}>
                    <ShoppingCart />
                </Provider>,
            );

            // Add items
            const addButtons = screen.getAllByRole('button', { name: /add to cart/i });
            await userEvent.click(addButtons[0]);
            await userEvent.click(addButtons[1]);

            // Verify cart
            expect(store.getState().cart).toHaveLength(2);

            // Update quantity
            const quantityInput = screen.getAllByRole('spinbutton')[0];
            await userEvent.clear(quantityInput);
            await userEvent.type(quantityInput, '3');

            // Remove item
            const removeButton = screen.getAllByRole('button', { name: /remove/i })[0];
            await userEvent.click(removeButton);

            // Final assertions
            expect(store.getState().cart).toHaveLength(1);
            expect(store.getState().cart[0].quantity).toBe(3);
        });
    });
});
```
