---
title: Questions JavaScript Avancées
description: Collection complète de questions techniques JavaScript pour les entretiens
date: 2024-03-14
icon: question
category:
    - JavaScript
    - Entretiens
tags:
    - JavaScript
    - Questions
    - Entretiens
    - Préparation
---

# Questions JavaScript Avancées

Une collection complète de questions techniques JavaScript pour vous aider à vous préparer aux entretiens.

# Questions Avancées en JavaScript

## 1. Quelle est la sortie ?

```javascript
function sayHi() {
    console.log(name);
    console.log(age);
    var name = 'Lydia';
    let age = 21;
}

sayHi();
```

-   A: `Lydia` et `undefined`
-   B: `Lydia` et `ReferenceError`
-   C: `ReferenceError` et `21`
-   D: `undefined` et `ReferenceError`
    **Réponse : D**

## 2. Quelle est la sortie ?

```javascript
for (var i = 0; i < 3; i++) {
    setTimeout(() => console.log(i), 1);
}

for (let i = 0; i < 3; i++) {
    setTimeout(() => console.log(i), 1);
}
```

-   A: `0 1 2` et `0 1 2`
-   B: `0 1 2` et `3 3 3`
-   C: `3 3 3` et `0 1 2`
    **Réponse : C**

## 3. Quelle est la sortie ?

```javascript
const shape = {
    radius: 10,
    diameter() {
        return this.radius * 2;
    },
    perimeter: () => 2 * Math.PI * this.radius,
};

shape.diameter();
shape.perimeter();
```

-   A: `20` et `62.83185307179586`
-   B: `20` et `NaN`
-   C: `20` et `63`
-   D: `NaN` et `63`
    **Réponse : B**

## 4. Quelle est la sortie ?

```javascript
+true;
!'Lydia';
```

-   A: `1` et `false`
-   B: `false` et `NaN`
-   C: `false` et `false`
    **Réponse : A**

## 5. Laquelle est vraie ?

```javascript
const bird = {
    size: 'small',
};

const mouse = {
    name: 'Mickey',
    small: true,
};
```

-   A: `mouse.bird.size` n'est pas valide
-   B: `mouse[bird.size]` n'est pas valide
-   C: `mouse[bird["size"]]` n'est pas valide
-   D: Toutes sont valides
    **Réponse : A**

## 6. Quelle est la sortie ?

```javascript
let c = { greeting: 'Hey!' };
let d;

d = c;
c.greeting = 'Hello';
console.log(d.greeting);
```

-   A: `Hello`
-   B: `Hey`
-   C: `undefined`
-   D: `ReferenceError`
-   E: `TypeError`
    **Réponse : A**

## 7. Quelle est la sortie ?

```javascript
let a = 3;
let b = new Number(3);
let c = 3;

console.log(a == b);
console.log(a === b);
console.log(b === c);
```

-   A: `true` `false` `true`
-   B: `false` `false` `true`
-   C: `true` `false` `false`
-   D: `false` `true` `true`
    **Réponse : C**

## 8. Quelle est la sortie ?

```javascript
class Chameleon {
    static colorChange(newColor) {
        this.newColor = newColor;
        return this.newColor;
    }

    constructor({ newColor = 'green' } = {}) {
        this.newColor = newColor;
    }
}

const freddie = new Chameleon({ newColor: 'purple' });
freddie.colorChange('orange');
```

-   A: `orange`
-   B: `purple`
-   C: `green`
-   D: `TypeError`
    **Réponse : D**

## 9. Quelle est la sortie ?

```javascript
let greeting;
greetign = {}; // Typo!
console.log(greetign);
```

-   A: `{}`
-   B: `ReferenceError: greetign is not defined`
-   C: `undefined`
    **Réponse : A**

## 10. Que se passe-t-il lorsque nous faisons ça ?

```javascript
function bark() {
    console.log('Woof!');
}

bark.animal = 'dog';
```

-   A: Rien, c'est tout à fait bon !
-   B: `SyntaxError`. Vous ne pouvez pas ajouter de propriétés à une fonction de cette façon.
-   C: `undefined`
-   D: `ReferenceError`
    **Réponse : A**

## 11. Quelle est la sortie ?

```javascript
function Person(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
}

const member = new Person('Lydia', 'Hallie');
Person.getFullName = function () {
    return `${this.firstName} ${this.lastName}`;
};

console.log(member.getFullName());
```

-   A: `TypeError`
-   B: `SyntaxError`
-   C: `Lydia Hallie`
-   D: `undefined `undefined`
    **Réponse : A**

## 12. Quelle est la sortie ?

```javascript
function Person(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
}

const lydia = new Person('Lydia', 'Hallie');
const sarah = Person('Sarah', 'Smith');

console.log(lydia);
console.log(sarah);
```

-   A: `Person {firstName: "Lydia", lastName: "Hallie"}` et `undefined`
-   B: `Person {firstName: "Lydia", lastName: "Hallie"}` et `Person {firstName: "Sarah", lastName: "Smith"}`
-   C: `Person {firstName: "Lydia", lastName: "Hallie"}` et `{}`
-   D: `Person {firstName: "Lydia", lastName: "Hallie"}` et `ReferenceError`
    **Réponse : A**

## 13. Quelle sont les trois phases de propagation des événements ?

-   A: Target > Capturing > Bubbling
-   B: Bubbling > Target > Capturing
-   C: Target > Bubbling > Capturing
-   D: Capturing > Target > Bubbling
    **Réponse : D**

## 14. Tous les objets ont des prototypes.

-   A: vrai
-   B: faux
    **Réponse : B**

## 15. Quelle est la sortie ?

```javascript
function sum(a, b) {
    return a + b;
}

sum(1, '2');
```

-   A: `NaN`
-   B: `TypeError`
-   C: `"12"`
-   D: `3`
    **Réponse : C**

## 16. Quelle est la sortie ?

```javascript
let number = 0;
console.log(number++);
console.log(++number);
console.log(number);
```

-   A: `1` `1` `2`
-   B: `1` `2` `2`
-   C: `0` `2` `2`
-   D: `0` `1` `2`
    **Réponse : C**

## 17. Quelle est la sortie ?

```javascript
function getPersonInfo(one, two, three) {
    console.log(one);
    console.log(two);
    console.log(three);
}

const person = 'Lydia';
const age = 21;

getPersonInfo`${person} is ${age} years old`;
```

-   A: `"Lydia"` `21` `["", " is ", " years old"]`
-   B: `["", " is ", " years old"]` `"Lydia"` `21`
-   C: `"Lydia"` `["", " is ", " years old"]` `21`
    **Réponse : B**

## 18. Quelle est la sortie ?

```javascript
function checkAge(data) {
    if (data === { age: 18 }) {
        console.log('Vous êtes un adulte !');
    } else if (data == { age: 18 }) {
        console.log('Vous êtes toujours un adulte.');
    } else {
        console.log(`Hmm.. Vous n'avez pas l'âge, je suppose.`);
    }
}

checkAge({ age: 18 });
```

-   A: `Vous êtes un adulte !`
-   B: `Vous êtes toujours un adulte.`
-   C: `Hmm.. Vous n'avez pas l'âge, je suppose.`
    **Réponse : C**

## 19. Quelle est la sortie ?

```javascript
function getAge(...args) {
    console.log(typeof args);
}

getAge(21);
```

-   A: `"number"`
-   B: `"array"`
-   C: `"object"`
-   D: `"NaN"`
    **Réponse : C**

## 20. Quelle est la sortie ?

```javascript
function getAge() {
    'use strict';
    age = 21;
    console.log(age);
}

getAge();
```

-   A: `21`
-   B: `undefined`
-   C: `ReferenceError`
-   D: `TypeError`
    **Réponse : C**

## 21. Quelle est la valeur de `sum` ?

```javascript
const sum = eval('10*10+5');
```

-   A: `105`
-   B: `"105"`
-   C: `TypeError`
-   D: `"10*10+5"`
    **Réponse : A**

## 22. Pendant combien de temps `cool_secret` sera-t-il accessible ?

```javascript
sessionStorage.setItem('cool_secret', 123);
```

-   A: Pour toujours, les données ne seront pas perdues.
-   B: Jusqu'à ce que l'utilisateur ferme l'onglet.
-   C: Jusqu'à ce que l'utilisateur ferme son navigateur en entier, pas seulement son onglet.
-   D: Jusqu'à ce que l'utilisateur éteindra son ordinateur.
    **Réponse : B**

## 23. Quelle est la sortie ?

```javascript
var num = 8;
var num = 10;

console.log(num);
```

-   A: `8`
-   B: `10`
-   C: `SyntaxError`
-   D: `ReferenceError`
    **Réponse : B**

## 24. Quelle est la sortie ?

```javascript
const obj = { 1: 'a', 2: 'b', 3: 'c' };
const set = new Set([1, 2, 3, 4, 5]);

obj.hasOwnProperty('1');
obj.hasOwnProperty(1);
set.has('1');
set.has(1);
```

-   A: `false` `true` `false` `true`
-   B: `false` `true` `true` `true`
-   C: `true` `true` `false` `true`
-   D: `true` `true` `true` `true`
    **Réponse : C**

## 25. Quelle est la sortie ?

```javascript
const obj = { a: 'un', b: 'deux', a: 'trois' };
console.log(obj);
```

-   A: `{ a: "un", b: "deux" }`
-   B: `{ b: "deux", a: "trois" }`
-   C: `{ a: "trois", b: "deux" }`
-   D: `SyntaxError`
    **Réponse : C**

## 26. Le contexte global d'exécution de JavaScript crée 2 choses pour vous : l'objet global et le mot-clé `this`.

-   A: Vrai
-   B: Faux
-   C: Ça dépend
    **Réponse : A**

## 27. Quelle est la sortie ?

```javascript
for (let i = 1; i < 5; i++) {
    if (i === 3) continue;
    console.log(i);
}
```

-   A: `1` `2`
-   B: `1` `2` `3`
-   C: `1` `2` `4`
-   D: `1` `3` `4`
    **Réponse : C**

## 28. Quelle est la sortie ?

```javascript
String.prototype.giveLydiaPizza = () => {
    return 'Just give Lydia pizza already!';
};

const name = 'Lydia';

console.log(name.giveLydiaPizza());
```

-   A: `"Just give Lydia pizza already!"`
-   B: `TypeError: not a function`
-   C: `SyntaxError`
-   D: `undefined`
    **Réponse : A**

## 29. Quelle est la sortie ?

```javascript
const a = {};
const b = { key: 'b' };
const c = { key: 'c' };

a[b] = 123;
a[c] = 456;

console.log(a[b]);
```

-   A: `123`
-   B: `456`
-   C: `undefined`
-   D: `ReferenceError`
    **Réponse : B**

## 30. Quelle est la sortie ?

```javascript
const foo = () => console.log('Premier');
const bar = () => setTimeout(() => console.log('Second'));
const baz = () => console.log('Troisième');

bar();
foo();
baz();
```

-   A: `Premier` `Second` `Troisième`
-   B: `Premier` `Troisième` `Second`
-   C: `Second` `Premier` `Troisième`
-   D: `Second` `Troisième` `Premier`
    **Réponse : B**

## 31. Quel est l'élément ciblé _(event.target)_ au clic sur le bouton _(button)_ ?

```html
<div onclick="console.log('first div')">
    <div onclick="console.log('second div')">
        <button onclick="console.log('button')">Click!</button>
    </div>
</div>
```

-   A: La `div` extérieure
-   B: La `div` intérieure
-   C: `button`
-   D: Un tableau de tous les éléments imbriqués.
    **Réponse : C**

## 32. Quand vous cliquez sur le paragraphe, quelle est la sortie ?

```html
<div onclick="console.log('div')">
    <p onclick="console.log('p')">Click here!</p>
</div>
```

-   A: `p` `div`
-   B: `div` `p`
-   C: `p`
-   D: `div`
    **Réponse : A**

## 33. Quelle est la sortie ?

```javascript
const person = { name: 'Lydia' };

function sayHi(age) {
    console.log(`${this.name} is ${age}`);
}

sayHi.call(person, 21);
sayHi.bind(person, 21);
```

-   A: `undefined is 21` `Lydia is 21`
-   B: `function` `function`
-   C: `Lydia is 21` `Lydia is 21`
-   D: `Lydia is 21` `function`
    **Réponse : D**

## 34. Quelle est la sortie ?

```javascript
function sayHi() {
    return (() => 0)();
}

typeof sayHi();
```

-   A: `"object"`
-   B: `"number"`
-   C: `"function"`
-   D: `"undefined"`
    **Réponse : B**

## 35. Les lesquelles de ces valeurs sont fausses ?

-   0;
-   new Number(0);
-   ("");
-   (" ");
-   new Boolean(false);
-   undefined;

-   A: `0`, `''`, `undefined`
-   B: `0`, `new Number(0)`, `''`, `new Boolean(false)`, `undefined`
-   C: `0`, `''`, `new Boolean(false)`, `undefined`
-   D: Toutes sont fausses
    **Réponse : A**

## 36. Quelle est la sortie ?

```javascript
console.log(typeof typeof 1);
```

-   A: `"number"`
-   B: `"string"`
-   C: `"object"`
-   D: `"undefined"`
    **Réponse : B**

## 37. Quelle est la sortie ?

```javascript
const numbers = [1, 2, 3];
numbers[10] = 11;
console.log(numbers);
```

-   A: `[1, 2, 3, 7 x null, 11]`
-   B: `[1, 2, 3, 11]`
-   C: `[1, 2, 3, 7 x empty, 11]`
-   D: `SyntaxError`
    **Réponse : C**

## 38. Quelle est la sortie ?

```javascript
(() => {
    let x, y;
    try {
        throw new Error();
    } catch (x) {
        (x = 1), (y = 2);
        console.log(x);
    }
    console.log(x);
    console.log(y);
})();
```

-   A: `1` `undefined` `2`
-   B: `undefined` `undefined` `undefined`
-   C: `1` `1` `2`
-   D: `1` `undefined` `undefined`
    **Réponse : A**

## 39. Tout en JavaScript est...

-   A: primitif ou objet
-   B: fonction ou objet
-   C: question délicate ! Seulement des objets
-   D: nombre ou objet
    **Réponse : A**

## 40. Quelle est la sortie ?

```javascript
[
    [0, 1],
    [2, 3],
].reduce(
    (acc, cur) => {
        return acc.concat(cur);
    },
    [1, 2],
);
```

-   A: `[0, 1, 2, 3, 1, 2]`
-   B: `[6, 1, 2]`
-   C: `[1, 2, 0, 1, 2, 3]`
-   D: `[1, 2, 6]`
    **Réponse : C**

## 41. Quelle est la sortie ?

```javascript
!!null;
!!'';
!!1;
```

-   A: `false` `true` `false`
-   B: `false` `false` `true`
-   C: `false` `true` `true`
-   D: `true` `true` `false`
    **Réponse : B**

## 42. Que retourne la méthode `setInterval` ?

```javascript
setInterval(() => console.log('Hi'), 1000);
```

-   A: un identifiant unique
-   B: le temps de millisecondes spécifié
-   C: la fonction passée en paramètre
-   D: `undefined`
    **Réponse : A**

## 43. Que retourne ceci ?

```javascript
[...'Lydia'];
```

-   A: `["L", "y", "d", "i", "a"]`
-   B: `["Lydia"]`
-   C: `[[], "Lydia"]`
-   D: `[["L", "y", "d", "i", "a"]]`
    **Réponse : A**

## 44. Quelle est la sortie ?

```javascript
function* generator(i) {
    yield i;
    yield i * 2;
}

const gen = generator(10);

console.log(gen.next().value);
console.log(gen.next().value);
```

-   A: `[0, 10], [10, 20]`
-   B: `20, 20`
-   C: `10, 20`
-   D: `0, 10 and 10, 20`
    **Réponse : C**

## 45. Qu'est-ce que cela retourne ?

```javascript
const firstPromise = new Promise((res, rej) => {
    setTimeout(res, 500, 'one');
});

const secondPromise = new Promise((res, rej) => {
    setTimeout(res, 100, 'two');
});

Promise.race([firstPromise, secondPromise]).then((res) => console.log(res));
```

-   A: `"one"`
-   B: `"two"`
-   C: `"two" "one"`
-   D: `"one" "two"`
    **Réponse : B**

## 46. Quelle est la sortie ?

```javascript
let person = { name: 'Lydia' };
const members = [person];
person = null;

console.log(members);
```

-   A: `null`
-   B: `[null]`
-   C: `[{}]`
-   D: `[{ name: "Lydia" }]`
    **Réponse : D**

## 47. Quelle est la sortie ?

```javascript
const person = { name: 'Lydia', age: 21 };

for (const item in person) {
    console.log(item);
}
```

-   A: `{ name: "Lydia" }, { age: 21 }`
-   B: `"name", "age"`
-   C: `"Lydia", 21`
-   D: `["name", "Lydia"], ["age", 21]`
    **Réponse : B**

## 48. Quelle est la sortie ?

```javascript
console.log(3 + 4 + '5');
```

-   A: `"345"`
-   B: `"75"`
-   C: `12`
-   D: `"12"`
    **Réponse : B**

## 49. Quelle est la valeur de `num` ?

```javascript
const num = parseInt('7*6', 10);
```

-   A: `42`
-   B: `"42"`
-   C: `7`
-   D: `NaN`
    **Réponse : C**

## 50. Quelle est la sortie ?

```javascript
[1, 2, 3].map((num) => {
    if (typeof num === 'number') return;
    return num * 2;
});
```

-   A: `[]`
-   B: `[null, null, null]`
-   C: `[undefined, undefined, undefined]`
-   D: `[ 3 x empty ]`
    **Réponse : C**

## 51. Quelle est la sortie ?

```javascript
function getInfo(member, year) {
    member.name = 'Lydia';
    year = '1998';
}

const person = { name: 'Sarah' };
const birthYear = '1997';

getInfo(person, birthYear);

console.log(person, birthYear);
```

-   A: `{ name: "Lydia" }, "1997"`
-   B: `{ name: "Sarah" }, "1998"`
-   C: `{ name: "Lydia" }, "1998"`
-   D: `{ name: "Sarah" }, "1997"`
    **Réponse : A**

## 52. Quelle est la sortie ?

```javascript
function greeting() {
    throw 'Hello world!';
}

function sayHi() {
    try {
        const data = greeting();
        console.log('It worked!', data);
    } catch (e) {
        console.log('Oh no an error!', e);
    }
}

sayHi();
```

-   A: `"It worked! Hello world!"`
-   B: `"Oh no an error: undefined`
-   C: `SyntaxError: can only throw Error objects`
-   D: `"Oh no an error: Hello world!`
    **Réponse : D**

## 53. Quelle est la sortie ?

```javascript
function Car() {
    this.make = 'Lamborghini';
    return { make: 'Maserati' };
}

const myCar = new Car();
console.log(myCar.make);
```

-   A: `"Lamborghini"`
-   B: `"Maserati"`
-   C: `ReferenceError`
-   D: `TypeError`
    **Réponse : B**

## 54. Quelle est la sortie ?

```javascript
(() => {
    let x, y;
    try {
        throw new Error();
    } catch (x) {
        (x = 1), (y = 2);
        console.log(x);
    }
    console.log(x);
    console.log(y);
})();
```

-   A: `1` `undefined` `2`
-   B: `undefined` `undefined` `undefined`
-   C: `1` `1` `2`
-   D: `1` `undefined` `undefined`
    **Réponse : A**

## 55. Le contexte global d'exécution de JavaScript crée 2 choses pour vous : l'objet global et le mot-clé `this`.

-   A: Vrai
-   B: Faux
-   C: Ça dépend
    **Réponse : A**

## 56. Quelle est la sortie ?

```javascript
for (let i = 1; i < 5; i++) {
    if (i === 3) continue;
    console.log(i);
}
```

-   A: `1` `2`
-   B: `1` `2` `3`
-   C: `1` `2` `4`
-   D: `1` `3` `4`
    **Réponse : C**

## 57. Quelle est la sortie ?

```javascript
String.prototype.giveLydiaPizza = () => {
    return 'Just give Lydia pizza already!';
};

const name = 'Lydia';

console.log(name.giveLydiaPizza());
```

-   A: `"Just give Lydia pizza already!"`
-   B: `TypeError: not a function`
-   C: `SyntaxError`
-   D: `undefined`
    **Réponse : A**

## 58. Quelle est la sortie ?

```javascript
const a = {};
const b = { key: 'b' };
const c = { key: 'c' };

a[b] = 123;
a[c] = 456;

console.log(a[b]);
```

-   A: `123`
-   B: `456`
-   C: `undefined`
-   D: `ReferenceError`
    **Réponse : B**

## 59. Quelle est la sortie ?

```javascript
const foo = () => console.log('Premier');
const bar = () => setTimeout(() => console.log('Second'));
const baz = () => console.log('Troisième');

bar();
foo();
baz();
```

-   A: `Premier` `Second` `Troisième`
-   B: `Premier` `Troisième` `Second`
-   C: `Second` `Premier` `Troisième`
-   D: `Second` `Troisième` `Premier`
    **Réponse : B**

## 60. Quel est l'élément ciblé _(event.target)_ au clic sur le bouton _(button)_ ?

```html
<div onclick="console.log('first div')">
    <div onclick="console.log('second div')">
        <button onclick="console.log('button')">Click!</button>
    </div>
</div>
```

-   A: La `div` extérieure
-   B: La `div` intérieure
-   C: `button`
-   D: Un tableau de tous les éléments imbriqués.
    **Réponse : C**

## 61. Quand vous cliquez sur le paragraphe, quelle est la sortie ?

```html
<div onclick="console.log('div')">
    <p onclick="console.log('p')">Click here!</p>
</div>
```

-   A: `p` `div`
-   B: `div` `p`
-   C: `p`
-   D: `div`
    **Réponse : A**

## 62. Quelle est la sortie ?

```javascript
const person = { name: 'Lydia' };

function sayHi(age) {
    console.log(`${this.name} is ${age}`);
}

sayHi.call(person, 21);
sayHi.bind(person, 21);
```

-   A: `undefined is 21` `Lydia is 21`
-   B: `function` `function`
-   C: `Lydia is 21` `Lydia is 21`
-   D: `Lydia is 21` `function`
    **Réponse : D**

## 63. Quelle est la sortie ?

```javascript
function sayHi() {
    return (() => 0)();
}

typeof sayHi();
```

-   A: `"object"`
-   B: `"number"`
-   C: `"function"`
-   D: `"undefined"`
    **Réponse : B**

## 64. Les lesquelles de ces valeurs sont fausses ?

-   0;
-   new Number(0);
-   ("");
-   (" ");
-   new Boolean(false);
-   undefined;

-   A: `0`, `''`, `undefined`
-   B: `0`, `new Number(0)`, `''`, `new Boolean(false)`, `undefined`
-   C: `0`, `''`, `new Boolean(false)`, `undefined`
-   D: Toutes sont fausses
    **Réponse : A**

## 65. Quelle est la sortie ?

```javascript
console.log(typeof typeof 1);
```

-   A: `"number"`
-   B: `"string"`
-   C: `"object"`
-   D: `"undefined"`
    **Réponse : B**

## 66. Quelle est la sortie ?

```javascript
const numbers = [1, 2, 3];
numbers[10] = 11;
console.log(numbers);
```

-   A: `[1, 2, 3, 7 x null, 11]`
-   B: `[1, 2, 3, 11]`
-   C: `[1, 2, 3, 7 x empty, 11]`
-   D: `SyntaxError`
    **Réponse : C**

## 67. Quelle est la sortie ?

```javascript
(() => {
    let x, y;
    try {
        throw new Error();
    } catch (x) {
        (x = 1), (y = 2);
        console.log(x);
    }
    console.log(x);
    console.log(y);
})();
```

-   A: `1` `undefined` `2`
-   B: `undefined` `undefined` `undefined`
-   C: `1` `1` `2`
-   D: `1` `undefined` `undefined`
    **Réponse : A**

## 68. Tout en JavaScript est...

-   A: primitif ou objet
-   B: fonction ou objet
-   C: question délicate ! Seulement des objets
-   D: nombre ou objet
    **Réponse : A**

## 69. Quelle est la sortie ?

```javascript
[
    [0, 1],
    [2, 3],
].reduce(
    (acc, cur) => {
        return acc.concat(cur);
    },
    [1, 2],
);
```

-   A: `[0, 1, 2, 3, 1, 2]`
-   B: `[6, 1, 2]`
-   C: `[1, 2, 0, 1, 2, 3]`
-   D: `[1, 2, 6]`
    **Réponse : C**

## 70. Quelle est la sortie ?

```javascript
!!null;
!!'';
!!1;
```

-   A: `false` `true` `false`
-   B: `false` `false` `true`
-   C: `false` `true` `true`
-   D: `true` `true` `false`
    **Réponse : B**

## 71. Que retourne la méthode `setInterval` ?

```javascript
setInterval(() => console.log('Hi'), 1000);
```

-   A: un identifiant unique
-   B: le temps de millisecondes spécifié
-   C: la fonction passée en paramètre
-   D: `undefined`
    **Réponse : A**

## 72. Que retourne ceci ?

```javascript
[...'Lydia'];
```

-   A: `["L", "y", "d", "i", "a"]`
-   B: `["Lydia"]`
-   C: `[[], "Lydia"]`
-   D: `[["L", "y", "d", "i", "a"]]`
    **Réponse : A**

## 73. Quelle est la sortie ?

```javascript
function* generator(i) {
    yield i;
    yield i * 2;
}

const gen = generator(10);

console.log(gen.next().value);
console.log(gen.next().value);
```

-   A: `[0, 10], [10, 20]`
-   B: `20, 20`
-   C: `10, 20`
-   D: `0, 10 and 10, 20`
    **Réponse : C**

## 74. Qu'est-ce que cela retourne ?

```javascript
const firstPromise = new Promise((res, rej) => {
    setTimeout(res, 500, 'one');
});

const secondPromise = new Promise((res, rej) => {
    setTimeout(res, 100, 'two');
});

Promise.race([firstPromise, secondPromise]).then((res) => console.log(res));
```

-   A: `"one"`
-   B: `"two"`
-   C: `"two" "one"`
-   D: `"one" "two"`
    **Réponse : B**

## 75. Quelle est la sortie ?

```javascript
let person = { name: 'Lydia' };
const members = [person];
person = null;

console.log(members);
```

-   A: `null`
-   B: `[null]`
-   C: `[{}]`
-   D: `[{ name: "Lydia" }]`
    **Réponse : D**

## 76. Quelle est la sortie ?

```javascript
const person = { name: 'Lydia', age: 21 };

for (const item in person) {
    console.log(item);
}
```

-   A: `{ name: "Lydia" }, { age: 21 }`
-   B: `"name", "age"`
-   C: `"Lydia", 21`
-   D: `["name", "Lydia"], ["age", 21]`
    **Réponse : B**

## 77. Quelle est la sortie ?

```javascript
console.log(3 + 4 + '5');
```

-   A: `"345"`
-   B: `"75"`
-   C: `12`
-   D: `"12"`
    **Réponse : B**

## 78. Quelle est la valeur de `num` ?

```javascript
const num = parseInt('7*6', 10);
```

-   A: `42`
-   B: `"42"`
-   C: `7`
-   D: `NaN`
    **Réponse : C**

## 79. Quelle est la sortie ?

```javascript
[1, 2, 3].map((num) => {
    if (typeof num === 'number') return;
    return num * 2;
});
```

-   A: `[]`
-   B: `[null, null, null]`
-   C: `[undefined, undefined, undefined]`
-   D: `[ 3 x empty ]`
    **Réponse : C**

## 80. Quelle est la sortie ?

```javascript
function getInfo(member, year) {
    member.name = 'Lydia';
    year = '1998';
}

const person = { name: 'Sarah' };
const birthYear = '1997';

getInfo(person, birthYear);

console.log(person, birthYear);
```

-   A: `{ name: "Lydia" }, "1997"`
-   B: `{ name: "Sarah" }, "1998"`
-   C: `{ name: "Lydia" }, "1998"`
-   D: `{ name: "Sarah" }, "1997"`
    **Réponse : A**

## 81. Quelle est la sortie ?

```javascript
function greeting() {
    throw 'Hello world!';
}

function sayHi() {
    try {
        const data = greeting();
        console.log('It worked!', data);
    } catch (e) {
        console.log('Oh no an error!', e);
    }
}

sayHi();
```

-   A: `"It worked! Hello world!"`
-   B: `"Oh no an error: undefined`
-   C: `SyntaxError: can only throw Error objects`
-   D: `"Oh no an error: Hello world!`
    **Réponse : D**

## 82. Quelle est la sortie ?

```javascript
function Car() {
    this.make = 'Lamborghini';
    return { make: 'Maserati' };
}

const myCar = new Car();
console.log(myCar.make);
```

-   A: `"Lamborghini"`
-   B: `"Maserati"`
-   C: `ReferenceError`
-   D: `TypeError`
    **Réponse : B**

## 83. Quelle est la sortie ?

```javascript
(() => {
    let x, y;
    try {
        throw new Error();
    } catch (x) {
        (x = 1), (y = 2);
        console.log(x);
    }
    console.log(x);
    console.log(y);
})();
```

-   A: `1` `undefined` `2`
-   B: `undefined` `undefined` `undefined`
-   C: `1` `1` `2`
-   D: `1` `undefined` `undefined`
    **Réponse : A**

## 84. Tout en JavaScript est...

-   A: primitif ou objet
-   B: fonction ou objet
-   C: question délicate ! Seulement des objets
-   D: nombre ou objet
    **Réponse : A**

## 85. Quelle est la sortie ?

```javascript
[
    [0, 1],
    [2, 3],
].reduce(
    (acc, cur) => {
        return acc.concat(cur);
    },
    [1, 2],
);
```

-   A: `[0, 1, 2, 3, 1, 2]`
-   B: `[6, 1, 2]`
-   C: `[1, 2, 0, 1, 2, 3]`
-   D: `[1, 2, 6]`
    **Réponse : C**

## 86. Quelle est la sortie ?

```javascript
!!null;
!!'';
!!1;
```

-   A: `false` `true` `false`
-   B: `false` `false` `true`
-   C: `false` `true` `true`
-   D: `true` `true` `false`
    **Réponse : B**

## 87. Que retourne la méthode `setInterval` ?

```javascript
setInterval(() => console.log('Hi'), 1000);
```

-   A: un identifiant unique
-   B: le temps de millisecondes spécifié
-   C: la fonction passée en paramètre
-   D: `undefined`
    **Réponse : A**

## 88. Que retourne ceci ?

```javascript
[...'Lydia'];
```

-   A: `["L", "y", "d", "i", "a"]`
-   B: `["Lydia"]`
-   C: `[[], "Lydia"]`
-   D: `[["L", "y", "d", "i", "a"]]`
    **Réponse : A**

## 89. Quelle est la sortie ?

```javascript
function* generator(i) {
    yield i;
    yield i * 2;
}

const gen = generator(10);

console.log(gen.next().value);
console.log(gen.next().value);
```

-   A: `[0, 10], [10, 20]`
-   B: `20, 20`
-   C: `10, 20`
-   D: `0, 10 and 10, 20`
    **Réponse : C**

## 90. Qu'est-ce que cela retourne ?

```javascript
const firstPromise = new Promise((res, rej) => {
    setTimeout(res, 500, 'one');
});

const secondPromise = new Promise((res, rej) => {
    setTimeout(res, 100, 'two');
});

Promise.race([firstPromise, secondPromise]).then((res) => console.log(res));
```

-   A: `"one"`
-   B: `"two"`
-   C: `"two" "one"`
-   D: `"one" "two"`
    **Réponse : B**

## 91. Quelle est la sortie ?

```javascript
let person = { name: 'Lydia' };
const members = [person];
person = null;

console.log(members);
```

-   A: `null`
-   B: `[null]`
-   C: `[{}]`
-   D: `[{ name: "Lydia" }]`
    **Réponse : D**

## 92. Quelle est la sortie ?

```javascript
const person = { name: 'Lydia', age: 21 };

for (const item in person) {
    console.log(item);
}
```

-   A: `{ name: "Lydia" }, { age: 21 }`
-   B: `"name", "age"`
-   C: `"Lydia", 21`
-   D: `["name", "Lydia"], ["age", 21]`
    **Réponse : B**

## 93. Quelle est la sortie ?

```javascript
console.log(3 + 4 + '5');
```

-   A: `"345"`
-   B: `"75"`
-   C: `12`
-   D: `"12"`
    **Réponse : B**

## 94. Quelle est la valeur de `num` ?

```javascript
const num = parseInt('7*6', 10);
```

-   A: `42`
-   B: `"42"`
-   C: `7`
-   D: `NaN`
    **Réponse : C**

## 95. Quelle est la sortie ?

```javascript
[1, 2, 3].map((num) => {
    if (typeof num === 'number') return;
    return num * 2;
});
```

-   A: `[]`
-   B: `[null, null, null]`
-   C: `[undefined, undefined, undefined]`
-   D: `[ 3 x empty ]`
    **Réponse : C**

## 96. Quelle est la sortie ?

```javascript
function getInfo(member, year) {
    member.name = 'Lydia';
    year = '1998';
}

const person = { name: 'Sarah' };
const birthYear = '1997';

getInfo(person, birthYear);

console.log(person, birthYear);
```

-   A: `{ name: "Lydia" }, "1997"`
-   B: `{ name: "Sarah" }, "1998"`
-   C: `{ name: "Lydia" }, "1998"`
-   D: `{ name: "Sarah" }, "1997"`
    **Réponse : A**

## 97. Quelle est la sortie ?

```javascript
function greeting() {
    throw 'Hello world!';
}

function sayHi() {
    try {
        const data = greeting();
        console.log('It worked!', data);
    } catch (e) {
        console.log('Oh no an error!', e);
    }
}

sayHi();
```

-   A: `"It worked! Hello world!"`
-   B: `"Oh no an error: undefined`
-   C: `SyntaxError: can only throw Error objects`
-   D: `"Oh no an error: Hello world!`
    **Réponse : D**

## 98. Quelle est la sortie ?

```javascript
function Car() {
    this.make = 'Lamborghini';
    return { make: 'Maserati' };
}

const myCar = new Car();
console.log(myCar.make);
```

-   A: `"Lamborghini"`
-   B: `"Maserati"`
-   C: `ReferenceError`
-   D: `TypeError`
    **Réponse : B**

## 99. Quelle est la sortie ?

```javascript
(() => {
    let x, y;
    try {
        throw new Error();
    } catch (x) {
        (x = 1), (y = 2);
        console.log(x);
    }
    console.log(x);
    console.log(y);
})();
```

-   A: `1` `undefined` `2`
-   B: `undefined` `undefined` `undefined`
-   C: `1` `1` `2`
-   D: `1` `undefined` `undefined`
    **Réponse : A**

## 100. Tout en JavaScript est...

-   A: primitif ou objet
-   B: fonction ou objet
-   C: question délicate ! Seulement des objets
-   D: nombre ou objet
    **Réponse : A**

## 101. Quelle est la sortie ?

```javascript
[
    [0, 1],
    [2, 3],
].reduce(
    (acc, cur) => {
        return acc.concat(cur);
    },
    [1, 2],
);
```

-   A: `[0, 1, 2, 3, 1, 2]`
-   B: `[6, 1, 2]`
-   C: `[1, 2, 0, 1, 2, 3]`
-   D: `[1, 2, 6]`
    **Réponse : C**

## 102. Quelle est la sortie ?

```javascript
!!null;
!!'';
!!1;
```

-   A: `false` `true` `false`
-   B: `false` `false` `true`
-   C: `false` `true` `true`
-   D: `true` `true` `false`
    **Réponse : B**

## 103. Que retourne la méthode `setInterval` ?

```javascript
setInterval(() => console.log('Hi'), 1000);
```

-   A: un identifiant unique
-   B: le temps de millisecondes spécifié
-   C: la fonction passée en paramètre
-   D: `undefined`
    **Réponse : A**

## 104. Que retourne ceci ?

```javascript
[...'Lydia'];
```

-   A: `["L", "y", "d", "i", "a"]`
-   B: `["Lydia"]`
-   C: `[[], "Lydia"]`
-   D: `[["L", "y", "d", "i", "a"]]`
    **Réponse : A**

## 105. Quelle est la sortie ?

```javascript
function* generator(i) {
    yield i;
    yield i * 2;
}

const gen = generator(10);

console.log(gen.next().value);
console.log(gen.next().value);
```

-   A: `[0, 10], [10, 20]`
-   B: `20, 20`
-   C: `10, 20`
-   D: `0, 10 and 10, 20`
    **Réponse : C**

## 106. Qu'est-ce que cela retourne ?

```javascript
const firstPromise = new Promise((res, rej) => {
    setTimeout(res, 500, 'one');
});

const secondPromise = new Promise((res, rej) => {
    setTimeout(res, 100, 'two');
});

Promise.race([firstPromise, secondPromise]).then((res) => console.log(res));
```

-   A: `"one"`
-   B: `"two"`
-   C: `"two" "one"`
-   D: `"one" "two"`
    **Réponse : B**

## 107. Quelle est la sortie ?

```javascript
let person = { name: 'Lydia' };
const members = [person];
person = null;

console.log(members);
```

-   A: `null`
-   B: `[null]`
-   C: `[{}]`
-   D: `[{ name: "Lydia" }]`
    **Réponse : D**

## 108. Quelle est la sortie ?

```javascript
const person = { name: 'Lydia', age: 21 };

for (const item in person) {
    console.log(item);
}
```

-   A: `{ name: "Lydia" }, { age: 21 }`
-   B: `"name", "age"`
-   C: `"Lydia", 21`
-   D: `["name", "Lydia"], ["age", 21]`
    **Réponse : B**

## 109. Quelle est la sortie ?

```javascript
console.log(3 + 4 + '5');
```

-   A: `"345"`
-   B: `"75"`
-   C: `12`
-   D: `"12"`
    **Réponse : B**

## 110. Quelle est la valeur de `num` ?

```javascript
const num = parseInt('7*6', 10);
```

-   A: `42`
-   B: `"42"`
-   C: `7`
-   D: `NaN`
    **Réponse : C**

## 111. Quelle est la sortie ?

```javascript
[1, 2, 3].map((num) => {
    if (typeof num === 'number') return;
    return num * 2;
});
```

-   A: `[]`
-   B: `[null, null, null]`
-   C: `[undefined, undefined, undefined]`
-   D: `[ 3 x empty ]`
    **Réponse : C**

## 112. Quelle est la sortie ?

```javascript
function getInfo(member, year) {
    member.name = 'Lydia';
    year = '1998';
}

const person = { name: 'Sarah' };
const birthYear = '1997';

getInfo(person, birthYear);

console.log(person, birthYear);
```

-   A: `{ name: "Lydia" }, "1997"`
-   B: `{ name: "Sarah" }, "1998"`
-   C: `{ name: "Lydia" }, "1998"`
-   D: `{ name: "Sarah" }, "1997"`
    **Réponse : A**

## 113. Quelle est la sortie ?

```javascript
function greeting() {
    throw 'Hello world!';
}

function sayHi() {
    try {
        const data = greeting();
        console.log('It worked!', data);
    } catch (e) {
        console.log('Oh no an error!', e);
    }
}

sayHi();
```

-   A: `"It worked! Hello world!"`
-   B: `"Oh no an error: undefined`
-   C: `SyntaxError: can only throw Error objects`
-   D: `"Oh no an error: Hello world!`
    **Réponse : D**

## 114. Quelle est la sortie ?

```javascript
function Car() {
    this.make = 'Lamborghini';
    return { make: 'Maserati' };
}

const myCar = new Car();
console.log(myCar.make);
```

-   A: `"Lamborghini"`
-   B: `"Maserati"`
-   C: `ReferenceError`
-   D: `TypeError`
    **Réponse : B**

## 115. Quelle est la sortie ?

```javascript
(() => {
    let x, y;
    try {
        throw new Error();
    } catch (x) {
        (x = 1), (y = 2);
        console.log(x);
    }
    console.log(x);
    console.log(y);
})();
```

-   A: `1` `undefined` `2`
-   B: `undefined` `undefined` `undefined`
-   C: `1` `1` `2`
-   D: `1` `undefined` `undefined`
    **Réponse : A**

## 116. Tout en JavaScript est...

-   A: primitif ou objet
-   B: fonction ou objet
-   C: question délicate ! Seulement des objets
-   D: nombre ou objet
    **Réponse : A**

## 117. Quelle est la sortie ?

```javascript
[
    [0, 1],
    [2, 3],
].reduce(
    (acc, cur) => {
        return acc.concat(cur);
    },
    [1, 2],
);
```

-   A: `[0, 1, 2, 3, 1, 2]`
-   B: `[6, 1, 2]`
-   C: `[1, 2, 0, 1, 2, 3]`
-   D: `[1, 2, 6]`
    **Réponse : C**

## 118. Quelle est la sortie ?

```javascript
!!null;
!!'';
!!1;
```

-   A: `false` `true` `false`
-   B: `false` `false` `true`
-   C: `false` `true` `true`
-   D: `true` `true` `false`
    **Réponse : B**

## 119. Que retourne la méthode `setInterval` ?

```javascript
setInterval(() => console.log('Hi'), 1000);
```

-   A: un identifiant unique
-   B: le temps de millisecondes spécifié
-   C: la fonction passée en paramètre
-   D: `undefined`
    **Réponse : A**

## 120. Que retourne ceci ?

```javascript
[...'Lydia'];
```

-   A: `["L", "y", "d", "i", "a"]`
-   B: `["Lydia"]`
-   C: `[[], "Lydia"]`
-   D: `[["L", "y", "d", "i", "a"]]`
    **Réponse : A**

## 121. Quelle est la sortie ?

```javascript
function* generator(i) {
    yield i;
    yield i * 2;
}

const gen = generator(10);

console.log(gen.next().value);
console.log(gen.next().value);
```

-   A: `[0, 10], [10, 20]`
-   B: `20, 20`
-   C: `10, 20`
-   D: `0, 10 and 10, 20`
    **Réponse : C**

## 122. Qu'est-ce que cela retourne ?

```javascript
const firstPromise = new Promise((res, rej) => {
    setTimeout(res, 500, 'one');
});

const secondPromise = new Promise((res, rej) => {
    setTimeout(res, 100, 'two');
});

Promise.race([firstPromise, secondPromise]).then((res) => console.log(res));
```

-   A: `"one"`
-   B: `"two"`
-   C: `"two" "one"`
-   D: `"one" "two"`
    **Réponse : B**

## 123. Quelle est la sortie ?

```javascript
let person = { name: 'Lydia' };
const members = [person];
person = null;

console.log(members);
```

-   A: `null`
-   B: `[null]`
-   C: `[{}]`
-   D: `[{ name: "Lydia" }]`
    **Réponse : D**

## 124. Quelle est la sortie ?

```javascript
const person = { name: 'Lydia', age: 21 };

for (const item in person) {
    console.log(item);
}
```

-   A: `{ name: "Lydia" }, { age: 21 }`
-   B: `"name", "age"`
-   C: `"Lydia", 21`
-   D: `["name", "Lydia"], ["age", 21]`
    **Réponse : B**

## 125. Quelle est la sortie ?

```javascript
console.log(3 + 4 + '5');
```

-   A: `"345"`
-   B: `"75"`
-   C: `12`
-   D: `"12"`
    **Réponse : B**

## 126. Quelle est la valeur de `num` ?

```javascript
const num = parseInt('7*6', 10);
```

-   A: `42`
-   B: `"42"`
-   C: `7`
-   D: `NaN`
    **Réponse : C**

## 127. Quelle est la sortie ?

```javascript
[1, 2, 3].map((num) => {
    if (typeof num === 'number') return;
    return num * 2;
});
```

-   A: `[]`
-   B: `[null, null, null]`
-   C: `[undefined, undefined, undefined]`
-   D: `[ 3 x empty ]`
    **Réponse : C**

## 128. Quelle est la sortie ?

```javascript
function getInfo(member, year) {
    member.name = 'Lydia';
    year = '1998';
}

const person = { name: 'Sarah' };
const birthYear = '1997';

getInfo(person, birthYear);

console.log(person, birthYear);
```

-   A: `{ name: "Lydia" }, "1997"`
-   B: `{ name: "Sarah" }, "1998"`
-   C: `{ name: "Lydia" }, "1998"`
-   D: `{ name: "Sarah" }, "1997"`
    **Réponse : A**

## 129. Quelle est la sortie ?

```javascript
function greeting() {
    throw 'Hello world!';
}

function sayHi() {
    try {
        const data = greeting();
        console.log('It worked!', data);
    } catch (e) {
        console.log('Oh no an error!', e);
    }
}

sayHi();
```

-   A: `"It worked! Hello world!"`
-   B: `"Oh no an error: undefined`
-   C: `SyntaxError: can only throw Error objects`
-   D: `"Oh no an error: Hello world!`
    **Réponse : D**

## 130. Quelle est la sortie ?

```javascript
function Car() {
    this.make = 'Lamborghini';
    return { make: 'Maserati' };
}

const myCar = new Car();
console.log(myCar.make);
```

-   A: `"Lamborghini"`
-   B: `"Maserati"`
-   C: `ReferenceError`
-   D: `TypeError`
    **Réponse : B**

## 131. Quelle est la sortie ?

```javascript
(() => {
    let x, y;
    try {
        throw new Error();
    } catch (x) {
        (x = 1), (y = 2);
        console.log(x);
    }
    console.log(x);
    console.log(y);
})();
```

-   A: `1` `undefined` `2`
-   B: `undefined` `undefined` `undefined`
-   C: `1` `1` `2`
-   D: `1` `undefined` `undefined`
    **Réponse : A**

## 132. Tout en JavaScript est...

-   A: primitif ou objet
-   B: fonction ou objet
-   C: question délicate ! Seulement des objets
-   D: nombre ou objet
    **Réponse : A**

## 133. Quelle est la sortie ?

```javascript
[
    [0, 1],
    [2, 3],
].reduce(
    (acc, cur) => {
        return acc.concat(cur);
    },
    [1, 2],
);
```

-   A: `[0, 1, 2, 3, 1, 2]`
-   B: `[6, 1, 2]`
-   C: `[1, 2, 0, 1, 2, 3]`
-   D: `[1, 2, 6]`
    **Réponse : C**

## 134. Quelle est la sortie ?

```javascript
!!null;
!!'';
!!1;
```

-   A: `false` `true` `false`
-   B: `false` `false` `true`
-   C: `false` `true` `true`
-   D: `true` `true` `false`
    **Réponse : B**

## 135. Que retourne la méthode `setInterval` ?

```javascript
setInterval(() => console.log('Hi'), 1000);
```

-   A: un identifiant unique
-   B: le temps de millisecondes spécifié
-   C: la fonction passée en paramètre
-   D: `undefined`
    **Réponse : A**

## 136. Que retourne ceci ?

```javascript
[...'Lydia'];
```

-   A: `["L", "y", "d", "i", "a"]`
-   B: `["Lydia"]`
-   C: `[[], "Lydia"]`
-   D: `[["L", "y", "d", "i", "a"]]`
    **Réponse : A**

## 137. Quelle est la sortie ?

```javascript
function* generator(i) {
    yield i;
    yield i * 2;
}

const gen = generator(10);

console.log(gen.next().value);
console.log(gen.next().value);
```

-   A: `[0, 10], [10, 20]`
-   B: `20, 20`
-   C: `10, 20`
-   D: `0, 10 and 10, 20`
    **Réponse : C**

## 138. Qu'est-ce que cela retourne ?

```javascript
const firstPromise = new Promise((res, rej) => {
    setTimeout(res, 500, 'one');
});

const secondPromise = new Promise((res, rej) => {
    setTimeout(res, 100, 'two');
});

Promise.race([firstPromise, secondPromise]).then((res) => console.log(res));
```

-   A: `"one"`
-   B: `"two"`
-   C: `"two" "one"`
-   D: `"one" "two"`
    **Réponse : B**

## 139. Quelle est la sortie ?

```javascript
let person = { name: 'Lydia' };
const members = [person];
person = null;

console.log(members);
```

-   A: `null`
-   B: `[null]`
-   C: `[{}]`
-   D: `[{ name: "Lydia" }]`
    **Réponse : D**

## 140. Quelle est la sortie ?

```javascript
const person = { name: 'Lydia', age: 21 };

for (const item in person) {
    console.log(item);
}
```

-   A: `{ name: "Lydia" }, { age: 21 }`
-   B: `"name", "age"`
-   C: `"Lydia", 21`
-   D: `["name", "Lydia"], ["age", 21]`
    **Réponse : B**

## 141. Quelle est la sortie ?

```javascript
console.log(3 + 4 + '5');
```

-   A: `"345"`
-   B: `"75"`
-   C: `12`
-   D: `"12"`
    **Réponse : B**

## 142. Quelle est la valeur de `num` ?

```javascript
const num = parseInt('7*6', 10);
```

-   A: `42`
-   B: `"42"`
-   C: `7`
-   D: `NaN`
    **Réponse : C**

## 143. Quelle est la sortie ?

```javascript
[1, 2, 3].map((num) => {
    if (typeof num === 'number') return;
    return num * 2;
});
```

-   A: `[]`
-   B: `[null, null, null]`
-   C: `[undefined, undefined, undefined]`
-   D: `[ 3 x empty ]`
    **Réponse : C**

## 144. Quelle est la sortie ?

```javascript
function getInfo(member, year) {
    member.name = 'Lydia';
    year = '1998';
}

const person = { name: 'Sarah' };
const birthYear = '1997';

getInfo(person, birthYear);

console.log(person, birthYear);
```

-   A: `{ name: "Lydia" }, "1997"`
-   B: `{ name: "Sarah" }, "1998"`
-   C: `{ name: "Lydia" }, "1998"`
-   D: `{ name: "Sarah" }, "1997"`
    **Réponse : A**

## 145. Quelle est la sortie ?

```javascript
function greeting() {
    throw 'Hello world!';
}

function sayHi() {
    try {
        const data = greeting();
        console.log('It worked!', data);
    } catch (e) {
        console.log('Oh no an error!', e);
    }
}

sayHi();
```

-   A: `"It worked! Hello world!"`
-   B: `"Oh no an error: undefined`
-   C: `SyntaxError: can only throw Error objects`
-   D: `"Oh no an error: Hello world!`
    **Réponse : D**

## 146. Quelle est la sortie ?

```javascript
function Car() {
    this.make = 'Lamborghini';
    return { make: 'Maserati' };
}

const myCar = new Car();
console.log(myCar.make);
```

-   A: `"Lamborghini"`
-   B: `"Maserati"`
-   C: `ReferenceError`
-   D: `TypeError`
    **Réponse : B**

## 147. Quelle est la sortie ?

```javascript
(() => {
    let x, y;
    try {
        throw new Error();
    } catch (x) {
        (x = 1), (y = 2);
        console.log(x);
    }
    console.log(x);
    console.log(y);
})();
```

-   A: `1` `undefined` `2`
-   B: `undefined` `undefined` `undefined`
-   C: `1` `1` `2`
-   D: `1` `undefined` `undefined`
    **Réponse : A**

## 148. Tout en JavaScript est...

-   A: primitif ou objet
-   B: fonction ou objet
-   C: question délicate ! Seulement des objets
-   D: nombre ou objet
    **Réponse : A**

## 149. Quelle est la sortie ?

```javascript
[
    [0, 1],
    [2, 3],
].reduce(
    (acc, cur) => {
        return acc.concat(cur);
    },
    [1, 2],
);
```

-   A: `[0, 1, 2, 3, 1, 2]`
-   B: `[6, 1, 2]`
-   C: `[1, 2, 0, 1, 2, 3]`
-   D: `[1, 2, 6]`
    **Réponse : C**

## 150. Quelle est la sortie ?

```javascript
!!null;
!!'';
!!1;
```

-   A: `false` `true` `false`
-   B: `false` `false` `true`
-   C: `false` `true` `true`
-   D: `true` `true` `false`
    **Réponse : B**

## 151. Que retourne la méthode `setInterval` ?

```javascript
setInterval(() => console.log('Hi'), 1000);
```

-   A: un identifiant unique
-   B: le temps de millisecondes spécifié
-   C: la fonction passée en paramètre
-   D: `undefined`
    **Réponse : A**

## 152. Que retourne ceci ?

```javascript
[...'Lydia'];
```

-   A: `["L", "y", "d", "i", "a"]`
-   B: `["Lydia"]`
-   C: `[[], "Lydia"]`
-   D: `[["L", "y", "d", "i", "a"]]`
    **Réponse : A**

## 153. Quelle est la sortie ?

```javascript
function* generator(i) {
    yield i;
    yield i * 2;
}

const gen = generator(10);

console.log(gen.next().value);
console.log(gen.next().value);
```

-   A: `[0, 10], [10, 20]`
-   B: `20, 20`
-   C: `10, 20`
-   D: `0, 10 and 10, 20`
    **Réponse : C**

## 154. Qu'est-ce que cela retourne ?

```javascript
const firstPromise = new Promise((res, rej) => {
    setTimeout(res, 500, 'one');
});

const secondPromise = new Promise((res, rej) => {
    setTimeout(res, 100, 'two');
});

Promise.race([firstPromise, secondPromise]).then((res) => console.log(res));
```

-   A: `"one"`
-   B: `"two"`
-   C: `"two" "one"`
-   D: `"one" "two"`
    **Réponse : B**

## 155. Quelle est la sortie ?

```javascript
let person = { name: 'Lydia' };
const members = [person];
person = null;

console.log(members);
```

-   A: `null`
-   B: `[null]`
-   C: `[{}]`
-   D: `[{ name: "Lydia" }]`
    **Réponse : D**

## 156. Quelle est la sortie ?

```javascript
const person = { name: 'Lydia', age: 21 };

for (const item in person) {
    console.log(item);
}
```

-   A: `{ name: "Lydia" }, { age: 21 }`
-   B: `"name", "age"`
-   C: `"Lydia", 21`
-   D: `["name", "Lydia"], ["age", 21]`
    **Réponse : B**

## 157. Quelle est la sortie ?

```javascript
console.log(3 + 4 + '5');
```

-   A: `"345"`
-   B: `"75"`
-   C: `12`
-   D: `"12"`
    **Réponse : B**

## 158. Quelle est la valeur de `num` ?

```javascript
const num = parseInt('7*6', 10);
```

-   A: `42`
-   B: `"42"`
-   C: `7`
-   D: `NaN`
    **Réponse : C**

## 159. Quelle est la sortie ?

```javascript
[1, 2, 3].map((num) => {
    if (typeof num === 'number') return;
    return num * 2;
});
```

-   A: `[]`
-   B: `[null, null, null]`
-   C: `[undefined, undefined, undefined]`
-   D: `[ 3 x empty ]`
    **Réponse : C**

## 160. Quelle est la sortie ?

```javascript
function getInfo(member, year) {
    member.name = 'Lydia';
    year = '1998';
}

const person = { name: 'Sarah' };
const birthYear = '1997';

getInfo(person, birthYear);

console.log(person, birthYear);
```

-   A: `{ name: "Lydia" }, "1997"`
-   B: `{ name: "Sarah" }, "1998"`
-   C: `{ name: "Lydia" }, "1998"`
-   D: `{ name: "Sarah" }, "1997"`
    **Réponse : A**

## 161. Quelle est la sortie ?

```javascript
function greeting() {
    throw 'Hello world!';
}

function sayHi() {
    try {
        const data = greeting();
        console.log('It worked!', data);
    } catch (e) {
        console.log('Oh no an error!', e);
    }
}

sayHi();
```

-   A: `"It worked! Hello world!"`
-   B: `"Oh no an error: undefined`
-   C: `SyntaxError: can only throw Error objects`
-   D: `"Oh no an error: Hello world!`
    **Réponse : D**

## 162. Quelle est la sortie ?

```javascript
function Car() {
    this.make = 'Lamborghini';
    return { make: 'Maserati' };
}

const myCar = new Car();
console.log(myCar.make);
```

-   A: `"Lamborghini"`
-   B: `"Maserati"`
-   C: `ReferenceError`
-   D: `TypeError`
    **Réponse : B**

## 163. Quelle est la sortie ?

```javascript
(() => {
    let x, y;
    try {
        throw new Error();
    } catch (x) {
        (x = 1), (y = 2);
        console.log(x);
    }
    console.log(x);
    console.log(y);
})();
```

-   A: `1` `undefined` `2`
-   B: `undefined` `undefined` `undefined`
-   C: `1` `1` `2`
-   D: `1` `undefined` `undefined`
    **Réponse : A**

## 164. Tout en JavaScript est...

-   A: primitif ou objet
-   B: fonction ou objet
-   C: question délicate ! Seulement des objets
-   D: nombre ou objet
    **Réponse : A**

## 165. Quelle est la sortie ?

```javascript
[
    [0, 1],
    [2, 3],
].reduce(
    (acc, cur) => {
        return acc.concat(cur);
    },
    [1, 2],
);
```

-   A: `[0, 1, 2, 3, 1, 2]`
-   B: `[6, 1, 2]`
-   C: `[1, 2, 0, 1, 2, 3]`
-   D: `[1, 2, 6]`
    **Réponse : C**

## 166. Quelle est la sortie ?

```javascript
!!null;
!!'';
!!1;
```

-   A: `false` `true` `false`
-   B: `false` `false` `true`
-   C: `false` `true` `true`
-   D: `true` `true` `false`
    **Réponse : B**

## 167. Que retourne la méthode `setInterval` ?

```javascript
setInterval(() => console.log('Hi'), 1000);
```

-   A: un identifiant unique
-   B: le temps de millisecondes spécifié
-   C: la fonction passée en paramètre
-   D: `undefined`
    **Réponse : A**

## 168. Que retourne ceci ?

```javascript
[...'Lydia'];
```

-   A: `["L", "y", "d", "i", "a"]`
-   B: `["Lydia"]`
-   C: `[[], "Lydia"]`
-   D: `[["L", "y", "d", "i", "a"]]`
    **Réponse : A**

## 169. Quelle est la sortie ?

```javascript
function* generator(i) {
    yield i;
    yield i * 2;
}

const gen = generator(10);

console.log(gen.next().value);
console.log(gen.next().value);
```

-   A: `[0, 10], [10, 20]`
-   B: `20, 20`
-   C: `10, 20`
-   D: `0, 10 and 10, 20`
    **Réponse : C**

## 170. Qu'est-ce que cela retourne ?

```javascript
const firstPromise = new Promise((res, rej) => {
    setTimeout(res, 500, 'one');
});

const secondPromise = new Promise((res, rej) => {
    setTimeout(res, 100, 'two');
});

Promise.race([firstPromise, secondPromise]).then((res) => console.log(res));
```

-   A: `"one"`
-   B: `"two"`
-   C: `"two" "one"`
-   D: `"one" "two"`
    **Réponse : B**

## 171. Quelle est la sortie ?

```javascript
let person = { name: 'Lydia' };
const members = [person];
person = null;

console.log(members);
```

-   A: `null`
-   B: `[null]`
-   C: `[{}]`
-   D: `[{ name: "Lydia" }]`
    **Réponse : D**

## 172. Quelle est la sortie ?

```javascript
const person = { name: 'Lydia', age: 21 };

for (const item in person) {
    console.log(item);
}
```

-   A: `{ name: "Lydia" }, { age: 21 }`
-   B: `"name", "age"`
-   C: `"Lydia", 21`
-   D: `["name", "Lydia"], ["age", 21]`
    **Réponse : B**

## 173. Quelle est la sortie ?

```javascript
console.log(3 + 4 + '5');
```

-   A: `"345"`
-   B: `"75"`
-   C: `12`
-   D: `"12"`
    **Réponse : B**

## 174. Quelle est la sortie ?

```javascript
const a = [1, 2, 3];
const b = a;
b.push(4);
console.log(a);
```

-   A: `[1, 2, 3]`
-   B: `[1, 2, 3, 4]`
-   C: `undefined`
-   D: `ReferenceError`
    **Réponse : B**

## 175. Quelle est la sortie ?

```javascript
const obj = { a: 1, b: 2 };
const { a, b } = obj;
console.log(a, b);
```

-   A: `1 2`
-   B: `{ a: 1, b: 2 }`
-   C: `undefined undefined`
-   D: `ReferenceError`
    **Réponse : A**

## 176. Quelle est la sortie ?

```javascript
const arr = [1, 2, 3];
const newArr = arr.map((num) => num * 2);
console.log(newArr);
```

-   A: `[2, 4, 6]`
-   B: `[1, 2, 3]`
-   C: `undefined`
-   D: `ReferenceError`
    **Réponse : A**

## 177. Quelle est la sortie ?

```javascript
const x = 10;
const y = 20;

function add() {
    return x + y;
}

console.log(add());
```

-   A: `30`
-   B: `undefined`
-   C: `ReferenceError`
-   D: `NaN`
    **Réponse : A**

## 178. Quelle est la sortie ?

```javascript
const obj = { name: 'Lydia' };
const obj2 = Object.assign({}, obj);
obj2.name = 'Sarah';
console.log(obj.name);
```

-   A: `Lydia`
-   B: `Sarah`
-   C: `undefined`
-   D: `ReferenceError`
    **Réponse : A**

## 179. Quelle est la sortie ?

```javascript
const arr = [1, 2, 3];
const newArr = arr.filter((num) => num > 1);
console.log(newArr);
```

-   A: `[1]`
-   B: `[2, 3]`
-   C: `[1, 2, 3]`
-   D: `undefined`
    **Réponse : B**

## 180. Quelle est la sortie ?

```javascript
const a = 1;
const b = '1';
console.log(a == b);
console.log(a === b);
```

-   A: `true` `true`
-   B: `true` `false`
-   C: `false` `true`
-   D: `false` `false`
    **Réponse : B**

## 181. Quelle est la sortie ?

```javascript
const person = {
    name: 'Lydia',
    age: 21,
    greet() {
        console.log(`Hello, my name is ${this.name}`);
    },
};

person.greet();
```

-   A: `Hello, my name is Lydia`
-   B: `Hello, my name is undefined`
-   C: `Hello, my name is null`
-   D: `ReferenceError`
    **Réponse : A**

## 182. Quelle est la sortie ?

```javascript
const a = [1, 2, 3];
const b = a;
b[0] = 4;
console.log(a);
```

-   A: `[1, 2, 3]`
-   B: `[4, 2, 3]`
-   C: `undefined`
-   D: `ReferenceError`
    **Réponse : B**

## 183. Quelle est la sortie ?

```javascript
const obj = { a: 1, b: 2 };
const { a, ...rest } = obj;
console.log(rest);
```

-   A: `{ b: 2 }`
-   B: `{ a: 1 }`
-   C: `undefined`
-   D: `ReferenceError`
    **Réponse : A**

## 184. Quelle est la sortie ?

```javascript
const arr = [1, 2, 3];
const newArr = arr.map((num) => num * 2);
console.log(newArr);
```

-   A: `[2, 4, 6]`
-   B: `[1, 2, 3]`
-   C: `undefined`
-   D: `ReferenceError`
    **Réponse : A**

## 185. Quelle est la sortie ?

```javascript
const a = 1;
const b = 2;
const c = 3;

console.log(a + b + c);
```

-   A: `6`
-   B: `3`
-   C: `undefined`
-   D: `ReferenceError`
    **Réponse : A**

## 186. Quelle est la sortie ?

```javascript
const obj = { a: 1, b: 2 };
const obj2 = { ...obj };
obj2.a = 3;
console.log(obj.a);
```

-   A: `1`
-   B: `3`
-   C: `undefined`
-   D: `ReferenceError`
    **Réponse : A**

## 187. Quelle est la sortie ?

```javascript
const arr = [1, 2, 3];
const newArr = arr.reduce((acc, num) => acc + num, 0);
console.log(newArr);
```

-   A: `6`
-   B: `3`
-   C: `undefined`
-   D: `ReferenceError`
    **Réponse : A**

## 188. Quelle est la sortie ?

```javascript
const a = 1;
const b = '1';
console.log(a == b);
console.log(a === b);
```

-   A: `true` `true`
-   B: `true` `false`
-   C: `false` `true`
-   D: `false` `false`
    **Réponse : B**

## 189. Quelle est la sortie ?

```javascript
const person = {
    name: 'Lydia',
    age: 21,
    greet() {
        console.log(`Hello, my name is ${this.name}`);
    },
};

person.greet();
```

-   A: `Hello, my name is Lydia`
-   B: `Hello, my name is undefined`
-   C: `Hello, my name is null`
-   D: `ReferenceError`
    **Réponse : A**

## 190. Quelle est la sortie ?

```javascript
const a = [1, 2, 3];
const b = a;
b[0] = 4;
console.log(a);
```

-   A: `[1, 2, 3]`
-   B: `[4, 2, 3]`
-   C: `undefined`
-   D: `ReferenceError`
    **Réponse : B**

## 191. Quelle est la sortie ?

```javascript
const obj = { a: 1, b: 2 };
const { a, ...rest } = obj;
console.log(rest);
```

-   A: `{ b: 2 }`
-   B: `{ a: 1 }`
-   C: `undefined`
-   D: `ReferenceError`
    **Réponse : A**

## 192. Quelle est la sortie ?

```javascript
const arr = [1, 2, 3];
const newArr = arr.map((num) => num * 2);
console.log(newArr);
```

-   A: `[2, 4, 6]`
-   B: `[1, 2, 3]`
-   C: `undefined`
-   D: `ReferenceError`
    **Réponse : A**

## 193. Quelle est la sortie ?

```javascript
const a = 1;
const b = 2;
const c = 3;

console.log(a + b + c);
```

-   A: `6`
-   B: `3`
-   C: `undefined`
-   D: `ReferenceError`
    **Réponse : A**

## 194. Quelle est la sortie ?

```javascript
const obj = { a: 1, b: 2 };
const obj2 = { ...obj };
obj2.a = 3;
console.log(obj.a);
```

-   A: `1`
-   B: `3`
-   C: `undefined`
-   D: `ReferenceError`
    **Réponse : A**

## 195. Quelle est la sortie ?

```javascript
const arr = [1, 2, 3];
const newArr = arr.reduce((acc, num) => acc + num, 0);
console.log(newArr);
```

-   A: `6`
-   B: `3`
-   C: `undefined`
-   D: `ReferenceError`
    **Réponse : A**

## 196. Quelle est la sortie ?

```javascript
const a = 1;
const b = '1';
console.log(a == b);
console.log(a === b);
```

-   A: `true` `true`
-   B: `true` `false`
-   C: `false` `true`
-   D: `false` `false`
    **Réponse : B**

## 197. Quelle est la sortie ?

```javascript
const person = {
    name: 'Lydia',
    age: 21,
    greet() {
        console.log(`Hello, my name is ${this.name}`);
    },
};

person.greet();
```

-   A: `Hello, my name is Lydia`
-   B: `Hello, my name is undefined`
-   C: `Hello, my name is null`
-   D: `ReferenceError`
    **Réponse : A**

## 198. Quelle est la sortie ?

```javascript
const a = [1, 2, 3];
const b = a;
b[0] = 4;
console.log(a);
```

-   A: `[1, 2, 3]`
-   B: `[4, 2, 3]`
-   C: `undefined`
-   D: `ReferenceError`
    **Réponse : B**

## 199. Quelle est la sortie ?

```javascript
const obj = { a: 1, b: 2 };
const { a, ...rest } = obj;
console.log(rest);
```

-   A: `{ b: 2 }`
-   B: `{ a: 1 }`
-   C: `undefined`
-   D: `ReferenceError`
    **Réponse : A**

## 200. Quelle est la sortie ?

```javascript
const arr = [1, 2, 3];
const newArr = arr.map((num) => num * 2);
console.log(newArr);
```

-   A: `[2, 4, 6]`
-   B: `[1, 2, 3]`
-   C: `undefined`
-   D: `ReferenceError`
    **Réponse : A**
