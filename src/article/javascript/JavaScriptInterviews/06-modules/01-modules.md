---
title: Les Modules en JavaScript
date: 2024-10-29
icon: code
category:
    - JavaScript
tags:
    - modules
    - fondamentaux
description: Une introduction aux modules en JavaScript, leur utilisation et leur organisation.
---

# Les Modules en JavaScript

## Introduction

Les modules permettent d'organiser le code en unités réutilisables et indépendantes. Ils offrent un moyen de gérer les dépendances et d'encapsuler le code. JavaScript propose plusieurs systèmes de modules, mais nous nous concentrerons sur les modules ES6+, qui sont maintenant le standard.

## Syntaxe de Base

### Export

```javascript
// math.js
// Named export
export const PI = 3.14159;
export const add = (a, b) => a + b;
export const subtract = (a, b) => a - b;

// Default export
export default class Calculator {
    static multiply(a, b) {
        return a * b;
    }

    static divide(a, b) {
        if (b === 0) throw new Error('Division by zero');
        return a / b;
    }
}

// Grouped export
const square = (x) => x * x;
const cube = (x) => x * x * x;
export { square, cube };
```

### Import

```javascript
// main.js
// Named import
import { PI, add, subtract } from './math.js';

// Default import
import Calculator from './math.js';

// Combined import
import Calculator, { PI, add } from './math.js';

// Import with alias
import { add as addition, subtract as subtraction } from './math.js';

// Import all
import * as MathUtils from './math.js';
```

## Organisation des Modules

### Structure de Projet

```plaintext
project/
├── src/
│   ├── modules/
│   │   ├── math.js
│   │   ├── utils.js
│   │   └── config.js
│   ├── services/
│   │   ├── api.js
│   │   └── auth.js
│   └── main.js
└── package.json
```

### Modules Réutilisables

```javascript
// utils.js
export const formatDate = (date) => {
    return new Intl.DateTimeFormat('en-US').format(date);
};

export const generateId = () => {
    return Math.random().toString(36).substr(2, 9);
};

// Usage
import { formatDate, generateId } from './utils.js';
```

## Gestion des Dépendances

### Modules Imbriqués

```javascript
// services/api.js
import { generateId } from '../utils.js';
import { API_URL } from '../config.js';

export class ApiService {
    constructor() {
        this.baseUrl = API_URL;
    }

    async getData(endpoint) {
        const response = await fetch(`${this.baseUrl}/${endpoint}`);
        return response.json();
    }
}

// main.js
import { ApiService } from './services/api.js';
const api = new ApiService();
```

### Réexportation

```javascript
// services/index.js
export { ApiService } from './api.js';
export { AuthService } from './auth.js';

// main.js
import { ApiService, AuthService } from './services/index.js';
```

## Modules Asynchrones

### Import Dynamique

```javascript
// Conditional module loading
async function loadModule() {
    try {
        if (condition) {
            const module = await import('./large-module.js');
            module.initialize();
        }
    } catch (error) {
        console.error('Loading error:', error);
    }
}

// Parallel loading
Promise.all([import('./module1.js'), import('./module2.js')]).then(([module1, module2]) => {
    // Using modules
});
```

## Bonnes Pratiques

### Organisation du Code

```javascript
// One module per responsibility
// userService.js
export class UserService {
    // ...
}

// Avoid large modules
// Don't do:
export {
    UserService,
    ProductService,
    OrderService,
    // ... many other exports
};

// Prefer:
// services/index.js
export { UserService } from './userService.js';
export { ProductService } from './productService.js';
export { OrderService } from './orderService.js';
```

### Gestion des Exports

```javascript
// config.js
// Default configuration
const defaultConfig = {
    apiUrl: 'https://api.example.com',
    timeout: 5000,
};

// Selective export
export const { apiUrl, timeout } = defaultConfig;

// Or export with modification possibility
export const config = { ...defaultConfig };
```

### Gestion des Imports

```javascript
// Grouped and ordered imports
// External library imports
import React from 'react';
import { useState, useEffect } from 'react';

// Local module imports
import { UserService } from './services/user.js';
import { formatDate } from './utils/date.js';

// Style imports (if applicable)
import './styles/main.css';
```

## Patterns Avancés

### Module Singleton

```javascript
// database.js
let instance = null;

export class Database {
    constructor() {
        if (instance) {
            return instance;
        }
        instance = this;
        this.connections = new Map();
    }

    connect(url) {
        if (!this.connections.has(url)) {
            this.connections.set(url, new Connection(url));
        }
        return this.connections.get(url);
    }
}

// Usage
import { Database } from './database.js';
const db1 = new Database();
const db2 = new Database();
console.log(db1 === db2); // true
```

### Module Factory

```javascript
// loggerFactory.js
export const createLogger = (type) => {
    const log = (message) => {
        const timestamp = new Date().toISOString();
        console.log(`[${type}] ${timestamp}: ${message}`);
    };

    return {
        debug: (msg) => log(`DEBUG: ${msg}`),
        info: (msg) => log(`INFO: ${msg}`),
        error: (msg) => log(`ERROR: ${msg}`),
    };
};

// Usage
import { createLogger } from './loggerFactory.js';
const logger = createLogger('APP');
logger.info('Application démarrée');
```

## Gestion des Erreurs

```javascript
// errorBoundary.js
export class ModuleError extends Error {
    constructor(message, module) {
        super(message);
        this.name = 'ModuleError';
        this.module = module;
    }
}

export function safeImport(modulePath) {
    return import(modulePath).catch((error) => {
        throw new ModuleError(`Erreur lors du chargement du module: ${error.message}`, modulePath);
    });
}

// Usage
try {
    const module = await safeImport('./monModule.js');
} catch (error) {
    if (error instanceof ModuleError) {
        console.error(`Erreur dans le module ${error.module}:`, error.message);
    }
}
```

## Optimisation

### Code Splitting

```javascript
// routes.js
export const routes = {
    home: () => import('./pages/home.js'),
    profile: () => import('./pages/profile.js'),
    settings: () => import('./pages/settings.js'),
};

// Chargement à la demande
async function loadPage(route) {
    const module = await routes[route]();
    return module.default;
}
```

### Préchargement

```javascript
// preload.js
export function preloadModule(modulePath) {
    const link = document.createElement('link');
    link.rel = 'modulepreload';
    link.href = modulePath;
    document.head.appendChild(link);
}

// Usage
preloadModule('./heavyModule.js');
// Plus tard...
import('./heavyModule.js').then((module) => {
    // Le module est déjà préchargé
});
```
