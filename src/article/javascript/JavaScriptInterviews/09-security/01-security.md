---
title: La Sécurité en JavaScript
date: 2024-10-29
icon: code
category:
    - JavaScript
tags:
    - sécurité
    - bonnes pratiques
description: Un guide sur la sécurité en JavaScript, y compris les vulnérabilités courantes et les meilleures pratiques.
---

# La Sécurité en JavaScript

## Introduction

La sécurité est un aspect crucial du développement JavaScript, particulièrement dans le contexte des applications web. Comprendre les vulnérabilités courantes et les meilleures pratiques de sécurité est essentiel pour protéger vos applications et vos utilisateurs.

## Vulnérabilités Courantes

### Cross-Site Scripting (XSS)

```javascript
// ❌ Vulnerable to XSS
function displayComment(comment) {
    document.getElementById('comments').innerHTML = comment;
}

// ✅ XSS Protection
function displayCommentSecure(comment) {
    const sanitizedText = DOMPurify.sanitize(comment);
    document.getElementById('comments').textContent = sanitizedText;
}

// Manual HTML escaping example
function escapeHTML(str) {
    return str
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#039;');
}
```

### Injection SQL (dans Node.js)

```javascript
// ❌ Vulnerable to SQL injection
function searchUser(name) {
    const query = `SELECT * FROM users WHERE name = '${name}'`;
    return db.execute(query);
}

// ✅ Using prepared statements
function searchUserSecure(name) {
    const query = 'SELECT * FROM users WHERE name = ?';
    return db.execute(query, [name]);
}

// Example with ORM (Sequelize)
async function searchUserORM(name) {
    return await User.findOne({
        where: { name: name },
    });
}
```

## Protection des Données Sensibles

### Stockage Sécurisé

```javascript
// Secure storage configuration
const securityConfig = {
    // API keys and secrets
    apiKeys: {
        production: process.env.API_KEY,
        development: process.env.DEV_API_KEY,
    },
    // Hash options
    hashOptions: {
        iterations: 100000,
        keylen: 64,
        digest: 'sha512',
    },
};

// Secure password hashing
async function hashPassword(password) {
    const salt = crypto.randomBytes(16);
    return new Promise((resolve, reject) => {
        crypto.pbkdf2(
            password,
            salt,
            securityConfig.hashOptions.iterations,
            securityConfig.hashOptions.keylen,
            securityConfig.hashOptions.digest,
            (err, derivedKey) => {
                if (err) reject(err);
                resolve(derivedKey.toString('hex') + ':' + salt.toString('hex'));
            },
        );
    });
}

// Password verification
async function verifyPassword(password, storedHash) {
    const [hash, salt] = storedHash.split(':');
    return new Promise((resolve, reject) => {
        crypto.pbkdf2(
            password,
            Buffer.from(salt, 'hex'),
            securityConfig.hashOptions.iterations,
            securityConfig.hashOptions.keylen,
            securityConfig.hashOptions.digest,
            (err, derivedKey) => {
                if (err) reject(err);
                resolve(derivedKey.toString('hex') === hash);
            },
        );
    });
}
```

### Gestion des Tokens

```javascript
class TokenManager {
    static generateToken(userId, expiration = '1h') {
        return jwt.sign({ userId }, process.env.JWT_SECRET, {
            expiresIn: expiration,
        });
    }

    static verifyToken(token) {
        try {
            return jwt.verify(token, process.env.JWT_SECRET);
        } catch (error) {
            throw new Error('Invalid or expired token');
        }
    }

    static extractTokenFromHeader(req) {
        const authHeader = req.headers.authorization;
        if (!authHeader || !authHeader.startsWith('Bearer ')) {
            throw new Error('Missing token or invalid format');
        }
        return authHeader.split(' ')[1];
    }
}

// Authentication middleware
const authenticate = async (req, res, next) => {
    try {
        const token = TokenManager.extractTokenFromHeader(req);
        const decoded = TokenManager.verifyToken(token);
        req.user = decoded;
        next();
    } catch (error) {
        res.status(401).json({
            success: false,
            message: 'Authentication required',
        });
    }
};
```

## Protection Contre les Attaques Courantes

### CSRF (Cross-Site Request Forgery)

```javascript
// CSRF Middleware
const csrfProtection = (req, res, next) => {
    // Generate CSRF token
    const token = crypto.randomBytes(32).toString('hex');

    // Store token in session
    req.session.csrfToken = token;

    // Add token to response
    res.locals.csrfToken = token;

    next();
};

// CSRF token validation
const validateCSRF = (req, res, next) => {
    const token = req.body._csrf || req.headers['x-csrf-token'];

    if (!token || token !== req.session.csrfToken) {
        return res.status(403).json({
            success: false,
            message: 'Invalid CSRF token',
        });
    }

    next();
};

// Usage in a route
app.post('/api/action', csrfProtection, validateCSRF, (req, res) => {
    // Secure processing
});
```

### Content Security Policy (CSP)

```javascript
// CSP Configuration
const cspConfig = {
    directives: {
        defaultSrc: ["'self'"],
        scriptSrc: ["'self'", "'unsafe-inline'", 'trusted-cdn.com'],
        styleSrc: ["'self'", "'unsafe-inline'"],
        imgSrc: ["'self'", 'data:', 'images.cdn.com'],
        connectSrc: ["'self'", 'api.mysite.com'],
        fontSrc: ["'self'", 'fonts.googleapis.com'],
        objectSrc: ["'none'"],
        mediaSrc: ["'self'"],
        frameSrc: ["'none'"],
    },
};

// Middleware to apply CSP
const applyCSP = (req, res, next) => {
    const csp = Object.entries(cspConfig.directives)
        .map(([key, value]) => `${key} ${value.join(' ')}`)
        .join('; ');

    res.setHeader('Content-Security-Policy', csp);
    next();
};
```

## Validation des Entrées

### Validation Côté Client

```javascript
class ClientValidation {
    static validateEmail(email) {
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return regex.test(email);
    }

    static validatePassword(password) {
        // At least 8 characters, one uppercase, one lowercase, one number
        const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
        return regex.test(password);
    }

    static validateUsername(username) {
        // Alphanumeric, 3-20 characters
        const regex = /^[a-zA-Z0-9]{3,20}$/;
        return regex.test(username);
    }

    static sanitizeInput(input) {
        // Remove potentially dangerous characters
        return input.replace(/[<>'"]/g, '');
    }
}

// Form validation example
function validateForm(formData) {
    const errors = {};

    if (!ClientValidation.validateEmail(formData.email)) {
        errors.email = 'Invalid email format';
    }

    if (!ClientValidation.validatePassword(formData.password)) {
        errors.password = 'Password does not meet requirements';
    }

    if (!ClientValidation.validateUsername(formData.username)) {
        errors.username = 'Invalid username format';
    }

    return {
        isValid: Object.keys(errors).length === 0,
        errors,
    };
}
```

### Validation Côté Serveur

```javascript
class ValidationServeur {
    static async validerDonnees(donnees, schema) {
        try {
            await schema.validate(donnees, { abortEarly: false });
            return { estValide: true };
        } catch (erreur) {
            const erreurs = erreur.inner.reduce((acc, err) => {
                acc[err.path] = err.message;
                return acc;
            }, {});

            return {
                estValide: false,
                erreurs,
            };
        }
    }

    static sanitizerDonnees(donnees) {
        const resultat = {};

        for (const [cle, valeur] of Object.entries(donnees)) {
            if (typeof valeur === 'string') {
                // Supprimer les caractères dangereux
                resultat[cle] = valeur.replace(/[<>]/g, '');
            } else {
                resultat[cle] = valeur;
            }
        }

        return resultat;
    }
}

// Exemple d'utilisation avec Express
app.post('/api/utilisateur', async (req, res) => {
    // Sanitizer les données
    const donneesPropres = ValidationServeur.sanitizerDonnees(req.body);

    // Valider les données
    const { estValide, erreurs } = await ValidationServeur.validerDonnees(donneesPropres, schemaUtilisateur);

    if (!estValide) {
        return res.status(400).json({ success: false, erreurs });
    }

    // Traitement des données validées
    try {
        const utilisateur = await creerUtilisateur(donneesPropres);
        res.status(201).json({ success: true, utilisateur });
    } catch (erreur) {
        res.status(500).json({
            success: false,
            message: "Erreur lors de la création de l'utilisateur",
        });
    }
});
```

## Bonnes Pratiques

1. **Sécurité par Défaut**

    - Utiliser HTTPS
    - Activer les en-têtes de sécurité
    - Implémenter le principe du moindre privilège
    - Valider toutes les entrées utilisateur

2. **Gestion des Secrets**

    - Ne jamais stocker de secrets en dur dans le code
    - Utiliser des variables d'environnement
    - Chiffrer les données sensibles
    - Utiliser des gestionnaires de secrets

3. **Authentification et Autorisation**

    - Implémenter une authentification forte
    - Utiliser des sessions sécurisées
    - Gérer correctement les permissions
    - Limiter les tentatives de connexion

4. **Protection des Données**

    - Chiffrer les données sensibles
    - Utiliser des algorithmes de hachage sécurisés
    - Implémenter une politique de rétention des données
    - Sauvegarder régulièrement les données

## Exemples Pratiques

### Middleware de Sécurité Express

```javascript
const helmet = require('helmet');
const rateLimit = require('express-rate-limit');

// Configuration de base de la sécurité
app.use(helmet());

// Limitation des requêtes
const limiter = rateLimit({
    windowMs: 15 * 60 * 1000, // 15 minutes
    max: 100, // limite par IP
});
app.use('/api/', limiter);

// En-têtes de sécurité personnalisés
app.use((req, res, next) => {
    res.setHeader('X-Content-Type-Options', 'nosniff');
    res.setHeader('X-Frame-Options', 'DENY');
    res.setHeader('X-XSS-Protection', '1; mode=block');
    next();
});

// Middleware de validation des entrées
const validerEntrees = (schema) => async (req, res, next) => {
    try {
        const donnees = await schema.validate(req.body);
        req.donnees = donnees;
        next();
    } catch (erreur) {
        res.status(400).json({
            success: false,
            message: 'Données invalides',
            erreurs: erreur.errors,
        });
    }
};
```

### Sécurité des WebSockets

```javascript
class WebSocketSecurite {
    constructor(wss) {
        this.wss = wss;
        this.configurerSecurite();
    }

    configurerSecurite() {
        // Validation de l'origine
        this.wss.on('connection', (ws, req) => {
            if (!this.validerOrigine(req)) {
                ws.terminate();
                return;
            }

            // Authentification
            if (!this.authentifier(req)) {
                ws.terminate();
                return;
            }

            // Gestion des messages
            ws.on('message', (message) => {
                if (!this.validerMessage(message)) {
                    ws.terminate();
                    return;
                }
                // Traitement du message
                this.traiterMessage(ws, message);
            });
        });
    }

    validerOrigine(req) {
        const origine = req.headers.origin;
        return origine === process.env.ALLOWED_ORIGIN;
    }

    authentifier(req) {
        const token = req.headers['sec-websocket-protocol'];
        try {
            const decoded = TokenManager.verifyToken(token);
            return !!decoded;
        } catch (erreur) {
            return false;
        }
    }

    validerMessage(message) {
        try {
            const data = JSON.parse(message);
            // Validation du schéma du message
            return this.validerSchemaMessage(data);
        } catch (erreur) {
            return false;
        }
    }

    validerSchemaMessage(data) {
        // Implémenter la validation selon vos besoins
        return true;
    }

    traiterMessage(ws, message) {
        // Traitement sécurisé du message
        try {
            const data = JSON.parse(message);
            // Traitement...
        } catch (erreur) {
            ws.send(
                JSON.stringify({
                    type: 'erreur',
                    message: 'Message invalide',
                }),
            );
        }
    }
}
```
