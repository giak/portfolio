---
title: Les Opérateurs en JavaScript
date: 2024-10-29
icon: code
category:
    - JavaScript
tags:
    - opérateurs
    - fondamentaux
description: Une exploration des différents opérateurs disponibles en JavaScript et leur utilisation.
---

# Les Opérateurs en JavaScript

## Introduction

Les opérateurs permettent d'effectuer des opérations sur des variables et des valeurs. JavaScript propose différents types d'opérateurs pour réaliser des calculs, des comparaisons et des manipulations logiques.

## Opérateurs Arithmétiques

Les opérateurs arithmétiques permettent d'effectuer des calculs mathématiques.

```javascript
// Basic operations
let a = 10;
let b = 3;

let addition = a + b; // 13
let subtraction = a - b; // 7
let multiplication = a * b; // 30
let division = a / b; // 3.3333...
let modulo = a % b; // 1 (division remainder)
let exponent = a ** b; // 1000 (10 to the power of 3)

// Increment and decrement
let counter = 0;
counter++; // Increment (counter = 1)
counter--; // Decrement (counter = 0)

// Prefix vs Postfix
let x = 5;
let y = ++x; // x = 6, y = 6 (increment then assign)
let z = x++; // x = 7, z = 6 (assign then increment)
```

## Opérateurs d'Affectation

Ces opérateurs permettent d'assigner des valeurs aux variables.

```javascript
let x = 10; // Simple assignment

// Compound assignments
x += 5; // x = x + 5
x -= 3; // x = x - 3
x *= 2; // x = x * 2
x /= 4; // x = x / 4
x %= 3; // x = x % 3
x **= 2; // x = x ** 2

// Destructuring assignment (ES6+)
let [a, b] = [1, 2]; // a = 1, b = 2
let { name, age } = { name: 'Alice', age: 25 }; // name = 'Alice', age = 25
```

## Opérateurs de Comparaison

Permettent de comparer des valeurs et retournent un booléen.

```javascript
let a = 5;
let b = '5';

// Simple comparisons
console.log(a == b); // true (equality with type conversion)
console.log(a === b); // false (strict equality)
console.log(a != b); // false (inequality with conversion)
console.log(a !== b); // true (strict inequality)

// Order comparisons
console.log(a > 3); // true
console.log(a < 10); // true
console.log(a >= 5); // true
console.log(a <= 4); // false

// Special cases
console.log(null == undefined); // true
console.log(null === undefined); // false
console.log(NaN == NaN); // false (NaN is not equal to anything)
```

## Opérateurs Logiques

Permettent de combiner des conditions logiques.

```javascript
let x = 5;
let y = 10;

// Logical AND (&&)
console.log(x > 0 && y < 20); // true (both conditions are true)
console.log(x > 10 && y < 20); // false

// Logical OR (||)
console.log(x > 0 || y > 20); // true (at least one condition is true)
console.log(x > 10 || y > 20); // false

// Logical NOT (!)
console.log(!true); // false
console.log(!(x > y)); // true

// Short-circuit
let a = null;
let b = a || 'default value'; // b = 'default value'
```

## Opérateurs de Chaînes

Pour manipuler les chaînes de caractères.

```javascript
// Concatenation
let firstName = 'John';
let lastName = 'Smith';
let fullName = firstName + ' ' + lastName; // 'John Smith'

// Template literals (ES6+)
let introduction = `My name is ${firstName} ${lastName}`;

// Concatenation with +=
let message = 'Hello ';
message += 'everyone'; // 'Hello everyone'
```

## Opérateurs Spéciaux

### Opérateur Ternaire

```javascript
// Syntax: condition ? valueIfTrue : valueIfFalse
let age = 20;
let status = age >= 18 ? 'adult' : 'minor';

// Chaining ternaries (avoid for readability)
let message = age < 13 ? 'child' : age < 18 ? 'teenager' : age < 65 ? 'adult' : 'senior';
```

### Opérateur Nullish Coalescing (??) - ES2020

```javascript
let value = null;
let result = value ?? 'default value'; // 'default value'

// Difference with ||
let zero = 0;
console.log(zero || 'default'); // 'default'
console.log(zero ?? 'default'); // 0
```

### Opérateur Optional Chaining (?.) - ES2020

```javascript
let user = {
    profile: {
        name: 'Alice',
    },
};

// Without optional chaining
let name1 = user && user.profile && user.profile.name;

// With optional chaining
let name2 = user?.profile?.name; // 'Alice'
let age = user?.profile?.age; // undefined
```

## Précédence des Opérateurs

La précédence détermine l'ordre d'évaluation des opérateurs.

```javascript
// Parentheses have the highest precedence
let result = (2 + 3) * 4; // 20
let calculation = 2 + 3 * 4; // 14

// Logical operators precedence
let test = (true && false) || true; // true (&& before ||)
let test2 = (true && false) || true; // same thing, more explicit
```

## Bonnes Pratiques

1. **Lisibilité**

    - Utilisez des parenthèses pour clarifier l'ordre des opérations
    - Évitez les expressions trop complexes

2. **Comparaisons**

    - Privilégiez === et !== pour éviter les conversions implicites
    - Attention aux comparaisons avec null et undefined

3. **Opérateurs Logiques**

    - Utilisez les courts-circuits avec précaution
    - Préférez ?? à || pour les valeurs par défaut quand 0 est valide

4. **Maintenance**
    - Évitez les opérateurs trop complexes
    - Documentez les expressions non évidentes
