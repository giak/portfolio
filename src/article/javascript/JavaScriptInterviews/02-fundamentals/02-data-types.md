---
title: Les Types de Données en JavaScript
date: 2024-10-29
icon: code
category:
    - JavaScript
tags:
    - types de données
    - fondamentaux
description: Un guide sur les types de données en JavaScript, y compris les types primitifs et complexes.
---

# Les Types de Données en JavaScript

## Introduction

JavaScript est un langage à typage dynamique, ce qui signifie que les variables peuvent contenir différents types de données et que ces types peuvent changer au cours de l'exécution du programme.

## Types Primitifs

JavaScript possède 6 types de données primitifs :

### 1. Number (Nombre)

```javascript
// Integers
let age = 25;
let temperature = -5;

// Decimals
let price = 19.99;
let pi = 3.14159;

// Special values
let infinity = Infinity;
let notANumber = NaN; // Not a Number
```

### 2. String (Chaîne de caractères)

```javascript
// With single quotes
let firstName = 'Mary';

// With double quotes
let lastName = 'Smith';

// With backticks (template literals)
let message = `Hello ${firstName} ${lastName}`;

// Special characters
let path = 'C:\\Documents\\Photos'; // Escaping with \
```

### 3. Boolean (Booléen)

```javascript
let isTrue = true;
let isFalse = false;

// Boolean expressions
let isGreater = 5 > 3; // true
let isEqual = 4 === '4'; // false
```

### 4. Undefined

```javascript
// Declared but not initialized variable
let variable;
console.log(variable); // undefined

// Non-existent object property
let obj = {};
console.log(obj.nonExistentProperty); // undefined
```

### 5. Null

```javascript
// Intentional null value
let data = null;

// Difference with undefined
console.log(typeof null); // 'object' (a historical bug)
console.log(typeof undefined); // 'undefined'
```

### 6. Symbol (ES6+)

```javascript
// Creating a unique symbol
let symbol = Symbol('description');
let otherSymbol = Symbol('description');

console.log(symbol === otherSymbol); // false
```

## Types Complexes

### 1. Object (Objet)

```javascript
// Literal object
let person = {
    name: 'Smith',
    age: 30,
    address: {
        street: '123 Main Street',
        city: 'London',
    },
};

// Accessing properties
console.log(person.name); // Dot notation
console.log(person['age']); // Bracket notation
```

### 2. Array (Tableau)

```javascript
// Creating an array
let fruits = ['apple', 'banana', 'orange'];

// Mixed array (not recommended)
let mixed = [42, 'text', true, { key: 'value' }];

// Accessing elements
console.log(fruits[0]); // First element
console.log(fruits.length); // Array length
```

## Conversion de Types

### Conversion Explicite

```javascript
// To String
let num = 123;
let str1 = String(num); // Method 1
let str2 = num.toString(); // Method 2

// To Number
let str = '123';
let num1 = Number(str); // Method 1
let num2 = parseInt(str); // For integers
let num3 = parseFloat(str); // For decimals

// To Boolean
let val = 1;
let bool = Boolean(val); // true
```

### Conversion Implicite

```javascript
// Addition with string
let result = '3' + 4; // '34' (concatenation)
let sum = '3' - 2; // 1 (numeric conversion)

// Comparison
console.log(5 == '5'); // true (comparison with conversion)
console.log(5 === '5'); // false (strict comparison)
```

## Vérification de Types

### Utilisation de typeof

```javascript
console.log(typeof 42); // 'number'
console.log(typeof 'text'); // 'string'
console.log(typeof true); // 'boolean'
console.log(typeof undefined); // 'undefined'
console.log(typeof null); // 'object'
console.log(typeof {}); // 'object'
console.log(typeof []); // 'object'
console.log(typeof function () {}); // 'function'
```

### Vérification des Tableaux

```javascript
// Recommended methods
console.log(Array.isArray([])); // true
console.log(Array.isArray({})); // false
```

## Bonnes Pratiques

1. **Cohérence des Types**

    - Évitez de changer le type d'une variable
    - Utilisez des noms explicites indiquant le type

2. **Comparaisons**

    - Privilégiez === et !== aux == et !=
    - Évitez les comparaisons implicites

3. **Conversions**

    - Préférez les conversions explicites
    - Documentez les conversions complexes

4. **Null et Undefined**
    - Utilisez null pour une absence intentionnelle de valeur
    - Laissez undefined pour les variables non initialisées
