---
title: Les Variables en JavaScript
date: 2024-10-29
icon: code
category:
    - JavaScript
tags:
    - variables
    - fondamentaux
description: Une introduction aux variables en JavaScript, leur déclaration et leur utilisation.
---

# Les Variables en JavaScript

## Introduction aux Variables

Les variables sont des "conteneurs" pour stocker des informations. En JavaScript, elles sont utilisées pour contenir des valeurs ou des expressions qui peuvent être utilisées et modifiées tout au long de l'exécution du script.

## Déclaration des Variables

Il existe plusieurs façons de déclarer des variables en JavaScript :

1. **Utilisation de `var`** (ancienne méthode)

    ```javascript
    var myVariable = 'Hello';
    var age = 25;
    var isActive = true;
    ```

2. **Utilisation de `let`** (ES6+, recommandé)

    ```javascript
    let name = 'Peter';
    let score = 100;
    ```

3. **Utilisation de `const`** (pour les valeurs constantes)
    ```javascript
    const PI = 3.14159;
    const SPEED_OF_LIGHT = 299792458;
    ```

## Règles de Nommage

Les variables en JavaScript doivent suivre certaines règles de nommage :

-   Les noms sont sensibles à la casse (`myVariable` et `myvariable` sont différents)
-   Ils doivent commencer par une lettre ou un underscore (\_)
-   Ils peuvent contenir des lettres, chiffres et underscores
-   Les mots réservés ne peuvent pas être utilisés comme noms de variables

### Bonnes Pratiques

```javascript
// ✅ Good variable names
let userName = "John";
let userAge = 30;
let isActive = true;

// ❌ Bad variable names
let a = "John";      // Not descriptive enough
let 123user = "Peter";  // Starts with a number
let user-name = "Paul";  // Contains a hyphen
```

## Portée des Variables

La portée d'une variable définit où elle peut être utilisée dans le code :

1. **Variables Globales**

    ```javascript
    var global = 'I am accessible everywhere';
    ```

2. **Variables Locales**

    ```javascript
    function myFunction() {
        let local = 'I am only accessible in myFunction';
    }
    ```

3. **Variables de Bloc**
    ```javascript
    if (true) {
        let blockVariable = 'I am only accessible in this block';
    }
    ```

## Réaffectation et Redéclaration

```javascript
// Reassignment (possible with let)
let score = 5;
score = 10; // ✅ Valid

// Redeclaration (impossible with let in the same block)
let points = 5;
let points = 10; // ❌ Error

// With const
const PI = 3.14;
PI = 3.15; // ❌ Error: cannot reassign a constant
```

## Hoisting (Remontée des Déclarations)

JavaScript "remonte" les déclarations de variables, mais pas les initialisations :

```javascript
console.log(x); // undefined
var x = 5;

// Equivalent to:
var x;
console.log(x);
x = 5;

// With let and const
console.log(y); // ❌ Error: y is not defined
let y = 5;
```

## Types de Valeurs

Les variables peuvent contenir différents types de valeurs :

```javascript
// Numbers
let age = 25;
let price = 19.99;

// Strings
let name = 'Mary';
let message = 'Hello';

// Booleans
let isTrue = true;
let isFalse = false;

// Null and Undefined
let empty = null;
let notDefined; // undefined by default

// Arrays
let colors = ['red', 'green', 'blue'];

// Objects
let person = {
    name: 'Peter',
    age: 30,
};
```

## Vérification du Type

Pour vérifier le type d'une variable, utilisez l'opérateur `typeof` :

```javascript
let number = 42;
console.log(typeof number); // "number"

let text = 'Hello';
console.log(typeof text); // "string"

let active = true;
console.log(typeof active); // "boolean"
```

## Bonnes Pratiques d'Utilisation

1. **Toujours déclarer les variables**

    - Utilisez `let` ou `const` plutôt que `var`
    - Déclarez les variables en début de fonction

2. **Nommage explicite**

    - Utilisez des noms qui décrivent le contenu
    - Suivez une convention de nommage cohérente

3. **Utilisation de const**

    - Privilégiez `const` quand la valeur ne doit pas changer
    - Utilisez `let` uniquement quand nécessaire

4. **Évitez les variables globales**
    - Limitez la portée des variables au minimum nécessaire
    - Utilisez des modules ou des fonctions pour encapsuler les variables
