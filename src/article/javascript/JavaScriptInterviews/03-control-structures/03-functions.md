---
title: Les Fonctions en JavaScript
date: 2024-10-29
icon: code
category:
    - JavaScript
tags:
    - fonctions
    - fondamentaux
description: Un guide sur les fonctions en JavaScript, y compris leur déclaration et leur utilisation.
---

# Les Fonctions en JavaScript

## Introduction

Les fonctions sont des blocs de code réutilisables qui peuvent être appelés à tout moment dans un programme. Elles sont fondamentales en JavaScript et constituent l'un des principaux mécanismes d'organisation et de réutilisation du code.

## Déclaration de Fonctions

### Fonction Classique

```javascript
// Basic function declaration
function sayHello() {
    console.log('Hello!');
}

// Function with parameters
function add(a, b) {
    return a + b;
}

// Function with default values (ES6+)
function greet(name = 'visitor') {
    console.log(`Hello ${name}!`);
}
```

### Expression de Fonction

```javascript
// Function expression
const multiply = function (a, b) {
    return a * b;
};

// Named function expression
const calculate = function calculateOperation(x, y, operation) {
    return operation(x, y);
};
```

### Fonctions Fléchées (ES6+)

```javascript
// Basic syntax
const double = (x) => x * 2;

// With multiple parameters
const add = (a, b) => a + b;

// With function body
const calculateArea = (length, width) => {
    const area = length * width;
    return area;
};

// Return an object
const createPerson = (name, age) => ({ name, age });
```

## Paramètres de Fonction

### Paramètres Rest

```javascript
// Collect remaining arguments
function sum(...numbers) {
    return numbers.reduce((total, n) => total + n, 0);
}

// Use with other parameters
function logger(prefix, ...messages) {
    messages.forEach((msg) => console.log(`${prefix}: ${msg}`));
}
```

### Déstructuration des Paramètres

```javascript
// Object destructuring
function displayPerson({ name, age, city = 'Unknown' }) {
    console.log(`${name}, ${age} years old, lives in ${city}`);
}

// Array destructuring
function displayCoordinates([x, y, z = 0]) {
    console.log(`Point: (${x}, ${y}, ${z})`);
}
```

## Portée et Contexte

### Portée Lexicale

```javascript
const variable = 'global';

function outer() {
    const variable = 'outer';

    function inner() {
        const variable = 'inner';
        console.log(variable); // 'inner'
    }

    inner();
    console.log(variable); // 'outer'
}

console.log(variable); // 'global'
```

### Le mot-clé this

```javascript
// In an object method
const person = {
    name: 'Alice',
    sayHello() {
        console.log(`Hello, I am ${this.name}`);
    },
};

// With arrow function
const counter = {
    count: 0,
    increment: function () {
        setInterval(() => {
            this.count++;
            console.log(this.count);
        }, 1000);
    },
};
```

## Fonctions Avancées

### Fonctions de Rappel (Callbacks)

```javascript
// Simple callback example
function processData(data, callback) {
    const result = data.map((x) => x * 2);
    callback(result);
}

processData([1, 2, 3], (result) => {
    console.log('Result:', result);
});
```

### Fonctions d'Ordre Supérieur

```javascript
// Function that returns a function
function multiplyBy(factor) {
    return function (x) {
        return x * factor;
    };
}

const double = multiplyBy(2);
const triple = multiplyBy(3);

console.log(double(5)); // 10
console.log(triple(5)); // 15
```

### Fonctions Pures

```javascript
// Pure function
function addPure(a, b) {
    return a + b;
}

// Impure function (side effect)
let total = 0;
function addImpure(a) {
    total += a;
    return total;
}
```

## Gestion des Erreurs

```javascript
// Using try/catch
function divide(a, b) {
    try {
        if (b === 0) {
            throw new Error('Division by zero is not allowed');
        }
        return a / b;
    } catch (error) {
        console.error('Error:', error.message);
        return null;
    }
}
```

## Bonnes Pratiques

1. **Nommage et Organisation**

    - Utilisez des noms descriptifs pour les fonctions
    - Une fonction = une responsabilité
    - Limitez le nombre de paramètres
    - Documentez les fonctions complexes

2. **Performance**

    - Évitez les fonctions trop longues
    - Optimisez les fonctions récursives
    - Utilisez la mémoïsation pour les calculs coûteux

3. **Maintenabilité**

    - Préférez les fonctions pures quand possible
    - Évitez les effets de bord
    - Utilisez des valeurs par défaut pour les paramètres optionnels

4. **Sécurité**

    - Validez toujours les paramètres d'entrée
    - Gérez correctement les erreurs
    - Évitez l'utilisation excessive de variables globales

## Exemples Pratiques

### Mémoïsation

```javascript
function memoize(fn) {
    const cache = new Map();
    return function (...args) {
        const key = JSON.stringify(args);
        if (cache.has(key)) {
            return cache.get(key);
        }
        const result = fn.apply(this, args);
        cache.set(key, result);
        return result;
    };
}

// Utilisation
const fibonacci = memoize(function (n) {
    if (n <= 1) return n;
    return fibonacci(n - 1) + fibonacci(n - 2);
});
```

### Composition de Fonctions

```javascript
// Composition simple
const compose = (f, g) => (x) => f(g(x));

const addOne = (x) => x + 1;
const double = (x) => x * 2;

const addOneThenDouble = compose(double, addOne);
console.log(addOneThenDouble(3)); // 8
```
