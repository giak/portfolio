---
title: Les Structures Conditionnelles en JavaScript
date: 2024-10-29
icon: code
category:
    - JavaScript
tags:
    - conditions
    - structures de contrôle
description: Un aperçu des structures conditionnelles en JavaScript, y compris if, else et switch.
---

# Les Structures Conditionnelles en JavaScript

## Introduction

Les structures conditionnelles permettent d'exécuter différentes portions de code en fonction de conditions spécifiques. JavaScript offre plusieurs façons d'implémenter la logique conditionnelle.

## La Structure if

La structure `if` est la plus simple des structures conditionnelles.

```javascript
// Simple if structure
if (condition) {
    // Code executed if condition is true
}

// Concrete example
let age = 18;
if (age >= 18) {
    console.log('You are an adult');
}
```

## La Structure if...else

Permet d'exécuter un bloc de code alternatif si la condition n'est pas vraie.

```javascript
let age = 16;

if (age >= 18) {
    console.log('You are an adult');
} else {
    console.log('You are a minor');
}

// Compact version (for simple instructions)
if (age >= 18) console.log('Adult');
else console.log('Minor');
```

## La Structure if...else if...else

Pour gérer plusieurs conditions différentes.

```javascript
let grade = 15;

if (grade >= 16) {
    console.log('Very good');
} else if (grade >= 14) {
    console.log('Good');
} else if (grade >= 12) {
    console.log('Fair');
} else if (grade >= 10) {
    console.log('Pass');
} else {
    console.log('Fail');
}
```

## La Structure switch

Utile pour comparer une valeur à plusieurs cas possibles.

```javascript
let day = 'Tuesday';

switch (day) {
    case 'Monday':
        console.log('Start of the week');
        break;
    case 'Tuesday':
    case 'Wednesday':
    case 'Thursday':
        console.log('Mid-week');
        break;
    case 'Friday':
        console.log('End of the week');
        break;
    case 'Saturday':
    case 'Sunday':
        console.log('Weekend');
        break;
    default:
        console.log('Invalid day');
}
```

## Opérateur Ternaire

Une forme concise pour les conditions simples.

```javascript
// Syntax: condition ? valueIfTrue : valueIfFalse
let age = 20;
let status = age >= 18 ? 'adult' : 'minor';

// Equivalent to:
let status2;
if (age >= 18) {
    status2 = 'adult';
} else {
    status2 = 'minor';
}

// Nested ternaries (use with moderation)
let message = age < 13 ? 'child' : age < 18 ? 'teenager' : age < 65 ? 'adult' : 'senior';
```

## Conditions avec Opérateurs Logiques

### Utilisation de AND (&&)

```javascript
let age = 25;
let hasLicense = true;

if (age >= 18 && hasLicense) {
    console.log('You can drive');
}

// Using as short-circuit
age >= 18 && hasLicense && console.log('You can drive');
```

### Utilisation de OR (||)

```javascript
let isAdmin = false;
let isModerator = true;

if (isAdmin || isModerator) {
    console.log('Access granted');
}

// Default value with OR
let name = user.name || 'Anonymous';
```

### Utilisation de NOT (!)

```javascript
let isLoggedIn = false;

if (!isLoggedIn) {
    console.log('Please log in');
}
```

## Conditions avec Nullish Coalescing (??)

```javascript
let value = 0;
let result = value ?? 'default value'; // returns 0

// Difference with OR
let resultOr = value || 'default value'; // returns 'default value'
```

## Bonnes Pratiques

1. **Lisibilité**

    - Utilisez des accolades même pour les instructions simples
    - Indentez correctement votre code
    - Évitez les conditions trop complexes

2. **Optimisation**

    - Ordonnez vos conditions du cas le plus probable au moins probable
    - Évitez les conditions redondantes
    - Utilisez switch pour les comparaisons multiples avec une même variable

3. **Maintenabilité**

    - Préférez des noms de variables explicites dans les conditions
    - Évitez les ternaires imbriqués complexes
    - Commentez les conditions complexes

4. **Sécurité**
    - Vérifiez toujours les valeurs nulles ou undefined
    - Utilisez === plutôt que == pour éviter les conversions implicites
    - Prévoyez toujours un cas par défaut

## Pièges Courants

```javascript
// Comparison with null/undefined
if (variable == null) {
    // checks for both null AND undefined
    console.log('Variable not defined');
}

// Avoid implicit comparisons
if (value) {
    // converts to boolean
    console.log('Truthy value');
}

// Falsy values in JavaScript
if (false) {
} // false
if (0) {
} // 0
if ('') {
} // empty string
if (null) {
} // null
if (undefined) {
} // undefined
if (NaN) {
} // NaN
```
