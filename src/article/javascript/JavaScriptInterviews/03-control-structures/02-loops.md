---
title: Les Boucles en JavaScript
date: 2024-10-29
icon: code
category:
    - JavaScript
tags:
    - boucles
    - structures de contrôle
description: Une introduction aux différentes boucles disponibles en JavaScript et leur utilisation.
---

# Les Boucles en JavaScript

## Introduction

Les boucles permettent d'exécuter un bloc de code plusieurs fois. JavaScript propose plusieurs types de boucles, chacune ayant ses particularités et cas d'utilisation spécifiques.

## La Boucle for

La boucle `for` est utilisée quand on connaît à l'avance le nombre d'itérations.

```javascript
// Basic structure
for (initialization; condition; increment) {
    // Code to repeat
}

// Simple example
for (let i = 0; i < 5; i++) {
    console.log(`Iteration ${i}`);
}

// Iterate through an array
const fruits = ['apple', 'banana', 'orange'];
for (let i = 0; i < fruits.length; i++) {
    console.log(fruits[i]);
}

// Decremental loop
for (let i = 10; i > 0; i--) {
    console.log(`Countdown: ${i}`);
}
```

## La Boucle for...of (ES6+)

Idéale pour parcourir les éléments d'un objet itérable (tableaux, chaînes, etc.).

```javascript
// Iterate through an array
const colors = ['red', 'green', 'blue'];
for (const color of colors) {
    console.log(color);
}

// Iterate through a string
const word = 'Hello';
for (const letter of word) {
    console.log(letter);
}

// With destructuring
const points = [
    [1, 2],
    [3, 4],
    [5, 6],
];
for (const [x, y] of points) {
    console.log(`Point: (${x}, ${y})`);
}
```

## La Boucle for...in

Utilisée pour parcourir les propriétés énumérables d'un objet.

```javascript
const person = {
    name: 'Smith',
    age: 30,
    city: 'London',
};

// Iterate through object properties
for (const property in person) {
    console.log(`${property}: ${person[property]}`);
}

// Be careful with arrays
const array = ['a', 'b', 'c'];
for (const index in array) {
    console.log(`Index ${index}: ${array[index]}`);
}
```

## La Boucle while

Utilisée quand la condition d'arrêt n'est pas connue à l'avance.

```javascript
// Basic structure
let counter = 0;
while (counter < 5) {
    console.log(counter);
    counter++;
}

// Example with dynamic condition
let number = 1;
while (number < 1000) {
    number *= 2;
    console.log(number);
}

// With complex stop condition
let shouldContinue = true;
while (shouldContinue) {
    // Simulate user action
    const response = askUser();
    shouldContinue = response !== 'stop';
}
```

## La Boucle do...while

Exécute le code au moins une fois avant de vérifier la condition.

```javascript
// Basic structure
let i = 0;
do {
    console.log(i);
    i++;
} while (i < 5);

// Practical example: user menu
let choice;
do {
    choice = displayMenu();
    processChoice(choice);
} while (choice !== 'quit');
```

## Instructions de Contrôle

### break

Permet de sortir complètement d'une boucle.

```javascript
// Exit a loop
for (let i = 0; i < 10; i++) {
    if (i === 5) {
        break;
    }
    console.log(i);
}

// In a nested loop
search: for (let i = 0; i < array.length; i++) {
    for (let j = 0; j < array[i].length; j++) {
        if (array[i][j] === searchValue) {
            console.log(`Found at [${i}][${j}]`);
            break search;
        }
    }
}
```

### continue

Passe à l'itération suivante de la boucle.

```javascript
// Skip certain iterations
for (let i = 0; i < 5; i++) {
    if (i === 2) {
        continue;
    }
    console.log(i);
}

// Filter elements
for (const element of array) {
    if (!isValid(element)) {
        continue;
    }
    processElement(element);
}
```

## Méthodes Modernes d'Itération

### forEach

```javascript
// Iterate through an array
const numbers = [1, 2, 3, 4, 5];
numbers.forEach((number, index) => {
    console.log(`${index}: ${number}`);
});

// With this object
const multiplier = {
    factor: 2,
    multiply(array) {
        array.forEach(function (number) {
            console.log(number * this.factor);
        }, this);
    },
};
```

### map, filter, reduce

```javascript
const numbers = [1, 2, 3, 4, 5];

// map: transform each element
const doubles = numbers.map((n) => n * 2);

// filter: filter elements
const evens = numbers.filter((n) => n % 2 === 0);

// reduce: reduce to a single value
const sum = numbers.reduce((acc, curr) => acc + curr, 0);
```

## Bonnes Pratiques

1. **Choix de la Boucle**

    - `for` : quand le nombre d'itérations est connu
    - `while` : quand la condition d'arrêt est dynamique
    - `for...of` : pour les collections itérables
    - `for...in` : pour les propriétés d'objets

2. **Performance**

    - Évitez de modifier le compteur dans la boucle
    - Stockez la longueur du tableau hors de la boucle
    - Préférez les méthodes modernes quand possible

3. **Lisibilité**

    - Utilisez des noms explicites pour les variables de boucle
    - Évitez les boucles imbriquées profondes
    - Commentez les conditions complexes

4. **Sécurité**
    - Prévoyez toujours une condition de sortie
    - Évitez les boucles infinies
    - Vérifiez les limites des tableaux
