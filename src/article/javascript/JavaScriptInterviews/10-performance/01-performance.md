---
title: Les Performances en JavaScript
date: 2024-10-29
icon: code
category:
    - JavaScript
tags:
    - performances
    - optimisation
description: Un guide sur l'optimisation des performances en JavaScript, y compris les bonnes pratiques.
---

# Les Performances en JavaScript

## Introduction

L'optimisation des performances est cruciale pour créer des applications JavaScript rapides et réactives. Comprendre les bonnes pratiques et les techniques d'optimisation permet d'améliorer significativement l'expérience utilisateur.

## Optimisation du Code

### Gestion de la Mémoire

```javascript
// ❌ Mauvaise gestion de la mémoire
function creerElements() {
    const elements = [];
    for (let i = 0; i < 10000; i++) {
        elements.push({ id: i, data: new Array(1000).fill('données') });
    }
    return elements;
}

// ✅ Meilleure gestion de la mémoire
function creerElementsOptimise() {
    return Array.from({ length: 10000 }, (_, i) => ({
        id: i,
        // Création à la demande
        get data() {
            return new Array(1000).fill('données');
        },
    }));
}

// Nettoyage des références
function traiterDonnees() {
    let grandesDonnees = new Array(1000000);
    // Traitement...
    grandesDonnees = null; // Libération de la mémoire
}
```

### Optimisation des Boucles

```javascript
// ❌ Boucle non optimisée
function sommeTableau(tableau) {
    let somme = 0;
    for (let i = 0; i < tableau.length; i++) {
        somme += tableau[i];
    }
    return somme;
}

// ✅ Boucle optimisée
function sommeTableauOptimise(tableau) {
    let somme = 0;
    const longueur = tableau.length;
    for (let i = 0; i < longueur; i++) {
        somme += tableau[i];
    }
    return somme;
}

// ✅ Utilisation de méthodes natives
function sommeTableauModerne(tableau) {
    return tableau.reduce((acc, val) => acc + val, 0);
}
```

### Mise en Cache des Résultats

```javascript
// Mémoïsation simple
function memoize(fn) {
    const cache = new Map();

    return function (...args) {
        const key = JSON.stringify(args);
        if (cache.has(key)) {
            return cache.get(key);
        }

        const result = fn.apply(this, args);
        cache.set(key, result);
        return result;
    };
}

// Exemple d'utilisation
const calculCouteux = memoize((n) => {
    console.log('Calcul effectué');
    return n * n;
});

console.log(calculCouteux(5)); // Calcul effectué, affiche 25
console.log(calculCouteux(5)); // Utilise le cache, affiche 25

// Cache avec limite de taille
class LRUCache {
    constructor(capacity) {
        this.cache = new Map();
        this.capacity = capacity;
    }

    get(key) {
        if (!this.cache.has(key)) return -1;

        const value = this.cache.get(key);
        this.cache.delete(key);
        this.cache.set(key, value);
        return value;
    }

    put(key, value) {
        if (this.cache.has(key)) {
            this.cache.delete(key);
        } else if (this.cache.size >= this.capacity) {
            this.cache.delete(this.cache.keys().next().value);
        }
        this.cache.set(key, value);
    }
}
```

## Optimisation du DOM

### Manipulation Efficace

```javascript
// ❌ Manipulations DOM inefficaces
function ajouterElements(nombres) {
    const container = document.getElementById('container');
    nombres.forEach((nombre) => {
        const div = document.createElement('div');
        div.textContent = nombre;
        container.appendChild(div);
    });
}

// ✅ Utilisation de DocumentFragment
function ajouterElementsOptimise(nombres) {
    const fragment = document.createDocumentFragment();
    nombres.forEach((nombre) => {
        const div = document.createElement('div');
        div.textContent = nombre;
        fragment.appendChild(div);
    });
    document.getElementById('container').appendChild(fragment);
}

// ✅ Mise à jour par lots
function mettreAJourListe(elements) {
    const container = document.getElementById('container');
    container.style.display = 'none'; // Évite les reflows
    elements.forEach((element) => {
        // Modifications...
    });
    container.style.display = ''; // Réactive l'affichage
}
```

### Virtualisation des Listes

```javascript
class ListeVirtuelle {
    constructor(container, items, rowHeight) {
        this.container = container;
        this.items = items;
        this.rowHeight = rowHeight;
        this.visibleItems = Math.ceil(container.clientHeight / rowHeight);
        this.setupScroll();
    }

    setupScroll() {
        const totalHeight = this.items.length * this.rowHeight;
        this.container.style.height = `${totalHeight}px`;

        const content = document.createElement('div');
        content.style.position = 'relative';
        this.container.appendChild(content);

        this.container.addEventListener('scroll', () => {
            this.renderItems();
        });
    }

    renderItems() {
        const scrollTop = this.container.scrollTop;
        const startIndex = Math.floor(scrollTop / this.rowHeight);
        const endIndex = Math.min(startIndex + this.visibleItems + 1, this.items.length);

        const content = this.container.firstElementChild;
        content.innerHTML = '';

        for (let i = startIndex; i < endIndex; i++) {
            const item = document.createElement('div');
            item.style.position = 'absolute';
            item.style.top = `${i * this.rowHeight}px`;
            item.style.height = `${this.rowHeight}px`;
            item.textContent = this.items[i];
            content.appendChild(item);
        }
    }
}
```

## Optimisation des Requêtes

### Debounce et Throttle

```javascript
// Debounce
function debounce(func, wait) {
    let timeout;
    return function executedFunction(...args) {
        const later = () => {
            clearTimeout(timeout);
            func(...args);
        };
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
    };
}

// Throttle
function throttle(func, limit) {
    let inThrottle;
    return function executedFunction(...args) {
        if (!inThrottle) {
            func(...args);
            inThrottle = true;
            setTimeout(() => {
                inThrottle = false;
            }, limit);
        }
    };
}

// Exemple d'utilisation
const recherche = debounce((terme) => {
    // Effectuer la recherche
    console.log('Recherche:', terme);
}, 300);

const scroll = throttle(() => {
    // Gérer le défilement
    console.log('Défilement');
}, 100);
```

### Mise en Cache des Requêtes

```javascript
class CacheRequetes {
    constructor(ttl = 5 * 60 * 1000) {
        // 5 minutes par défaut
        this.cache = new Map();
        this.ttl = ttl;
    }

    async get(url, options = {}) {
        const key = this.getCleCache(url, options);
        const cached = this.cache.get(key);

        if (cached && Date.now() - cached.timestamp < this.ttl) {
            return cached.data;
        }

        const response = await fetch(url, options);
        const data = await response.json();

        this.cache.set(key, {
            data,
            timestamp: Date.now(),
        });

        return data;
    }

    getCleCache(url, options) {
        return `${url}-${JSON.stringify(options)}`;
    }

    nettoyer() {
        const maintenant = Date.now();
        for (const [key, value] of this.cache.entries()) {
            if (maintenant - value.timestamp > this.ttl) {
                this.cache.delete(key);
            }
        }
    }
}

// Utilisation
const cache = new CacheRequetes();
setInterval(() => cache.nettoyer(), 60000); // Nettoyage toutes les minutes
```

## Optimisation des Images

### Chargement Différé

```javascript
class ChargementDiffereImages {
    constructor(options = {}) {
        this.options = {
            root: null,
            rootMargin: '0px',
            threshold: 0.1,
            ...options,
        };
        this.observer = this.createObserver();
    }

    createObserver() {
        return new IntersectionObserver((entries) => {
            entries.forEach((entry) => {
                if (entry.isIntersecting) {
                    this.chargerImage(entry.target);
                    this.observer.unobserve(entry.target);
                }
            });
        }, this.options);
    }

    chargerImage(img) {
        const src = img.dataset.src;
        if (!src) return;

        img.src = src;
        img.classList.add('loaded');
    }

    observer(images) {
        images.forEach((img) => this.observer.observe(img));
    }
}

// Utilisation
const chargeur = new ChargementDiffereImages();
chargeur.observer(document.querySelectorAll('img[data-src]'));
```

### Optimisation des Ressources

```javascript
// Préchargement des ressources critiques
function prechargerRessources() {
    const ressources = ['/images/logo.png', '/css/critical.css', '/js/main.js'];

    ressources.forEach((url) => {
        const link = document.createElement('link');
        link.rel = 'preload';
        link.as = getResourceType(url);
        link.href = url;
        document.head.appendChild(link);
    });
}

function getResourceType(url) {
    const extension = url.split('.').pop();
    const types = {
        css: 'style',
        js: 'script',
        png: 'image',
        jpg: 'image',
        jpeg: 'image',
        gif: 'image',
        woff2: 'font',
    };
    return types[extension] || 'fetch';
}
```

## Bonnes Pratiques

1. **Optimisation du Code**

    - Éviter les fuites de mémoire
    - Utiliser les structures de données appropriées
    - Optimiser les boucles et les calculs
    - Mettre en cache les résultats coûteux

2. **Manipulation du DOM**

    - Minimiser les manipulations directes
    - Utiliser DocumentFragment
    - Éviter les reflows inutiles
    - Virtualiser les grandes listes

3. **Gestion des Ressources**

    - Optimiser le chargement des images
    - Utiliser la mise en cache
    - Implémenter le chargement différé
    - Compresser les ressources

4. **Requêtes et Données**

    - Mettre en cache les requêtes
    - Utiliser debounce et throttle
    - Optimiser les transferts de données
    - Implémenter la pagination

## Exemples Pratiques

### Optimisation d'une Application

```javascript
class ApplicationOptimisee {
    constructor() {
        this.cache = new CacheRequetes();
        this.setupEventListeners();
        this.setupImageLoading();
    }

    setupEventListeners() {
        // Gestion optimisée des événements
        window.addEventListener(
            'scroll',
            throttle(() => {
                this.handleScroll();
            }, 100),
        );

        const searchInput = document.getElementById('search');
        searchInput.addEventListener(
            'input',
            debounce((e) => {
                this.handleSearch(e.target.value);
            }, 300),
        );
    }

    setupImageLoading() {
        // Configuration du chargement différé des images
        const imageLoader = new ChargementDiffereImages();
        imageLoader.observer(document.querySelectorAll('img[data-src]'));
    }

    async handleSearch(terme) {
        try {
            const resultats = await this.cache.get(`/api/search?q=${terme}`);
            this.renderResults(resultats);
        } catch (erreur) {
            console.error('Erreur de recherche:', erreur);
        }
    }

    handleScroll() {
        // Gestion optimisée du défilement
        const scrollPosition = window.scrollY;
        const windowHeight = window.innerHeight;
        const documentHeight = document.documentElement.scrollHeight;

        if (scrollPosition + windowHeight >= documentHeight - 500) {
            this.chargerPlusDeContenu();
        }
    }

    async chargerPlusDeContenu() {
        // Chargement optimisé de contenu supplémentaire
        const fragment = document.createDocumentFragment();
        const nouveauContenu = await this.chargerContenu();

        nouveauContenu.forEach((item) => {
            const element = this.creerElement(item);
            fragment.appendChild(element);
        });

        document.getElementById('content').appendChild(fragment);
    }

    creerElement(item) {
        const div = document.createElement('div');
        div.textContent = item.titre;
        return div;
    }
}

// Initialisation
const app = new ApplicationOptimisee();
```

### Optimisation des Animations

```javascript
class AnimationOptimisee {
    constructor() {
        this.animations = new Map();
        this.setupRequestAnimationFrame();
    }

    setupRequestAnimationFrame() {
        let ticking = false;

        window.addEventListener('scroll', () => {
            if (!ticking) {
                window.requestAnimationFrame(() => {
                    this.updateAnimations();
                    ticking = false;
                });
                ticking = true;
            }
        });
    }

    addAnimation(element, options) {
        const animation = {
            element,
            options,
            start: performance.now(),
        };

        this.animations.set(element, animation);
        this.updateAnimation(animation);
    }

    updateAnimations() {
        for (const animation of this.animations.values()) {
            this.updateAnimation(animation);
        }
    }

    updateAnimation(animation) {
        const { element, options, start } = animation;
        const elapsed = performance.now() - start;

        if (elapsed >= options.duration) {
            this.animations.delete(element);
            return;
        }

        const progress = elapsed / options.duration;
        const value = this.easeInOutCubic(progress);

        element.style.transform = `translateY(${value * 100}px)`;
    }

    easeInOutCubic(t) {
        return t < 0.5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1;
    }
}
```
