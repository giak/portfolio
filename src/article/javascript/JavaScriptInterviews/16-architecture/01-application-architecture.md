---
title: Architecture des Applications JavaScript
date: 2024-10-29
icon: code
category:
    - JavaScript
tags:
    - architecture
    - bonnes pratiques
description: Un guide sur l'architecture des applications JavaScript, y compris la Clean Architecture.
---

# Architecture des Applications JavaScript

## Introduction

L'architecture d'une application JavaScript est fondamentale pour sa maintenabilité, sa scalabilité et sa performance. Cet article explore les différentes approches architecturales et les meilleures pratiques pour construire des applications robustes.

## Clean Architecture

### Principes Fondamentaux

```javascript
// Entités (Couche la plus interne)
class User {
    constructor(id, name, email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    validate() {
        if (!this.email.includes('@')) {
            throw new Error('Email invalide');
        }
    }
}

// Cas d'utilisation
class CreateUserUseCase {
    constructor(userRepository, emailService) {
        this.userRepository = userRepository;
        this.emailService = emailService;
    }

    async execute(userData) {
        const user = new User(userData.id, userData.name, userData.email);
        user.validate();

        await this.userRepository.save(user);
        await this.emailService.sendWelcomeEmail(user.email);

        return user;
    }
}

// Interface Adapters
class UserController {
    constructor(createUserUseCase) {
        this.createUserUseCase = createUserUseCase;
    }

    async handleCreateUser(req, res) {
        try {
            const user = await this.createUserUseCase.execute(req.body);
            res.status(201).json(user);
        } catch (error) {
            res.status(400).json({ error: error.message });
        }
    }
}
```

### Implémentation des Interfaces

```javascript
// Interfaces (Ports)
class UserRepository {
    async save(user) {
        throw new Error('Not implemented');
    }

    async findById(id) {
        throw new Error('Not implemented');
    }
}

// Adaptateurs (Implementations)
class MongoUserRepository extends UserRepository {
    constructor(database) {
        super();
        this.database = database;
    }

    async save(user) {
        return this.database.collection('users').insertOne(user);
    }

    async findById(id) {
        return this.database.collection('users').findOne({ id });
    }
}
```

## Architecture Modulaire

### Structure des Modules

```javascript
// module/
// ├── core/
// │   ├── entities/
// │   ├── use-cases/
// │   └── interfaces/
// ├── infrastructure/
// │   ├── database/
// │   └── external-services/
// └── presentation/
//     ├── web/
//     └── api/

// core/entities/user.js
export class User {
    // ... implementation
}

// core/use-cases/create-user.js
export class CreateUserUseCase {
    // ... implementation
}

// infrastructure/database/mongo-repository.js
export class MongoUserRepository {
    // ... implementation
}

// presentation/api/user-controller.js
export class UserController {
    // ... implementation
}
```

### Gestion des Dépendances

```javascript
// di-container.js
class Container {
    constructor() {
        this.services = new Map();
        this.singletons = new Map();
    }

    register(name, constructor, dependencies = []) {
        this.services.set(name, { constructor, dependencies });
    }

    registerSingleton(name, constructor, dependencies = []) {
        this.register(name, constructor, dependencies);
        this.singletons.set(name, null);
    }

    resolve(name) {
        const service = this.services.get(name);
        if (!service) {
            throw new Error(`Service not found: ${name}`);
        }

        if (this.singletons.has(name)) {
            if (!this.singletons.get(name)) {
                this.singletons.set(name, this.createInstance(service));
            }
            return this.singletons.get(name);
        }

        return this.createInstance(service);
    }

    createInstance(service) {
        const dependencies = service.dependencies.map((dep) => this.resolve(dep));
        return new service.constructor(...dependencies);
    }
}

// Utilisation
const container = new Container();

container.register('database', MongoDB, []);
container.register('userRepository', MongoUserRepository, ['database']);
container.register('emailService', EmailService, []);
container.register('createUserUseCase', CreateUserUseCase, ['userRepository', 'emailService']);
container.register('userController', UserController, ['createUserUseCase']);
```

## Patterns Architecturaux

### MVC (Model-View-Controller)

```javascript
// Model
class UserModel {
    constructor(data) {
        this.id = data.id;
        this.name = data.name;
        this.email = data.email;
    }

    async save() {
        // Logique de persistance
    }

    static async findById(id) {
        // Logique de récupération
    }
}

// View
class UserView {
    constructor(container) {
        this.container = container;
    }

    render(user) {
        this.container.innerHTML = `
            <div class="user-card">
                <h2>${user.name}</h2>
                <p>${user.email}</p>
            </div>
        `;
    }

    showError(error) {
        this.container.innerHTML = `
            <div class="error">
                ${error.message}
            </div>
        `;
    }
}

// Controller
class UserController {
    constructor(view) {
        this.view = view;
    }

    async showUser(id) {
        try {
            const user = await UserModel.findById(id);
            this.view.render(user);
        } catch (error) {
            this.view.showError(error);
        }
    }
}
```

### MVVM (Model-View-ViewModel)

```javascript
// Model
class UserModel {
    constructor(data) {
        this.id = data.id;
        this.name = data.name;
        this.email = data.email;
    }
}

// ViewModel
class UserViewModel {
    constructor(model) {
        this.model = model;
        this.observers = new Set();
    }

    get displayName() {
        return `${this.model.name} (${this.model.email})`;
    }

    updateName(newName) {
        this.model.name = newName;
        this.notifyObservers();
    }

    subscribe(observer) {
        this.observers.add(observer);
    }

    notifyObservers() {
        this.observers.forEach((observer) => observer());
    }
}

// View
class UserView {
    constructor(viewModel) {
        this.viewModel = viewModel;
        this.viewModel.subscribe(() => this.render());
        this.render();
    }

    render() {
        document.getElementById('user').innerHTML = `
            <div class="user-card">
                <h2>${this.viewModel.displayName}</h2>
                <input type="text" value="${this.viewModel.model.name}"
                    onchange="this.viewModel.updateName(this.value)" />
            </div>
        `;
    }
}
```

## Bonnes Pratiques

1. **Organisation du Code**

    - Séparation claire des responsabilités
    - Structure modulaire cohérente
    - Dépendances explicites
    - Documentation des interfaces

2. **Gestion de l'État**

    - État immutable quand possible
    - Flux de données unidirectionnel
    - Gestion centralisée de l'état
    - Persistence des données

3. **Performance**

    - Chargement à la demande
    - Mise en cache intelligente
    - Optimisation des dépendances
    - Monitoring des performances

4. **Maintenabilité**

    - Tests automatisés
    - Documentation claire
    - Convention de code
    - Revues de code

## Exemples Pratiques

### Application avec État Global

```javascript
// state-management.js
class StateManager {
    constructor(initialState = {}) {
        this.state = initialState;
        this.listeners = new Set();
    }

    getState() {
        return { ...this.state };
    }

    setState(newState) {
        this.state = { ...this.state, ...newState };
        this.notify();
    }

    subscribe(listener) {
        this.listeners.add(listener);
        return () => this.listeners.delete(listener);
    }

    notify() {
        this.listeners.forEach((listener) => listener(this.state));
    }
}

// Utilisation
const store = new StateManager({
    user: null,
    theme: 'light',
    notifications: [],
});

class App {
    constructor(store) {
        this.store = store;
        this.unsubscribe = this.store.subscribe((state) => this.render(state));
    }

    render(state) {
        // Logique de rendu
        console.log('Nouvel état:', state);
    }

    destroy() {
        this.unsubscribe();
    }
}
```

### Router Modulaire

```javascript
// router.js
class Router {
    constructor(routes) {
        this.routes = routes;
        this.currentRoute = null;

        window.addEventListener('popstate', () => this.handleRoute());
        this.handleRoute();
    }

    handleRoute() {
        const path = window.location.pathname;
        const route = this.routes.find((r) => r.path === path);

        if (route) {
            this.currentRoute = route;
            route.component.render();
        } else {
            // Gestion 404
            this.handle404();
        }
    }

    navigate(path) {
        window.history.pushState(null, '', path);
        this.handleRoute();
    }

    handle404() {
        document.body.innerHTML = '<h1>Page non trouvée</h1>';
    }
}

// Utilisation
const router = new Router([
    {
        path: '/',
        component: new HomeComponent(),
    },
    {
        path: '/users',
        component: new UsersComponent(),
    },
    {
        path: '/settings',
        component: new SettingsComponent(),
    },
]);
```
