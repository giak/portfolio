---
title: Les Classes en JavaScript
date: 2024-10-29
icon: code
category:
    - JavaScript
tags:
    - classes
    - objets
description: Une introduction aux classes en JavaScript, leur déclaration et leur utilisation.
---

# Les Classes en JavaScript

## Introduction

Les classes ont été introduites avec ES6 pour fournir une syntaxe plus claire et plus simple pour créer des objets et implémenter l'héritage orienté objet. Elles sont en réalité une syntaxe simplifiée du système de prototypes de JavaScript.

## Déclaration de Classes

```javascript
// Basic class declaration
class Animal {
    constructor(name) {
        this.name = name;
    }

    speak() {
        console.log(`${this.name} makes a sound`);
    }
}

// Class expression
const Vehicle = class {
    constructor(brand) {
        this.brand = brand;
    }
};
```

## Constructeur et Méthodes

```javascript
class Person {
    // Constructor
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }

    // Instance method
    introduce() {
        return `My name is ${this.name} and I am ${this.age} years old`;
    }

    // Static method
    static createAdult(name) {
        return new Person(name, 18);
    }

    // Getter
    get fullName() {
        return `Mr/Mrs ${this.name}`;
    }

    // Setter
    set fullName(value) {
        this.name = value.replace('Mr/Mrs ', '');
    }
}
```

## Propriétés de Classe

```javascript
class Account {
    // Private property (ES2022+)
    #balance = 0;

    // Static property
    static numberOfAccounts = 0;

    // Public property
    currency = 'USD';

    constructor(holder) {
        this.holder = holder;
        Account.numberOfAccounts++;
    }

    // Method to access private property
    checkBalance() {
        return this.#balance;
    }

    // Method to modify private property
    deposit(amount) {
        if (amount > 0) {
            this.#balance += amount;
            return true;
        }
        return false;
    }
}
```

## Héritage

```javascript
// Base class
class Animal {
    constructor(name) {
        this.name = name;
    }

    speak() {
        console.log(`${this.name} makes a sound`);
    }
}

// Derived class
class Dog extends Animal {
    constructor(name, breed) {
        super(name); // Call parent constructor
        this.breed = breed;
    }

    speak() {
        super.speak(); // Call parent method
        console.log(`${this.name} barks`);
    }

    // New specific method
    wagTail() {
        console.log(`${this.name} wags its tail`);
    }
}
```

## Mixins et Composition

```javascript
// Create a mixin
const SwimmerMixin = {
    swim() {
        console.log(`${this.name} swims`);
    },
};

const FlyerMixin = {
    fly() {
        console.log(`${this.name} flies`);
    },
};

// Use mixins
class Duck extends Animal {
    constructor(name) {
        super(name);
        Object.assign(this, SwimmerMixin, FlyerMixin);
    }
}
```

## Classes Abstraites

```javascript
// Simulate an abstract class
class GeometricShape {
    constructor() {
        if (new.target === GeometricShape) {
            throw new Error('This class cannot be instantiated directly');
        }
    }

    calculateArea() {
        throw new Error('The calculateArea method must be implemented');
    }
}

// Concrete class
class Rectangle extends GeometricShape {
    constructor(width, height) {
        super();
        this.width = width;
        this.height = height;
    }

    calculateArea() {
        return this.width * this.height;
    }
}
```

## Interfaces et Types

```javascript
// Simulate an interface in JavaScript
class AnimalInterface {
    constructor() {
        if (this.constructor === AnimalInterface) {
            throw new Error('Interfaces cannot be instantiated');
        }
    }

    eat() {
        throw new Error('The eat method must be implemented');
    }

    sleep() {
        throw new Error('The sleep method must be implemented');
    }
}

// Interface implementation
class Cat extends AnimalInterface {
    eat() {
        console.log('The cat eats kibbles');
    }

    sleep() {
        console.log('The cat sleeps on the couch');
    }
}
```

## Gestion des Erreurs

```javascript
class ValidationError extends Error {
    constructor(message) {
        super(message);
        this.name = 'ValidationError';
    }
}

class User {
    constructor(email) {
        if (!email.includes('@')) {
            throw new ValidationError('Invalid email');
        }
        this.email = email;
    }
}

try {
    const user = new User('invalid_email');
} catch (error) {
    if (error instanceof ValidationError) {
        console.error('Validation error:', error.message);
    }
}
```

## Bonnes Pratiques

1. **Organisation du Code**

    - Une classe par fichier
    - Noms de classes en PascalCase
    - Méthodes et propriétés en camelCase
    - Regroupement logique des méthodes

2. **Encapsulation**

    - Utilisation de propriétés privées
    - Getters et setters appropriés
    - Validation des données dans les setters
    - Protection des propriétés sensibles

3. **Héritage**

    - Éviter les hiérarchies profondes
    - Préférer la composition à l'héritage
    - Respecter le principe de substitution de Liskov
    - Documenter les classes abstraites

4. **Performance**

    - Éviter les méthodes trop complexes
    - Optimiser les constructeurs
    - Gérer correctement les ressources
    - Utiliser les méthodes statiques judicieusement

## Exemples Pratiques

### Gestionnaire d'Événements

```javascript
class EventEmitter {
    constructor() {
        this.events = new Map();
    }

    on(event, callback) {
        if (!this.events.has(event)) {
            this.events.set(event, []);
        }
        this.events.get(event).push(callback);
    }

    emit(event, data) {
        if (this.events.has(event)) {
            this.events.get(event).forEach((callback) => callback(data));
        }
    }
}

// Utilisation
class ChatRoom extends EventEmitter {
    sendMessage(user, message) {
        const data = { user, message, timestamp: new Date() };
        this.emit('message', data);
    }
}
```

### Singleton

```javascript
class Configuration {
    static #instance;

    constructor() {
        if (Configuration.#instance) {
            return Configuration.#instance;
        }
        this.config = new Map();
        Configuration.#instance = this;
    }

    set(key, value) {
        this.config.set(key, value);
    }

    get(key) {
        return this.config.get(key);
    }

    static getInstance() {
        if (!Configuration.#instance) {
            Configuration.#instance = new Configuration();
        }
        return Configuration.#instance;
    }
}
```
