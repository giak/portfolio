---
title: Les Objets en JavaScript
date: 2024-10-29
icon: code
category:
    - JavaScript
tags:
    - objets
    - fondamentaux
description: Une introduction aux objets en JavaScript, leur création et leur manipulation.
---

# Les Objets en JavaScript

## Introduction

Les objets sont l'un des types de données fondamentaux en JavaScript. Ils permettent de regrouper des données connexes et des fonctionnalités dans une seule structure. En JavaScript, presque tout est un objet.

## Création d'Objets

### Littéral d'Objet

```javascript
// Create a simple object
const person = {
    lastName: 'Smith',
    firstName: 'John',
    age: 30,
    city: 'London',
};

// Object with methods
const user = {
    name: 'Alice',
    sayHello() {
        console.log(`Hello, I am ${this.name}`);
    },
    getInfo() {
        return `User: ${this.name}`;
    },
};
```

### Constructeur d'Objet

```javascript
// Constructor function
function Person(name, age) {
    this.name = name;
    this.age = age;
    this.introduction = function () {
        return `My name is ${this.name} and I am ${this.age} years old`;
    };
}

const peter = new Person('Peter', 25);
console.log(peter.introduction()); // "My name is Peter and I am 25 years old"

// ES6+ Class
class User {
    constructor(name, email) {
        this.name = name;
        this.email = email;
    }

    getInfo() {
        return `${this.name} (${this.email})`;
    }
}
```

## Accès aux Propriétés

```javascript
const book = {
    title: 'Advanced JavaScript',
    author: 'John Doe',
    year: 2023,
};

// Dot notation
console.log(book.title); // "Advanced JavaScript"

// Bracket notation
console.log(book['author']); // "John Doe"

// Dynamic property
const prop = 'year';
console.log(book[prop]); // 2023
```

## Modification d'Objets

```javascript
const car = {
    brand: 'Toyota',
    model: 'Camry',
};

// Add properties
car.year = 2020;
car['color'] = 'red';

// Modify properties
car.brand = 'Honda';

// Delete properties
delete car.color;
```

## Méthodes d'Objets

### Object.keys(), values(), entries()

```javascript
const product = {
    name: 'Laptop',
    price: 999,
    stock: 5,
};

// Get keys
console.log(Object.keys(product)); // ['name', 'price', 'stock']

// Get values
console.log(Object.values(product)); // ['Laptop', 999, 5]

// Get key-value pairs
console.log(Object.entries(product));
// [['name', 'Laptop'], ['price', 999], ['stock', 5]]
```

### Object.assign() et Spread

```javascript
// Merge objects with assign
const baseObject = { a: 1, b: 2 };
const extension = { b: 3, c: 4 };
const result = Object.assign({}, baseObject, extension);
// result = { a: 1, b: 3, c: 4 }

// Merge with spread operator
const merged = { ...baseObject, ...extension };
// merged = { a: 1, b: 3, c: 4 }
```

## Propriétés et Descripteurs

```javascript
const account = {
    balance: 1000,
};

// Define property with descriptor
Object.defineProperty(account, 'minimum', {
    value: 0,
    writable: false,
    enumerable: true,
    configurable: false,
});

// Get property descriptor
console.log(Object.getOwnPropertyDescriptor(account, 'minimum'));
```

## Getters et Setters

```javascript
const thermometer = {
    temperature: 0,

    get celsius() {
        return this.temperature;
    },

    set celsius(value) {
        if (typeof value === 'number') {
            this.temperature = value;
        }
    },

    get fahrenheit() {
        return (this.temperature * 9) / 5 + 32;
    },

    set fahrenheit(value) {
        this.temperature = ((value - 32) * 5) / 9;
    },
};
```

## Prototypes et Héritage

```javascript
// Inheritance with prototype
function Animal(name) {
    this.name = name;
}

Animal.prototype.makeSound = function () {
    return `${this.name} makes a sound`;
};

function Dog(name, breed) {
    Animal.call(this, name);
    this.breed = breed;
}

Dog.prototype = Object.create(Animal.prototype);
Dog.prototype.constructor = Dog;

// Inheritance with ES6+ classes
class Animal {
    constructor(name) {
        this.name = name;
    }

    makeSound() {
        return `${this.name} makes a sound`;
    }
}

class Dog extends Animal {
    constructor(name, breed) {
        super(name);
        this.breed = breed;
    }

    bark() {
        return `${this.name} barks`;
    }
}
```

## Méthodes Avancées

### Object.freeze() et Object.seal()

```javascript
// Freeze an object (immutable)
const config = {
    host: 'localhost',
    port: 3000,
};
Object.freeze(config);

// Seal an object (modification allowed, no add/delete)
const preferences = {
    theme: 'dark',
    language: 'en',
};
Object.seal(preferences);
```

### Proxy et Reflect

```javascript
// Utilisation de Proxy
const handler = {
    get: function (target, prop) {
        return prop in target ? target[prop] : 'Propriété non trouvée';
    },
};

const objet = { a: 1, b: 2 };
const proxy = new Proxy(objet, handler);

// Utilisation de Reflect
const resultat = Reflect.get(objet, 'a');
const aLaPropriete = Reflect.has(objet, 'b');
```

## Bonnes Pratiques

1. **Structure et Organisation**

    - Groupez les propriétés logiquement
    - Utilisez des noms descriptifs
    - Préférez la notation littérale quand possible
    - Évitez les objets trop complexes

2. **Performance**

    - Évitez de modifier constamment la structure
    - Utilisez Object.freeze() pour les objets constants
    - Attention aux références circulaires

3. **Maintenabilité**

    - Documentez les objets complexes
    - Utilisez des getters/setters pour la validation
    - Préférez les classes pour les structures complexes

4. **Sécurité**

    - Validez les données entrantes
    - Utilisez Object.freeze() pour les données sensibles
    - Évitez eval() avec des données d'objets
