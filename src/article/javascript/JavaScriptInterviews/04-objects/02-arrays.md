---
title: Les Tableaux en JavaScript
date: 2024-10-29
icon: code
category:
    - JavaScript
tags:
    - tableaux
    - fondamentaux
description: Un guide sur les tableaux en JavaScript, leur création et leurs méthodes.
---

# Les Tableaux en JavaScript

## Introduction

Les tableaux sont des objets spéciaux en JavaScript qui permettent de stocker des collections ordonnées d'éléments. Ils peuvent contenir n'importe quel type de données et leur taille est dynamique.

## Création de Tableaux

```javascript
// Create a literal array
const fruits = ['apple', 'banana', 'orange'];

// Create with Array constructor
const numbers = new Array(1, 2, 3, 4, 5);

// Create an empty array with predefined size
const array = new Array(3); // [undefined, undefined, undefined]

// Create with Array.from()
const letters = Array.from('hello'); // ['h', 'e', 'l', 'l', 'o']
const numbers2 = Array.from({ length: 5 }, (_, i) => i + 1); // [1, 2, 3, 4, 5]
```

## Accès et Modification

```javascript
const fruits = ['apple', 'banana', 'orange'];

// Access elements
console.log(fruits[0]); // 'apple'
console.log(fruits[fruits.length - 1]); // 'orange'

// Modify elements
fruits[1] = 'pear';

// Add elements
fruits.push('strawberry'); // Add to end
fruits.unshift('kiwi'); // Add to beginning

// Remove elements
const last = fruits.pop(); // Remove and return last element
const first = fruits.shift(); // Remove and return first element
```

## Méthodes de Tableau

### Méthodes de Recherche

```javascript
const numbers = [1, 2, 3, 4, 5, 3];

// indexOf and lastIndexOf
console.log(numbers.indexOf(3)); // 2
console.log(numbers.lastIndexOf(3)); // 5

// includes
console.log(numbers.includes(4)); // true

// find and findIndex
const first = numbers.find((n) => n > 3); // 4
const index = numbers.findIndex((n) => n > 3); // 3
```

### Méthodes de Transformation

```javascript
const numbers = [1, 2, 3, 4, 5];

// map
const doubles = numbers.map((n) => n * 2); // [2, 4, 6, 8, 10]

// filter
const evens = numbers.filter((n) => n % 2 === 0); // [2, 4]

// reduce
const sum = numbers.reduce((acc, curr) => acc + curr, 0); // 15

// flatMap
const words = ['hello world', 'javascript array'];
const allWords = words.flatMap((str) => str.split(' '));
// ['hello', 'world', 'javascript', 'array']
```

### Méthodes de Tri et d'Ordre

```javascript
const fruits = ['banana', 'apple', 'orange'];
const numbers = [23, 5, 100, 56, 9, 13];

// sort
fruits.sort(); // Alphabetical sort
numbers.sort((a, b) => a - b); // Numeric sort

// reverse
fruits.reverse(); // Reverse element order

// Method chaining
const result = numbers
    .filter((n) => n > 10)
    .sort((a, b) => a - b)
    .map((n) => n * 2);
```

### Méthodes de Modification

```javascript
const fruits = ['apple', 'banana', 'orange'];

// splice: remove/insert
fruits.splice(1, 1); // Remove 1 element at index 1
fruits.splice(1, 0, 'pear', 'strawberry'); // Insert elements

// slice: extract
const portion = fruits.slice(1, 3); // Extract elements from index 1 to 2

// concat
const more = fruits.concat(['kiwi', 'mango']); // New combined array
```

## Tableaux Multidimensionnels

```javascript
// Create a matrix
const matrix = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
];

// Access elements
console.log(matrix[1][1]); // 5

// Traverse a matrix
matrix.forEach((row) => {
    row.forEach((element) => {
        console.log(element);
    });
});
```

## Méthodes ES6+ Modernes

```javascript
// Array.from()
const sequence = Array.from({ length: 5 }, (_, i) => i * 2);

// Array.of()
const numbers = Array.of(1, 2, 3); // Preferred over new Array()

// fill
const zeros = new Array(3).fill(0); // [0, 0, 0]

// copyWithin
[1, 2, 3, 4, 5].copyWithin(0, 3); // [4, 5, 3, 4, 5]
```

## Déstructuration et Spread

```javascript
const numbers = [1, 2, 3, 4, 5];

// Destructuring
const [first, second, ...rest] = numbers;

// Spread operator
const copy = [...numbers];
const combined = [...numbers, 6, 7, 8];

// Use in functions
const maximum = Math.max(...numbers);
```

## Itération sur les Tableaux

```javascript
const fruits = ['apple', 'banana', 'orange'];

// forEach
fruits.forEach((fruit, index) => {
    console.log(`${index}: ${fruit}`);
});

// for...of
for (const fruit of fruits) {
    console.log(fruit);
}

// entries()
for (const [index, fruit] of fruits.entries()) {
    console.log(`${index}: ${fruit}`);
}
```

## Bonnes Pratiques

1. **Performance**

    - Préallouez la taille du tableau si connue
    - Évitez de modifier la longueur du tableau fréquemment
    - Utilisez les méthodes appropriées pour les opérations courantes

2. **Lisibilité**

    - Utilisez des noms explicites
    - Préférez les méthodes modernes (map, filter, reduce)
    - Évitez les tableaux trop profondément imbriqués

3. **Immutabilité**

    - Utilisez des méthodes non-mutables quand possible
    - Créez des copies plutôt que de modifier directement
    - Utilisez Object.freeze() pour les tableaux constants

4. **Sécurité**

    - Vérifiez toujours les limites du tableau
    - Validez les entrées avant d'ajouter au tableau
    - Faites attention aux références partagées

## Exemples Pratiques

### Filtrage et Transformation

```javascript
const products = [
    { id: 1, name: 'Laptop', price: 999 },
    { id: 2, name: 'Smartphone', price: 699 },
    { id: 3, name: 'Tablet', price: 399 },
];

// Filter expensive products and extract their names
const expensiveProducts = products.filter((p) => p.price > 500).map((p) => p.name);
```

### Agrégation de Données

```javascript
const sales = [
    { date: '2024-01', amount: 1000 },
    { date: '2024-01', amount: 500 },
    { date: '2024-02', amount: 750 },
];

// Group sales by month
const salesByMonth = sales.reduce((acc, sale) => {
    acc[sale.date] = (acc[sale.date] || 0) + sale.amount;
    return acc;
}, {});
```
