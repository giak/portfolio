# Les Progressive Web Apps (PWA) en JavaScript

## Introduction

Les Progressive Web Apps (PWA) représentent une évolution majeure dans le développement web, combinant le meilleur des applications web et natives. Elles offrent une expérience utilisateur optimale avec des fonctionnalités avancées comme le fonctionnement hors ligne et les notifications push.

## Service Workers

### Enregistrement du Service Worker

```javascript
// app.js
if ('serviceWorker' in navigator) {
    window.addEventListener('load', async () => {
        try {
            const registration = await navigator.serviceWorker.register('/sw.js');
            console.log('Service Worker enregistré:', registration.scope);
        } catch (error) {
            console.error('Erreur d'enregistrement du SW:', error);
        }
    });
}

// sw.js
const CACHE_NAME = 'app-v1';
const ASSETS = [
    '/',
    '/index.html',
    '/styles.css',
    '/app.js',
    '/manifest.json',
    '/offline.html',
];

// Installation
self.addEventListener('install', (event) => {
    event.waitUntil(
        caches.open(CACHE_NAME).then((cache) => {
            return cache.addAll(ASSETS);
        }),
    );
});

// Activation et nettoyage
self.addEventListener('activate', (event) => {
    event.waitUntil(
        caches.keys().then((cacheNames) => {
            return Promise.all(
                cacheNames
                    .filter((name) => name !== CACHE_NAME)
                    .map((name) => caches.delete(name)),
            );
        }),
    );
});

// Stratégie de cache
self.addEventListener('fetch', (event) => {
    event.respondWith(
        caches.match(event.request).then((response) => {
            return (
                response ||
                fetch(event.request).then((response) => {
                    if (!response || response.status !== 200 || response.type !== 'basic') {
                        return response;
                    }

                    const responseToCache = response.clone();
                    caches.open(CACHE_NAME).then((cache) => {
                        cache.put(event.request, responseToCache);
                    });

                    return response;
                })
            );
        }),
    );
});
```

## Manifest Web App

```json
// manifest.json
{
    "name": "Mon Application PWA",
    "short_name": "MonApp",
    "description": "Une application web progressive moderne",
    "start_url": "/",
    "display": "standalone",
    "background_color": "#ffffff",
    "theme_color": "#4a90e2",
    "icons": [
        {
            "src": "/icons/icon-72x72.png",
            "sizes": "72x72",
            "type": "image/png"
        },
        {
            "src": "/icons/icon-192x192.png",
            "sizes": "192x192",
            "type": "image/png"
        },
        {
            "src": "/icons/icon-512x512.png",
            "sizes": "512x512",
            "type": "image/png"
        }
    ]
}
```

## Stratégies de Mise en Cache

### Cache-First Strategy

```javascript
// Cache-First pour les ressources statiques
self.addEventListener('fetch', (event) => {
    event.respondWith(
        caches.match(event.request).then((response) => {
            return response || fetchAndCache(event.request);
        }),
    );
});

async function fetchAndCache(request) {
    const response = await fetch(request);
    const cache = await caches.open(CACHE_NAME);
    cache.put(request, response.clone());
    return response;
}
```

### Network-First Strategy

```javascript
// Network-First pour les données dynamiques
self.addEventListener('fetch', (event) => {
    if (event.request.url.includes('/api/')) {
        event.respondWith(
            fetch(event.request)
                .then((response) => {
                    const responseClone = response.clone();
                    caches.open(CACHE_NAME).then((cache) => {
                        cache.put(event.request, responseClone);
                    });
                    return response;
                })
                .catch(() => caches.match(event.request)),
        );
    }
});
```

## Notifications Push

### Configuration des Notifications

```javascript
// notifications.js
class NotificationManager {
    constructor() {
        this.publicVapidKey = 'VOTRE_CLE_PUBLIQUE_VAPID';
    }

    async requestPermission() {
        const permission = await Notification.requestPermission();
        if (permission !== 'granted') {
            throw new Error('Permission refusée');
        }
    }

    async subscribeToPush() {
        try {
            await this.requestPermission();

            const registration = await navigator.serviceWorker.ready;
            const subscription = await registration.pushManager.subscribe({
                userVisibleOnly: true,
                applicationServerKey: this.urlBase64ToUint8Array(this.publicVapidKey),
            });

            await this.sendSubscriptionToServer(subscription);
        } catch (error) {
            console.error('Erreur d'abonnement:', error);
        }
    }

    urlBase64ToUint8Array(base64String) {
        const padding = '='.repeat((4 - (base64String.length % 4)) % 4);
        const base64 = (base64String + padding).replace(/\-/g, '+').replace(/_/g, '/');
        const rawData = window.atob(base64);
        const outputArray = new Uint8Array(rawData.length);
        for (let i = 0; i < rawData.length; ++i) {
            outputArray[i] = rawData.charCodeAt(i);
        }
        return outputArray;
    }

    async sendSubscriptionToServer(subscription) {
        await fetch('/api/push/subscribe', {
            method: 'POST',
            body: JSON.stringify(subscription),
            headers: {
                'Content-Type': 'application/json',
            },
        });
    }
}
```

### Gestion des Notifications dans le Service Worker

```javascript
// sw.js
self.addEventListener('push', (event) => {
    const options = {
        body: event.data.text(),
        icon: '/icons/icon-192x192.png',
        badge: '/icons/badge-72x72.png',
        vibrate: [100, 50, 100],
        data: {
            dateOfArrival: Date.now(),
            primaryKey: 1,
        },
        actions: [
            {
                action: 'explore',
                title: 'Voir plus',
            },
            {
                action: 'close',
                title: 'Fermer',
            },
        ],
    };

    event.waitUntil(self.registration.showNotification('Nouvelle notification', options));
});

self.addEventListener('notificationclick', (event) => {
    event.notification.close();

    if (event.action === 'explore') {
        event.waitUntil(clients.openWindow('/details'));
    }
});
```

## Synchronisation en Arrière-plan

```javascript
// sync.js
class BackgroundSync {
    async registerSync(tag) {
        try {
            const registration = await navigator.serviceWorker.ready;
            await registration.sync.register(tag);
        } catch (error) {
            console.error('Background Sync non supporté:', error);
        }
    }

    async saveForSync(data) {
        const db = await this.openDatabase();
        await db.add('sync-store', {
            timestamp: Date.now(),
            data,
        });
        await this.registerSync('sync-data');
    }

    async openDatabase() {
        return new Promise((resolve, reject) => {
            const request = indexedDB.open('SyncDB', 1);

            request.onerror = () => reject(request.error);
            request.onsuccess = () => resolve(request.result);

            request.onupgradeneeded = (event) => {
                const db = event.target.result;
                db.createObjectStore('sync-store', { keyPath: 'timestamp' });
            };
        });
    }
}

// Dans le Service Worker
self.addEventListener('sync', (event) => {
    if (event.tag === 'sync-data') {
        event.waitUntil(syncData());
    }
});

async function syncData() {
    const db = await openDatabase();
    const data = await db.getAll('sync-store');

    for (const item of data) {
        try {
            await fetch('/api/sync', {
                method: 'POST',
                body: JSON.stringify(item.data),
                headers: {
                    'Content-Type': 'application/json',
                },
            });
            await db.delete('sync-store', item.timestamp);
        } catch (error) {
            console.error('Erreur de synchronisation:', error);
        }
    }
}
```

## Bonnes Pratiques

1. **Performance**

    - Optimiser la taille du cache
    - Utiliser des stratégies de cache appropriées
    - Implémenter le lazy loading
    - Optimiser les assets

2. **Expérience Utilisateur**

    - Fournir un feedback clair
    - Gérer gracieusement le mode hors ligne
    - Optimiser le temps de chargement initial
    - Implémenter des transitions fluides

3. **Sécurité**

    - Utiliser HTTPS
    - Valider les données du cache
    - Sécuriser les notifications push
    - Gérer les permissions utilisateur

4. **Maintenance**

    - Versionner le cache
    - Nettoyer régulièrement le cache
    - Mettre à jour le manifest
    - Surveiller les performances

## Exemples Pratiques

### Application Hors Ligne

```javascript
// offlineManager.js
class OfflineManager {
    constructor() {
        this.db = null;
        this.initDatabase();
    }

    async initDatabase() {
        const request = indexedDB.open('OfflineDB', 1);

        request.onupgradeneeded = (event) => {
            const db = event.target.result;
            db.createObjectStore('articles', { keyPath: 'id' });
            db.createObjectStore('comments', { keyPath: 'id' });
        };

        request.onsuccess = (event) => {
            this.db = event.target.result;
        };
    }

    async saveForOffline(storeType, data) {
        return new Promise((resolve, reject) => {
            const transaction = this.db.transaction([storeType], 'readwrite');
            const store = transaction.objectStore(storeType);
            const request = store.put(data);

            request.onsuccess = () => resolve(request.result);
            request.onerror = () => reject(request.error);
        });
    }

    async getOfflineData(storeType, id) {
        return new Promise((resolve, reject) => {
            const transaction = this.db.transaction([storeType]);
            const store = transaction.objectStore(storeType);
            const request = store.get(id);

            request.onsuccess = () => resolve(request.result);
            request.onerror = () => reject(request.error);
        });
    }
}
```
