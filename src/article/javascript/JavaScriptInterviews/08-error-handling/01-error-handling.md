---
title: La Gestion des Erreurs en JavaScript
date: 2024-10-29
icon: code
category:
    - JavaScript
tags:
    - gestion des erreurs
    - qualité
description: Un guide sur la gestion des erreurs en JavaScript, y compris les types d'erreurs et les bonnes pratiques.
---

# La Gestion des Erreurs en JavaScript

## Introduction

La gestion des erreurs est un aspect fondamental du développement JavaScript. Une bonne gestion des erreurs permet de créer des applications plus robustes et plus fiables, tout en améliorant l'expérience utilisateur et la maintenabilité du code.

## Types d'Erreurs

### Erreurs Standards

```javascript
// Common errors in JavaScript
const errors = {
    SyntaxError: 'Syntax error',
    ReferenceError: 'Undefined variable',
    TypeError: 'Incorrect type',
    RangeError: 'Value out of bounds',
    URIError: 'Malformed URI',
    EvalError: 'Error in eval()',
};

// Error examples
function errorExamples() {
    // SyntaxError
    try {
        eval('if (true) {');
    } catch (e) {
        console.log(e instanceof SyntaxError); // true
    }

    // ReferenceError
    try {
        console.log(nonExistentVariable);
    } catch (e) {
        console.log(e instanceof ReferenceError); // true
    }

    // TypeError
    try {
        null.method();
    } catch (e) {
        console.log(e instanceof TypeError); // true
    }
}
```

### Erreurs Personnalisées

```javascript
// Creating a custom error class
class ValidationError extends Error {
    constructor(message) {
        super(message);
        this.name = 'ValidationError';
        this.code = 'VAL_ERR';
        // Capture stack trace
        Error.captureStackTrace(this, ValidationError);
    }
}

// Usage
function validateAge(age) {
    if (typeof age !== 'number') {
        throw new ValidationError('Age must be a number');
    }
    if (age < 0 || age > 150) {
        throw new ValidationError('Age must be between 0 and 150');
    }
    return true;
}

try {
    validateAge('twenty');
} catch (error) {
    if (error instanceof ValidationError) {
        console.error('Validation error:', error.message);
    } else {
        console.error('Unexpected error:', error);
    }
}
```

## Try...Catch...Finally

### Structure de Base

```javascript
function divide(a, b) {
    try {
        // Code that might generate an error
        if (b === 0) {
            throw new Error('Division by zero is not allowed');
        }
        return a / b;
    } catch (error) {
        // Error handling
        console.error('An error occurred:', error.message);
        // We can return a default value
        return Infinity;
    } finally {
        // Code executed in all cases
        console.log('Operation completed');
    }
}
```

### Gestion Avancée

```javascript
async function performOperation() {
    let resource = null;
    try {
        // Resource acquisition
        resource = await openResource();

        // Risky operations
        const result = await processData(resource);
        return result;
    } catch (error) {
        // Specific handling based on error type
        if (error instanceof NetworkError) {
            // Retry attempt
            return await retryOperation();
        } else if (error instanceof ValidationError) {
            // Log error and notify
            logError(error);
            notifyUser(error.message);
        } else {
            // Unexpected error
            throw error; // Propagation
        }
    } finally {
        // Resource cleanup
        if (resource) {
            await closeResource(resource);
        }
    }
}
```

## Promesses et Gestion d'Erreurs

### Chaînage de Promesses

```javascript
function fetchData(id) {
    return fetch(`/api/data/${id}`)
        .then((response) => {
            if (!response.ok) {
                throw new Error(`HTTP error! status: ${response.status}`);
            }
            return response.json();
        })
        .then((data) => {
            return processData(data);
        })
        .catch((error) => {
            console.error('Error during fetch:', error);
            // Return default data or rethrow error
            throw error;
        });
}
```

### Async/Await

```javascript
async function fetchAndProcess(id) {
    try {
        const response = await fetch(`/api/data/${id}`);
        if (!response.ok) {
            throw new Error(`HTTP error! status: ${response.status}`);
        }

        const data = await response.json();
        return await processData(data);
    } catch (error) {
        if (error instanceof TypeError) {
            // Network error
            console.error('Network error:', error);
            return await useLocalCache(id);
        } else {
            // Other types of errors
            console.error('Error during processing:', error);
            throw error;
        }
    }
}
```

## Patterns de Gestion d'Erreurs

### Pattern de Retry

```javascript
async function withRetry(operation, maxAttempts = 3, delay = 1000) {
    let lastError;

    for (let attempt = 1; attempt <= maxAttempts; attempt++) {
        try {
            return await operation();
        } catch (error) {
            lastError = error;
            if (attempt === maxAttempts) break;

            console.log(`Attempt ${attempt} failed, retrying in ${delay}ms`);
            await new Promise((resolve) => setTimeout(resolve, delay));
            // Exponential backoff
            delay *= 2;
        }
    }

    throw new Error(`Failed after ${maxAttempts} attempts: ${lastError}`);
}

// Usage
try {
    const result = await withRetry(async () => {
        return await unstableAPICall();
    });
    console.log('Success:', result);
} catch (error) {
    console.error('All attempts failed:', error);
}
```

### Pattern de Fallback

```javascript
class ServiceWithFallback {
    constructor(prioritizedServices) {
        this.services = prioritizedServices;
    }

    async executeOperation(operation, ...args) {
        let lastError;

        for (const service of this.services) {
            try {
                return await service[operation](...args);
            } catch (error) {
                lastError = error;
                console.warn(`Service ${service.name} failed:`, error);
                continue;
            }
        }

        throw new Error(`All services failed: ${lastError}`);
    }
}

// Usage
const service = new ServiceWithFallback([new PrimaryService(), new BackupService(), new FallbackService()]);

try {
    const data = await service.executeOperation('getData', params);
    console.log('Operation successful:', data);
} catch (error) {
    console.error('All services failed:', error);
}
```

## Bonnes Pratiques

1. **Gestion Contextuelle**

    - Adapter le traitement selon le type d'erreur
    - Fournir des messages d'erreur clairs et utiles
    - Logger les erreurs de manière appropriée
    - Éviter d'exposer les détails sensibles

2. **Performance et Sécurité**

    - Éviter les blocs try/catch trop larges
    - Ne pas attraper les erreurs sans les traiter
    - Nettoyer les ressources dans les blocs finally
    - Valider les entrées pour prévenir les erreurs

3. **Maintenance**

    - Centraliser la gestion des erreurs
    - Documenter les erreurs possibles
    - Utiliser des codes d'erreur constants
    - Maintenir une hiérarchie claire des erreurs

4. **Expérience Utilisateur**

    - Fournir des messages d'erreur compréhensibles
    - Proposer des solutions alternatives
    - Maintenir l'état de l'application cohérent
    - Permettre la récupération après une erreur

## Exemples Pratiques

### Gestion d'Erreurs dans une API REST

```javascript
class APIError extends Error {
    constructor(message, status, code) {
        super(message);
        this.name = 'APIError';
        this.status = status;
        this.code = code;
    }
}

async function handleAPIRequest(req, res) {
    try {
        // Input validation
        if (!req.body.userId) {
            throw new APIError('UserId is required', 400, 'MISSING_USERID');
        }

        // Main operation
        const data = await fetchData(req.body.userId);

        // Response
        res.json({ success: true, data });
    } catch (error) {
        // Specific handling based on error type
        if (error instanceof APIError) {
            res.status(error.status).json({
                success: false,
                error: {
                    code: error.code,
                    message: error.message,
                },
            });
        } else {
            // Unexpected error
            console.error('Server error:', error);
            res.status(500).json({
                success: false,
                error: {
                    code: 'INTERNAL_ERROR',
                    message: 'Internal server error',
                },
            });
        }
    }
}
```

### Gestion d'Erreurs dans une Application Frontend

```javascript
class UIErrorHandler {
    static async handleError(error, component) {
        // Log error
        console.error(`Error in ${component}:`, error);

        // User notification
        if (error instanceof ValidationError) {
            await this.displayValidationMessage(error);
        } else if (error instanceof NetworkError) {
            await this.handleNetworkError(error);
        } else {
            await this.displayGenericError(error);
        }

        // Analytics
        this.sendAnalytics({
            type: error.name,
            component,
            message: error.message,
            timestamp: new Date(),
        });
    }

    static async displayValidationMessage(error) {
        // Display in the interface
        const message = this.formatValidationMessage(error);
        await this.displayNotification(message, 'warning');
    }

    static async handleNetworkError(error) {
        // Retry attempt
        if (await this.attemptReconnection()) {
            await this.displayNotification('Reconnection successful', 'success');
        } else {
            await this.displayNotification('Connection problem', 'error');
        }
    }

    static async displayGenericError(error) {
        await this.displayNotification('An unexpected error occurred', 'error');
    }
}

// Usage in a component
async function handleAction() {
    try {
        await performOperation();
    } catch (error) {
        await UIErrorHandler.handleError(error, 'MyComponent');
    }
}
```
