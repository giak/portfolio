---
title: Les Frameworks et Bibliothèques JavaScript Modernes
date: 2024-10-29
icon: code
category:
    - JavaScript
tags:
    - frameworks
    - bibliothèques
description: Un aperçu des frameworks et bibliothèques modernes en JavaScript, y compris React et Vue.js.
---

# Les Frameworks et Bibliothèques JavaScript Modernes

## Introduction

Les frameworks et bibliothèques JavaScript modernes sont essentiels pour le développement d'applications web complexes. Ils fournissent des outils, des patterns et des abstractions qui permettent de développer plus rapidement et de manière plus maintenable.

## React

### Composants et JSX

```javascript
// Composant fonctionnel moderne
import React, { useState, useEffect } from 'react';

function UserProfile({ userId }) {
    const [user, setUser] = useState(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        async function fetchUser() {
            try {
                const response = await fetch(`/api/users/${userId}`);
                const data = await response.json();
                setUser(data);
            } catch (error) {
                console.error('Erreur:', error);
            } finally {
                setLoading(false);
            }
        }

        fetchUser();
    }, [userId]);

    if (loading) return <div>Chargement...</div>;
    if (!user) return <div>Utilisateur non trouvé</div>;

    return (
        <div className="user-profile">
            <h2>{user.name}</h2>
            <p>{user.email}</p>
            <div className="user-stats">
                <span>Posts: {user.posts}</span>
                <span>Followers: {user.followers}</span>
            </div>
        </div>
    );
}
```

### Hooks Personnalisés

```javascript
// Hook personnalisé pour la gestion des formulaires
function useForm(initialValues = {}) {
    const [values, setValues] = useState(initialValues);
    const [errors, setErrors] = useState({});
    const [touched, setTouched] = useState({});

    const handleChange = (event) => {
        const { name, value } = event.target;
        setValues((prev) => ({
            ...prev,
            [name]: value,
        }));
    };

    const handleBlur = (event) => {
        const { name } = event.target;
        setTouched((prev) => ({
            ...prev,
            [name]: true,
        }));
    };

    const reset = () => {
        setValues(initialValues);
        setErrors({});
        setTouched({});
    };

    return {
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        reset,
    };
}

// Utilisation
function LoginForm() {
    const { values, handleChange, handleBlur } = useForm({
        email: '',
        password: '',
    });

    const handleSubmit = (e) => {
        e.preventDefault();
        // Logique de soumission
    };

    return (
        <form onSubmit={handleSubmit}>
            <input type="email" name="email" value={values.email} onChange={handleChange} onBlur={handleBlur} />
            <input
                type="password"
                name="password"
                value={values.password}
                onChange={handleChange}
                onBlur={handleBlur}
            />
            <button type="submit">Connexion</button>
        </form>
    );
}
```

## Vue.js

### Composition API

```javascript
// Composant Vue moderne
<script setup>
import { ref, onMounted, computed } from 'vue';

const count = ref(0);
const doubleCount = computed(() => count.value * 2);

function increment() {
    count.value++;
}

onMounted(() => {
    console.log('Composant monté');
});
</script>

<template>
    <div class="counter">
        <p>Compteur: {{ count }}</p>
        <p>Double: {{ doubleCount }}</p>
        <button @click="increment">Incrémenter</button>
    </div>
</template>

<style scoped>
.counter {
    padding: 1rem;
    text-align: center;
}
</style>
```

### Store avec Pinia

```javascript
// Store definition
import { defineStore } from 'pinia';

export const useAuthStore = defineStore('auth', {
    state: () => ({
        user: null,
        token: null,
    }),
    getters: {
        isAuthenticated: (state) => !!state.token,
        username: (state) => state.user?.name,
    },
    actions: {
        async login(credentials) {
            try {
                const response = await fetch('/api/login', {
                    method: 'POST',
                    body: JSON.stringify(credentials),
                });
                const data = await response.json();
                this.user = data.user;
                this.token = data.token;
            } catch (error) {
                console.error('Erreur de connexion:', error);
                throw error;
            }
        },
        logout() {
            this.user = null;
            this.token = null;
        },
    },
});
```

## Angular

### Composants et Services

```typescript
// Composant
@Component({
    selector: 'app-todo-list',
    template: `
        <div class="todo-list">
            <h2>{{ title }}</h2>
            <ul>
                <li *ngFor="let todo of todos" [class.completed]="todo.completed">
                    {{ todo.text }}
                    <button (click)="toggleTodo(todo)">Toggle</button>
                </li>
            </ul>
            <input [(ngModel)]="newTodo" (keyup.enter)="addTodo()" />
        </div>
    `,
    styles: [
        `
            .completed {
                text-decoration: line-through;
            }
        `,
    ],
})
export class TodoListComponent implements OnInit {
    title = 'Ma Liste de Tâches';
    todos: Todo[] = [];
    newTodo = '';

    constructor(private todoService: TodoService) {}

    ngOnInit() {
        this.loadTodos();
    }

    async loadTodos() {
        this.todos = await this.todoService.getTodos();
    }

    async addTodo() {
        if (this.newTodo.trim()) {
            await this.todoService.addTodo({
                text: this.newTodo,
                completed: false,
            });
            this.newTodo = '';
            await this.loadTodos();
        }
    }

    async toggleTodo(todo: Todo) {
        await this.todoService.toggleTodo(todo);
        await this.loadTodos();
    }
}

// Service
@Injectable({
    providedIn: 'root',
})
export class TodoService {
    private apiUrl = '/api/todos';

    constructor(private http: HttpClient) {}

    getTodos(): Observable<Todo[]> {
        return this.http.get<Todo[]>(this.apiUrl);
    }

    addTodo(todo: Todo): Observable<Todo> {
        return this.http.post<Todo>(this.apiUrl, todo);
    }

    toggleTodo(todo: Todo): Observable<Todo> {
        return this.http.patch<Todo>(`${this.apiUrl}/${todo.id}`, {
            completed: !todo.completed,
        });
    }
}
```

## Svelte

### Composants Réactifs

```svelte
<script>
    import { onMount } from 'svelte';

    let count = 0;
    $: doubled = count * 2;

    function increment() {
        count += 1;
    }

    onMount(() => {
        console.log('Composant monté');
    });
</script>

<div class="counter">
    <h2>Compteur Svelte</h2>
    <p>Valeur: {count}</p>
    <p>Double: {doubled}</p>
    <button on:click={increment}>
        Incrémenter
    </button>
</div>

<style>
    .counter {
        padding: 1rem;
        text-align: center;
    }

    button {
        background: #ff3e00;
        color: white;
        border: none;
        padding: 0.5rem 1rem;
        border-radius: 4px;
        cursor: pointer;
    }

    button:hover {
        background: #ff6240;
    }
</style>
```

## Bonnes Pratiques

1. **Architecture**

    - Suivre les principes SOLID
    - Utiliser une architecture modulaire
    - Séparer les préoccupations
    - Implémenter le pattern Container/Presentational

2. **Performance**

    - Optimiser le rendu
    - Utiliser le code splitting
    - Implémenter la lazy loading
    - Minimiser les re-rendus

3. **Maintenance**

    - Documenter les composants
    - Utiliser TypeScript
    - Créer des tests unitaires
    - Suivre les conventions de nommage

4. **État et Données**

    - Centraliser la gestion d'état
    - Utiliser l'immutabilité
    - Gérer efficacement le cache
    - Implémenter des stratégies de mise à jour optimistes

## Exemples Pratiques

### Application React avec Gestion d'État

```javascript
// Store avec Redux Toolkit
import { createSlice, configureStore } from '@reduxjs/toolkit';

const todoSlice = createSlice({
    name: 'todos',
    initialState: {
        items: [],
        loading: false,
        error: null,
    },
    reducers: {
        setTodos: (state, action) => {
            state.items = action.payload;
        },
        addTodo: (state, action) => {
            state.items.push(action.payload);
        },
        toggleTodo: (state, action) => {
            const todo = state.items.find((item) => item.id === action.payload);
            if (todo) {
                todo.completed = !todo.completed;
            }
        },
    },
});

const store = configureStore({
    reducer: {
        todos: todoSlice.reducer,
    },
});

// Composant avec hooks
function TodoApp() {
    const dispatch = useDispatch();
    const todos = useSelector((state) => state.todos.items);

    useEffect(() => {
        async function loadTodos() {
            const response = await fetch('/api/todos');
            const data = await response.json();
            dispatch(setTodos(data));
        }
        loadTodos();
    }, [dispatch]);

    return (
        <div className="todo-app">
            <TodoList todos={todos} onToggle={(id) => dispatch(toggleTodo(id))} />
            <AddTodo onAdd={(todo) => dispatch(addTodo(todo))} />
        </div>
    );
}
```

### Application Vue avec Composition API et TypeScript

```typescript
// Composant avec TypeScript
import { defineComponent, ref, computed } from 'vue';
import type { Todo } from '@/types';

export default defineComponent({
    name: 'TodoList',
    props: {
        initialTodos: {
            type: Array as PropType<Todo[]>,
            required: true,
        },
    },
    setup(props) {
        const todos = ref<Todo[]>(props.initialTodos);
        const filter = ref<'all' | 'active' | 'completed'>('all');

        const filteredTodos = computed(() => {
            switch (filter.value) {
                case 'active':
                    return todos.value.filter((todo) => !todo.completed);
                case 'completed':
                    return todos.value.filter((todo) => todo.completed);
                default:
                    return todos.value;
            }
        });

        function addTodo(text: string) {
            todos.value.push({
                id: Date.now(),
                text,
                completed: false,
            });
        }

        function toggleTodo(id: number) {
            const todo = todos.value.find((t) => t.id === id);
            if (todo) {
                todo.completed = !todo.completed;
            }
        }

        return {
            todos,
            filter,
            filteredTodos,
            addTodo,
            toggleTodo,
        };
    },
});
```
