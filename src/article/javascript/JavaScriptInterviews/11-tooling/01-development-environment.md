---
title: Les Outils et l'Environnement de Développement JavaScript
date: 2024-10-29
icon: code
category:
    - JavaScript
tags:
    - outils
    - environnement
description: Un guide sur les outils et l'environnement de développement pour JavaScript.
---

# Les Outils et l'Environnement de Développement JavaScript

## Introduction

Un environnement de développement bien configuré est essentiel pour écrire du code JavaScript efficacement et de manière productive. Cette section couvre les outils essentiels et les meilleures pratiques pour mettre en place un environnement de développement professionnel.

## Gestionnaires de Paquets

### NPM (Node Package Manager)

```json
// package.json
{
    "name": "mon-projet",
    "version": "1.0.0",
    "scripts": {
        "start": "node src/index.js",
        "dev": "nodemon src/index.js",
        "build": "webpack --mode production",
        "test": "jest",
        "lint": "eslint src/**/*.js",
        "format": "prettier --write \"src/**/*.{js,jsx,ts,tsx}\""
    },
    "dependencies": {
        "express": "^4.18.2",
        "react": "^18.2.0"
    },
    "devDependencies": {
        "eslint": "^8.40.0",
        "jest": "^29.5.0",
        "prettier": "^2.8.8",
        "webpack": "^5.82.1"
    }
}
```

### Yarn

```yaml
# .yarnrc.yml
nodeLinker: node-modules
enableGlobalCache: true
nmMode: hardlinks-local

plugins:
    - path: .yarn/plugins/@yarnpkg/plugin-typescript.cjs
      spec: '@yarnpkg/plugin-typescript'
```

## Outils de Build

### Webpack

```javascript
// webpack.config.js
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].[contenthash].js',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                    },
                },
            },
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader'],
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
        }),
        new MiniCssExtractPlugin(),
    ],
    optimization: {
        splitChunks: {
            chunks: 'all',
        },
    },
    devServer: {
        static: './dist',
        hot: true,
    },
};
```

### Vite

```javascript
// vite.config.js
import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

export default defineConfig({
    plugins: [react()],
    server: {
        port: 3000,
        proxy: {
            '/api': 'http://localhost:8080',
        },
    },
    build: {
        outDir: 'dist',
        sourcemap: true,
        rollupOptions: {
            output: {
                manualChunks: {
                    vendor: ['react', 'react-dom'],
                },
            },
        },
    },
});
```

## Linting et Formatage

### ESLint

```javascript
// .eslintrc.js
module.exports = {
    env: {
        browser: true,
        es2021: true,
        node: true,
        jest: true,
    },
    extends: ['eslint:recommended', 'plugin:react/recommended', 'plugin:@typescript-eslint/recommended', 'prettier'],
    parser: '@typescript-eslint/parser',
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 12,
        sourceType: 'module',
    },
    plugins: ['react', '@typescript-eslint', 'prettier'],
    rules: {
        'prettier/prettier': 'error',
        'no-unused-vars': 'warn',
        'no-console': 'warn',
    },
    settings: {
        react: {
            version: 'detect',
        },
    },
};
```

### Prettier

```json
// .prettierrc
{
    "semi": true,
    "tabWidth": 4,
    "printWidth": 100,
    "singleQuote": true,
    "trailingComma": "es5",
    "bracketSpacing": true,
    "arrowParens": "always"
}
```

## Tests

### Jest

```javascript
// jest.config.js
module.exports = {
    testEnvironment: 'jsdom',
    setupFilesAfterEnv: ['<rootDir>/src/setupTests.js'],
    moduleNameMapper: {
        '\\.(css|less|scss|sass)$': 'identity-obj-proxy',
        '\\.(gif|ttf|eot|svg)$': '<rootDir>/__mocks__/fileMock.js',
    },
    transform: {
        '^.+\\.(js|jsx|ts|tsx)$': ['babel-jest', { presets: ['next/babel'] }],
    },
    collectCoverageFrom: ['src/**/*.{js,jsx,ts,tsx}', '!src/**/*.d.ts'],
    coverageThreshold: {
        global: {
            branches: 80,
            functions: 80,
            lines: 80,
            statements: 80,
        },
    },
};

// exemple.test.js
describe('Module de calcul', () => {
    test('additionne correctement deux nombres', () => {
        expect(addition(2, 3)).toBe(5);
    });

    test('gère les nombres négatifs', () => {
        expect(addition(-1, 1)).toBe(0);
    });
});
```

## Gestion des Versions

### Git

```gitignore
# .gitignore
node_modules/
dist/
coverage/
.env
.DS_Store
*.log
```

```bash
# Hooks Git
#!/bin/sh
# .git/hooks/pre-commit

npm run lint
npm run test
```

## Débogage

### Chrome DevTools

```javascript
// Configuration du débogage
{
    // Point d'arrêt dans le code
    debugger;

    // Console avec formatage
    console.log('Données:', data);
    console.table(arrayData);
    console.time('opération');
    // ... code ...
    console.timeEnd('opération');

    // Groupement de logs
    console.group('Groupe de logs');
    console.log('Log 1');
    console.log('Log 2');
    console.groupEnd();
}
```

### VS Code

```json
// .vscode/launch.json
{
    "version": "0.2.0",
    "configurations": [
        {
            "type": "node",
            "request": "launch",
            "name": "Debug Node.js",
            "program": "${workspaceFolder}/src/index.js",
            "skipFiles": ["<node_internals>/**"],
            "outFiles": ["${workspaceFolder}/dist/**/*.js"]
        },
        {
            "type": "chrome",
            "request": "launch",
            "name": "Debug Chrome",
            "url": "http://localhost:3000",
            "webRoot": "${workspaceFolder}/src"
        }
    ]
}
```

## Intégration Continue

### GitHub Actions

```yaml
# .github/workflows/ci.yml
name: CI

on:
    push:
        branches: [main]
    pull_request:
        branches: [main]

jobs:
    build:
        runs-on: ubuntu-latest

        steps:
            - uses: actions/checkout@v2
            - name: Use Node.js
              uses: actions/setup-node@v2
              with:
                  node-version: '16.x'
            - run: npm ci
            - run: npm run build
            - run: npm test
            - run: npm run lint
```

## Documentation

### JSDoc

```javascript
/**
 * Calcule la somme de deux nombres.
 * @param {number} a - Premier nombre.
 * @param {number} b - Deuxième nombre.
 * @returns {number} La somme des deux nombres.
 * @throws {TypeError} Si les paramètres ne sont pas des nombres.
 * @example
 * const resultat = addition(2, 3);
 * console.log(resultat); // 5
 */
function addition(a, b) {
    if (typeof a !== 'number' || typeof b !== 'number') {
        throw new TypeError('Les paramètres doivent être des nombres');
    }
    return a + b;
}
```

## Bonnes Pratiques

1. **Configuration du Projet**

    - Utiliser des fichiers de configuration standardisés
    - Maintenir les dépendances à jour
    - Documenter les scripts npm
    - Utiliser le versioning sémantique

2. **Qualité du Code**

    - Configurer ESLint et Prettier
    - Mettre en place des hooks pre-commit
    - Maintenir une couverture de tests élevée
    - Documenter le code avec JSDoc

3. **Workflow de Développement**

    - Utiliser le hot reloading
    - Configurer les outils de débogage
    - Mettre en place l'intégration continue
    - Automatiser les tâches répétitives

4. **Sécurité**

    - Vérifier les vulnérabilités des dépendances
    - Utiliser des variables d'environnement
    - Mettre en place des audits de sécurité
    - Suivre les meilleures pratiques OWASP

## Exemples Pratiques

### Configuration Complète d'un Projet

```javascript
// Structure du projet
project/
├── src/
│   ├── components/
│   ├── utils/
│   └── index.js
├── tests/
├── docs/
├── .eslintrc.js
├── .prettierrc
├── jest.config.js
├── package.json
├── README.md
└── webpack.config.js

// Scripts npm utiles
{
    "scripts": {
        "start": "webpack serve --mode development",
        "build": "webpack --mode production",
        "test": "jest --coverage",
        "lint": "eslint . --fix",
        "format": "prettier --write \"**/*.{js,jsx,ts,tsx,json,md}\"",
        "prepare": "husky install",
        "docs": "jsdoc -c jsdoc.json"
    }
}
```

### Automatisation des Tâches

```javascript
// gulpfile.js
const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const minify = require('gulp-clean-css');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');

// Compilation SASS
gulp.task('styles', () => {
    return gulp
        .src('src/styles/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(minify())
        .pipe(gulp.dest('dist/css'));
});

// Transpilation JavaScript
gulp.task('scripts', () => {
    return gulp
        .src('src/js/**/*.js')
        .pipe(
            babel({
                presets: ['@babel/env'],
            }),
        )
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'));
});

// Surveillance des fichiers
gulp.task('watch', () => {
    gulp.watch('src/styles/**/*.scss', gulp.series('styles'));
    gulp.watch('src/js/**/*.js', gulp.series('scripts'));
});

// Tâche par défaut
gulp.task('default', gulp.parallel('styles', 'scripts', 'watch'));
```
