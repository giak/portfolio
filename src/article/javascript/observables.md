# Les Observables 👁️ en JavaScript : Du Natif à RxJS ⚡, en passant par TC39 🚀

## Introduction 🌟

Dans l'univers du développement web moderne, la gestion efficace des flux de données asynchrones est devenue un enjeu majeur. C'est dans ce contexte que la programmation réactive, et plus particulièrement les Observables, ont gagné en popularité. En tant qu'expert en développement web avec plus de 10 ans d'expérience dans l'écosystème JavaScript/TypeScript, je vous propose aujourd'hui un voyage à travers l'évolution des Observables : des implémentations natives actuelles aux futures propositions TC39, en passant par la puissance de RxJS.

Cet article vise à vous offrir une compréhension approfondie des Observables, leurs avantages, leurs limites, et comment ils s'intègrent dans le paysage du développement web moderne. Que vous soyez un développeur chevronné ou en pleine montée en compétences, cette exploration vous donnera les clés pour faire des choix éclairés dans vos projets. 🗝️

[📦 Vous trouverez plusieurs exemples dans ce dépôt](https://github.com/giak/design-patterns-typescript/tree/main/src/observable)

## Qu'est-ce qu'un Observable en JavaScript/TypeScript 5 ? 🤔

Un Observable représente une collection de valeurs ou d'événements futurs. C'est un patron de conception puissant pour gérer des séquences de données asynchrones. Contrairement aux Promises qui gèrent une seule valeur asynchrone, les Observables peuvent émettre de multiples valeurs au fil du temps.

Les Observables peuvent être vus comme une généralisation des Promises, capables de gérer des séquences de valeurs plutôt qu'une seule. Ils sont particulièrement utiles dans des scénarios où les données arrivent au fil du temps, comme des flux d'événements ou des mises à jour en temps réel.

Voici trois cas d'usage concrets où les Observables brillent particulièrement :

1. 🖱️ **Gestion d'événements utilisateurs** : Capturer et traiter des séries de clics ou de frappes au clavier.
2. 📊 **Flux de données en temps réel** : Gérer des mises à jour continues de données, comme des cours boursiers ou des données IoT.
3. 🔄 **Requêtes HTTP avec annulation** : Effectuer des requêtes HTTP qui peuvent être annulées ou retentées facilement.

Examinons un exemple simple d'Observable natif :

```jsx
type ObserverType<T> = {
    next: (value: T) => void,
    error?: (error: Error) => void,
    complete?: () => void,
};

function createObservable() {
    return {
        subscribe: (observer: ObserverType<Date>) => {
            const intervalId = setInterval(() => {
                observer.next(new Date());
            }, 1000);

            return {
                unsubscribe: () => {
                    clearInterval(intervalId);
                },
            };
        },
    };
}

const timeObservable = createObservable();
const subscription = timeObservable.subscribe({
    next: (value: Date) => console.log('Date reçue:', value.toLocaleString()),
    error: (err) => console.error('Erreur:', err),
    complete: () => console.log('Observable terminé'),
});

setTimeout(() => {
    console.log('Unsubscribing...');
    subscription.unsubscribe();
}, 5000);
```

Cet exemple crée un Observable qui émet la date actuelle toutes les secondes. Il illustre les concepts clés : souscription, émission de valeurs, et désabonnement.

Voici un exemple plus élaboré d'Observable natif qui simule un flux de données de capteur :

```jsx
interface SensorDataInterface {
    timestamp: string;
    temperature?: number;
    pressure?: number;
    humidity?: number;
}

interface SensorObserverInterface {
    next: (value: SensorDataInterface) => void;
    error?: (error: Error) => void;
    complete?: () => void;
}

function createSensorDataObservable() {
    return {
        subscribe: (observer: SensorObserverInterface) => {
            const intervalId = setInterval(() => {
                const sensorData = {
                    timestamp: new Date().toISOString(),
                    temperature: Math.random() * 30 + 10,
                    humidity: Math.random() * 50 + 30,
                    pressure: Math.random() * 10 + 1000,
                };

                observer.next(sensorData);
            }, 2000);

            return {
                unsubscribe: () => {
                    clearInterval(intervalId);
                    observer.complete?.();
                },
            };
        },
    };
}

const sensorObservable = createSensorDataObservable();
const subscription = sensorObservable.subscribe({
    next: (data) =>
        console.log(
            `Température: ${data.temperature?.toFixed(1)}°C, Humidité: ${data.humidity?.toFixed(
                1,
            )}%, Pression: ${data.pressure?.toFixed(1)} hPa`,
        ),
    error: (err) => console.error('Erreur:', err),
    complete: () => console.log('Flux de données terminé'),
});

// Arrêt du flux après 10 secondes
setTimeout(() => {
    subscription.unsubscribe();
}, 10000);
```

Cet exemple illustre comment un Observable peut être utilisé pour modéliser un flux continu de données de capteur, avec la possibilité d'arrêter le flux à tout moment.

## Les avantages des Observables natifs en JavaScript/TypeScript 5 💪

Les Observables natifs offrent plusieurs avantages significatifs :

-   🔄 **Réactivité** : Ils permettent de réagir en temps réel aux changements de données.
-   🔀 **Composition** : Les Observables peuvent être facilement combinés et transformés.
-   🎛️ **Contrôle du flux** : Ils offrent un contrôle fin sur le flux de données (pause, reprise, annulation).
-   🧹 **Nettoyage automatique** : La gestion des ressources est simplifiée grâce au mécanisme de désabonnement.

Par exemple, voici comment les Observables peuvent simplifier la gestion d'un flux de données complexe :

```tsx
// Exemple non fonctionnel
function getUserUpdates(): ObserverInterface {
    return new Observable((observer) => {
        const eventSource = new EventSource('/api/users/updates');
        eventSource.onmessage = (event) => {
            observer.next(JSON.parse(event.data));
        };
        return () => eventSource.close();
    });
}

const subscription = getUserUpdates().subscribe({
    next: (user) => console.log(`User updated: ${user.name}`),
    error: (err) => console.error('Error:', err),
    complete: () => console.log('Stream ended'),
});

// Désabonnement après 5 minutes
setTimeout(() => subscription.unsubscribe(), 300000);
```

Cet exemple montre comment les Observables peuvent gérer élégamment un flux de mises à jour d'utilisateurs en temps réel, avec une gestion propre des ressources.

## Les inconvénients et les limites des Observables natifs 🚧

Malgré leurs avantages, les Observables natifs présentent certaines limitations :

-   🌐 **Support limité** : Ils ne sont pas encore nativement supportés par tous les navigateurs.
-   🛠️ **Manque d'opérateurs avancés** : Comparés à des bibliothèques comme RxJS, les Observables natifs offrent moins de fonctionnalités out-of-the-box.
-   🔍 **Débogage complexe** : Le suivi des flux de données asynchrones peut devenir difficile dans des scénarios complexes.

Par exemple, la gestion d'un scénario de retry avec backoff exponentiel est moins triviale avec les Observables natifs :

```jsx
// Exemple non fonctionnel
function fetchWithRetry(url, maxRetries = 3) {
    return new Observable((observer) => {
        let retries = 0;

        function attempt() {
            fetch(url)
                .then((response) => {
                    if (!response.ok) throw new Error('Network response was not ok');
                    observer.next(response);
                    observer.complete();
                })
                .catch((error) => {
                    if (retries < maxRetries) {
                        retries++;
                        const delay = Math.pow(2, retries) * 1000; // Backoff exponentiel
                        setTimeout(attempt, delay);
                    } else {
                        observer.error(error);
                    }
                });
        }

        attempt();

        return () => {
            // Pas de mécanisme simple pour annuler la requête en cours
        };
    });
}
```

Ce code montre les limites des Observables natifs pour des scénarios avancés comme la gestion de retry avec backoff exponentiel, qui serait plus simple à implémenter avec RxJS.

## Patterns et meilleures pratiques avec les Observables natifs 🏗️

Bien que les Observables natifs aient certaines limitations, il existe des patterns et des meilleures pratiques qui peuvent améliorer leur utilisation :

### 1. Pattern Producteur-Consommateur 🔄

```jsx
type ObserverType<T> = {
  next: (value: T) => void;
  error?: (error: Error) => void;
  complete?: () => void;
};

function createProducer<T>() {
  const observers = new Set<ObserverType<T>>();
  return {
    subscribe: (observer: ObserverType<T>) => {
      observers.add(observer);
      return {
        unsubscribe: () => observers.delete(observer),
      };
    },
    notify: (data: T) => {
      for (const observer of observers) {
        observer.next(data);
      }
    },
  };
}

const producer = createProducer();
producer.subscribe({
  next: (data) => console.log('Consumer 1:', data),
});
producer.subscribe({
  next: (data) => console.log('Consumer 2:', data),
});

producer.notify('Hello, Observers!');
```

### 2. Création d'Observables réutilisables 🔁

```jsx
function fromEvent<T extends Event>(target: EventTarget, eventName: string) {
  return {
    subscribe: (observer: { next: (event: T) => void }) => {
      const handler = ((event: T) => observer.next(event)) as EventListener;
      target.addEventListener(eventName, handler);
      return {
        unsubscribe: () => target.removeEventListener(eventName, handler),
      };
    },
  };
}

const clickObservable = fromEvent<MouseEvent>(document, 'click');
const subscription = clickObservable.subscribe({
  next: (event: MouseEvent) => console.log('Click at:', event.clientX, event.clientY),
});

// Arrêt du flux après 10 secondes
setTimeout(() => {
  subscription.unsubscribe();
}, 10000);
```

Ces patterns montrent comment créer des Observables plus flexibles et réutilisables, même avec les limitations des implémentations natives.

## Comment TypeScript améliore les Observables natifs ? 🦾

TypeScript apporte une couche de typage statique qui améliore considérablement l'expérience de développement avec les Observables :

-   📝 **Typage fort** : Permet de détecter les erreurs à la compilation plutôt qu'à l'exécution.
-   🧠 **Autocomplétion intelligente** : L'IDE peut suggérer des méthodes et propriétés appropriées.
-   📚 **Documentation intégrée** : Les types fournissent une forme de documentation en ligne.

## Alternatives et Comparaison : Quand utiliser autre chose ? 🤹

Bien que puissants, les Observables natifs ne sont pas la meilleure solution. Voici une comparaison avec d'autres approches :

| Fonctionnalité               | Observables natifs | Promises | EventEmitters | Design Pattern Observer | Object Proxy |
| ---------------------------- | ------------------ | -------- | ------------- | ----------------------- | ------------ |
| Valeurs multiples            | ✅                 | ❌       | ✅            | ✅                      | ✅           |
| Annulation                   | ✅                 | ❌       | ✅            | ✅                      | ❌           |
| Opérations de transformation | Limitées           | Limitées | ❌            | ❌                      | ✅           |
| Facilité d'utilisation       | Moyenne            | Élevée   | Moyenne       | Moyenne                 | Élevée       |
| Support natif                | ✅ (TypeScript 5+) | ✅       | ✅ (Node.js)  | ✅                      | ✅           |

Examinons chaque approche en détail avec des exemples :

### 1. Observables natifs (JavaScript/TypeScript 5)

```tsx
// Exmaple du début de l'article
type ObserverType<T> = {
    next: (value: T) => void;
    error?: (error: Error) => void;
    complete?: () => void;
};

function createObservable() {
    return {
        subscribe: (observer: ObserverType<Date>) => {
            const intervalId = setInterval(() => {
                observer.next(new Date());
            }, 1000);

            return {
                unsubscribe: () => {
                    clearInterval(intervalId);
                },
            };
        },
    };
}

const timeObservable = createObservable();
const subscription = timeObservable.subscribe({
    next: (value: Date) => console.log('Date reçue:', value.toLocaleString()),
    error: (err) => console.error('Erreur:', err),
    complete: () => console.log('Observable terminé'),
});

setTimeout(() => {
    console.log('Unsubscribing...');
    subscription.unsubscribe();
}, 5000);
```

Les Observables natifs excellent dans la gestion des flux de données asynchrones et permettent l'annulation.

### 2. Promises

```jsx
function fetchData() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve('Data fetched');
        }, 1000);
    });
}

fetchData()
    .then((data) => console.log(data))
    .catch((error) => console.error(error));
```

Les Promises sont idéales pour les opérations asynchrones uniques et ne peuvent pas être annulées.

### 3. EventEmitters (Node.js)

```jsx
import EventEmitter from 'node:events';

class MyEmitter extends EventEmitter {}

const myEmitter = new MyEmitter();
myEmitter.on('event', () => {
    console.log('an event occurred!');
});

myEmitter.emit('event');
```

Les EventEmitters sont utiles pour la gestion d'événements dans Node.js, mais moins flexibles que les Observables.

### 4. Design Pattern Observer

[Article : 🚀Maîtriser le Design Pattern Observer avec TypeScript 5 : Un Guide Complet pour Développeurs Avancés](https://www.linkedin.com/pulse/ma%25C3%25AEtriser-le-design-pattern-observer-avec-typescript-5-giacomel--rojye/)

```tsx
interface ObserverInterface<T> {
    update(data: T): void;
}

class Observable<T> {
    private observers: ObserverInterface<T>[] = [];

    addObserver(observer: ObserverInterface<T>) {
        this.observers.push(observer);
    }

    removeObserver(observer: ObserverInterface<T>) {
        const index = this.observers.indexOf(observer);
        if (index > -1) {
            this.observers.splice(index, 1);
        }
    }

    notify(data: T): void {
        for (const observer of this.observers) {
            observer.update(data);
        }
    }
}

class ConcreteObserver implements ObserverInterface<string> {
    update(data: string) {
        console.log('Received update:', data);
    }
}

const subject = new Observable<string>();
const observer = new ConcreteObserver();

subject.addObserver(observer);
subject.notify('Hello, Observer!');
```

Le design pattern Observer offre une grande flexibilité mais nécessite plus de code boilerplate que les Observables natifs.

### 5. Object Proxy

[Article : 🚀 Maîtrisez le Design Pattern "Proxy" 🌐 en TypeScript 5.](https://www.linkedin.com/pulse/ma%25C3%25AEtrisez-le-design-pattern-proxy-en-typescript-5-giacomel--aolee/)

```jsx
const target = {
    message1: 'hello',
    message2: 'everyone',
};

const handler: ProxyHandler<Record<string | symbol, unknown>> = {
    get(target: Record<string | symbol, unknown>, prop: string | symbol, receiver: unknown) {
        console.log(`Property ${String(prop)} accessed`);
        return Reflect.get(target, prop, receiver);
    },
    set(target, prop, value, receiver) {
        console.log(`Property ${String(prop)} set to ${value}`);
        return Reflect.set(target, prop, value, receiver);
    },
};

const proxy = new Proxy(target, handler);

console.log(proxy.message1); // Logs: Property message1 accessed
proxy.message2 = 'world'; // Logs: Property message2 set to world
```

Les Proxies sont excellents pour intercepter et personnaliser les opérations fondamentales sur les objets, mais ne sont pas conçus spécifiquement pour les flux de données asynchrones.

Choisissez l'approche qui convient le mieux à votre cas d'utilisation spécifique. Les Observables natifs sont particulièrement adaptés aux flux de données complexes et transformables, tandis que les autres approches peuvent être plus appropriées pour des scénarios plus simples ou spécifiques.

## La proposition TC39 et les futurs Observables natifs en JavaScript 🔮

La proposition TC39 pour les Observables vise à standardiser cette fonctionnalité directement dans JavaScript. Voici un aperçu de ce que cela pourrait apporter :

```jsx
// Classe `Observable` non fonctionnelle, typescript ne le supporte pas encore
interface Observer<T> {
    next: (value: T) => void;
    error: (err: unknown) => void;
    complete: () => void;
}

const observable =
    new Observable() <
    number >
    ((observer: Observer<number>) => {
        observer.next(1);
        observer.next(2);
        observer.next(3);
        setTimeout(() => {
            observer.next(4);
            observer.complete();
        }, 1000);
    });

// Souscription à l'Observable
observable.subscribe({
    next(x: number) {
        console.log(`got value ${x}`);
    },
    error(err: unknown) {
        console.error(`something wrong occurred: ${err}`);
    },
    complete() {
        console.log('done');
    },
});
```

Cette proposition apporterait une syntaxe plus claire et une meilleure intégration avec l'écosystème JavaScript existant. Elle faciliterait également l'adoption des principes de programmation réactive dans le développement web mainstream.

## Réactivité dans les frameworks modernes : Une autre forme d'Observables 🔄

Les frameworks JavaScript modernes comme Vue.js, React et Angular ont tous implémenté leur propre système de réactivité, qui partage certaines similitudes avec les Observables. Explorons comment chacun d'eux gère la réactivité :

### 1. Vue.js - Système de réactivité 🟢

Vue.js utilise un système de réactivité basé sur la détection fine des dépendances. Il transforme les propriétés d'un objet en getters et setters, créant ainsi un système similaire aux Observables.

```jsx
import { ref, watch } from 'vue';

const count = ref(0);

watch(count, (newValue, oldValue) => {
    console.log(`Count changed from ${oldValue} to ${newValue}`);
});

// Plus tard
count.value++;
// Console: "Count changed from 0 to 1"
```

Dans cet exemple, `ref` crée un objet réactif, et `watch` agit comme un observateur.

### 2. React - État et effets 🔵

React utilise un système de mise à jour basé sur l'état et les effets, qui, bien que différent des Observables classiques, partage le concept de réaction aux changements.

```jsx
import React, { useState, useEffect } from 'react';

function Counter() {
  const [count, setCount] = useState(0);

  useEffect(() => {
    console.log(`Count changed to ${count}`);
  }, [count]);

  return (
Count: {count} setCount(count + 1)}>Increment
  );
}
```

Ici, `useState` crée un état réactif, et `useEffect` observe les changements de cet état.

### 3. Angular - RxJS intégré 🔴

Angular va plus loin en intégrant directement RxJS, offrant ainsi une expérience proche des Observables natifs.

```tsx
import { Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
    selector: 'app-counter',
    template: ` Count: {{ count$ | async }}Increment `,
})
export class CounterComponent {
    private countSubject = new BehaviorSubject(0);
    count$ = this.countSubject.asObservable();

    increment() {
        this.countSubject.next(this.countSubject.value + 1);
    }
}
```

Angular utilise pleinement les Observables via RxJS, comme on peut le voir avec `BehaviorSubject`.

### Comparaison avec les Observables natifs 📊

| Fonctionnalité                  | Observables natifs | Vue.js Reactivity | React Hooks             | Angular (RxJS) |
| ------------------------------- | ------------------ | ----------------- | ----------------------- | -------------- |
| Gestion des flux de données     | ✅                 | ✅ (limité)       | ✅ (via custom hooks)   | ✅             |
| Opérateurs de transformation    | Limités            | ❌                | ❌ (nécessite des libs) | ✅ (RxJS)      |
| Intégration native au framework | ❌                 | ✅                | ✅                      | ✅             |
| Courbe d'apprentissage          | Moyenne            | Faible            | Moyenne                 | Élevée (RxJS)  |

Bien que ces systèmes de réactivité partagent des concepts avec les Observables, ils sont souvent plus simples à utiliser dans le contexte de leur framework respectif. Cependant, pour des scénarios complexes de gestion de flux de données, les Observables (particulièrement via RxJS) offrent généralement plus de flexibilité et de puissance.

### Les frameworks back

Tous les framework javascript/typescript peuvent implémenter RxJS.

## Pourquoi et Comment RxJS est la Meilleure Solution Actuelle pour les Observables 🏆

> 🌊 Restez à l'affût !
> Dans un prochain article, nous aborderons **RxJS** 🚀 en détail, à travers un exemple de tableau de bord de surveillance sur un barrage. 🌉
> 🤔 Imaginez un système capable d'agréger et d'analyser en temps réel les données de milliers de capteurs 📊🔍 !
> Ne le manquez pas ! 👀

RxJS offre une implémentation mature et complète des Observables, avec plusieurs avantages clés :

-   🛠️ **Riche en opérateurs** : Plus de 100 opérateurs pour transformer, combiner et manipuler les flux de données.
-   🧪 **Testabilité améliorée** : Des utilitaires pour tester les flux asynchrones.
-   🐛 **Débogage avancé** : Des outils pour visualiser et comprendre les flux de données complexes.
-   🔄 **Gestion de la concurrence** : Des opérateurs pour gérer efficacement les tâches concurrentes.

RxJS offre également des fonctionnalités avancées pour gérer la rétro-pression (backpressure), debounce et l'annulation, ce qui est crucial pour les applications à grande échelle. Voici un exemple illustrant ces capacités :

```tsx
import { EMPTY, Observable, interval } from 'rxjs';
import { catchError, map, retry, take, takeUntil } from 'rxjs/operators';

const source$ = interval(1000).pipe(
    map((val) => {
        if (val > 5 && Math.random() > 0.5) throw new Error('Random error');
        return val;
    }),
    retry(2),
    take(10),
    catchError((err) => {
        console.error('Error caught:', err);
        return EMPTY;
    }),
);

const stop$ = new Observable((observer) => {
    setTimeout(() => {
        observer.next();
        observer.complete();
    }, 5000);
});

source$.pipe(takeUntil(stop$)).subscribe({
    next: (val) => console.log('Received:', val),
    error: (err) => console.error('Error in subscription:', err),
    complete: () => console.log('Completed'),
});
```

1. Création d'un Observable source$ qui :

Émet une valeur toutes les secondes (avec interval(1000))

-   Émet une valeur toutes les secondes (avec interval(1000))

Transforme chaque valeur, et peut générer une erreur aléatoire si la valeur est supérieure à 5

-   Transforme chaque valeur, et peut générer une erreur aléatoire si la valeur est supérieure à 5

Essaie de se rétablir jusqu'à 2 fois en cas d'erreur

-   Essaie de se rétablir jusqu'à 2 fois en cas d'erreur

Prend seulement les 10 premières émissions

-   Prend seulement les 10 premières émissions

Capture toute erreur non gérée et termine silencieusement

-   Capture toute erreur non gérée et termine silencieusement

Création d'un Observable stop$ qui émet une valeur et se termine après 5 secondes.

-   Création d'un Observable stop$ qui émet une valeur et se termine après 5 secondes.

Souscription à source$, mais arrêt dès que stop$ émet (grâce à takeUntil(stop$)).

-   Souscription à source$, mais arrêt dès que stop$ émet (grâce à takeUntil(stop$)).

Lors de la souscription :

-   Lors de la souscription :

Affichage de chaque valeur reçue

-   Affichage de chaque valeur reçue

Affichage des erreurs éventuelles dans la souscription

-   Affichage des erreurs éventuelles dans la souscription

Indication quand l'Observable est terminé

-   Indication quand l'Observable est terminé

Cet exemple montre comment RxJS peut gérer élégamment les erreurs, les reprises, la limitation du nombre d'émissions, et l'annulation basée sur un autre Observable. Ces fonctionnalités sont cruciales pour construire des applications robustes et réactives.

## Cas d'Utilisation Précis et Scénarios d'Implémentation 🎯

Voyons quelques cas d'utilisation spécifiques où les Observables brillent particulièrement :

### 1. Flux de données en temps réel (WebSockets) 🔄

```tsx
import { webSocket } from 'rxjs/webSocket';

const socket$ = webSocket('wss://api.example.com/realtime');

socket$.subscribe({
    next: (data) => console.log('Received:', data),
    error: (err) => console.error(err),
});

// Envoi de données
socket$.next({ message: 'Hello, server!' });
```

Création d'un Observable socket$ qui :

-   Création d'un Observable socket$ qui :

Se connecte à un serveur WebSocket à l'adresse 'wss://api.example.com/realtime'

-   Se connecte à un serveur WebSocket à l'adresse 'wss://api.example.com/realtime'

Gère la communication bidirectionnelle avec ce serveur

-   Gère la communication bidirectionnelle avec ce serveur

Souscription à socket$ qui :

-   Souscription à socket$ qui :

Affiche dans la console chaque message reçu du serveur

-   Affiche dans la console chaque message reçu du serveur

Affiche les erreurs éventuelles dans la console

-   Affiche les erreurs éventuelles dans la console

Envoi d'un message au serveur en utilisant la méthode next de socket$

-   Envoi d'un message au serveur en utilisant la méthode next de socket$

En résumé, ce code démontre l'utilisation de RxJS pour gérer une connexion WebSocket. Il illustre comment :

Établir une connexion WebSocket

-   Établir une connexion WebSocket

Recevoir des messages du serveur de manière réactive

-   Recevoir des messages du serveur de manière réactive

Envoyer des messages au serveur

-   Envoyer des messages au serveur

Gérer les erreurs potentielles

-   Gérer les erreurs potentielles

### 2. Gestion des événements utilisateurs complexes 🖱️

```tsx
import { fromEvent } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';

const input = document.getElementById('search-input');
const input$ = fromEvent(input, 'input').pipe(
    debounceTime(300),
    map((event) => (event.target as HTMLInputElement).value),
);

input$.subscribe((value) => {
    console.log('Searching for:', value);
    // Effectuer la recherche
});
```

Création de l'Observable :

Récupération de l'élément HTML avec l'ID 'search-input'

-   Récupération de l'élément HTML avec l'ID 'search-input'

Création d'un Observable input$ à partir des événements 'input' de cet élément

-   Création d'un Observable input$ à partir des événements 'input' de cet élément

Application d'un pipeline de transformation :

-   Application d'un pipeline de transformation :

1. Utilisation de debounceTime(300) pour limiter la fréquence des émissions

Utilisation de map pour extraire la valeur de l'input

-   Utilisation de map pour extraire la valeur de l'input

Souscription :

Souscription à input$

-   Souscription à input$

Pour chaque valeur émise :

-   Pour chaque valeur émise :

Affichage de la valeur dans la console

-   Affichage de la valeur dans la console

Commentaire indiquant où effectuer la recherche

-   Commentaire indiquant où effectuer la recherche

Résumé :

Ce code illustre :

La création d'un Observable à partir d'événements DOM

-   La création d'un Observable à partir d'événements DOM

L'utilisation d'opérateurs RxJS pour transformer le flux de données

-   L'utilisation d'opérateurs RxJS pour transformer le flux de données

La gestion efficace des entrées utilisateur en temps réel

-   La gestion efficace des entrées utilisateur en temps réel

### 3. Communication entre modules dans une architecture de microservices 🔀

```tsx
import { Subject } from 'rxjs';

const messageBus = new Subject();

// Module 1
messageBus.subscribe((message) => {
    console.log('Module 1 received:', message);
});

// Module 2
messageBus.subscribe((message) => {
    console.log('Module 2 received:', message);
});

// Envoi d'un message
messageBus.next({ type: 'USER_UPDATED', data: { id: 1, name: 'John Doe' } });
```

Importation :

Importation de la classe Subject depuis RxJS. Un Subject est à la fois un Observable et un Observer, permettant la communication multicast.

-   Importation de la classe Subject depuis RxJS. Un Subject est à la fois un Observable et un Observer, permettant la communication multicast.

Création du bus de messages :

Création d'un Subject nommé messageBus qui servira de bus de messages central.

-   Création d'un Subject nommé messageBus qui servira de bus de messages central.

Souscription des modules :

Deux modules différents souscrivent au messageBus.

-   Deux modules différents souscrivent au messageBus.

Chaque module reçoit tous les messages émis sur le bus et les affiche dans la console.

-   Chaque module reçoit tous les messages émis sur le bus et les affiche dans la console.

Envoi d'un message :

Émission d'un message sur le messageBus en utilisant la méthode next.

-   Émission d'un message sur le messageBus en utilisant la méthode next.

Le message est un objet contenant un type ('USER_UPDATED') et des données (un objet utilisateur avec un id et un nom).

-   Le message est un objet contenant un type ('USER_UPDATED') et des données (un objet utilisateur avec un id et un nom).

Résumé :

Ce code illustre :

L'utilisation d'un Subject comme bus de messages central

-   L'utilisation d'un Subject comme bus de messages central

La communication entre différents modules d'une application de manière découplée

-   La communication entre différents modules d'une application de manière découplée

L'émission et la réception de messages structurés

-   L'émission et la réception de messages structurés

Ce pattern est particulièrement utile pour :

Implémenter une architecture de microservices

-   Implémenter une architecture de microservices

Faciliter la communication entre différentes parties d'une application

-   Faciliter la communication entre différentes parties d'une application

Créer des systèmes événementiels découplés et extensibles

-   Créer des systèmes événementiels découplés et extensibles

## Questions à se poser pour déterminer la pertinence des Observables 🤔

1. Mon application gère-t-elle des flux de données continus ou des événements répétitifs ?
2. Ai-je besoin de transformer ou de combiner ces flux de données de manière complexe ?
3. La gestion de l'état et des effets de bord est-elle un défi dans mon application ?
4. Mon équipe est-elle familière avec les concepts de programmation réactive ?
5. Les performances et la scalabilité sont-elles des préoccupations majeures ?

Si vous répondez "oui" à plusieurs de ces questions, les Observables (et potentiellement RxJS) pourraient être bénéfiques pour votre projet.

## Conclusion 🎓

[📦 Vous trouverez plusieurs exemples dans ce dépôt](https://github.com/giak/design-patterns-typescript/tree/main/src/observable)

Les Observables représentent une évolution significative dans la manière dont nous gérons les flux de données asynchrones en JavaScript. De leur implémentation native à leur intégration dans les frameworks modernes, en passant par la proposition TC39, ils démontrent leur valeur et leur versatilité.

Bien que les implémentations natives offrent déjà des avantages considérables, et que les frameworks comme Vue.js, React et Angular proposent leurs propres systèmes de réactivité, RxJS reste actuellement la solution la plus complète et robuste pour des applications complexes nécessitant une gestion avancée des flux de données.

Le choix entre ces différentes approches dépendra grandement du contexte de votre projet :

-   Pour des applications simples ou des projets utilisant déjà un framework spécifique, le système de réactivité intégré peut être suffisant.
-   Pour des applications JavaScript/TypeScript standalone avec des besoins en gestion de flux de données, les Observables natifs ou la future implémentation TC39 peuvent être appropriés.
-   Pour des applications complexes nécessitant une gestion avancée des flux de données, ou des projets Angular, RxJS reste l'outil de choix.

Je vous encourage à explorer ces différentes approches dans vos projets, en gardant à l'esprit les spécificités de chacune. N'hésitez pas à partager vos expériences ou à poser vos questions en commentaires - le partage de connaissances est clé dans notre communauté de développeurs !

**Réflexion finale :** L'évolution des Observables et des systèmes de réactivité dans l'écosystème JavaScript illustre la richesse et la diversité des solutions disponibles pour les développeurs modernes. Rester à jour sur ces différentes approches nous permet non seulement d'améliorer nos compétences, mais aussi de choisir les outils les plus appropriés pour construire des applications robustes, réactives et performantes.
