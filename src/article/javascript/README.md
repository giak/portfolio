---
title: JavaScript & TypeScript
description: Collection d'articles approfondis sur JavaScript moderne, TypeScript et les concepts avancés de programmation
icon: fa-brands fa-square-js
lang: fr
category:
    - JavaScript
    - TypeScript
    - Development
tags:
    - JavaScript
    - TypeScript
    - RxJS
    - ES6+
    - Best Practices
dir:
    text: JavaScript
    icon: javascript
    order: 3
    collapsible: true
    link: true
---

# JavaScript & TypeScript

Une collection d'articles détaillés sur JavaScript moderne, TypeScript et les concepts avancés de programmation.

## 🔄 Programmation Réactive

Exploration de RxJS et des concepts de programmation réactive.

<div class="article-grid">

<div class="article-card">
  <div class="article-image">
    <img src="./articleRxJs.jpeg" alt="RxJS" style="width: 100%; height: 120px; object-fit: cover;">
  </div>
  <div class="article-content">
    <h3><a href="./articleRxJs">Introduction à RxJS</a></h3>
    <p>Les fondamentaux de la programmation réactive avec RxJS.</p>
  </div>
</div>

<div class="article-card">
  <div class="article-image">
    <img src="https://raw.githubusercontent.com/giak/dam-dashboard/refs/heads/main/docs/assets/rxjs.jpg" alt="RxJS Avancé" style="width: 100%; height: 120px; object-fit: cover;">
  </div>
  <div class="article-content">
    <h3><a href="./DamRxJS">RxJS Avancé</a></h3>
    <p>Concepts avancés et patterns de programmation réactive avec RxJS.</p>
  </div>
</div>

<div class="article-card">
  <div class="article-image">
    <img src="./observables.jpg" alt="Observables" style="width: 100%; height: 120px; object-fit: cover;">
  </div>
  <div class="article-content">
    <h3><a href="./observables">Les Observables en JavaScript</a></h3>
    <p>Du natif à RxJS, en passant par TC39 : tout comprendre sur les Observables.</p>
  </div>
</div>

</div>

## 🎯 Préparation aux Entretiens JavaScript

Guide complet pour la préparation aux entretiens techniques JavaScript.

### 📚 Questions JavaScript Avancées

<div class="interview-grid">
  <div class="interview-card">
    <img src="./JavaScriptInterviews/JSInterview.jpg" alt="Questions JavaScript" class="interview-image">
    <div class="interview-content">
      <h4><a href="./JavaScriptInterviews/javascript-questions">Questions JavaScript</a></h4>
      <p>Collection complète de questions techniques JavaScript pour les entretiens</p>
    </div>
  </div>
</div>

### 📚 Fondamentaux et Concepts de Base

<div class="interview-grid">
  <div class="interview-card">
    <img src="./JavaScriptInterviews/JSInterview.jpg" alt="Introduction" class="interview-image">
    <div class="interview-content">
      <h4><a href="./JavaScriptInterviews/01-introduction/">1. Introduction</a></h4>
      <p>Histoire, caractéristiques et capacités de JavaScript</p>
      <div class="article-links">
        <a href="./JavaScriptInterviews/01-introduction/01-what-is-javascript">Qu'est-ce que JavaScript ?</a>
        <a href="./JavaScriptInterviews/01-introduction/02-javascript-capabilities">Capacités de JavaScript</a>
      </div>
    </div>
  </div>
  <div class="interview-card">
    <img src="./JavaScriptInterviews/JSInterview.jpg" alt="Fundamentals" class="interview-image">
    <div class="interview-content">
      <h4><a href="./JavaScriptInterviews/02-fundamentals/">2. Fondamentaux</a></h4>
      <p>Variables, types de données et opérateurs</p>
      <div class="article-links">
        <a href="./JavaScriptInterviews/02-fundamentals/01-variables">Variables</a>
        <a href="./JavaScriptInterviews/02-fundamentals/02-data-types">Types de données</a>
        <a href="./JavaScriptInterviews/02-fundamentals/03-operators">Opérateurs</a>
      </div>
    </div>
  </div>
  <div class="interview-card">
    <img src="./JavaScriptInterviews/JSInterview.jpg" alt="Control Structures" class="interview-image">
    <div class="interview-content">
      <h4><a href="./JavaScriptInterviews/03-control-structures/">3. Structures de Contrôle</a></h4>
      <p>Conditions, boucles et fonctions avancées</p>
      <div class="article-links">
        <a href="./JavaScriptInterviews/03-control-structures/01-conditions">Conditions</a>
        <a href="./JavaScriptInterviews/03-control-structures/02-loops">Boucles</a>
        <a href="./JavaScriptInterviews/03-control-structures/03-functions">Fonctions</a>
      </div>
    </div>
  </div>
  <div class="interview-card">
    <img src="./JavaScriptInterviews/JSInterview.jpg" alt="Objects" class="interview-image">
    <div class="interview-content">
      <h4><a href="./JavaScriptInterviews/04-objects/">4. Objets et Classes</a></h4>
      <p>POO, prototypes et classes ES6+</p>
      <div class="article-links">
        <a href="./JavaScriptInterviews/04-objects/01-objects">Objets</a>
        <a href="./JavaScriptInterviews/04-objects/02-arrays">Tableaux</a>
        <a href="./JavaScriptInterviews/04-objects/03-classes">Classes</a>
      </div>
    </div>
  </div>
</div>

### 🔄 Concepts Avancés

<div class="interview-grid">
  <div class="interview-card">
    <img src="./JavaScriptInterviews/JSInterview.jpg" alt="Async" class="interview-image">
    <div class="interview-content">
      <h4><a href="./JavaScriptInterviews/05-async/">5. Programmation Asynchrone</a></h4>
      <p>Promises, async/await et gestion avancée de l'asynchrone</p>
      <div class="article-links">
        <a href="./JavaScriptInterviews/05-async/01-promises">Promises et Async/Await</a>
      </div>
    </div>
  </div>
  <div class="interview-card">
    <img src="./JavaScriptInterviews/JSInterview.jpg" alt="Modules" class="interview-image">
    <div class="interview-content">
      <h4><a href="./JavaScriptInterviews/06-modules/">6. Modules</a></h4>
      <p>Systèmes de modules ES6 et organisation du code</p>
      <div class="article-links">
        <a href="./JavaScriptInterviews/06-modules/01-modules">Systèmes de Modules</a>
      </div>
    </div>
  </div>
  <div class="interview-card">
    <img src="./JavaScriptInterviews/JSInterview.jpg" alt="Testing" class="interview-image">
    <div class="interview-content">
      <h4><a href="./JavaScriptInterviews/07-testing/">7. Tests Unitaires</a></h4>
      <p>Tests unitaires et d'intégration en JavaScript</p>
      <div class="article-links">
        <a href="./JavaScriptInterviews/07-testing/01-testing">Tests Unitaires</a>
      </div>
    </div>
  </div>
  <div class="interview-card">
    <img src="./JavaScriptInterviews/JSInterview.jpg" alt="Error Handling" class="interview-image">
    <div class="interview-content">
      <h4><a href="./JavaScriptInterviews/08-error-handling/">8. Gestion des Erreurs</a></h4>
      <p>Stratégies de gestion des erreurs et debugging</p>
      <div class="article-links">
        <a href="./JavaScriptInterviews/08-error-handling/01-error-handling">Gestion des Erreurs</a>
      </div>
    </div>
  </div>
</div>

### 🛠 Architecture et Performance

<div class="interview-grid">
  <div class="interview-card">
    <img src="./JavaScriptInterviews/JSInterview.jpg" alt="Security" class="interview-image">
    <div class="interview-content">
      <h4><a href="./JavaScriptInterviews/09-security/">9. Sécurité</a></h4>
      <p>Vulnérabilités et meilleures pratiques de sécurité</p>
      <div class="article-links">
        <a href="./JavaScriptInterviews/09-security/01-security">Sécurité Web</a>
      </div>
    </div>
  </div>
  <div class="interview-card">
    <img src="./JavaScriptInterviews/JSInterview.jpg" alt="Performance" class="interview-image">
    <div class="interview-content">
      <h4><a href="./JavaScriptInterviews/10-performance/">10. Performance</a></h4>
      <p>Optimisation et techniques de performance avancées</p>
      <div class="article-links">
        <a href="./JavaScriptInterviews/10-performance/01-performance">Optimisation des Performances</a>
      </div>
    </div>
  </div>
  <div class="interview-card">
    <img src="./JavaScriptInterviews/JSInterview.jpg" alt="Tooling" class="interview-image">
    <div class="interview-content">
      <h4><a href="./JavaScriptInterviews/11-tooling/">11. Outillage</a></h4>
      <p>Webpack, Babel et outils de développement modernes</p>
      <div class="article-links">
        <a href="./JavaScriptInterviews/11-tooling/01-development-environment">Environnement de Développement</a>
      </div>
    </div>
  </div>
  <div class="interview-card">
    <img src="./JavaScriptInterviews/JSInterview.jpg" alt="Frameworks" class="interview-image">
    <div class="interview-content">
      <h4><a href="./JavaScriptInterviews/12-frameworks/">12. Frameworks</a></h4>
      <p>React, Vue, Angular et architectures modernes</p>
      <div class="article-links">
        <a href="./JavaScriptInterviews/12-frameworks/01-modern-frameworks">Frameworks Modernes</a>
      </div>
    </div>
  </div>
</div>

### 🚀 Sujets Avancés et Tendances

<div class="interview-grid">
  <div class="interview-card">
    <img src="./JavaScriptInterviews/JSInterview.jpg" alt="Future" class="interview-image">
    <div class="interview-content">
      <h4><a href="./JavaScriptInterviews/13-future/">13. Futur de JavaScript</a></h4>
      <p>TC39, nouvelles propositions et évolutions futures</p>
      <div class="article-links">
        <a href="./JavaScriptInterviews/13-future/01-trends-and-future">Tendances et Futur</a>
      </div>
    </div>
  </div>
  <div class="interview-card">
    <img src="./JavaScriptInterviews/JSInterview.jpg" alt="Deployment" class="interview-image">
    <div class="interview-content">
      <h4><a href="./JavaScriptInterviews/14-deployment/">14. Déploiement</a></h4>
      <p>CI/CD et stratégies de déploiement modernes</p>
      <div class="article-links">
        <a href="./JavaScriptInterviews/14-deployment/01-production-deployment">Déploiement en Production</a>
      </div>
    </div>
  </div>
  <div class="interview-card">
    <img src="./JavaScriptInterviews/JSInterview.jpg" alt="Design Patterns" class="interview-image">
    <div class="interview-content">
      <h4><a href="./JavaScriptInterviews/15-design-patterns/">15. Design Patterns</a></h4>
      <p>Patterns GoF et patterns JavaScript modernes</p>
      <div class="article-links">
        <a href="./JavaScriptInterviews/15-design-patterns/01-design-patterns">Design Patterns</a>
      </div>
    </div>
  </div>
  <div class="interview-card">
    <img src="./JavaScriptInterviews/JSInterview.jpg" alt="Architecture" class="interview-image">
    <div class="interview-content">
      <h4><a href="./JavaScriptInterviews/16-architecture/">16. Architecture</a></h4>
      <p>Clean Architecture et DDD en JavaScript</p>
      <div class="article-links">
        <a href="./JavaScriptInterviews/16-architecture/01-clean-architecture">Clean Architecture</a>
      </div>
    </div>
  </div>
</div>

### 🧪 Tests et PWA

<div class="interview-grid">
  <div class="interview-card">
    <img src="./JavaScriptInterviews/JSInterview.jpg" alt="Advanced Testing" class="interview-image">
    <div class="interview-content">
      <h4><a href="./JavaScriptInterviews/17-advanced-testing/">17. Tests Avancés</a></h4>
      <p>TDD, BDD et tests E2E avancés</p>
      <div class="article-links">
        <a href="./JavaScriptInterviews/17-advanced-testing/01-advanced-testing">Tests Avancés</a>
      </div>
    </div>
  </div>
  <div class="interview-card">
    <img src="./JavaScriptInterviews/JSInterview.jpg" alt="PWA" class="interview-image">
    <div class="interview-content">
      <h4><a href="./JavaScriptInterviews/18-pwa/">18. PWA</a></h4>
      <p>Service Workers et fonctionnalités offline</p>
      <div class="article-links">
        <a href="./JavaScriptInterviews/18-pwa/01-progressive-web-apps">Applications Web Progressives</a>
      </div>
    </div>
  </div>
</div>

## 📚 Fondamentaux

Les concepts fondamentaux de JavaScript et TypeScript.

<div class="article-grid">

<div class="article-card">
  <div class="article-image">
    <img src="./steampunk.jpg" alt="TypeScript" style="width: 100%; height: 120px; object-fit: cover;">
  </div>
  <div class="article-content">
    <h3><a href="./articleGestionErreurTypeScript">Gestion des Erreurs en TypeScript</a></h3>
    <p>Techniques avancées de gestion des erreurs et typage en TypeScript.</p>
  </div>
</div>

<div class="article-card">
  <div class="article-image">
    <img src="./fondamentaux.svg" alt="Introduction JavaScript" style="width: 100%; height: 120px; object-fit: contain;">
  </div>
  <div class="article-content">
    <h3><a href="./introductionJavaScript">Introduction à JavaScript</a></h3>
    <p>Les bases essentielles de JavaScript moderne et les bonnes pratiques de développement.</p>
  </div>
</div>

<div class="article-card">
  <div class="article-image">
    <img src="./fondamentaux.svg" alt="JavaScript Moderne" style="width: 100%; height: 120px; object-fit: contain;">
  </div>
  <div class="article-content">
    <h3><a href="./introJavaScript">JavaScript Moderne</a></h3>
    <p>Guide approfondi des concepts modernes et des fonctionnalités avancées de JavaScript.</p>
  </div>
</div>

</div>

## 🎯 TypeScript Avancé

Exploration approfondie des concepts avancés de TypeScript.

<div class="article-grid">

<div class="article-card">
  <div class="article-image">
    <img src="./typescript/img/typescript-types.jpg" alt="Types TypeScript" style="width: 100%; height: 120px; object-fit: cover;">
  </div>
  <div class="article-content">
    <h3><a href="./typescript/types">Les Types en TypeScript</a></h3>
    <p>Guide complet sur le système de types en TypeScript et les bonnes pratiques.</p>
  </div>
</div>

<div class="article-card">
  <div class="article-image">
    <img src="./typescript/img/typescript.jpg" alt="Mapped Types" style="width: 100%; height: 120px; object-fit: cover;">
  </div>
  <div class="article-content">
    <h3><a href="./typescript/mappedTypes">Types Mappés et Système de Permissions</a></h3>
    <p>Implémentation avancée d'un système de permissions avec les types mappés.</p>
  </div>
</div>

</div>

## ⚙️ Configuration & Bonnes Pratiques

Guides et configurations pour des projets robustes.

<div class="article-grid">

<div class="article-card">
  <div class="article-image">
    <img src="./img/tsconfigs.png" alt="TypeScript Config" style="width: 100%; height: 120px; object-fit: cover;">
  </div>
  <div class="article-content">
    <h3><a href="./tsconfig">Guide Progressif de Configuration TypeScript</a></h3>
    <p>Maîtrisez votre tsconfig.json pour des projets TypeScript optimaux.</p>
  </div>
</div>

</div>

## 🔄 Itérateurs et Générateurs

Exploration des fonctionnalités avancées d'itération en JavaScript.

<div class="article-grid">

<div class="article-card">
  <div class="article-image">
    <img src="./symboliterator.jpg" alt="Symbol.iterator" style="width: 100%; height: 120px; object-fit: cover;">
  </div>
  <div class="article-content">
    <h3><a href="./symbolIterator">Symbol.iterator et Itérables</a></h3>
    <p>Comprendre et implémenter le protocole d'itération en JavaScript.</p>
  </div>
</div>

<div class="article-card">
  <div class="article-image">
    <img src="./yield.png" alt="Yield" style="width: 100%; height: 120px; object-fit: cover;">
  </div>
  <div class="article-content">
    <h3><a href="./yield">Générateurs et Yield</a></h3>
    <p>Maîtriser les générateurs et l'instruction yield pour un code asynchrone élégant.</p>
  </div>
</div>

</div>

<style>
.article-grid {
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
    gap: 2rem;
    margin: 2rem 0;
}

.article-card {
    border: 1px solid var(--card-border-color, #e2e8f0);
    border-radius: 12px;
    overflow: hidden;
    transition: all 0.3s cubic-bezier(0.4, 0, 0.2, 1);
    background: var(--card-bg-color, white);
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.1);
}

.article-card:hover {
    transform: translateY(-4px);
    box-shadow: 0 8px 24px rgba(0, 0, 0, 0.12);
}

.article-image {
    width: 100%;
    height: 120px;
    overflow: hidden;
    background: #f8f9fa;
    display: flex;
    align-items: center;
    justify-content: center;
}

.article-content {
    padding: 1rem;
}

.article-content h3 {
    margin: 0 0 0.5rem 0;
    font-size: 1.2rem;
}

.article-content p {
    margin: 0;
    color: #4a5568;
    font-size: 0.95rem;
    line-height: 1.5;
}

.article-content a {
    color: #2b6cb0;
    text-decoration: none;
}

.article-content a:hover {
    text-decoration: underline;
}

.interview-grid {
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(280px, 1fr));
    gap: 1.5rem;
    margin: 1.5rem 0;
}

.interview-card {
    display: flex;
    align-items: flex-start;
    background: var(--card-bg-color, white);
    border-radius: 12px;
    padding: 1rem;
    transition: all 0.3s cubic-bezier(0.4, 0, 0.2, 1);
    border: 1px solid var(--card-border-color, #e2e8f0);
    min-height: 70px;
}

.interview-card:hover {
    transform: translateY(-4px);
    box-shadow: 0 8px 24px rgba(0, 0, 0, 0.12);
}

.interview-image {
    width: 70px;
    height: 70px;
    object-fit: cover;
    border-radius: 8px;
    margin-right: 1rem;
    box-shadow: 0 2px 8px rgba(0, 0, 0, 0.08);
}

.interview-content {
    flex: 1;
}

.interview-content h4 {
    margin: 0 0 0.5rem 0;
    font-size: 1.1rem;
    font-weight: 600;
    line-height: 1.3;
    color: var(--heading-color, #1a202c);
}

.interview-content p {
    margin: 0;
    font-size: 0.9rem;
    color: var(--text-color, #4a5568);
    line-height: 1.5;
}

.article-links {
    margin-top: 1rem;
    padding-top: 1rem;
    border-top: 1px solid var(--border-color, #edf2f7);
    font-size: 0.9rem;
    display: flex;
    flex-direction: column;
    gap: 0.75rem;
}

.article-links a {
    color: var(--link-color, #4a5568);
    text-decoration: none;
    transition: all 0.3s ease;
    display: inline-flex;
    align-items: center;
    padding: 0.5rem 0.75rem;
    border-radius: 6px;
    background: var(--link-bg, rgba(43, 108, 176, 0.05));
    position: relative;
    overflow: hidden;
}

.article-links a::before {
    content: "→";
    display: inline-flex;
    align-items: center;
    justify-content: center;
    margin-right: 0.75rem;
    color: var(--accent-color, #2b6cb0);
    font-weight: bold;
    transition: transform 0.3s ease;
}

.article-links a:hover {
    color: var(--link-hover-color, #2b6cb0);
    background: var(--link-hover-bg, rgba(43, 108, 176, 0.1));
    transform: translateX(4px);
}

.article-links a:hover::before {
    transform: translateX(2px);
}

.article-links a:active {
    transform: translateX(4px) scale(0.98);
}

.article-links a::after {
    content: "";
    position: absolute;
    inset: 0;
    background: linear-gradient(
        to right,
        transparent,
        rgba(255, 255, 255, 0.1),
        transparent
    );
    transform: translateX(-100%);
    transition: transform 0.6s ease;
}

.article-links a:hover::after {
    transform: translateX(100%);
}

@media (max-width: 768px) {
    .interview-grid {
        grid-template-columns: 1fr;
    }
    
    .interview-card {
        min-height: auto;
    }
    
    .interview-image {
        width: 60px;
        height: 60px;
    }
}

:root {
    --card-border-color: #e2e8f0;
    --card-bg-color: white;
    --heading-color: #1a202c;
    --text-color: #4a5568;
    --link-color: #4a5568;
    --link-hover-color: #2b6cb0;
    --accent-color: #2b6cb0;
    --border-color: #edf2f7;
    --link-bg: rgba(43, 108, 176, 0.05);
    --link-hover-bg: rgba(43, 108, 176, 0.1);
}

[data-theme="dark"] {
    --card-border-color: #2d3748;
    --card-bg-color: #1a202c;
    --heading-color: #f7fafc;
    --text-color: #cbd5e0;
    --link-color: #cbd5e0;
    --link-hover-color: #63b3ed;
    --accent-color: #63b3ed;
    --border-color: #2d3748;
    --link-bg: rgba(99, 179, 237, 0.05);
    --link-hover-bg: rgba(99, 179, 237, 0.1);
}

/* Ajout d'un style focus pour l'accessibilité */
.article-links a:focus-visible {
    outline: 2px solid var(--accent-color, #2b6cb0);
    outline-offset: 2px;
}

/* Animation subtile au chargement */
@keyframes fadeIn {
    from {
        opacity: 0;
        transform: translateY(5px);
    }
    to {
        opacity: 1;
        transform: translateY(0);
    }
}

.article-links a {
    animation: fadeIn 0.3s ease backwards;
    animation-delay: calc(var(--link-index, 0) * 0.1s);
}
</style>
