---
title: 📡 Intégration de RxJS dans une Application Vue.js - Retour d'Expérience sur un Système de Gestion Hydraulique 🌊
date: 2024-10-29
icon: code
sticky: 1
star: 1
category:
    - code
    - JavaScript
tags:
    - RxJS
    - Vue.js
    - TypeScript
    - JavaScript
    - Clean Architecture
description: Un guide complet sur l'intégration de RxJS avec Vue.js à travers un cas d'étude concret d'un système de gestion hydraulique.
image: https://raw.githubusercontent.com/giak/dam-dashboard/refs/heads/main/docs/assets/rxjs.jpg
showPageImage: true
---
<div class="article-content">

![](https://raw.githubusercontent.com/giak/dam-dashboard/refs/heads/main/docs/assets/rxjs.jpg)

# 📡 Intégration de RxJS dans une Application Vue.js : Retour d'Expérience sur un Système de Gestion Hydraulique 🌊

## Introduction

La gestion des flux de données en temps réel dans les applications web modernes présente des défis complexes, particulièrement lorsqu'il s'agit de systèmes critiques comme la gestion hydraulique.
Cet article présente un cas d'étude simplifié, basé sur une expérience dans le développement d'un système de contrôle de barrage hydroélectrique et son écosystème, pour illustrer l'intégration de RxJS avec Vue.js.

### Contexte

Notre application de démonstration, bien que volontairement simplifiée par rapport au système de production, reproduit les problématiques essentielles de la gestion de données en temps réel (démonstrateur RxJS, réactivité Vue.js 3) :

-   🏗️ Synchronisation de multiples sources de données (barrages, glaciers, stations météo)
-   🔄 Traitement et agrégation de flux continus d'informations
-   🎯 Maintien de la cohérence de l'état global du système
-   🖥️ Gestion réactive de l'interface utilisateur

### Objectifs Pédagogiques

À travers cet exemple concret, nous souhaitons :

1. 📚 Démontrer l'utilité de la programmation réactive dans un contexte métier
2. 🔗 Illustrer les patterns d'intégration entre RxJS et Vue.js
3. 💡 Partager des solutions pratiques à des problèmes courants
4. 🏛️ Présenter les compromis et les choix architecturaux
5. ✨ Les principes SOLID et la clean architecture sont très important
6. 🧪 Les test unitaires sont un calvaire dans un contexte temps réel mais indispensables

### Portée de l'Article

Cette démonstration se concentre sur les aspects fondamentaux de l'intégration RxJS/Vue.js.
Bien que notre système de production gère des problématiques plus complexes (distribution d'eau potable, irrigation, production hydroélectrique), nous avons choisi de nous concentrer sur un sous-ensemble simplifié pour maintenir la clarté pédagogique.

## 1. Contexte Technique

### 1.1 Stack Technique

Notre application de gestion des systèmes hydrauliques s'appuie sur un stack technique moderne et robuste, soigneusement sélectionné pour répondre aux exigences de performance et de maintenabilité :

#### Frontend

-   **TypeScript 5.5** 📘
    -   Typage fort pour une meilleure fiabilité du code
    -   Utilisation des dernières fonctionnalités (decorators, template literal types)
    -   Configuration stricte pour maximiser la sécurité du typage
-   **Vue.js 3.4** 💚
    -   Composition API pour une meilleure réutilisation du code
    -   Script setup pour une syntaxe plus concise
    -   Intégration native avec TypeScript
-   **RxJS 7.8** ⚡
    -   Gestion avancée des flux de données réactifs
    -   Opérateurs optimisés pour les performances
    -   Support complet de TypeScript

#### Styling et UI

-   **TailwindCSS 3.4** et **PrimeVue** 🎨
    -   Approche utility-first pour un développement rapide
    -   Configuration personnalisée pour notre système de design
    -   Optimisation automatique du bundle CSS

#### Outils de Développement

-   **Vite 5.0** ⚡
    -   Build ultra-rapide en développement
    -   Configuration optimisée pour Vue.js
    -   Support des modules ES natifs
-   **ESLint & Prettier** 🧹
    -   Configuration stricte pour maintenir la qualité du code
    -   Règles personnalisées pour RxJS
    -   Intégration avec le CI/CD

#### Tests

-   **Vitest** ✅
    -   Compatible avec l'écosystème Vite
    -   Support natif de TypeScript
    -   API compatible Jest

#### Choix Technologiques et Limitations : ce que nous n'allons pas aborder ici

1. **Choix Architecturaux** 🏗️

    - Solution Vue.js native privilégiée pour des raisons de performance
    - Frameworks plus lourds (Angular, Next.js) délibérément écartés
    - React non retenu pour maintenir une empreinte légère (JSX moche, Hooks verbeux et plus complexe, test de montée en charge lourd)

2. **Périmètre Backend** 🔧

    - L'application originale s'appuie sur une architecture backend la plus simple possible :
        - Node.js/Fastify pour le serveur
        - MongoDB pour la persistance
        - Server-Sent Events pour les flux temps réel
        - Socket.IO pour la communication bidirectionnelle
    - Ces aspects ne sont pas couverts dans cet article qui se concentre sur le frontend

3. **Gestion d'État** 📦

    - Pinia est utilisé de manière ciblée :
        - Persistance des configurations des tableaux de bord
        - État global de l'application
        - Préférences utilisateur
    - Complémentaire à RxJS qui gère les flux de données temps réel

4. **Aspects Non Couverts** 🔒
    - La sécurité des échanges de données n'est pas abordée :
        - Protocoles sécurisés (HTTPS)
        - Politiques CORS
        - Authentification (JWT)
        - Protections XSS et CSRF
        - Architecture DMZ
    - Ces aspects critiques méritent un traitement dédié dans un article séparé
5. **CI/CD avec GitLab Auto-hébergé** 🔄
    - Infrastructure GitLab déployée localement sous Docker
6. **Développement sous Docker** 🐳
    - Conteneur Vue.js/RxJS avec hot-reload pour le développement, build multi-stage pour la production via Nginx

### 1.2 Contraintes et Exigences 📋

Notre système de gestion hydraulique (démonstrateur) doit répondre à des exigences strictes en termes de performance et de fiabilité.
La sécurité n'est pas abordé (il faudrait écrire un livre pour tout expliquer).

#### Sources de Données en Temps Réel 📊

Le système doit gérer simultanément plusieurs types de données :

##### Données Météorologiques 🌤️

-   Température (mise à jour toutes les 5 secondes)
-   Précipitations (mise à jour en temps réel)
-   Vitesse du vent (échantillonnage toutes les 30 secondes)
-   Radiation solaire (mesures quotidiennes)

##### Données Hydrauliques 💧

-   Niveaux d'eau des barrages (monitoring continu)
-   Débits des rivières (mise à jour toutes les minutes)
-   État des glaciers (mesures horaires)
-   Prévisions de fonte (calculs quotidiens)

#### Contraintes Techniques ⚙️

##### Performance ⚡

-   Latence maximale de 100ms pour les mises à jour critiques
-   Capacité de traitement de 1000 événements par seconde
-   Utilisation mémoire optimisée < 50MB
-   Support de 100+ stations météo simultanées

##### Fiabilité 🛡️

-   Disponibilité système 99.99%
-   Tolérance aux pannes réseau
-   Reprise automatique après erreur
-   Conservation des données en cas de déconnexion

##### Sécurité 🔒

-   Validation stricte des données entrantes
-   Protection contre les débordements de buffer
-   Gestion sécurisée des erreurs
-   Audit trail complet des opérations

#### Exigences Fonctionnelles 📱

##### Monitoring 📺

-   Visualisation en temps réel des données
-   Alertes sur dépassement de seuils
-   Historique des mesures
-   Tableaux de bord personnalisables

##### Analyse 📈

-   Calculs statistiques en temps réel
-   Détection d'anomalies
-   Prédictions à court terme
-   Rapports automatisés

Ces contraintes et exigences ont directement influencé nos choix architecturaux et l'utilisation intensive de RxJS pour la gestion des flux de données.

### 1.3 Architecture du Système 🏗️

Notre application suit une architecture Clean Architecture, optimisée pour les flux de données réactifs avec RxJS.

#### Vue d'Ensemble 🔍

L'architecture est organisée en couches distinctes, chacune ayant une responsabilité spécifique :

![](https://raw.githubusercontent.com/giak/dam-dashboard/264ae70b68b94cb0d21da229f61afb66eff3404e/docs/assets/SOLID_meteo.svg)

##### Couche Présentation (UI) 🎨

-   **Composants Vue.js**

    -   `WaterSystemDashboard.vue` : Dashboard principal
    -   `MainWeatherStationComponent.vue` : Affichage station météo
    -   `DamComponent.vue` : Gestion des barrages
    -   `GlacierComponent.vue` : Données des glaciers
    -   `RiverComponent.vue` : État des rivières
    -   `ErrorNotification.vue` : Notifications d'erreurs

-   **Composants Communs**
    -   `StatItem.vue` : Affichage des statistiques
    -   `TrendIndicator.vue` : Indicateurs de tendance
    -   `WaterLevelChart.vue` : Graphiques de niveau d'eau

##### Couche Application (Use Cases) ⚡

-   **Composables**
    -   `useWaterSystem` : Orchestration du système
    -   `useMainWeatherStation` : Gestion station principale
    -   `useDam` : Logique des barrages
    -   `useGlacier` : Gestion des glaciers
    -   `useRiver` : Gestion des rivières
-   **Services**
    -   `ErrorHandlingService` : Gestion centralisée des erreurs
    -   `LoggingService` : Journalisation système

##### Couche Domaine (Business Logic) 🧮

-   **Interfaces**

    -   `WeatherStationInterface` : Contrat station météo
    -   `DamInterface` : Structure des barrages
    -   `GlacierInterface` : Modèle des glaciers
    -   `RiverInterface` : Définition des rivières
    -   `ErrorDataInterface` : Format des erreurs

-   **Types**
    -   `WeatherDataInterface` : Données météorologiques
    -   `SystemStateInterface` : État global du système

##### Couche Infrastructure (Data Sources) 💾

-   **Simulations**

    -   `weatherSimulation.ts` : Données météo simulées
    -   `damSimulation.ts` : Simulation des barrages
    -   `glacierSimulation.ts` : Simulation des glaciers
    -   `riverSimulation.ts` : Simulation des rivières

-   **Services Techniques**
    -   `loggingService.ts` : Service de journalisation
    -   `metricsService.ts` : Collecte de métriques

Cette architecture assure :

-   Une séparation claire des responsabilités ✅
-   Une maintenance facilitée 🔧
-   Une testabilité optimale 🧪
-   Une évolutivité du système 📈

#### Flux de Données 🔄

Les données circulent de manière unidirectionnelle :

![](https://raw.githubusercontent.com/giak/dam-dashboard/264ae70b68b94cb0d21da229f61afb66eff3404e/docs/assets/architecture_onion_flux.svg)

1. **Sources de Données** → **Services** 📊

    - Données brutes des capteurs
    - Simulations météorologiques
    - États des systèmes

2. **Services** → **Use Cases** ⚙️

    - Agrégation des données
    - Validation et transformation
    - Application des règles métier

3. **Use Cases** → **UI** 🖥️
    - Présentation des données
    - Mise à jour réactive
    - Gestion des erreurs

Cette architecture assure :

-   Une séparation claire des responsabilités
-   Une testabilité optimale
-   Une maintenance facilitée
-   Une évolutivité du système

### 2. Fondamentaux de RxJS 🔧

RxJS joue un rôle central dans notre application de gestion hydraulique. Il gère les flux de données complexes provenant de multiples sources (stations météo, barrages, glaciers) et assure leur synchronisation avec l'interface utilisateur Vue.js.

#### 2.1 Observables et Subjects 🎯

![](https://raw.githubusercontent.com/giak/dam-dashboard/264ae70b68b94cb0d21da229f61afb66eff3404e/docs/assets/observable_subject.svg)

##### Observable : Le Bloc de Base 🧱

###### Concept

Un Observable est un flux de données qui peut émettre des valeurs dans le temps. C'est le fondement de la programmation réactive.

###### Utilisation dans Notre Application 💡

1. **Flux de Données Simples**

```typescript
// Dans weatherSimulation.ts
const weatherData$ = weatherData.pipe(
    map((data) => ({
        ...data,
        temperature: normalizeTemperature(data.temperature),
    })),
);
```

```mermaid
graph LR
    %% Styles
    classDef source fill:#e3f2fd,stroke:#42a5f5,stroke-width:2px
    classDef operator fill:#e8f5e9,stroke:#66bb6a,stroke-width:2px
    classDef result fill:#fff3e0,stroke:#ffa726,stroke-width:2px

    %% Nodes
    weatherData[/"weatherData"/]:::source
    mapOp["map()"]:::operator
    weatherData$[/"weatherData$"/]:::result

    %% Flow
    weatherData -->|"data"| mapOp
    mapOp -->|"{...data, temperature: normalizeTemperature(data.temperature)}"| weatherData$
```

```mermaid
sequenceDiagram
    participant Source as weatherData
    participant Map as map()
    participant Result as weatherData$

    Note over Source,Result: Time →

    Source->>Map: {temp: 25}
    Map->>Result: {temp: 77}

    Source->>Map: {temp: 30}
    Map->>Result: {temp: 86}

    Source->>Map: {temp: 20}
    Map->>Result: {temp: 68}

    Note over Map: normalizeTemperature()
```

**Explication**

Le diagramme montre exactement ce que fait le code :

1. `weatherData` est la source
2. L'opérateur `map()` transforme chaque émission
3. Pour chaque donnée, il :
    - Conserve toutes les propriétés existantes (`...data`)
    - Normalise la température (`normalizeTemperature(data.temperature)`)
4. Le résultat est émis dans `weatherData$`

5. **Flux Composés**

```typescript
// Dans useDam.ts
const damState$ = combineLatest([initialState$, aggregatedInflow$]).pipe(
    map(([state, inflow]) => ({
        ...state,
        inflowRate: inflow.totalInflow,
        lastUpdated: new Date(),
    })),
);
```

```mermaid
graph LR
    %% Styles
    classDef source fill:#e3f2fd,stroke:#42a5f5,stroke-width:2px
    classDef operator fill:#e8f5e9,stroke:#66bb6a,stroke-width:2px
    classDef result fill:#fff3e0,stroke:#ffa726,stroke-width:2px

    %% Sources
    damState[/"damState$"/]:::source
    glacierState[/"glacierState$"/]:::source
    riverState[/"riverState$"/]:::source
    weatherState[/"weatherState$"/]:::source

    %% Operators
    combine["combineLatest()"]:::operator
    mapCalc["map(calculateTotalVolume)"]:::operator

    %% Result
    systemState[/"systemState$"/]:::result

    %% Flow
    damState --> combine
    glacierState --> combine
    riverState --> combine
    weatherState --> combine
    combine --> mapCalc
    mapCalc -->|"{...states, totalWaterVolume}"| systemState
```

```mermaid
sequenceDiagram
    participant Initial as "initialState$"
    participant Inflow as "aggregatedInflow$"
    participant Combine as "combineLatest()"
    participant Map as "map()"
    participant Result as "damState$"

    Note over Initial,Result: Time →

    Initial->>Combine: "{level: 50}"
    Inflow->>Combine: "{totalInflow: 100}"
    Combine->>Map: "[{level: 50}, {totalInflow: 100}]"
    Map->>Result: "{level: 50, inflowRate: 100}"

    Initial->>Combine: "{level: 55}"
    Combine->>Map: "[{level: 55}, {totalInflow: 100}]"
    Map->>Result: "{level: 55, inflowRate: 100}"

    Inflow->>Combine: "{totalInflow: 150}"
    Combine->>Map: "[{level: 55}, {totalInflow: 150}]"
    Map->>Result: "{level: 55, inflowRate: 150}"

    Note over Combine: Combine latest values
    Note over Map: Merge state with inflow

```

**Explication**

Le diagramme représente exactement le flux de données du code :

1. Quatre sources d'état distinctes sont combinées via `combineLatest()`
2. La combinaison produit un objet avec tous les états
3. L'opérateur `map()` :
    - Préserve tous les états combinés (`...states`)
    - Calcule le volume total (`calculateTotalVolume(states)`)
4. Le résultat final est émis dans `systemState$`

###### Avantages/Inconvénients ⚖️

✅ Avantages :

-   Flux de données unidirectionnel clair
-   Transformation des données chainable
-   Gestion élégante de l'asynchrone

❌ Inconvénients :

-   Courbe d'apprentissage initiale
-   Complexité potentielle des opérateurs
-   Besoin de gestion explicite des souscriptions

##### Subject : L'Observable Bidirectionnel 🔄

###### Concept

Un Subject est à la fois un Observable et un Observer, permettant l'émission et la réception de valeurs.

###### Utilisation dans Notre Application 🛠️

1. **Gestion du Cycle de Vie**

```typescript
// Pattern de cleanup
const destroy$ = new Subject<void>();

onUnmounted(() => {
    destroy$.next();
    destroy$.complete();
});
```

```mermaid
sequenceDiagram
    participant Component as "Vue Component"
    participant Subject as "destroy$"
    participant Subs as "Subscriptions"

    Note over Component,Subs: Component Lifecycle →

    Component->>Subs: "onMounted()"
    Note over Subs: Active subscriptions

    Note over Subs: Running...

    Component->>Subject: "onUnmounted()"
    Subject->>Subs: "next()"
    Note over Subs: Trigger cleanup

    Subject->>Subs: "complete()"
    Note over Subs: All subscriptions terminated

    Note over Component: Component destroyed

    Note over Subject: Subject completed
    Note over Subs: Resources freed

```

2. **Communication Entre Composants**

```typescript
// Dans ErrorHandlingService
private errorSubject = new Subject<ErrorDataInterface>();

public emitError(error: ErrorDataInterface): void {
  this.errorSubject.next(error);
}
```

```mermaid
sequenceDiagram
    participant Client as "Client Code"
    participant Service as "ErrorHandlingService"
    participant Subject as "errorSubject"
    participant Observers as "Subscribers"

    Note over Client,Observers: Time →

    Note over Subject: Subject<ErrorDataInterface> created

    Observers->>Subject: subscribe()
    Note over Subject: First subscriber

    Client->>Service: emitError({message: "Error 1"})
    Service->>Subject: next({message: "Error 1"})
    Subject->>Observers: {message: "Error 1"}

    Observers->>Subject: subscribe()
    Note over Subject: Second subscriber

    Client->>Service: emitError({message: "Error 2"})
    Service->>Subject: next({message: "Error 2"})
    Subject-->>Observers: {message: "Error 2"}
    Note over Observers: Both subscribers receive error

    Note over Subject: No value stored<br>(unlike BehaviorSubject)
```

Ce diagramme montre :

1. La création du Subject dans le service
2. Les souscriptions des observateurs
3. L'émission d'erreurs via `emitError()`
4. La distribution des erreurs aux abonnés
5. Le fait qu'un Subject ne conserve pas de valeur (contrairement à un BehaviorSubject)

###### Avantages/Inconvénients ⚖️

✅ Avantages :

-   Communication bidirectionnelle
-   Multicast natif
-   Flexibilité d'utilisation

❌ Inconvénients :

-   Pas de valeur initiale
-   Risque de perte d'émissions
-   Complexité de gestion accrue

##### BehaviorSubject : L'Observable avec État 🔄

###### Concept

Un BehaviorSubject est un type spécial de Subject qui :

-   📦 Maintient une valeur courante
-   🔄 Émet cette valeur aux nouveaux abonnés
-   ⚡ Requiert une valeur initiale

###### Utilisation dans Notre Application

1. **Gestion de l'État des Stations Météo**

```typescript
// Dans useMainWeatherStation.ts
const mainWeatherState$ = new BehaviorSubject<MainWeatherStationInterface>({
    id,
    name,
    subStations,
    averageTemperature: 0,
    totalPrecipitation: 0,
    lastUpdate: new Date(),
});
```

```mermaid
sequenceDiagram
    participant Init as "Initial Value"
    participant BS as "mainWeatherState$"
    participant Sub1 as "Subscriber 1"
    participant Sub2 as "Subscriber 2"

    Note over Init,Sub2: Time →

    Init->>BS: "{id, name, temp: 0, precip: 0}"
    Note over BS: BehaviorSubject created

    Sub1->>BS: "subscribe()"
    BS->>Sub1: "{id, name, temp: 0, precip: 0}"

    BS-->>BS: "next({...state, temp: 22})"
    BS->>Sub1: "{id, name, temp: 22, precip: 0}"

    Sub2->>BS: "subscribe()"
    BS->>Sub2: "{id, name, temp: 22, precip: 0}"

    BS-->>BS: "next({...state, precip: 5})"
    BS->>Sub1: "{id, name, temp: 22, precip: 5}"
    BS->>Sub2: "{id, name, temp: 22, precip: 5}"

    Note over BS: Maintains latest value
    Note over Sub1,Sub2: All subscribers receive updates
```

Ce diagramme montre :

1. L'initialisation du BehaviorSubject avec une valeur initiale
2. Les nouveaux abonnés reçoivent immédiatement la dernière valeur
3. Tous les abonnés reçoivent les mises à jour suivantes
4. Le BehaviorSubject maintient toujours l'état courant

5. **Gestion des Flux d'Eau**

```typescript
// Dans useDam.ts
const aggregatedInflow$ = new BehaviorSubject<AggregatedInflowInterface>({
    totalInflow: 100,
    sources: { TestSource: 100 },
});
```

```mermaid
sequenceDiagram
    participant Subject as "aggregatedInflow$"
    participant Sub1 as "Subscriber 1"
    participant Sub2 as "Subscriber 2"

    Note over Subject,Sub2: Time →

    Note over Subject: Initial value:<br>{totalInflow: 100, sources: {'TestSource': 100}}

    Subject->>Sub1: Initial value on subscribe

    Subject->>Sub1: "{totalInflow: 100, sources: {'TestSource': 100}}"

    Note right of Subject: New subscriber joins
    Subject->>Sub2: Receives last value immediately

    Note over Subject: next() called with new value
    Subject->>Sub1: "{totalInflow: 150, sources: {'TestSource': 150}}"
    Subject->>Sub2: "{totalInflow: 150, sources: {'TestSource': 150}}"

    Note over Subject: Complete
    Subject-->>Sub1: Complete
    Subject-->>Sub2: Complete

    Note over Subject,Sub2: BehaviorSubject characteristics:
    Note over Subject,Sub2: 1. Maintains current value
    Note over Subject,Sub2: 2. Emits to new subscribers
    Note over Subject,Sub2: 3. Multicasts to all subscribers
```

Ce diagramme montre les caractéristiques clés d'un BehaviorSubject :

1. Il est initialisé avec une valeur
2. Les nouveaux abonnés reçoivent immédiatement la dernière valeur
3. Tous les abonnés reçoivent les nouvelles valeurs
4. Le flux peut être complété

###### Avantages/Inconvénients

✅ Avantages :

-   🎯 État initial garanti
-   ⚡ Synchronisation immédiate
-   🔍 Accès synchrone à la valeur courante

❌ Inconvénients :

-   💾 Consommation mémoire constante
-   ⚠️ Nécessite une valeur initiale
-   🔧 Complexité de gestion d'état

##### ReplaySubject : L'Observable avec Historique 📚

###### Concept

🔄 Un ReplaySubject mémorise un nombre spécifié de valeurs émises et les rejoue aux nouveaux abonnés.

###### Utilisation dans Notre Application

1. **Simulation Météorologique**

```typescript
// Dans weatherSimulation.ts
const weatherData = new ReplaySubject<WeatherDataInterface>(1);

const weatherData$ = weatherData.pipe(
    observeOn(asyncScheduler),
    bufferTime(100, undefined, 5),
    filter((updates): updates is WeatherDataInterface[] => updates.length > 0),
    map((updates) => updates[updates.length - 1]),
);
```

```mermaid
sequenceDiagram
    participant Source as "weatherData (ReplaySubject)"
    participant Async as "observeOn(asyncScheduler)"
    participant Buffer as "bufferTime(100ms, 5)"
    participant Filter as "filter(length > 0)"
    participant Map as "map(last)"
    participant Result as "weatherData$"

    Note over Source,Result: Time →

    Source->>Async: "{temp: 20}"
    Source->>Async: "{temp: 21}"
    Source->>Async: "{temp: 22}"
    Note over Buffer: Buffer collecting for 100ms
    Async->>Buffer: ["{temp: 20}", "{temp: 21}", "{temp: 22}"]
    Buffer->>Filter: Array of 3 updates
    Filter->>Map: Array passed (length > 0)
    Map->>Result: "{temp: 22}"

    Source->>Async: "{temp: 23}"
    Note over Buffer: New 100ms window
    Async->>Buffer: ["{temp: 23}"]
    Buffer->>Filter: Array of 1 update
    Filter->>Map: Array passed (length > 0)
    Map->>Result: "{temp: 23}"

    Note over Buffer: Empty buffer not emitted
    Buffer->>Filter: []
    Filter-->>Map: Filtered out (length = 0)

    Note over Source: ReplaySubject(1) stores last value
    Note over Buffer: Groups updates within 100ms
    Note over Map: Takes last value from buffer
```

Ce diagramme montre :

1. Le ReplaySubject source qui stocke la dernière valeur
2. L'exécution asynchrone avec `observeOn`
3. Le regroupement des mises à jour par `bufferTime` sur 100ms
4. Le filtrage des groupes vides
5. La sélection de la dernière valeur de chaque groupe

Les points clés illustrés :

-   Buffering temporel des événements
-   Filtrage des buffers vides
-   Sélection de la valeur la plus récente
-   Exécution asynchrone du pipeline

2. **Historique des Mesures**

```typescript
// Dans useWeatherStation.ts
const measurements = new ReplaySubject<Measurement>(10);

// Stockage des 10 dernières mesures
measurements.pipe(scan((acc, value) => [...acc, value].slice(-10), [] as Measurement[]));
```

```mermaid
sequenceDiagram
    participant Source as "measurements"
    participant Scan as "scan()"
    participant Result as "measurements$"

    Note over Source,Result: Time →
    Note over Source: ReplaySubject(10)

    Source->>Scan: "M1"
    Scan->>Result: "[M1]"

    Source->>Scan: "M2"
    Scan->>Result: "[M1, M2]"

    Source->>Scan: "M3"
    Scan->>Result: "[M1, M2, M3]"

    Note over Source: ... after 10 measurements ...

    Source->>Scan: "M11"
    Scan->>Result: "[M2, M3, ..., M11]"
    Note over Result: Only last 10 values kept

    Source->>Scan: "M12"
    Scan->>Result: "[M3, M4, ..., M12]"
    Note over Result: M1 dropped

    Note over Scan: scan((acc, value) => [...acc, value].slice(-10))
```

Ce diagramme illustre :

1. Le ReplaySubject comme source avec une capacité de 10 mesures
2. L'opérateur scan qui accumule les valeurs
3. Le mécanisme de sliding window qui :
    - Ajoute chaque nouvelle mesure
    - Garde uniquement les 10 dernières valeurs
    - Supprime automatiquement les plus anciennes
4. Le flux résultant qui maintient toujours les 10 mesures les plus récentes

###### Avantages/Inconvénients

✅ Avantages :

-   📚 Historique configurable
-   ⏱️ Parfait pour les données temporelles
-   🔧 Flexibilité de la taille du buffer

❌ Inconvénients :

-   💾 Consommation mémoire variable
-   ⚠️ Risque de surcharge mémoire
-   🔧 Complexité de configuration

#### 2.3 Patterns d'Utilisation Avancés 🛠️

###### 1. Combinaison d'Observables

```typescript
// Dans useWaterSystem.ts
const systemState$ = combineLatest({
    dam: damState$,
    glacier: glacierState$,
    river: riverState$,
    weather: weatherState$,
}).pipe(
    map((states) => ({
        ...states,
        totalWaterVolume: calculateTotalVolume(states),
    })),
);
```

```mermaid
sequenceDiagram
    participant Dam as "damState$"
    participant Glacier as "glacierState$"
    participant River as "riverState$"
    participant Weather as "weatherState$"
    participant Combine as "combineLatest()"
    participant Map as "map()"
    participant Result as "systemState$"

    Note over Dam,Result: Time →

    Dam->>Combine: "{level: 75}"
    Glacier->>Combine: "{volume: 1000}"
    River->>Combine: "{flow: 50}"
    Weather->>Combine: "{temp: 20}"

    Combine->>Map: "{ dam: {level: 75},<br/>glacier: {volume: 1000},<br/>river: {flow: 50},<br/>weather: {temp: 20} }"
    Map->>Result: "{ ...states, totalWaterVolume: 1125 }"

    Dam->>Combine: "{level: 80}"
    Combine->>Map: "{ dam: {level: 80},<br/>glacier: {volume: 1000},<br/>river: {flow: 50},<br/>weather: {temp: 20} }"
    Map->>Result: "{ ...states, totalWaterVolume: 1130 }"

    River->>Combine: "{flow: 60}"
    Combine->>Map: "{ dam: {level: 80},<br/>glacier: {volume: 1000},<br/>river: {flow: 60},<br/>weather: {temp: 20} }"
    Map->>Result: "{ ...states, totalWaterVolume: 1140 }"

    Note over Combine: Combines latest values from all sources
    Note over Map: Calculates total water volume
```

Ce diagramme montre :

1. Les 4 sources de données indépendantes (dam, glacier, river, weather)
2. La combinaison de leurs dernières valeurs via `combineLatest()`
3. Le calcul du volume total d'eau via `map()`
4. L'émission du nouvel état système complet

Chaque fois qu'une source émet une nouvelle valeur, `combineLatest()` combine cette valeur avec les dernières valeurs connues des autres sources, puis `map()` calcule le volume total d'eau à partir de toutes ces données.

###### 2. Transformation et Filtrage

```typescript
const processedWeatherData$ = weatherData$.pipe(
    debounceTime(100),
    filter(isValidWeatherData),
    distinctUntilChanged(),
    share(),
);
```

```mermaid
sequenceDiagram
    participant Source as "weatherData$"
    participant Debounce as "debounceTime(100)"
    participant Filter as "filter()"
    participant Distinct as "distinctUntilChanged()"
    participant Share as "share()"
    participant Result as "processedWeatherData$"

    Note over Source,Result: Time →

    Source->>Debounce: "{temp: 20}"
    Note over Debounce: Wait 100ms

    Source->>Debounce: "{temp: 20.1}"
    Note over Debounce: Ignored (< 100ms)

    Source->>Debounce: "{temp: 20.2}"
    Note over Debounce: Ignored (< 100ms)

    Note over Debounce: 100ms elapsed
    Debounce->>Filter: "{temp: 20.2}"
    Filter->>Distinct: "{temp: 20.2}"
    Distinct->>Share: "{temp: 20.2}"
    Share->>Result: "{temp: 20.2}"

    Source->>Debounce: "invalid data"
    Note over Debounce: Wait 100ms
    Debounce->>Filter: "invalid data"
    Note over Filter: Filtered out

    Source->>Debounce: "{temp: 20.2}"
    Note over Debounce: Wait 100ms
    Debounce->>Filter: "{temp: 20.2}"
    Filter->>Distinct: "{temp: 20.2}"
    Note over Distinct: Ignored (unchanged)

    Note over Debounce: Reduces frequency
    Note over Filter: Validates data
    Note over Distinct: Prevents duplicates
    Note over Share: Multicast to subscribers
```

Ce diagramme montre :

1. Le debouncing qui ignore les mises à jour trop fréquentes (< 100ms)
2. Le filtrage des données invalides
3. L'élimination des doublons avec distinctUntilChanged()
4. Le partage du flux avec share() pour plusieurs abonnés

###### Patterns de Gestion d'Erreur et Cleanup

![](https://raw.githubusercontent.com/giak/dam-dashboard/264ae70b68b94cb0d21da229f61afb66eff3404e/docs/assets/patterns.svg)

###### 1. Pattern de Retry avec Backoff Exponentiel

```typescript
// Dans weatherSimulation.ts
const weatherData$ = source$.pipe(
    retry({
        count: 3,
        delay: (error, retryCount) => {
            loggingService.error('Simulation error, retrying...', {
                error,
                retryCount,
            });
            // Délai exponentiel : 1s, 2s, 4s
            return timer(Math.min(1000 * Math.pow(2, retryCount), 10000));
        },
    }),
    catchError((error) => {
        loggingService.error('Simulation error', { error });
        return EMPTY;
    }),
);
```

```mermaid
sequenceDiagram
    participant Source as "source$"
    participant Retry as "retry()"
    participant Timer as "timer"
    participant CatchError as "catchError()"
    participant Result as "weatherData$"

    Note over Source,Result: Time →

    Source->>Retry: "Data1"
    Retry->>Result: "Data1"

    Source--xRetry: "Error!"
    Note over Retry: Retry #1
    Note over Timer: Wait 1s
    Timer-->>Retry: "Timeout 1s"

    Source--xRetry: "Error!"
    Note over Retry: Retry #2
    Note over Timer: Wait 2s
    Timer-->>Retry: "Timeout 2s"

    Source--xRetry: "Error!"
    Note over Retry: Retry #3
    Note over Timer: Wait 4s
    Timer-->>Retry: "Timeout 4s"

    Source--xRetry: "Error!"
    Note over Retry: Max retries reached
    Retry->>CatchError: "Final Error"
    CatchError->>Result: "EMPTY"

    Note over Retry: Exponential backoff
    Note over CatchError: Handle final failure
```

Ce diagramme montre :

1. Le flux initial de données avec `source$`
2. Une erreur survient et déclenche le retry
3. Chaque retry attend un délai exponentiel (1s, 2s, 4s)
4. Après 3 tentatives échouées, catchError retourne EMPTY
5. Les lignes pointillées (--x) représentent les erreurs
6. Les lignes pointillées (-->>) représentent les délais d'attente

Le diagramme illustre bien le pattern de retry avec backoff exponentiel et la gestion finale de l'erreur.

###### Pourquoi ce Pattern ?

-   🛡️ **Résilience** : Gestion automatique des erreurs transitoires
-   🔒 **Protection du Système** : Évite la surcharge en cas d'erreurs répétées
-   🎯 **UX Améliorée** : Tentatives de récupération transparentes

###### 2. Pattern de Cleanup Complet 🧹

```typescript
// Dans useWaterSystem.ts
const cleanup$ = new Subject<void>();

onMounted(() => {
    const subscriptions = new Subscription();

    subscriptions.add(
        systemState$.pipe(takeUntil(cleanup$)).subscribe({
            next: updateState,
            error: handleError,
            complete: () => loggingService.info('System state stream completed'),
        }),
    );

    onUnmounted(() => {
        cleanup$.next();
        cleanup$.complete();
        subscriptions.unsubscribe();
    });
});
```

```mermaid
sequenceDiagram
    participant System as "systemState$"
    participant Take as "takeUntil()"
    participant Cleanup as "cleanup$"
    participant Sub as "subscription"
    participant Life as "Component Lifecycle"

    Note over System,Life: Time →

    Life->>Sub: "onMounted()"

    System->>Take: "{state: 1}"
    Take->>Sub: "{state: 1}"
    Note over Sub: "updateState()"

    System->>Take: "{state: 2}"
    Take->>Sub: "{state: 2}"
    Note over Sub: "updateState()"

    Life->>Cleanup: "onUnmounted()"
    Cleanup->>Take: "void"
    Note over Take: "Complete stream"
    Take-->>Sub: "complete"
    Sub-->>Life: "unsubscribe()"

    System->>Take: "{state: 3}"
    Note over Take: "Ignored (completed)"

    Note over Take: takeUntil stops stream
    Note over Sub: Cleanup on unmount
```

Ce diagramme montre :

1. Le cycle de vie du composant Vue.js
2. Le flux de données `systemState$`
3. L'opérateur `takeUntil` qui surveille `cleanup$`
4. L'arrêt du flux quand `cleanup$` émet (lors du démontage)
5. Le nettoyage complet des souscriptions

Les points clés illustrés :

-   🔄 La souscription commence au montage du composant
-   ⚡ Les mises à jour d'état sont traitées normalement
-   🧹 Le démontage déclenche le cleanup
-   🚫 Les émissions après cleanup sont ignorées

###### Avantages de cette Approche

-   🎯 **Nettoyage Centralisé** : Un seul point de gestion
-   🛡️ **Prévention des Fuites** : Désabonnement automatique
-   🔍 **Debugging Facilité** : Logs explicites

#### 3. Pattern de Partage avec Protection 🔒

```typescript
// Dans useMainWeatherStation.ts
const sharedWeatherData$ = weatherData$.pipe(
    // Protection contre les erreurs
    catchError((error) => {
        errorHandlingService.handleError('WeatherStation', error);
        return of(null);
    }),
    // Optimisation des souscriptions multiples
    shareReplay({
        bufferSize: 1,
        refCount: true,
        windowTime: 1000,
    }),
    // Filtrage des valeurs nulles
    filter((data): data is WeatherData => data !== null),
);
```

```mermaid
sequenceDiagram
    participant Source as "weatherData$"
    participant Error as "catchError()"
    participant Share as "shareReplay()"
    participant Filter as "filter()"
    participant Result as "sharedWeatherData$"
    participant Sub1 as "Subscriber 1"
    participant Sub2 as "Subscriber 2"

    Note over Source,Sub2: Time →

    Source->>Error: "{temp: 25}"
    Error->>Share: "{temp: 25}"
    Share->>Filter: "{temp: 25}"
    Filter->>Result: "{temp: 25}"
    Result->>Sub1: "{temp: 25}"

    Note over Source: Error occurs!
    Source-xError: "❌ Error"
    Error-->>Share: "null"
    Share-->>Filter: "null"
    Note over Filter: Filtered out

    Source->>Error: "{temp: 30}"
    Error->>Share: "{temp: 30}"
    Share->>Filter: "{temp: 30}"
    Filter->>Result: "{temp: 30}"
    Result->>Sub1: "{temp: 30}"

    Note over Sub2: Late subscriber
    Share->>Sub2: "{temp: 30}"

    Note over Share: Buffer size: 1
    Note over Share: Window time: 1000ms
    Note over Error: Returns null on error
    Note over Filter: Removes null values

```

Ce diagramme montre :

1. Le flux source `weatherData$`
2. La gestion d'erreur avec `catchError()` qui transforme les erreurs en `null`
3. Le partage des données avec `shareReplay()` qui :
    - Garde en mémoire la dernière valeur (bufferSize: 1)
    - Partage le flux entre les abonnés
    - Nettoie la mémoire après 1000ms sans abonnés
4. Le filtrage des valeurs nulles
5. Un exemple d'abonné tardif recevant la dernière valeur

###### Points Clés

1. 🚨 **Gestion d'Erreur**

    - Capture et log des erreurs
    - Fallback vers valeur par défaut
    - Notification du service d'erreur

2. ⚡ **Optimisation**

    - Partage des données entre abonnés
    - Buffer limité dans le temps
    - Nettoyage automatique

3. 🔒 **Type Safety**
    - Filtrage type-safe des nulls
    - Typage strict des données
    - Prédictibilité du flux

###### Patterns de Transformation Avancés 🔄

###### 1. Pattern de Buffering Intelligent 🧠

```typescript
// Dans weatherSimulation.ts
const weatherData$ = source$.pipe(
    // Groupement temporel des mises à jour
    bufferTime(100, undefined, 5),
    // Filtrage des buffers vides
    filter((updates) => updates.length > 0),
    // Sélection de la dernière valeur du buffer
    map((updates) => updates[updates.length - 1]),
    // Optimisation des émissions
    distinctUntilKeyChanged('temperature'),
);
```

```mermaid
sequenceDiagram
    participant Source as "source$"
    participant Buffer as "bufferTime(100)"
    participant Filter as "filter()"
    participant Map as "map()"
    participant Distinct as "distinctUntilKeyChanged()"
    participant Result as "weatherData$"

    Note over Source,Result: Time →

    Source->>Buffer: "{temp: 20}"
    Source->>Buffer: "{temp: 20.5}"
    Source->>Buffer: "{temp: 21}"
    Note over Buffer: 100ms window
    Buffer->>Filter: "[{temp: 20}, {temp: 20.5}, {temp: 21}]"
    Filter->>Map: "[{temp: 20}, {temp: 20.5}, {temp: 21}]"
    Map->>Distinct: "{temp: 21}"
    Distinct->>Result: "{temp: 21}"

    Source->>Buffer: "{temp: 21}"
    Source->>Buffer: "{temp: 21}"
    Note over Buffer: 100ms window
    Buffer->>Filter: "[{temp: 21}, {temp: 21}]"
    Filter->>Map: "[{temp: 21}, {temp: 21}]"
    Map->>Distinct: "{temp: 21}"
    Note over Distinct: Skipped (same temp)

    Source->>Buffer: "{temp: 22}"
    Note over Buffer: 100ms window
    Buffer->>Filter: "[{temp: 22}]"
    Filter->>Map: "[{temp: 22}]"
    Map->>Distinct: "{temp: 22}"
    Distinct->>Result: "{temp: 22}"

    Note over Buffer: Groups events in 100ms windows
    Note over Filter: Removes empty buffers
    Note over Map: Takes last value
    Note over Distinct: Only emits on temperature change
```

Ce diagramme montre :

1. La source émettant des valeurs
2. Le regroupement par fenêtres de 100ms avec `bufferTime`
3. Le filtrage des buffers non vides
4. La sélection de la dernière valeur du buffer
5. L'émission uniquement lors d'un changement de température
6. Le résultat final optimisé

###### Pourquoi ce Pattern ?

-   ⚡ **Performance** : Réduit le nombre de mises à jour
-   🎯 **Cohérence** : Garantit des données à jour
-   📊 **Optimisation** : Évite les calculs inutiles

###### Points Clés

1. ⏱️ **Buffering Temporel**

    - Accumulation pendant 100ms
    - Maximum 5 valeurs par buffer
    - Réduction de la charge

2. 🔍 **Filtrage Intelligent**
    - Élimination des buffers vides
    - Sélection optimisée
    - Réduction du bruit

###### 2. Pattern de Dérivation d'État 📊

```typescript
// Dans useWaterSystem.ts
const derivedState$ = systemState$.pipe(
    // Calcul des états dérivés
    map((state) => ({
        ...state,
        totalVolume: calculateTotalVolume(state),
        efficiency: calculateEfficiency(state),
        alerts: generateAlerts(state),
    })),
    // Cache des calculs
    shareReplay(1),
    // Débounce pour les calculs coûteux
    debounceTime(100),
);
```

```mermaid
sequenceDiagram
    participant Source as "systemState$"
    participant Map as "map()"
    participant Share as "shareReplay(1)"
    participant Debounce as "debounceTime(100)"
    participant Result as "derivedState$"
    participant Sub1 as "Subscriber 1"
    participant Sub2 as "Subscriber 2"

    Note over Source,Sub2: Time →

    Source->>Map: "{dam: {...}, river: {...}}"
    Map->>Share: "{dam, river, totalVolume: 1000, efficiency: 0.8}"
    Share->>Debounce: "{...derived state...}"
    Debounce->>Result: "{...derived state...}"
    Result->>Sub1: "{...derived state...}"

    Note over Source: 50ms later
    Source->>Map: "{dam: {...}, river: {...}}"
    Map->>Share: "{dam, river, totalVolume: 1200, efficiency: 0.85}"
    Note over Debounce: Ignored (< 100ms)

    Note over Source: 60ms later
    Source->>Map: "{dam: {...}, river: {...}}"
    Map->>Share: "{dam, river, totalVolume: 1300, efficiency: 0.9}"

    Note over Debounce: 100ms passed
    Debounce->>Result: "{...latest derived state...}"
    Result->>Sub1: "{...latest derived state...}"

    Note over Sub2: New subscriber
    Share->>Sub2: "{...latest derived state...}"

    Note over Share: Replays last value
    Note over Debounce: Waits 100ms
    Note over Map: Calculates derived values
```

Ce diagramme montre :

1. La source `systemState$` émettant des mises à jour d'état
2. `map()` calculant les valeurs dérivées (totalVolume, efficiency, alerts)
3. `shareReplay(1)` mettant en cache la dernière valeur
4. `debounceTime(100)` filtrant les mises à jour trop fréquentes
5. Les abonnés recevant les valeurs mises en cache

Le diagramme illustre également comment :

-   Le debounce ignore les mises à jour trop rapprochées
-   Le shareReplay permet aux nouveaux abonnés de recevoir la dernière valeur
-   Les calculs coûteux sont optimisés grâce au debounce

###### Avantages

-   🔄 **Séparation des Préoccupations**

    -   Logique de calcul isolée
    -   État principal non pollué
    -   Maintenance facilitée

-   ⚡ **Performance**
    -   Calculs mis en cache
    -   Mises à jour optimisées
    -   Réutilisation des résultats

###### 3. Pattern de Synchronisation Multi-Source 🔄

```typescript
// Dans useMainWeatherStation.ts
const synchronizedData$ = combineLatest({
    weather: weatherData$,
    system: systemState$,
    alerts: alertState$,
}).pipe(
    // Transformation des données combinées
    map(({ weather, system, alerts }) => ({
        temperature: weather.temperature,
        waterLevel: system.waterLevel,
        activeAlerts: alerts.filter(isActive),
        timestamp: new Date(),
    })),
    // Optimisation des émissions
    distinctUntilChanged((prev, curr) => isEqual(prev, curr)),
    // Partage entre les abonnés
    share(),
);
```

```mermaid
sequenceDiagram
    participant Weather as "weatherData$"
    participant System as "systemState$"
    participant Alerts as "alertState$"
    participant Combine as "combineLatest()"
    participant Map as "map()"
    participant Distinct as "distinctUntilChanged()"
    participant Share as "share()"
    participant Result as "synchronizedData$"

    Note over Weather,Result: Time →

    Weather->>Combine: "{temperature: 25}"
    System->>Combine: "{waterLevel: 80}"
    Alerts->>Combine: "[{id: 1, active: true}]"

    Combine->>Map: "{ weather: {temperature: 25}, system: {waterLevel: 80}, alerts: [{id: 1, active: true}] }"
    Map->>Distinct: "{ temperature: 25, waterLevel: 80, activeAlerts: [{id: 1}], timestamp: Date }"
    Distinct->>Share: "{ temperature: 25, waterLevel: 80, activeAlerts: [{id: 1}], timestamp: Date }"
    Share->>Result: "{ temperature: 25, waterLevel: 80, activeAlerts: [{id: 1}], timestamp: Date }"

    Weather->>Combine: "{temperature: 26}"
    Combine->>Map: "{ weather: {temperature: 26}, system: {waterLevel: 80}, alerts: [{id: 1, active: true}] }"
    Map->>Distinct: "{ temperature: 26, waterLevel: 80, activeAlerts: [{id: 1}], timestamp: Date }"
    Distinct->>Share: "{ temperature: 26, waterLevel: 80, activeAlerts: [{id: 1}], timestamp: Date }"
    Share->>Result: "{ temperature: 26, waterLevel: 80, activeAlerts: [{id: 1}], timestamp: Date }"

    Note over Combine: Combines latest values from all sources
    Note over Map: Transforms and filters active alerts
    Note over Distinct: Skips if equal to previous
    Note over Share: Shares result with multiple subscribers
```

Ce diagramme montre :

1. Trois sources de données (`weatherData$`, `systemState$`, `alertState$`)
2. La combinaison via `combineLatest()`
3. La transformation et le filtrage des alertes via `map()`
4. L'optimisation avec `distinctUntilChanged()`
5. Le partage du résultat via `share()`
6. Le flux final `synchronizedData$`

###### Caractéristiques

1. 🔄 **Synchronisation**

    - Données cohérentes
    - Mises à jour atomiques
    - État global synchronisé

2. ⚡ **Optimisation**
    - Comparaison profonde
    - Émissions contrôlées
    - Partage efficace

##### Patterns de Gestion des Données en Temps Réel ⚡

###### 1. Pattern de Fenêtre Glissante 📈

```typescript
// Dans useWeatherStation.ts
const rollingAverageTemperature$ = temperature$.pipe(
    // Collecte des dernières 5 minutes de données
    bufferTime(5 * 60 * 1000),
    // Calcul de la moyenne glissante
    map((temperatures) => {
        if (temperatures.length === 0) return null;
        return temperatures.reduce((sum, t) => sum + t, 0) / temperatures.length;
    }),
    // Filtrage des valeurs nulles
    filter((avg): avg is number => avg !== null),
    // Partage pour les multiples abonnés
    share(),
);
```

```mermaid
sequenceDiagram
    participant Source as "temperature$"
    participant Buffer as "bufferTime(5min)"
    participant Map as "map(average)"
    participant Filter as "filter(notNull)"
    participant Share as "share()"
    participant Result as "rollingAverageTemperature$"

    Note over Source,Result: Time →

    Source->>Buffer: "22°C"
    Source->>Buffer: "24°C"
    Source->>Buffer: "23°C"
    Note over Buffer: Wait 5min
    Buffer->>Map: "[22,24,23]"
    Map->>Filter: "23°C (avg)"
    Filter->>Share: "23°C"
    Share->>Result: "23°C"

    Source->>Buffer: "25°C"
    Source->>Buffer: "26°C"
    Source->>Buffer: "24°C"
    Note over Buffer: Wait 5min
    Buffer->>Map: "[25,26,24]"
    Map->>Filter: "25°C (avg)"
    Filter->>Share: "25°C"
    Share->>Result: "25°C"

    Note over Buffer: Collects 5min of data
    Note over Map: Calculates average
    Note over Filter: Removes null values
    Note over Share: Shares with subscribers
```

Ce diagramme montre :

1. La source émettant des températures individuelles
2. Le buffer collectant 5 minutes de données
3. Le calcul de la moyenne sur chaque buffer
4. Le filtrage des valeurs nulles
5. Le partage du résultat avec les abonnés

Le diagramme illustre comment les températures sont collectées par intervalles de 5 minutes, moyennées, puis partagées avec tous les abonnés.

###### Pourquoi ce Pattern ?

-   📊 **Lissage des Données** : Réduit le bruit des mesures
-   📈 **Tendances** : Permet de détecter les évolutions
-   ⚡ **Performance** : Optimise le traitement des données haute fréquence

###### 2. Pattern de Détection d'Anomalies 🔍

```typescript
// Dans useWaterSystem.ts
const anomalies$ = waterLevel$.pipe(
    // Création de fenêtres de 10 valeurs avec chevauchement
    windowCount(10, 1),
    // Conversion des fenêtres en tableaux
    mergeMap((window) => window.pipe(toArray())),
    // Détection des anomalies
    map((values) => {
        const avg = calculateAverage(values);
        const stdDev = calculateStdDev(values, avg);
        const lastValue = values[values.length - 1];
        return {
            value: lastValue,
            isAnomaly: Math.abs(lastValue - avg) > 2 * stdDev,
        };
    }),
    // Filtrage des anomalies uniquement
    filter(({ isAnomaly }) => isAnomaly),
    // Notification du service d'erreur
    tap(({ value }) => {
        errorHandlingService.handleWarning('Anomalie détectée', { value });
    }),
);
```

```mermaid
sequenceDiagram
    participant Source as "waterLevel$"
    participant Window as "windowCount(10,1)"
    participant MergeMap as "mergeMap(toArray())"
    participant Analyze as "map(analyze)"
    participant Filter as "filter(isAnomaly)"
    participant Tap as "tap(handleWarning)"
    participant Result as "anomalies$"

    Note over Source,Result: Time →

    Source->>Window: "50"
    Source->>Window: "52"
    Source->>Window: "51"
    Source->>Window: "53"
    Source->>Window: "49"
    Source->>Window: "48"
    Source->>Window: "51"
    Source->>Window: "50"
    Source->>Window: "52"
    Source->>Window: "80"

    Window->>MergeMap: "[50,52,51,53,49,48,51,50,52,80]"
    MergeMap->>Analyze: "[50,52,51,53,49,48,51,50,52,80]"

    Note over Analyze: avg=50.6, stdDev=9.2

    Analyze->>Filter: "{ value: 80, isAnomaly: true }"
    Filter->>Tap: "{ value: 80, isAnomaly: true }"
    Tap->>Result: "{ value: 80, isAnomaly: true }"

    Note over Window: Fenêtre glissante de 10 valeurs
    Note over Analyze: Calcul moyenne et écart-type
    Note over Filter: Garde uniquement les anomalies
    Note over Tap: Notification des anomalies

```

Ce diagramme illustre :

1. La source émettant des niveaux d'eau 💧
2. La création de fenêtres glissantes de 10 valeurs 🔄
3. L'analyse statistique (moyenne et écart-type) 📊
4. Le filtrage des anomalies ⚠️
5. La notification via le service d'erreur 🚨

Dans cet exemple, la valeur 80 est détectée comme une anomalie car elle dévie de plus de 2 écarts-types de la moyenne.

###### Points Clés ⭐

1. **Détection en Temps Réel** ⚡

    - Analyse continue des données
    - Détection rapide des problèmes
    - Alertes immédiates

2. **Optimisation** 🔧
    - Traitement par lots
    - Réduction des faux positifs
    - Performance optimisée

### 2.3 Schedulers et Asynchronicité ⚙️

Les Schedulers RxJS sont essentiels pour contrôler la concurrence et optimiser les performances dans notre application.

![](https://raw.githubusercontent.com/giak/dam-dashboard/264ae70b68b94cb0d21da229f61afb66eff3404e/docs/assets/schedulers.svg)

#### asyncScheduler 🔄

Le `asyncScheduler` est utilisé pour les opérations asynchrones non bloquantes :

##### Exemple Simple - Mise à jour UI

```typescript
// Extrait de damService.ts
const currentWaterLevel$ = sharedDamState$.pipe(
    observeOn(asyncScheduler),
    map((state) => state.currentWaterLevel),
    distinctUntilChanged(),
);
```

```mermaid
sequenceDiagram
    participant Source as "sharedDamState$"
    participant Scheduler as "observeOn(asyncScheduler)"
    participant Map as "map()"
    participant Distinct as "distinctUntilChanged()"
    participant Result as "currentWaterLevel$"

    Note over Source,Result: Time →

    Source->>Scheduler: "{currentWaterLevel: 75, ...}"
    Scheduler->>Map: "{currentWaterLevel: 75, ...}"
    Map->>Distinct: "75"
    Distinct->>Result: "75"

    Source->>Scheduler: "{currentWaterLevel: 75, ...}"
    Scheduler->>Map: "{currentWaterLevel: 75, ...}"
    Map->>Distinct: "75"
    Note over Distinct: Filtered (unchanged)

    Source->>Scheduler: "{currentWaterLevel: 80, ...}"
    Scheduler->>Map: "{currentWaterLevel: 80, ...}"
    Map->>Distinct: "80"
    Distinct->>Result: "80"

    Note over Scheduler: Async execution
    Note over Map: Extract level
    Note over Distinct: Skip duplicates
```

Ce diagramme montre :

1. La source `sharedDamState$` émettant l'état complet du barrage
2. Le scheduler asynchrone qui déplace l'exécution hors du thread principal
3. L'opérateur `map` qui extrait uniquement le niveau d'eau
4. `distinctUntilChanged` qui filtre les valeurs identiques consécutives
5. Le flux résultant `currentWaterLevel$` ne contenant que les changements de niveau

Ce scheduler est spécialement conçu pour :

-   Éviter le blocage du thread principal 🚫
-   Gérer les mises à jour UI fluides 🖥️
-   Optimiser les opérations de rendu ⚡

##### Exemple Avancé - Logging Asynchrone

```typescript
// Extrait de weatherService.ts
tap({
    next: (data) => {
        asyncScheduler.schedule(
            () => {
                loggingService.info('Weather data updated', 'weatherSimulation', {
                    data,
                });
            },
            0,
            { priority: -1 },
        );
    },
});
```

```mermaid
sequenceDiagram
    participant Source as "Source$"
    participant Tap as "tap()"
    participant Scheduler as "asyncScheduler"
    participant Logger as "loggingService"
    participant Result as "Result$"

    Note over Source,Result: Time →

    Source->>Tap: "{temp: 25}"
    Tap->>Result: "{temp: 25}"
    Tap-->>Scheduler: "Schedule log (priority: -1)"
    Scheduler-->>Logger: "info('Weather data updated')"

    Source->>Tap: "{temp: 30}"
    Tap->>Result: "{temp: 30}"
    Tap-->>Scheduler: "Schedule log (priority: -1)"
    Scheduler-->>Logger: "info('Weather data updated')"

    Note over Tap: Side effect: doesn't modify data
    Note over Scheduler: Async scheduling with low priority
    Note over Logger: Non-blocking logging operation

```

Ce diagramme montre :

1. Le flux source émettant des données météo
2. L'opérateur `tap()` qui laisse passer les données sans modification
3. L'ordonnancement asynchrone des logs via `asyncScheduler`
4. Les opérations de logging non bloquantes avec priorité basse (-1)
5. Le flux résultant identique au flux source

Les lignes pointillées (-->>) représentent les opérations asynchrones de logging qui n'affectent pas le flux principal des données.

Cette utilisation permet :

-   La priorisation des tâches avec `priority` 📊
-   Le report des opérations non critiques ⏳
-   L'optimisation des performances globales 🚀

#### queueScheduler 📋

Le `queueScheduler` est utilisé pour les calculs intensifs et le traitement synchrone :

##### Exemple Simple - Calculs Synchrones

```typescript
// Extrait de damService.ts
const updateDam = (update: DamUpdateInterface): void => {
    queueScheduler.schedule(() => {
        try {
            validateDamUpdate(update);
            const currentState = damState$.getValue();
            const newState = {
                ...currentState,
                ...update,
                lastUpdated: new Date(),
            };
            damState$.next(newState);
        } catch (error) {
            // Gestion des erreurs
        }
    });
};
```

```mermaid
sequenceDiagram
    participant Client as "Client Code"
    participant Queue as "queueScheduler"
    participant Validate as "validateDamUpdate"
    participant State as "damState$"
    participant Error as "Error Handler"

    Note over Client,Error: Time →

    Client->>Queue: "updateDam({level: 75})"
    activate Queue
    Queue->>Validate: "validate update"

    alt Validation Success
        Validate-->>State: "get current state"
        State-->>Queue: "{level: 50, flow: 100}"
        Queue->>State: "next({level: 75, flow: 100})"
    else Validation Error
        Validate--xError: "validation failed"
        Error-->>Queue: "handle error"
    end
    deactivate Queue

    Note over Queue: Synchronous execution
    Note over State: BehaviorSubject
```

Ce diagramme montre :

1. L'appel client à `updateDam`
2. L'exécution synchrone via `queueScheduler`
3. Le processus de validation
4. La mise à jour de l'état ou la gestion d'erreur
5. L'utilisation du BehaviorSubject `damState$`

Les points clés illustrés :

-   Exécution synchrone dans une file d'attente ↔️
-   Gestion des erreurs avec try/catch ���️
-   Mise à jour atomique de l'état 🔄
-   Validation des données avant mise à jour ✅

Ce scheduler est spécialement conçu pour :

-   Garantir l'ordre d'exécution
-   Maintenir la cohérence des données
-   Optimiser les calculs intensifs

##### Exemple Avancé - Traitement par Lots

```typescript
// Extrait de useWaterSystem.ts
const totalWaterVolume$ = combineLatest([states.damState$, states.riverState$]).pipe(
    observeOn(queueScheduler),
    bufferTime(100, undefined, 5),
    filter((updates) => updates.length > 0),
    map((updates) => updates[updates.length - 1]),
);
```

```mermaid
sequenceDiagram
    participant Dam as "damState$"
    participant River as "riverState$"
    participant Combine as "combineLatest()"
    participant Buffer as "bufferTime(100ms)"
    participant Filter as "filter()"
    participant Map as "map()"
    participant Result as "totalWaterVolume$"

    Note over Dam,Result: Time →

    Note over Buffer: Window 1 (0-100ms)
    Dam->>Combine: "{level: 50}"
    River->>Combine: "{flow: 100}"
    Combine->>Buffer: "[{level: 50}, {flow: 100}]"
    Dam->>Combine: "{level: 52}"
    Combine->>Buffer: "[{level: 52}, {flow: 100}]"
    River->>Combine: "{flow: 110}"
    Combine->>Buffer: "[{level: 52}, {flow: 110}]"

    Note over Buffer: Buffer Full (100ms)
    Buffer->>Filter: "[[state1, state2, state3]]"
    Filter->>Map: "[[state1, state2, state3]]"
    Map->>Result: "[{level: 52}, {flow: 110}]"

    Note over Buffer: Window 2 (100-200ms)
    Dam->>Combine: "{level: 55}"
    Combine->>Buffer: "[{level: 55}, {flow: 110}]"

    Note over Combine: queueScheduler
    Note over Buffer: Max 5 updates per 100ms
    Note over Map: Take last update only
```

Ce diagramme illustre :

1. Les deux sources de données (`damState$` et `riverState$`)
2. La combinaison via `combineLatest()`
3. Le buffering des événements sur 100ms (max 5 updates)
4. Le filtrage des buffers non vides
5. La sélection du dernier état via map
6. L'utilisation du `queueScheduler` pour la synchronisation

Les notes expliquent les aspects clés du traitement :

-   La fenêtre de temps de 100ms
-   La limite de 5 updates par buffer
-   La sélection du dernier état uniquement

Cette approche permet :

-   Le traitement synchrone des lots de données
-   L'optimisation des calculs complexes
-   La réduction de la charge de traitement

Les Schedulers sont essentiels pour :

1. Contrôler le flux d'exécution 🎮
2. Optimiser les performances ⚡
3. Gérer efficacement les ressources système 💻

## 3. Implémentation dans l'application 🏗️

### 3.1 Clean Architecture Layers 🎯

Notre implémentation RxJS respecte les principes de la Clean Architecture, avec une séparation claire des responsabilités entre les couches.

```mermaid
---
config:
  theme: default
  look: classic
---
flowchart TB
    %% Styles
    classDef presentation fill:#E3F2FD,stroke:#42A5F5,stroke-width:2px
    classDef application fill:#E8F5E9,stroke:#66BB6A,stroke-width:2px
    classDef domain fill:#FFF3E0,stroke:#FFA726,stroke-width:2px
    classDef infrastructure fill:#F3E5F5,stroke:#AB47BC,stroke-width:2px

    %% Couche Présentation
    subgraph UI["Couche Présentation (UI)"]
        direction TB
        V1[WaterSystemDashboard]
        V2[MainWeatherStation]
        V3[DamComponent]
        V4[GlacierComponent]
        V5[RiverComponent]
        V6[ErrorNotification]
    end

    %% Couche Application
    subgraph APP["Couche Application (Use Cases)"]
        direction TB
        S1[useWaterSystem]
        S2[useMainWeatherStation]
        S3[useDam]
        S4[useGlacier]
        S5[useRiver]
        S6[ErrorHandlingService]
    end

    %% Couche Domaine
    subgraph DOM["Couche Domaine (Business Logic)"]
        direction TB
        E1[WeatherStationInterface]
        E2[DamInterface]
        E3[GlacierInterface]
        E4[RiverInterface]
        E5[ErrorDataInterface]
    end

    %% Couche Infrastructure
    subgraph INF["Couche Infrastructure (Data Sources)"]
        direction TB
        D1[weatherSimulation]
        D2[damSimulation]
        D3[glacierSimulation]
        D4[riverSimulation]
        D5[loggingService]
    end

    %% Flux de données
    UI --> APP
    APP --> DOM
    APP --> INF
    INF --> DOM

    %% Application des styles
    UI:::presentation
    APP:::application
    DOM:::domain
    INF:::infrastructure
```

#### Couche Infrastructure (Sources de données) 💾

Cette couche est responsable de la génération et de la gestion des données brutes.

##### Exemple Simple - Simulation Météo 🌤️

```typescript
// Extrait de weatherSimulation.ts
export function createWeatherSimulation(): WeatherSimulationInterface {
    const weatherData = new ReplaySubject<WeatherDataInterface>(1);

    const weatherData$ = weatherData.pipe(observeOn(asyncScheduler), share());

    return {
        weatherData$,
        start: () => startSimulation(weatherData),
        stop: () => weatherData.complete(),
    };
}
```

Cette implémentation :

-   ✅ Isole la logique de simulation
-   ✅ Fournit une interface claire
-   ✅ Gère son propre cycle de vie

#### Couche Application (Services) ⚙️

Cette couche orchestre les flux de données et implémente la logique métier.

##### Exemple Simple - Service Météo

```typescript
// Service basique
export function createWeatherService(config: WeatherStationConfig): WeatherServiceInterface {
    const { weatherData$, start } = createWeatherSimulation();

    return {
        getData$: () => weatherData$,
        start,
        cleanup: () => weatherData$.complete(),
    };
}
```

#### Couche Présentation (Composables Vue.js) 🖥️

Cette couche gère l'interaction avec l'interface utilisateur.

##### Exemple Simple - Composable Basique

```typescript
// Composable simple
export function useWeatherDisplay(weatherService: WeatherServiceInterface) {
    const temperature = ref(0);

    onMounted(() => {
        const subscription = weatherService.getData$().subscribe((data) => (temperature.value = data.temperature));

        onUnmounted(() => subscription.unsubscribe());
    });

    return { temperature };
}
```

Cette architecture en couches permet :

1. 🎯 Une séparation claire des responsabilités
2. ✅ Une meilleure testabilité
3. 🔧 Une maintenance facilitée
4. 📈 Une évolution indépendante des couches

### 3.2 Application des principes SOLID 🏛️

Notre application implémente rigoureusement les principes SOLID à travers son architecture RxJS.

![](https://raw.githubusercontent.com/giak/dam-dashboard/264ae70b68b94cb0d21da229f61afb66eff3404e/docs/assets/SOLID_meteo.svg)

#### Single Responsibility Principle (SRP) 🎯

##### Exemple Simple - Service Météo

```typescript
// Service avec une seule responsabilité : la gestion des données météo
export class WeatherDataService {
    private weatherData$ = new BehaviorSubject<WeatherDataInterface>(initialState);

    getWeatherData$(): Observable<WeatherDataInterface> {
        return this.weatherData$.asObservable();
    }

    updateWeatherData(data: WeatherDataInterface): void {
        this.weatherData$.next(data);
    }
}
```

Ce service est responsable uniquement de :

-   La gestion des données météorologiques
-   La mise à jour de l'état
-   La distribution des données aux abonnés

##### Exemple Avancé - Séparation des Responsabilités

```typescript
// Séparation des responsabilités en services spécialisés
export class WeatherValidationService {
    validateData(data: unknown): data is WeatherDataInterface {
        // Logique de validation
    }
}

export class WeatherTransformationService {
    transformData(data: WeatherDataInterface): ProcessedWeatherData {
        // Logique de transformation
    }
}

export class WeatherStorageService {
    storeData(data: ProcessedWeatherData): Observable<void> {
        // Logique de stockage
    }
}
```

#### Open/Closed Principle (OCP) 🔓

##### Exemple Simple - Extension par Composition

```typescript
// Interface de base pour les sources de données
interface DataSource<T> {
    getData$: Observable<T>;
}

// Extensions sans modification du code existant
class WeatherStation implements DataSource<WeatherData> {
    getData$: Observable<WeatherData>;
}
```

-   L'interface DataSource&lt;T&rt; définit un contrat générique qui est "fermé pour modification" - nous n'avons pas besoin de modifier cette interface pour ajouter de nouvelles sources de données.
-   Le système est "ouvert à l'extension" car nous pouvons créer de nouvelles classes qui implémentent cette interface sans toucher au code existant.

##### Exemple Avancé - Pipeline Extensible

```typescript
// Pipeline extensible via des opérateurs personnalisés
type DataTransformer<T> = (source: Observable<T>) => Observable<T>;

class WeatherPipeline {
    private transformers: DataTransformer<WeatherData>[] = [];

    addTransformer(transformer: DataTransformer<WeatherData>): void {
        this.transformers.push(transformer);
    }

    createPipeline(source$: Observable<WeatherData>): Observable<WeatherData> {
        return this.transformers.reduce((acc, transformer) => transformer(acc), source$);
    }
}
```

-   La classe WeatherPipeline est "fermée à la modification" car sa logique de base (ajout et application des transformateurs) n'a pas besoin d'être modifiée.
-   Elle est "ouverte à l'extension" car on peut ajouter de nouveaux transformateurs sans modifier le code existant.

#### Liskov Substitution Principle (LSP) 🔄

##### Exemple Simple - Hiérarchie de Services

```typescript
abstract class BaseWeatherService {
    abstract getData$(): Observable<WeatherData>;
}

class LocalWeatherService extends BaseWeatherService {
    getData$(): Observable<WeatherData> {
        // Implémentation locale
    }
}

class RemoteWeatherService extends BaseWeatherService {
    getData$(): Observable<WeatherData> {
        // Implémentation distante
    }
}
```

Cet exemple respecte le LSP car :

1. Les sous-classes (`LocalWeatherService` et `RemoteWeatherService`) peuvent être utilisées partout où `BaseWeatherService` est attendu sans changer le comportement du programme.
2. Les sous-classes respectent les 3 règles fondamentales du LSP :
    - **Règle de Signature** : Chaque sous-classe impl��mente correctement la méthode `getData$()` définie dans la classe abstraite
    - **Règle des Méthodes** : Les implémentations respectent la sémantique de la classe de base
    - **Règle des Propriétés** : Les invariants sont préservés
3. Deux avantages majeurs :
    - **Réutilisabilité du code** : Le code peut être réutilisé facilement grâce à l'utilisation d'interfaces génériques
    - **Amélioration de la qualité** : Le code devient plus compréhensible et plus facile à maintenir

Dans cet exemple, peu importe que nous utilisions `LocalWeatherService` ou `RemoteWeatherService`, le comportement attendu de `getData$()` reste cohérent avec ce que promet `BaseWeatherService`, respectant ainsi le principe de substitution de Liskov.

Ce principe est crucial car sa violation conduit souvent à des vérifications de type conditionnelles dispersées dans l'application, ce qui devient une source de bugs potentiels.

#### Interface Segregation Principle (ISP) 🔍

L'ISP stipule que "les clients ne devraient pas être forcés de dépendre d'interfaces qu'ils n'utilisent pas".

##### Exemple Simple - Interfaces Spécifiques

```typescript
interface WeatherReader {
    getData$(): Observable<WeatherData>;
}

interface WeatherWriter {
    updateData(data: WeatherData): void;
}

interface WeatherMonitor {
    getStatus$(): Observable<SystemStatus>;
}

// Un service peut implémenter uniquement les interfaces dont il a besoin
class WeatherStation implements WeatherReader, WeatherMonitor {
    getData$(): Observable<WeatherData> {
        /* ... */
    }
    getStatus$(): Observable<SystemStatus> {
        /* ... */
    }
}
```

1. **Interface unique et ciblée** :

-   `DataTransformer<T>` définit une seule responsabilité claire : transformer un flux de données
-   Chaque transformateur implémente uniquement la fonctionnalité dont il a besoin

2. **Composition flexible** :

-   La classe `WeatherPipeline` permet d'ajouter uniquement les transformateurs nécessaires
-   Aucun transformateur n'est forcé d'implémenter des méthodes inutiles

Cette approche offre plusieurs avantages :

-   🔧 **Maintenabilité** : Les interfaces restent petites et focalisées
-   🔄 **Flexibilité** : Les clients peuvent implémenter uniquement ce dont ils ont besoin
-   📈 **Évolutivité** : De nouveaux transformateurs peuvent être ajoutés sans modifier l'existant

Cette approche évite le problème des "interfaces polluées" qui forceraient les implémentations à avoir des méthodes vides ou à lever des exceptions pour des fonctionnalités non supportées.

En résumé, ce code illustre parfaitement l'ISP en fournissant une interface simple et ciblée (`DataTransformer`) que les clients peuvent implémenter selon leurs besoins spécifiques, sans être forcés d'implémenter des fonctionnalités superflues.

#### Dependency Inversion Principle (DIP) 🔄

Le DIP stipule que :

1. 🔼 Les modules de haut niveau ne devraient pas dépendre des modules de bas niveau
2. 🎯 Les deux devraient dépendre d'abstractions

##### Exemple Simple - Injection de Dépendances

```typescript
interface WeatherRepository {
    getData$(): Observable<WeatherData>;
}

class WeatherService {
    constructor(private repository: WeatherRepository) {}

    getProcessedData$(): Observable<ProcessedWeatherData> {
        return this.repository.getData$().pipe(map((data) => this.processData(data)));
    }
}
```

Ce code respecte le DIP pour plusieurs raisons :

1. **Dépendance vers l'abstraction** :

-   `WeatherService` dépend de l'interface `WeatherRepository` (abstraction) et non d'une implémentation concrète
-   L'injection se fait via le constructeur, permettant différentes implémentations

2. **Inversion du contrôle** :

-   Le service ne crée pas son repository
-   La dépendance est injectée de l'extérieur, inversant ainsi le contrôle

Cette approche offre plusieurs avantages :

-   🔧 **Maintenabilité** : Les changements dans les implémentations concrètes n'affectent pas le service
-   ✅ **Testabilité** : Facilite les tests unitaires en permettant l'injection de mocks
-   🔄 **Flexibilité** : Permet de changer facilement d'implémentation sans modifier le code existant

Cette implémentation permet de :

-   Découpler les composants de haut niveau des détails d'implémentation
-   Faciliter les tests en permettant l'injection de mocks
-   Rendre le code plus modulaire et réutilisable

En résumé, le DIP est un principe fondamental qui favorise un code plus flexible, testable et maintenable en inversant la direction traditionnelle des dépendances à travers l'utilisation d'abstractions.

### 3.3 Optimisation des performances 🚀

Pour optimiser les performances, nous utilisons plusieurs techniques de RxJS :

![](https://raw.githubusercontent.com/giak/dam-dashboard/264ae70b68b94cb0d21da229f61afb66eff3404e/docs/assets/optimisation.svg)

#### Buffering et Debouncing ⚡

```typescript
// Extrait de weatherSimulation.ts
const weatherData$ = weatherData.pipe(
    observeOn(asyncScheduler),
    bufferTime(100, undefined, 5), // Buffer max 5 items sur 100ms
    filter((updates): updates is WeatherDataInterface[] => updates.length > 0),
    map((updates) => updates[updates.length - 1]),
    distinctUntilKeyChanged('temperature'),
    debounceTime(100),
    share(),
);
```

```mermaid
sequenceDiagram
    participant Source as "weatherData"
    participant Buffer as "bufferTime(100ms)"
    participant Filter as "filter()"
    participant Map as "map()"
    participant Distinct as "distinctUntilKeyChanged()"
    participant Debounce as "debounceTime(100)"
    participant Result as "weatherData$"

    Note over Source,Result: Time →

    Source->>Buffer: "{temp: 20}"
    Source->>Buffer: "{temp: 21}"
    Source->>Buffer: "{temp: 22}"
    Note over Buffer: Wait 100ms
    Buffer->>Filter: "[{temp: 20}, {temp: 21}, {temp: 22}]"
    Filter->>Map: "[{temp: 20}, {temp: 21}, {temp: 22}]"
    Map->>Distinct: "{temp: 22}"
    Distinct->>Debounce: "{temp: 22}"
    Note over Debounce: Wait 100ms
    Debounce->>Result: "{temp: 22}"

    Source->>Buffer: "{temp: 22}"
    Source->>Buffer: "{temp: 22}"
    Note over Buffer: Wait 100ms
    Buffer->>Filter: "[{temp: 22}, {temp: 22}]"
    Filter->>Map: "[{temp: 22}, {temp: 22}]"
    Map->>Distinct: "{temp: 22}"
    Note over Distinct: Skipped (same temp)

    Source->>Buffer: "{temp: 25}"
    Note over Buffer: Wait 100ms
    Buffer->>Filter: "[{temp: 25}]"
    Filter->>Map: "[{temp: 25}]"
    Map->>Distinct: "{temp: 25}"
    Distinct->>Debounce: "{temp: 25}"
    Note over Debounce: Wait 100ms
    Debounce->>Result: "{temp: 25}"

    Note over Buffer: Groups events within 100ms
    Note over Filter: Removes empty arrays
    Note over Map: Takes last value
    Note over Distinct: Filters unchanged temps
    Note over Debounce: Rate limits emissions
```

Ce diagramme montre :

1. 📦 Le buffering des événements sur 100ms
2. 🔍 Le filtrage des tableaux non vides
3. ⚡ La sélection de la dernière valeur du buffer
4. 🎯 L'élimination des doublons de température
5. ⏱️ Le debouncing final pour limiter le débit
6. 🔄 Le partage du flux résultant

Le flux complet optimise les performances en :

-   📊 Regroupant les mises à jour fréquentes
-   🎯 Éliminant les données redondantes
-   ⚡ Contrôlant le débit des émissions

**Avantages:** ✅

-   Réduit la charge serveur en limitant le nombre d'appels
-   Améliore les performances en évitant les calculs inutiles
-   Optimise la consommation de ressources

**Pourquoi l'utiliser:**

-   Le buffering (`bufferTime`) permet de regrouper plusieurs mises à jour en une seule opération
-   Le debouncing (`debounceTime`) évite les mises à jour trop fréquentes
-   Cela est particulièrement utile pour:
    -   Les recherches en temps réel
    -   Les mises à jour d'interface utilisateur
    -   La gestion des événements fréquents

**Inconvénients:** ⚠️

-   Légère latence dans les mises à jour (100ms dans cet exemple)
-   Complexité accrue du code
-   Risque de perte de données si le buffer est plein

#### Memoïsation des calculs 🧮

Nous utilisons la memoïsation pour éviter les calculs redondants :

```typescript
// Extrait de useWaterSystem.ts
const memoizedCalculateTotalWaterVolume = (() => {
    let lastDam: DamInterface | null = null;
    let lastRiver: RiverStateInterface | null = null;
    let lastResult: number | null = null;

    return (dam: DamInterface | null, river: RiverStateInterface | null): number => {
        if (dam === lastDam && river === lastRiver && lastResult !== null) {
            return lastResult;
        }
        lastDam = dam;
        lastRiver = river;

        lastResult = calculateTotalWaterVolumeUtil(dam, river);
        return lastResult;
    };
})();
```

**Avantages:** ✅

-   Cache les résultats des calculs précédents
-   Évite les calculs redondants
-   Améliore significativement les performances pour les opérations coûteuses

**Pourquoi l'utiliser:** 💡

-   C'est particulièrement utile pour:
    -   Les calculs complexes
    -   Les opérations qui sont appelées fréquemment avec les mêmes paramètres
    -   L'optimisation des performances dans les applications temps réel

**Inconvénients:** ⚠️

-   Consommation de mémoire pour stocker les résultats
-   Risque de fuites mémoire si mal implémenté
-   Complexité accrue pour la gestion du cache

![](https://raw.githubusercontent.com/giak/dam-dashboard/264ae70b68b94cb0d21da229f61afb66eff3404e/docs/assets/technique_optimisation.svg)

### 3.4 Gestion des erreurs 🛡️

Notre système implémente une stratégie robuste de gestion des erreurs à plusieurs niveaux :

#### Retry avec backoff exponentiel 🔄

Pour gérer les erreurs transitoires, nous utilisons un mécanisme de retry avec backoff exponentiel :

```typescript
// Extrait de weatherSimulation.ts
const simulation$ = interval(updateInterval, asyncScheduler).pipe(
    observeOn(queueScheduler),
    map(() => {
        // ... logique de simulation
    }),
    retry({
        count: 3,
        delay: (error, retryCount) => {
            loggingService.error('Simulation error, retrying...', 'weatherSimulation', {
                error,
                retryCount,
            });
            return timer(Math.min(1000 * Math.pow(2, retryCount), 10000));
        },
    }),
    catchError((error) => {
        loggingService.error('Simulation error', 'weatherSimulation', { error });
        return EMPTY;
    }),
);
```

```mermaid
sequenceDiagram
    participant Source as "interval()"
    participant Queue as "queueScheduler"
    participant Map as "map()"
    participant Retry as "retry()"
    participant Error as "catchError()"
    participant Result as "simulation$"

    Note over Source,Result: Time →

    Source->>Queue: "tick"
    Queue->>Map: "tick"
    Map--xRetry: "❌ Error"
    Note over Retry: Wait 1s (2^0)

    Retry->>Map: "retry #1"
    Map--xRetry: "❌ Error"
    Note over Retry: Wait 2s (2^1)

    Retry->>Map: "retry #2"
    Map--xRetry: "❌ Error"
    Note over Retry: Wait 4s (2^2)

    Retry->>Map: "retry #3"
    Map--xRetry: "❌ Error"
    Retry->>Error: "Max retries reached"
    Error->>Result: "EMPTY"

    Note over Retry: Exponential backoff
    Note over Error: Fallback to EMPTY

```

Ce diagramme illustre :

1. 🔄 Le flux initial avec `interval()`
2. ⚡ L'utilisation du `queueScheduler`
3. ❌ Une erreur dans la fonction `map()`
4. 🔁 La stratégie de retry avec backoff exponentiel
5. 🎯 Après 3 échecs, le catchError retourne EMPTY

**Avantages:** ✅

-   Gestion intelligente des erreurs temporaires
-   Réduction de la charge sur le système
-   Augmentation progressive du délai entre les tentatives
-   Cette approche est particulièrement efficace pour :
    -   Les problèmes de réseau transitoires
    -   Les timeouts temporaires
    -   Les surcharges momentanées

**Pourquoi l'utiliser:** 💡

-   Évite les échecs immédiats
-   Donne le temps au système de se rétablir
-   Limite l'impact sur les performances

**Inconvénients:** ⚠️

-   Peut retarder la détection d'erreurs permanentes
-   Complexité accrue du code
-   Consommation de ressources supplémentaires

#### Gestion centralisée des erreurs 🎯

Les erreurs sont centralisées via un service dédié :

```typescript
// Extrait de useWaterSystem.ts
const errorHandler = (context: string, error: any) =>
    handleWaterSystemError(context, error, dependencies.errorHandlingService, dependencies.loggingService);

const systemState$ = createSystemStateObservable(
    damState$,
    glacierState$,
    riverState$,
    mainWeatherState$,
    errorHandler,
).pipe(
    catchError((error) => {
        errorHandler('useWaterSystem.systemState$', error);
        return EMPTY;
    }),
);
```

```mermaid
sequenceDiagram
    participant s1 as damState
    participant s2 as glacierState
    participant s3 as riverState
    participant s4 as weatherState
    participant s5 as systemState
    participant s6 as errorHandler
    participant s7 as result

    Note over s1,s7: Time flows right

    s1->>s5: dam data
    s2->>s5: glacier data
    s3->>s5: river data
    s4->>s5: weather data
    s5->>s7: combined state

    Note over s5: Error occurs
    s5->>s6: process error
    s6-->>s7: empty state

    Note over s6: Error handling
    s6->>s6: log error
    s6->>s6: notify service

    Note over s5,s7: Continue

    s1->>s5: new dam data
    s5->>s7: new state

    Note over s5: Using catchError
```

Ce diagramme illustre :

1. 📊 Les quatre sources de données principales
2. ⚡ Le processus de création de l'état système
3. 🛡️ La gestion des erreurs via `errorHandler`
4. 🔄 Le comportement en cas d'erreur
5. ✅ La reprise du flux après une erreur

**Avantages:** ✅

-   Point unique de gestion des erreurs
-   Cohérence dans le traitement des erreurs
-   Facilité de maintenance et de débogage
-   Cette approche favorise :
    -   La séparation des responsabilités
    -   La réutilisation du code
    -   La maintenabilité

**Pourquoi l'utiliser:** 💡

-   Simplification du code client
-   Meilleure traçabilité des erreurs
-   Facilité d'évolution des stratégies de gestion d'erreurs

**Inconvénients:** ⚠️

-   Risque de point unique de défaillance
-   Peut masquer certains contextes d'erreur spécifiques
-   Nécessite une bonne conception initiale

![](https://raw.githubusercontent.com/giak/dam-dashboard/264ae70b68b94cb0d21da229f61afb66eff3404e/docs/assets/errors.svg)

### 3.5 Gestion des Erreurs dans l'Interface Utilisateur 🖥️

La gestion des erreurs ne se limite pas au backend, mais s'étend jusqu'à l'interface utilisateur avec des composants dédiés.

![](https://raw.githubusercontent.com/giak/dam-dashboard/264ae70b68b94cb0d21da229f61afb66eff3404e/docs/assets/errors_ui.svg)

#### Notification des Erreurs

##### Exemple Simple - Composant de Notification

```typescript
// Extrait de ErrorNotification.vue
<script setup lang="ts">
import { errorHandlingService, type ErrorDataInterface } from '@services/errorHandlingService';
import { Subscription } from 'rxjs';
import { onMounted, onUnmounted, ref } from 'vue';

const errors = ref<ErrorDataInterface[]>([]);
let subscription: Subscription | null = null;

onMounted(() => {
  subscription = errorHandlingService.getErrorObservable().subscribe(
    (error) => {
      errors.value.push(error);
      // Suppression automatique après 5 secondes
      setTimeout(() => {
        errors.value.shift();
      }, 5000);
    }
  );
});
```

Ce composant est spécialement conçu pour :

-   Afficher les erreurs de manière non intrusive
-   Gérer automatiquement la durée d'affichage
-   Permettre la fermeture manuelle des notifications

##### Exemple Avancé - Gestionnaire Global d'Erreurs

```typescript
// Extrait de GlobalErrorHandler.vue
<template>
  <ErrorNotification
    v-if="currentError"
    :message="currentError"
    @close="clearError"
  />
</template>

<script setup lang="ts">
import { errorHandlingService } from '@services/errorHandlingService';
import { onMounted, onUnmounted, ref } from 'vue';

const currentError = ref<string | null>(null);

let errorSubscription: any;

onMounted(() => {
  errorSubscription = errorHandlingService.getErrorObservable().subscribe(
    (error) => {
      if (error) {
        currentError.value = `${error.context}: ${error.message}`;
        setTimeout(() => {
          clearError();
        }, 5000);
      }
    }
  );
});
```

Cette implémentation offre :

-   Une gestion centralisée des erreurs au niveau UI
-   Un formatage cohérent des messages d'erreur
-   Une intégration transparente avec le service d'erreurs

## 4. Testing et Monitoring 🧪

### 4.1 Tests Unitaires avec RxJS 🎯

![](https://raw.githubusercontent.com/giak/dam-dashboard/264ae70b68b94cb0d21da229f61afb66eff3404e/docs/assets/test_unitaire.svg)

#### TestScheduler

Le TestScheduler est un outil puissant pour tester les Observables de manière déterministe.

##### Exemple Simple - Test de Flux Basique

```typescript
describe('WeatherSimulation', () => {
    let testScheduler: TestScheduler;

    beforeEach(() => {
        testScheduler = new TestScheduler((actual, expected) => {
            expect(actual).toEqual(expected);
        });
    });

    it('should buffer and debounce weather updates', () => {
        testScheduler.run(({ cold, expectObservable }) => {
            const source = cold('abcd|', {
                a: { temperature: 20 },
                b: { temperature: 21 },
                c: { temperature: 22 },
                d: { temperature: 23 },
            });
            // ... reste du code
        });
    });
});
```

```mermaid
sequenceDiagram
    participant Source as "cold source"
    participant Test as "TestScheduler"
    participant Expect as "expectObservable"

    Note over Source,Expect: Time →
    Note over Source: cold('abcd|')

    Source->>Test: temp: 20
    Note over Source: a
    Source->>Test: temp: 21
    Note over Source: b
    Source->>Test: temp: 22
    Note over Source: c
    Source->>Test: temp: 23
    Note over Source: d
    Source-xTest: complete
    Note over Source: Complete

    Note over Test: TestScheduler verifies marble diagram "abcd|"
    Note over Test: Each letter represents a temperature emission

    Test->>Expect: Compare actual vs expected
```

Ce diagramme montre :

1. Une source froide émettant une séquence de températures
2. Le TestScheduler qui contrôle le temps virtuel
3. Les émissions synchronisées avec le marble diagram "abcd|"
4. La comparaison finale des valeurs attendues vs réelles

Le diagramme illustre comment le TestScheduler permet de tester des flux asynchrones de manière synchrone et déterministe.

**Avantages:** ✅

-   Tests déterministes et reproductibles
-   Simulation du temps virtuel
-   Syntaxe déclarative pour les séquences d'événements

**Pourquoi l'utiliser:** 🤔

-   Pour tester des opérateurs temporels complexes
-   Pour vérifier les séquences d'émissions
-   Pour simuler différents scénarios temporels

**Inconvénients:** ⚠️

-   Courbe d'apprentissage importante
-   Syntaxe marble diagrams parfois complexe
-   Difficile à déboguer

##### Exemple Avancé - Test de Composant avec RxJS

```typescript
describe('MainWeatherStationComponent', () => {
    it('updates when data changes', async () => {
        const temperature$ = new BehaviorSubject(20.5);
        const precipitation$ = new BehaviorSubject(5.2);

        const wrapper = mount(MainWeatherStationComponent, {
            props: { mainWeatherState: mockMainWeatherStation },
        });

        temperature$.next(22.0);
        await wrapper.vm.$nextTick();
    });
});
```

```mermaid
sequenceDiagram
    participant Temp as "temperature$"
    participant Precip as "precipitation$"
    participant Comp as "MainWeatherStationComponent"
    participant Vue as "Vue nextTick"

    Note over Temp,Vue: Time →

    Note over Temp,Precip: Initialize BehaviorSubjects
    Temp->>Comp: "20.5"
    Precip->>Comp: "5.2"

    Note over Comp: Component Mounted

    Temp->>Comp: "22.0"
    Comp->>Vue: "Request UI Update"
    Vue-->>Comp: "UI Updated"

    Note over Comp: Assert temperature = 22.0

    Note over Temp,Vue: BehaviorSubject keeps latest value
```

Ce diagramme montre :

1. L'initialisation des BehaviorSubjects avec leurs valeurs initiales
2. Le montage du composant qui reçoit ces valeurs
3. L'émission d'une nouvelle valeur de température
4. Le cycle de mise à jour Vue.js avec `nextTick()`
5. La vérification de l'état final

Le diagramme met en évidence comment BehaviorSubject maintient l'état et comment Vue.js synchronise les mises à jour du DOM.

**Avantages:** ✅

-   Tests d'intégration Vue.js/RxJS
-   Vérification des mises à jour réactives
-   Simulation d'événements asynchrones

**Pourquoi l'utiliser:** 🎯

-   Pour tester l'intégration complète
-   Pour vérifier le cycle de vie des composants
-   Pour valider la réactivité du système

**Inconvénients:** ⚠️

-   Tests plus lents que les tests unitaires purs
-   Setup plus complexe
-   Possibles faux positifs dus à l'asynchronicité

### 4.2 Tests d'Intégration 🔄

![](https://raw.githubusercontent.com/giak/dam-dashboard/264ae70b68b94cb0d21da229f61afb66eff3404e/docs/assets/test_integration.svg)

#### Tests de Flux de Données

##### Exemple Simple - Test de Service

```typescript
describe('DamService', () => {
    it('should handle water level updates', async () => {
        const service = createDamService(initialData, aggregatedInflow$);
        service.updateDam({ currentWaterLevel: 75 });
        // ... reste du code
    });
});
```

**Avantages:** ✅

-   Validation du comportement global
-   Détection des problèmes d'intégration
-   Tests plus proches des cas réels

**Pourquoi l'utiliser:** 🎯

-   Pour vérifier les interactions entre services
-   Pour tester les scénarios complexes
-   Pour valider les flux de données complets

**Inconvénients:** ⚠️

-   Tests plus lents à exécuter
-   Plus difficiles à maintenir
-   Dépendances multiples à mocker

### 4.3 Monitoring des Performances 📊

![](https://raw.githubusercontent.com/giak/dam-dashboard/264ae70b68b94cb0d21da229f61afb66eff3404e/docs/assets/performance.svg)

#### Métriques RxJS

##### Exemple Simple - Mesures Basiques

```typescript
const monitoredWeatherData$ = weatherData$.pipe(
    tap({
        next: (data) => {
            metrics.recordValue('weather.update.value', data.temperature);
            metrics.recordTiming('weather.update.latency');
        },
    }),
);
```

**Avantages:** ✨

-   Mesures en temps réel
-   Détection précoce des problèmes
-   Données quantitatives sur les performances

**Pourquoi l'utiliser:** 🎯

-   Pour surveiller les performances
-   Pour identifier les goulots d'étranglement
-   Pour optimiser le système

**Inconvénients:** ⚠️

-   Impact sur les performances
-   Complexité accrue du code
-   Volume important de données à gérer

##### Exemple Avancé - Monitoring Complet

```typescript
function createPerformanceMonitor<T>(source$: Observable<T>) {
    return source$.pipe(
        timeInterval(),
        tap((interval) => {
            metrics.recordTiming('processing.time', interval.interval);
            metrics.recordMemory('heap.used', process.memoryUsage().heapUsed);
        }),
    );
}
```

**Avantages:** ✨

-   Monitoring complet du système
-   Métriques détaillées
-   Traçabilité des performances

**Pourquoi l'utiliser:** 🎯

-   Pour un suivi approfondi
-   Pour l'analyse des tendances
-   Pour la détection d'anomalies

**Inconvénients:** ⚠️

-   Overhead significatif
-   Complexité de configuration
-   Besoin de stockage important

### 4.4 Outils de debugging 🛠️

![](https://raw.githubusercontent.com/giak/dam-dashboard/264ae70b68b94cb0d21da229f61afb66eff3404e/docs/assets/debugging_tools.svg)

#### RxJS Debugging Map 🗺️

Le diagramme des flux RxJS aide à visualiser :

-   Les sources de données
-   Les transformations
-   Les points d'erreur potentiels
-   Les schedulers utilisés

```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
      'primaryColor': '#e3f2fd',
      'primaryTextColor': '#333',
      'primaryBorderColor': '#42a5f5',
      'lineColor': '#66bb6a',
      'secondaryColor': '#f3e5f5',
      'tertiaryColor': '#fff'
    }
  }
}%%

graph BT
    %% Styles
    classDef observable fill:#ffebee,stroke:#ef5350,stroke-width:2px,rx:5px;
    classDef subject fill:#e3f2fd,stroke:#42a5f5,stroke-width:2px,rx:5px;
    classDef operator fill:#e8f5e9,stroke:#66bb6a,stroke-width:2px,rx:5px;
    classDef scheduler fill:#fff3e0,stroke:#ffa726,stroke-width:2px,rx:5px;
    classDef component fill:#f3e5f5,stroke:#ab47bc,stroke-width:2px,rx:5px;
    classDef error fill:#ffebee,stroke:#ef5350,stroke-width:2px,rx:5px;

    %% Niveau 1 - Source de données
    subgraph WeatherSources ["Sources Météo (Niveau 1)"]
        WS((weatherSource$<br>Observable<br><b>weatherSimulation.ts</b>))
        WEA((weatherData$<br>ReplaySubject<1><br><b>weatherSimulation.ts</b>))
        SIM((simulation$<br>Observable<br><b>weatherSimulation.ts</b>))
    end

    %% Niveau 2 - Services
    subgraph WeatherServices ["Services Météo (Niveau 2)"]
        MS((mainWeatherState$<br>BehaviorSubject<br><b>weatherService.ts</b>))
        SS((subStations$<br>Observable Array<br><b>weatherService.ts</b>))
    end

    %% Niveau 3 - Composables
    subgraph WeatherComposables ["Composables (Niveau 3)"]
        TEMP((temperature$<br>BehaviorSubject<br><b>useMainWeatherStation.ts</b>))
        PREC((precipitation$<br>BehaviorSubject<br><b>useMainWeatherStation.ts</b>))
        SYS((systemState$<br>BehaviorSubject<br><b>useWaterSystem.ts</b>))
    end

    %% Niveau 4 - Composants Vue
    subgraph VueComponents ["Composants Vue (Niveau 4)"]
        UI1[/MainWeatherStationComponent.vue/]
        UI2[/WeatherDataDisplay.vue/]
        UI3[/WaterSystemDashboard.vue/]
    end

    %% Connexions entre niveaux
    WeatherSources --> WeatherServices
    WeatherServices --> WeatherComposables
    WeatherComposables --> VueComponents

    %% Application des styles
    class WS,WEA,SIM subject
    class MS,SS subject
    class TEMP,PREC,SYS subject
    class UI1,UI2,UI3 component

    %% Style des subgraphs
    style WeatherSources fill:#f8f9fa,stroke:#dee2e6,stroke-width:1px
    style WeatherServices fill:#f8f9fa,stroke:#dee2e6,stroke-width:1px
    style WeatherComposables fill:#f8f9fa,stroke:#dee2e6,stroke-width:1px
    style VueComponents fill:#f8f9fa,stroke:#dee2e6,stroke-width:1px
```

##### Exemple Simple - Visualisation des Flux

```typescript
// Extrait de weatherService.ts
const weatherData$ = weatherData.pipe(
    tap((data) => {
        debugLog('Raw weather data', data);
    }),
    map((data) => transformWeatherData(data)),
    tap((data) => {
        debugLog('Transformed weather data', data);
    }),
);
```

**Avantages:** ✅

-   Visualisation claire du flux de données
-   Identification rapide des problèmes
-   Facilite le débogage

**Pourquoi l'utiliser:** 🎯

-   Pour comprendre le chemin des données
-   Pour identifier les goulots d'étranglement
-   Pour valider les transformations

**Inconvénients:** ⚠️

-   Impact sur les performances en développement
-   Peut générer beaucoup de logs
-   Nécessite une configuration appropriée

![](https://raw.githubusercontent.com/giak/dam-dashboard/264ae70b68b94cb0d21da229f61afb66eff3404e/docs/assets/log_simple.svg)

#### Logging Détaillé 📝

##### Exemple Simple - Logging Basique

```typescript
// Extrait de weatherService.ts
tap({
    next: (data) => {
        loggingService.info('Weather data updated', 'weatherSimulation', { data });
    },
    error: (error) => {
        loggingService.error('Simulation error', 'weatherSimulation', { error });
    },
});
```

##### Exemple Avancé - Logging Multiniveau

```typescript
// Système de logging avancé
const createDebugOperator = <T>(context: string, level: LogLevel = 'debug') => {
    return tap({
        next: (value) => {
            asyncScheduler.schedule(
                () => {
                    loggingService[level](`${context}: Value processed`, context, {
                        value,
                        timestamp: Date.now(),
                        memory: process.memoryUsage(),
                    });
                },
                0,
                { priority: -1 },
            );
        },
        error: (error) => {
            loggingService.error(`${context}: Error occurred`, context, { error });
        },
        complete: () => {
            loggingService.info(`${context}: Stream completed`, context);
        },
    });
};
```

**Avantages:** ✨

-   Traçabilité complète des opérations
-   Différents niveaux de logging
-   Contexte riche pour le débogage

**Pourquoi l'utiliser:** 🎯

-   Pour le diagnostic des problèmes
-   Pour l'analyse des performances
-   Pour la maintenance du système

**Inconvénients:** ⚠️

-   Overhead de performance
-   Gestion de grands volumes de logs
-   Configuration complexe en production

![](https://raw.githubusercontent.com/giak/dam-dashboard/264ae70b68b94cb0d21da229f61afb66eff3404e/docs/assets/log_detaille.svg)

#### Debug Operators 🔍

##### Exemple Simple - Opérateur de Debug

```typescript
const debugOperator = <T>(tag: string) =>
    tap<T>({
        next: (value) => console.debug(`[${tag}] Next:`, value),
        error: (error) => console.error(`[${tag}] Error:`, error),
        complete: () => console.info(`[${tag}] Complete`),
    });

// Utilisation
weatherData$.pipe(
    debugOperator('WeatherStream'),
    // ... autres opérateurs
);
```

```mermaid
sequenceDiagram
    participant Source as "weatherData$"
    participant Debug as "debugOperator('WeatherStream')"
    participant Console as "Console Output"
    participant Next as "Next Operator"

    Note over Source,Next: Time →

    Source->>Debug: "{temp: 25}"
    Debug->>Console: "[WeatherStream] Next: {temp: 25}"
    Debug->>Next: "{temp: 25}"

    Source->>Debug: "{temp: 30}"
    Debug->>Console: "[WeatherStream] Next: {temp: 30}"
    Debug->>Next: "{temp: 30}"

    Source-xDebug: "Error!"
    Debug->>Console: "[WeatherStream] Error: Error!"

    Source->>Debug: "Complete"
    Debug->>Console: "[WeatherStream] Complete"

    Note over Debug: tap() operator
    Note over Console: Debug logs
```

Ce diagramme illustre :

1. La source `weatherData$` émettant des valeurs
2. Le `debugOperator` interceptant chaque événement (next/error/complete)
3. Les logs dans la console pour chaque type d'événement
4. La transmission transparente des valeurs à l'opérateur suivant
5. La gestion des erreurs et de la complétion

Le diagramme montre comment le debugOperator agit comme un "espion" qui observe et log le flux sans le modifier.

##### Exemple Avancé - Debug Conditionnel

```typescript
const createConditionalDebug = <T>(context: string, condition: (value: T) => boolean) => {
    return tap<T>({
        next: (value) => {
            if (condition(value)) {
                console.debug(`[${context}] Condition matched:`, value);
                debugger; // Point d'arrêt conditionnel
            }
        },
    });
};

// Utilisation
weatherData$.pipe(createConditionalDebug('Temperature', (data) => data.temperature > THRESHOLD));
```

```mermaid
sequenceDiagram
    participant Source as "weatherData$"
    participant Debug as "createConditionalDebug()"
    participant Console as "console.debug"
    participant Result as "output$"

    Note over Source,Result: Time →
    Note over Debug: condition: temp > THRESHOLD (20°C)

    Source->>Debug: "{temperature: 18}"
    Debug->>Result: "{temperature: 18}"
    Note over Debug: Skip (18 ≤ 20)

    Source->>Debug: "{temperature: 25}"
    Debug->>Console: "[Temperature] Condition matched: {temperature: 25}"
    Debug->>Result: "{temperature: 25}"
    Note over Debug: Debug (25 > 20)

    Source->>Debug: "{temperature: 19}"
    Debug->>Result: "{temperature: 19}"
    Note over Debug: Skip (19 ≤ 20)

    Source->>Debug: "{temperature: 22}"
    Debug->>Console: "[Temperature] Condition matched: {temperature: 22}"
    Debug->>Result: "{temperature: 22}"
    Note over Debug: Debug (22 > 20)

    Note over Debug: tap() operator with conditional logging
```

Ce diagramme illustre :

1. La source `weatherData$` émettant des températures
2. L'opérateur `createConditionalDebug()` qui :
    - Laisse passer toutes les valeurs
    - Log uniquement celles dépassant le seuil (THRESHOLD)
3. Les appels conditionnels à `console.debug`
4. Le flux de sortie inchangé

**Avantages:** ✅

-   Débogage ciblé et conditionnel
-   Intégration naturelle dans les pipelines RxJS
-   Facilement activable/désactivable

**Pourquoi l'utiliser:** 🎯

-   Pour le débogage en développement
-   Pour la surveillance de conditions spécifiques
-   Pour l'analyse des flux de données

**Inconvénients:** ⚠️

-   Peut affecter les performances
-   Risque d'oubli en production
-   Nécessite une bonne gestion des environnements

![](https://raw.githubusercontent.com/giak/dam-dashboard/264ae70b68b94cb0d21da229f61afb66eff3404e/docs/assets/debug_operator.svg)

### 4.5 Tests des Composants Vue.js avec RxJS 🧪

#### Tests des Composants avec Observables 🔄

##### Exemple Simple - Test de Souscription

```typescript
// Extrait de MainWeatherStationComponent.spec.ts
describe('MainWeatherStationComponent', () => {
    it('renders properly', () => {
        const mockSubStation: WeatherStationInterface = {
            id: '1',
            name: 'Sub 1',
            latitude: 45.5 as any,
            longitude: -73.5 as any,
            elevation: 100,
            weatherData$: new Observable(),
            cleanup: vi.fn(),
        };

        const mockMainWeatherStation: MainWeatherStationInterface = {
            id: '1',
            name: 'Test Station',
            subStations: [mockSubStation],
            averageTemperature: computed(() => 20.5),
            totalPrecipitation: computed(() => 5.2),
            lastUpdate: computed(() => new Date('2023-01-01T12:00:00')),
            temperature$: new BehaviorSubject(20.5),
            precipitation$: new BehaviorSubject(5.2),
        };

        const wrapper = mount(MainWeatherStationComponent, {
            props: {
                mainWeatherState: mockMainWeatherStation,
            },
        });

        expect(wrapper.text()).toContain('20.5');
        expect(wrapper.text()).toContain('5.20');
    });
});
```

```mermaid
sequenceDiagram
    participant SubStation as "mockSubStation"
    participant MainStation as "mockMainWeatherStation"
    participant Component as "MainWeatherStationComponent"
    participant Template as "Template"

    Note over SubStation,Template: Test Setup & Rendering Flow

    SubStation->>MainStation: Create with weatherData$

    Note over MainStation: Initialize BehaviorSubjects
    Note right of MainStation: temperature$: 20.5<br/>precipitation$: 5.2

    MainStation->>Component: Mount with props

    Note over Component: Component Mounted

    Component->>Template: Render initial values

    Note over Template: Assertions
    Note right of Template: Contains "20.5"<br/>Contains "5.20"

    Note over SubStation,Template: Reactive Data Flow

    MainStation-->>Component: temperature$ (20.5)
    Component-->>Template: Update temperature

    MainStation-->>Component: precipitation$ (5.2)
    Component-->>Template: Update precipitation

    Note over Component: Test Validation
```

Ce diagramme montre :

1. La création du mock de sous-station
2. L'initialisation de la station principale avec les BehaviorSubjects
3. Le montage du composant avec les props
4. Le flux de données réactif vers le template
5. Les assertions de test vérifiant le rendu

Le diagramme met en évidence comment les BehaviorSubjects alimentent le composant en données et comment ces données sont rendues dans le template.

Ce test est conçu pour :

-   Vérifier le rendu initial des données
-   Tester l'intégration Vue.js/RxJS
-   Valider l'affichage des valeurs formatées

##### Exemple Avancé - Test de Mise à Jour Réactive

```typescript
// Extrait de MainWeatherStationComponent.spec.ts
it('updates when data changes', async () => {
    const temperature$ = new BehaviorSubject(20.5);
    const precipitation$ = new BehaviorSubject(5.2);

    const mockMainWeatherStation: MainWeatherStationInterface = {
        id: '1',
        name: 'Test Station',
        subStations: [],
        averageTemperature: computed(() => 20.5),
        totalPrecipitation: computed(() => 5.2),
        lastUpdate: computed(() => new Date('2023-01-01T12:00:00')),
        temperature$,
        precipitation$,
    };

    const wrapper = mount(MainWeatherStationComponent, {
        props: {
            mainWeatherState: mockMainWeatherStation,
        },
    });

    // Test initial values
    expect(wrapper.text()).toContain('20.5');
    expect(wrapper.text()).toContain('5.20');

    // Update values through RxJS subjects
    temperature$.next(22.0);
    precipitation$.next(6.5);
    await wrapper.vm.$nextTick();

    // Test updated values
    expect(wrapper.text()).toContain('22.0');
    expect(wrapper.text()).toContain('6.50');
});
```

```mermaid
sequenceDiagram
    participant Temp as "temperature$"
    participant Prec as "precipitation$"
    participant Comp as "MainWeatherStationComponent"
    participant View as "Vue Template"

    Note over Temp,View: Time →

    Note over Temp,Prec: Initial Values
    Temp->>Comp: "20.5"
    Prec->>Comp: "5.2"
    Comp->>View: "Display: 20.5°C, 5.20mm"

    Note over Temp,View: Component Mounted

    Note over Temp,View: Update Values
    Temp->>Comp: "22.0"
    Comp->>View: "Display: 22.0°C, 5.20mm"
    Prec->>Comp: "6.5"
    Comp->>View: "Display: 22.0°C, 6.50mm"

    Note over View: Vue nextTick

    Note over Comp: Assertions
    Note over View: Verify "22.0" and "6.50"

    Note over Temp,View: Test Complete
```

Ce diagramme montre :

1. L'initialisation des BehaviorSubjects avec leurs valeurs initiales
2. Le montage du composant et l'affichage initial
3. Les mises à jour successives via `.next()`
4. L'attente du cycle de rendu Vue avec `nextTick`
5. La vérification des nouvelles valeurs dans le template

Le diagramme illustre bien le flux asynchrone et la réactivité entre les BehaviorSubjects et le composant Vue.

Cette approche permet de tester :

-   La réactivité des composants
-   La propagation des mises à jour
-   Le cycle de vie des souscriptions

#### Tests des Composants d'Erreur ⚠️

##### Exemple Simple - Test de Notification d'Erreur

```typescript
// Extrait de ErrorNotification.spec.ts
describe('ErrorNotification', () => {
    it('displays and removes errors correctly', async () => {
        const wrapper = mount(ErrorNotification);
        const errorService = errorHandlingService;

        // Émettre une erreur
        errorService.emitError({
            message: 'Test Error',
            context: 'TestComponent',
            timestamp: Date.now(),
        });

        await wrapper.vm.$nextTick();
        expect(wrapper.text()).toContain('Test Error');
        expect(wrapper.text()).toContain('TestComponent');

        // Attendre la suppression automatique
        await new Promise((resolve) => setTimeout(resolve, 5000));
        await wrapper.vm.$nextTick();
        expect(wrapper.text()).not.toContain('Test Error');
    });
});
```

```mermaid
sequenceDiagram
    participant Test as "Test"
    participant Service as "errorService"
    participant Component as "ErrorNotification"
    participant UI as "UI State"

    Note over Test,UI: Time →

    Test->>Component: mount()
    Test->>Service: emitError()
    Service->>Component: {message: "Test Error", context: "TestComponent"}
    Component->>UI: Display Error
    Note over UI: Error Visible

    Note over Test,UI: Wait 5000ms

    Component->>UI: Remove Error
    Note over UI: Error Removed

    Note over Component: Lifecycle:
    Note over Component: 1. Mount
    Note over Component: 2. Display Error
    Note over Component: 3. Auto-remove after 5s
```

Ce diagramme montre :

1. Le montage du composant
2. L'émission d'une erreur via le service
3. L'affichage de l'erreur dans l'UI
4. La période d'attente de 5000ms
5. La suppression automatique de l'erreur

Le diagramme met en évidence le flux asynchrone et le comportement temporel du composant de notification d'erreur.

Ce test vérifie :

-   L'affichage des erreurs
-   La suppression automatique
-   L'intégration avec le service d'erreurs

Ces tests assurent que nos composants Vue.js interagissent correctement avec les flux RxJS et maintiennent un état cohérent tout au long de leur cycle de vie.

# Analyse Détaillée de l'Architecture RxJS/Vue.js 🏗️

## Introduction : WaterSystemDashboard comme Point de Convergence 🎯

Le composant WaterSystemDashboard.vue représente un élément architectural crucial dans notre application. Il agit comme le point de convergence entre la logique métier (use cases) et la couche de présentation, illustrant parfaitement l'intégration entre RxJS et Vue.js.

![](https://raw.githubusercontent.com/giak/dam-dashboard/264ae70b68b94cb0d21da229f61afb66eff3404e/docs/assets/waterSystemDashboard.svg)

### Rôle Stratégique 🔑

Ce composant :

-   Orchestre les flux de données de multiples sources (barrages, glaciers, rivières, stations météo)
-   Gère la synchronisation entre l'état global et l'interface utilisateur
-   Assure la cohérence des données à travers l'application

### Point d'Intégration 🔄

En tant que "nœud gordien", il :

1. **Agrège les Données** : Centralise les flux RxJS de différentes sources
2. **Coordonne l'Affichage** : Distribue les données aux composants enfants
3. **Gère le Cycle de Vie** : Synchronise les souscriptions avec Vue.js

Cette position stratégique en fait un excellent cas d'étude pour comprendre l'architecture globale du système.

## 1. Système d'État et Réactivité ⚡

### État Central avec TypeScript 📦

```typescript
const systemState = ref<SystemStateInterface>({
    dam: null,
    glacier: null,
    river: null,
    mainWeather: null,
});
```

#### Comment ça fonctionne ?

1. Un ref Vue.js contient l'état global
2. L'interface SystemStateInterface définit la structure
3. Chaque sous-système peut être null initialement

#### Pourquoi cette approche ?

-   **Single Source of Truth** : Un seul endroit pour l'état global
-   **Type Safety** : TypeScript garantit la cohérence des données
-   **Initialisation Lazy** : Les composants peuvent être chargés progressivement

#### Avantages/Inconvénients

✅ Avantages :

-   Maintenance simplifiée 🛠️
-   Débogage facilité 🔍
-   Typage fort 💪

❌ Inconvénients :

-   Structure rigide
-   Possible sur-engineering pour petites applications
-   Overhead initial

### Données Dérivées 🔄

```typescript
const averageTemperature = computed(() => systemState.value.mainWeather?.averageTemperature || 0);
const totalPrecipitation = computed(() => systemState.value.mainWeather?.totalPrecipitation || 0);

const computedMainWeatherState = computed((): MainWeatherStationInterface | null => {
    if (!systemState.value.mainWeather) return null;
    return {
        ...systemState.value.mainWeather,
        averageTemperature: averageTemperature.value,
        totalPrecipitation: totalPrecipitation.value,
    };
});
```

#### Comment ça fonctionne ?

1. Les computed properties dérivent des données de l'état principal
2. Utilisation de l'optional chaining (?.) pour la sécurité
3. Valeurs par défaut pour éviter les undefined

#### Pourquoi cette approche ?

-   **Performance** : Calculs mis en cache et réactifs
-   **DRY** : ��vite la duplication de logique
-   **Encapsulation** : Cache la complexité des calculs

#### Avantages/Inconvénients

✅ Avantages :

-   Mises à jour automatiques
-   Code plus maintenable
-   Meilleure performance

❌ Inconvénients :

-   Complexité accrue
-   Dépendances potentiellement difficiles à tracer
-   Risque de sur-optimisation

## 2. Gestion du Cycle de Vie ⚙️

### Initialisation et Souscriptions

```typescript
onMounted(() => {
    const subscription = waterSystem.systemState$.subscribe({
        next: (state) => {
            systemState.value = state;
        },
        error: (err) => {
            console.error('Error in systemState$ subscription', err);
        },
    });

    const errorSubscription = errorHandlingService.getErrorObservable().subscribe((error) => {
        if (error) {
            console.error('Water system error', error);
        }
    });

    onUnmounted(() => {
        subscription.unsubscribe();
        errorSubscription.unsubscribe();
        waterSystem.cleanup();
    });
});
```

#### Comment ça fonctionne ?

1. Les souscriptions sont créées au montage du composant
2. Deux flux distincts : état système et erreurs
3. Nettoyage explicite lors du démontage

#### Pourquoi cette approche ?

-   **Gestion des Ressources** : Évite les fuites mémoire
-   **Séparation des Préoccupations** : État et erreurs séparés
-   **Cycle de Vie Contrôlé** : Synchronisation avec Vue.js

#### Avantages/Inconvénients

✅ Avantages :

-   Pas de fuites mémoire
-   Code prévisible
-   Facilité de test

❌ Inconvénients :

-   Verbosité du code
-   Risque d'oubli du cleanup
-   Complexité de la gestion d'erreur

## 3. Architecture des Composants 🏛️

### Rendu Conditionnel et Props

```typescript
<template>
  <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
    <DamComponent v-if="systemState.dam" :damState="systemState.dam" />
    <GlacierComponent
      v-if="systemState.glacier"
      :glacierState="systemState.glacier"
      :currentTemperature="averageTemperature"
    />
  </div>
</template>
```

#### Comment ça fonctionne ?

1. Rendu conditionnel avec v-if
2. Props typées pour chaque composant
3. Données dérivées passées aux composants enfants

#### Pourquoi cette approche ?

-   **Performance** : Rendu uniquement si données disponibles
-   **Isolation** : Chaque composant gère ses propres données
-   **Réutilisabilité** : Composants indépendants

#### Avantages/Inconvénients

✅ Avantages :

-   Code modulaire
-   Performance optimisée
-   Maintenance facilitée

❌ Inconvénients :

-   Possible flickering UI
-   Complexité des props
-   Dépendances implicites

Cette architecture combine efficacement RxJS et Vue.js, créant un système robuste mais qui nécessite une bonne compréhension des deux technologies pour être maintenu efficacement.

## 4. BehaviorSubjects dans les Composants Météo 🌡️

### Introduction aux BehaviorSubjects 📡

Les BehaviorSubjects représentent un type spécial d'Observable dans RxJS, particulièrement adapté à la gestion d'état et aux données en temps réel. Contrairement aux Subjects classiques, ils maintiennent et émettent leur dernière valeur aux nouveaux abonnés.

![](https://raw.githubusercontent.com/giak/dam-dashboard/264ae70b68b94cb0d21da229f61afb66eff3404e/docs/assets/behaviorSubjectMeteo.svg)

#### Caractéristiques Fondamentales 🎯

-   Conserve toujours une valeur courante
-   Émet immédiatement la valeur actuelle aux nouveaux abonnés
-   Permet la modification de la valeur via `.next()`
-   Maintient un état interne

#### Pourquoi les BehaviorSubjects ? 🤔

1. **Gestion d'État**

    - Idéal pour les valeurs qui changent dans le temps
    - Parfait pour les données de capteurs en temps réel
    - Garantit toujours une valeur disponible

2. **Réactivité**
    - Propagation instantanée des changements
    - Synchronisation automatique des composants
    - Cohérence des données garantie

#### Avantages et Inconvénients Généraux

✅ Avantages :

-   **Valeur Initiale Garantie**

    -   Pas de conditions de course
    -   UX améliorée (pas d'état de chargement initial)
    -   Simplicité de l'implémentation

-   **Synchronisation**

    -   Mise à jour atomique
    -   Propagation fiable
    -   État cohérent

-   **Flexibilité**
    -   Compatible avec les opérateurs RxJS
    -   Transformable via pipe()
    -   Intégration facile avec Vue.js

❌ Inconvénients :

-   **Consommation Mémoire**

    -   Garde toujours la dernière valeur
    -   Nécessite une gestion explicite de la mémoire
    -   Potentiel impact sur les performances

-   **Complexité**

    -   Concept plus avancé que les props Vue
    -   Nécessite une bonne compréhension de RxJS
    -   Peut compliquer le debugging

-   **Gestion du Cycle de Vie**
    -   Nécessite un unsubscribe explicite
    -   Risque de fuites mémoire
    -   Complexité accrue des tests

### Vue d'Ensemble du Système Météo 🌍

Le système météo utilise les BehaviorSubjects pour gérer les données en temps réel des stations météorologiques. Examinons l'implémentation dans MainWeatherStationComponent :

```typescript
interface MainWeatherStationInterface {
    id: string;
    name: string;
    subStations: WeatherStationInterface[];
    averageTemperature: ComputedRef<number>;
    totalPrecipitation: ComputedRef<number>;
    lastUpdate: ComputedRef<Date>;
    temperature$: BehaviorSubject<number>;
    precipitation$: BehaviorSubject<number>;
}
```

### Pattern de Données Réactives 🔄

#### Implémentation des BehaviorSubjects

```typescript
const mockMainWeatherStation: MainWeatherStationInterface = {
    id: '1',
    name: 'Test Station',
    subStations: [],
    averageTemperature: computed(() => 20.5),
    totalPrecipitation: computed(() => 5.2),
    lastUpdate: computed(() => new Date()),
    temperature$: new BehaviorSubject(20.5),
    precipitation$: new BehaviorSubject(5.2),
};
```

#### Comment ça fonctionne ?

1. **État Initial Garanti**

    - Les BehaviorSubjects sont initialisés avec des valeurs par défaut
    - Les nouveaux abonnés reçoivent immédiatement la dernière valeur
    - Pas de condition de course possible

2. **Flux de Données Bidirectionnel**

    - Les BehaviorSubjects peuvent émettre et recevoir des valeurs
    - Permettent la mise à jour depuis différentes sources
    - Maintiennent un historique de la dernière valeur

3. **Intégration Vue.js**
    - Les computed properties dérivent des BehaviorSubjects
    - Réactivité automatique lors des mises à jour
    - Pattern Observer/Observable transparent

### Avantages de cette Architecture 🌟

✅ Points Forts :

-   **Temps Réel Efficace** ⚡

    -   Mises à jour instantanées
    -   Pas de délai d'initialisation
    -   Gestion optimisée de la mémoire

-   **Maintenance Simplifiée** 🛠️

    -   Séparation claire des responsabilités
    -   Code prévisible et testable
    -   Facilité d'extension

-   **Performance** 🚀
    -   Pas de calculs inutiles
    -   Mise en cache automatique
    -   Partage efficace des données

### Défis et Solutions ⚔️

❌ Défis :

1. **Gestion de la Mémoire** 💾

    - Problème : Les BehaviorSubjects conservent la dernière valeur
    - Solution : Nettoyage explicite dans onUnmounted
    - Impact : Légère consommation mémoire supplémentaire

2. **Complexité du Code** 📝

    - Problème : Pattern plus complexe qu'une simple prop
    - Solution : Encapsulation dans des composables
    - Impact : Courbe d'apprentissage initiale

3. **Synchronisation** 🔄
    - Problème : Multiples sources de vérité
    - Solution : Architecture unidirectionnelle
    - Impact : Besoin de documentation claire

### Exemple de Simulation Météo avec BehaviorSubjects 🌤️

Le système de simulation météo offre un autre exemple intéressant d'utilisation des BehaviorSubjects pour la simulation de données en temps réel :

```typescript
// Dans weatherSimulation.ts
export function createWeatherSimulation(): WeatherSimulationInterface {
    const weatherData = new ReplaySubject<WeatherDataInterface>(1);

    const weatherData$ = weatherData.pipe(
        observeOn(asyncScheduler),
        bufferTime(100, undefined, 5),
        filter((updates): updates is WeatherDataInterface[] => updates.length > 0),
        map((updates) => updates[updates.length - 1]),
        distinctUntilKeyChanged('temperature'),
        debounceTime(100),
        share(),
    );

    return {
        weatherData$,
        start: () => startSimulation(weatherData),
        stop: () => weatherData.complete(),
    };
}
```

#### Points Clés de cette Implémentation

1. **Gestion du Flux de Données**

    - Utilisation de ReplaySubject pour la mise en mémoire tampon
    - Transformation via pipe() pour optimiser les mises à jour
    - Contrôle fin du flux avec les opérateurs RxJS

2. **Optimisations**

    - `bufferTime` : Regroupe les mises à jour fréquentes
    - `distinctUntilKeyChanged` : Évite les mises à jour redondantes
    - `debounceTime` : Limite la fréquence des émissions

3. **Intégration avec le Composable**

```typescript
// Dans useWeatherStation.ts
export function useWeatherStation(config: WeatherStationConfig) {
    const { weatherData$, start, stop } = createWeatherSimulation();

    const stationState = ref<WeatherStationState>({
        temperature: 0,
        precipitation: 0,
        lastUpdate: new Date(),
    });

    onMounted(() => {
        const subscription = weatherData$.subscribe((data) => {
            stationState.value = {
                ...stationState.value,
                ...data,
                lastUpdate: new Date(),
            };
        });

        start();

        onUnmounted(() => {
            subscription.unsubscribe();
            stop();
        });
    });

    return {
        stationState,
        cleanup: stop,
    };
}
```

#### Avantages de cette Architecture

1. **Simulation Réaliste**

    - Génération de données proche du temps réel
    - Contrôle précis des intervalles de mise à jour
    - Possibilité de simuler différents scénarios

2. **Performance**

    - Optimisation des mises à jour via bufferTime
    - Réduction de la charge via distinctUntilKeyChanged
    - Gestion efficace de la mémoire

3. **Maintenabilité**
    - Séparation claire simulation/utilisation
    - Interface cohérente
    - Facilité de test

Cette implémentation montre comment les BehaviorSubjects et autres Subjects de RxJS peuvent être utilisés pour créer un système de simulation sophistiqué tout en maintenant une architecture propre et performante.

### Exemple Avancé : Tests avec BehaviorSubjects 🧪

Le fichier useDam.spec.ts montre comment les BehaviorSubjects sont utilisés pour tester les flux de données :

```typescript
describe('useDam', () => {
    let aggregatedInflow$: BehaviorSubject<AggregatedInflowInterface>;

    beforeEach(() => {
        // Initialisation avec une valeur par défaut
        aggregatedInflow$ = new BehaviorSubject<AggregatedInflowInterface>({
            totalInflow: 100,
            sources: { TestSource: 100 },
        });
    });

    it('should update dam state correctly', async () => {
        const { updateDam, damState$ } = useDam(initialData, aggregatedInflow$);
        const update = { currentWaterLevel: 60, outflowRate: 90 };

        updateDam(update);
        const state = await firstValueFrom(damState$);

        expect(state.currentWaterLevel).toBe(update.currentWaterLevel);
        expect(state.outflowRate).toBe(update.outflowRate);
    });
});
```

#### Points Clés de cet Exemple

1. **Test Synchrone** ⚡

    - BehaviorSubject permet des tests synchrones
    - Valeur immédiatement disponible
    - Pas besoin d'attendre une émission

2. **Simulation de Données** 🔄

    - Parfait pour simuler des flux de données réels
    - Contrôle total sur les émissions
    - Facilite les scénarios de test

3. **Vérification d'État** ✅
    - Accès direct à la dernière valeur
    - Assertions simplifiées
    - Tests plus robustes

Cet exemple montre comment les BehaviorSubjects facilitent non seulement le développement mais aussi les tests, en fournissant un moyen prévisible et contrôlable de gérer les flux de données.

## Glossaire RxJS

### Concepts Fondamentaux

#### Observable

> Représente une collection de valeurs ou d'événements futurs. C'est le bloc de construction fondamental de RxJS, permettant de travailler avec des données asynchrones comme si elles étaient synchrones.

**Exemple dans notre application :**

```typescript
const weatherData$ = weatherData.pipe(map((data) => normalizeWeatherData(data)));
```

#### Subject

> Un type spécial d'Observable qui permet la multicasting et peut émettre des valeurs à la demande. Agit à la fois comme un Observable et un Observer.

**Exemple dans notre application :**

```typescript
const destroy$ = new Subject<void>();
```

#### BehaviorSubject

> Un Subject qui conserve la dernière valeur émise et la fournit immédiatement aux nouveaux abonnés.

**Exemple dans notre application :**

```typescript
const mainWeatherState$ = new BehaviorSubject<MainWeatherStationInterface>(initialState);
```

### Opérateurs Essentiels

#### pipe()

> Méthode permettant de chaîner plusieurs opérateurs pour transformer un flux de données.

```typescript
weatherData$.pipe(
    debounceTime(100),
    map((data) => transformData(data)),
);
```

#### map()

> Transforme chaque valeur émise selon une fonction donnée.

```typescript
map((data) => ({ ...data, temperature: normalizeTemperature(data.temperature) }));
```

#### filter()

> Ne laisse passer que les valeurs qui satisfont une condition.

```typescript
filter((data): data is WeatherData => isValidWeatherData(data));
```

### Patterns de Combinaison

#### combineLatest()

> Combine plusieurs Observables en émettant un tableau des dernières valeurs dès qu'un des Observables émet.

```typescript
combineLatest({
    weather: weatherData$,
    dam: damState$,
});
```

#### merge()

> Fusionne plusieurs Observables en un seul.

```typescript
merge(mainStation$, subStations$);
```

### Gestion des Erreurs

#### catchError()

> Gère les erreurs dans un flux Observable.

```typescript
catchError((error) => {
    errorHandlingService.handleError(error);
    return EMPTY;
});
```

#### retry()

> Réessaie une opération en cas d'erreur.

```typescript
retry({
    count: 3,
    delay: (error, retryCount) => timer(Math.pow(2, retryCount) * 1000),
});
```

### Optimisation

#### shareReplay()

> Partage un Observable entre plusieurs abonnés et rejoue les dernières valeurs.

```typescript
shareReplay({
    bufferSize: 1,
    refCount: true,
});
```

#### debounceTime()

> Limite la fréquence des émissions.

```typescript
debounceTime(100);
```

### Nettoyage

#### takeUntil()

> Émet des valeurs jusqu'à ce qu'un autre Observable émette.

```typescript
takeUntil(destroy$);
```

#### finalize()

> Exécute une action lors de la complétion ou de l'erreur.

```typescript
finalize(() => cleanup());
```

### Conventions de Nommage

-   **Suffixe $** : Indique qu'une variable est un Observable

    ```typescript
    const weather$ = new Observable<WeatherData>();
    ```

-   **Suffixe Subject** : Indique un type de Subject

    ```typescript
    const weatherSubject = new BehaviorSubject<WeatherData>(initialData);
    ```

-   **Suffixe $$** : Indique un Subject ou un BehaviorSubject (convention alternative)
    ```typescript
    const weather$$ = new BehaviorSubject<WeatherData>(initialData);
    ```

### Schedulers

#### asyncScheduler

> Utilisé pour exécuter des tâches de manière asynchrone, idéal pour les opérations non bloquantes.

**Exemple dans notre application :**

```typescript
// Dans weatherSimulation.ts
const weatherData$ = weatherData.pipe(observeOn(asyncScheduler), bufferTime(100, undefined, 5));
```

#### queueScheduler

> Exécute les tâches de manière synchrone dans une file d'attente.

**Exemple dans notre application :**

```typescript
// Dans useDam.ts
const damState$ = combineLatest([...]).pipe(
  observeOn(queueScheduler),
  map(calculateState)
);
```

### Opérateurs de Transformation Avancés

#### distinctUntilKeyChanged()

> Filtre les émissions basées sur un changement de propriété spécifique.

**Exemple dans notre application :**

```typescript
// Dans weatherSimulation.ts
weatherData$.pipe(distinctUntilKeyChanged('temperature'));
```

#### bufferTime()

> Regroupe les émissions dans un tableau pendant une période donnée.

**Exemple dans notre application :**

```typescript
// Dans useWeatherStation.ts
const measurements = weatherData$.pipe(bufferTime(100, undefined, 5));
```

### Patterns Spécifiques

#### Pattern de Simulation

> Utilisation de ReplaySubject pour simuler des données en temps réel.

**Exemple dans notre application :**

```typescript
// Dans weatherSimulation.ts
const weatherData = new ReplaySubject<WeatherDataInterface>(1);
```

#### Pattern de Cleanup Vue.js

> Combinaison de Subject avec le cycle de vie de Vue.

**Exemple dans notre application :**

```typescript
// Dans useMainWeatherStation.ts
const destroy$ = new Subject<void>();
onUnmounted(() => {
    destroy$.next();
    destroy$.complete();
});
```

Ce glossaire enrichi couvre tous les concepts RxJS utilisés dans notre application de démonstration, avec des exemples concrets tirés de notre code source.

# Conclusion

## Retour d'Expérience sur l'Intégration RxJS/Vue.js

Cette étude de cas sur un système de gestion hydraulique nous a permis d'explorer l'intégration de RxJS avec Vue.js.
Voici les principaux enseignements :

### Points Clés

1. **Architecture Réactive Robuste**

    - La combinaison RxJS/Vue.js offre une base solide pour les applications temps réel
    - Les patterns réactifs facilitent la gestion des flux de données complexes
    - L'architecture en couches permet une séparation claire des responsabilités

2. **Compromis et Considérations**

    - La courbe d'apprentissage de RxJS peut être abrupte
    - La complexité accrue doit être justifiée par les besoins du projet
    - L'overhead initial en termes de setup et configuration

3. **Bonnes Pratiques Émergentes**
    - Utilisation systématique des Schedulers pour la performance
    - Patterns de cleanup rigoureux pour éviter les fuites mémoire
    - Stratégies de test adaptées à la programmation réactive

### Disclaimers Potentiels

1. **Complexité**

    - Cette approche peut être excessive pour des projets simples
    - Nécessite une équipe expérimentée en programmation réactive
    - Peut impacter la maintenabilité si mal implémentée

2. **Performance**

    - L'overhead de RxJS doit être pris en compte
    - Les optimisations nécessitent une bonne compréhension des Schedulers
    - Le debugging peut devenir complexe, voir horriblement complexe

3. **Alternatives**
    - D'autres approches peuvent être plus appropriées selon le contexte
    - Vue.js seul peut suffire pour des cas simples avec Pinia
    - Considérer des solutions plus légères pour des projets moins complexes

### Recommandations Pratiques

1. **Avant d'Adopter RxJS**

    - ✅ Évaluer si votre application nécessite réellement une gestion complexe de flux de données
    - ✅ S'assurer que l'équipe est prête à investir dans la formation RxJS
    - ❌ Ne pas l'utiliser uniquement parce que c'est "tendance"
    - ❌ Éviter sur des projets avec une équipe junior ou des deadlines serrées

2. **Architecture et Organisation**

    - ✅ Créer une couche d'abstraction entre RxJS et Vue.js via des composables
    - ✅ Centraliser la logique RxJS dans des services dédiés
    - ✅ Documenter exhaustivement les flux de données complexes
    - ❌ Ne pas mélanger la logique RxJS directement dans les composants
    - ❌ Éviter les dépendances circulaires entre observables

3. **Performance et Maintenance**

    - ✅ Mettre en place des outils de monitoring des observables
    - ✅ Implémenter des tests unitaires pour chaque flux
    - ✅ Utiliser les opérateurs de performance (shareReplay, debounceTime)
    - ❌ Ne pas négliger le cleanup des souscriptions
    - ❌ Éviter les chaînes d'opérateurs trop longues

4. **Patterns de Développement**

    - ✅ Favoriser les BehaviorSubjects pour l'état
    - ✅ Utiliser les Schedulers appropriés selon le contexte
    - ✅ Implémenter une gestion d'erreur robuste
    - ❌ Ne pas abuser des Subjects
    - ❌ Éviter les side effects dans les pipes

5. **Debugging et Monitoring**

    - ✅ Mettre en place des outils de debug spécialisés
    - ✅ Utiliser des opérateurs de debug (tap) en développement
    - ✅ Implémenter des métriques de performance
    - ❌ Ne pas laisser les opérateurs de debug en production
    - ❌ Éviter les logs verbeux qui impactent les performances

6. **Documentation**
    - ✅ Maintenir une documentation des patterns utilisés
    - ✅ Créer des exemples concrets
    - ❌ Ne pas supposer que RxJS est intuitif
    - ❌ Éviter la documentation obsolète

### Réflexion Personnelle sur cet Article

#### Contexte de Rédaction

Cet article est le fruit d'une expérience approfondie sur un projet d'envergure dans le secteur hydraulique, s'étalant sur plus de deux ans. La rédaction elle-même a nécessité environ 40 heures de travail, incluant :

-   La création d'une application de démonstration
-   La synthèse des apprentissages
-   La création des diagrammes explicatifs
-   La simplification des concepts complexes
-   La validation des patterns présentés

#### Base Expérimentale

Les patterns et recommandations présentés ici proviennent :

-   D'innombrables itérations et refactorisations
-   De multiples sessions de debugging complexes
-   De retours d'une équipe de 5 développeurs
-   De situations de production réelles
-   D'échecs et de succès concrets

#### Limitations et Perspectives

1. **Une Approche Parmi d'Autres**

    - Cette architecture représente une solution possible, non LA solution
    - Elle résulte de nos contraintes et contexte spécifiques
    - D'autres approches peuvent être tout aussi valides, voire plus adaptées selon les cas

2. **Évolution Continue**

    - Les patterns présentés continuent d'évoluer
    - De nouvelles solutions émergent régulièrement
    - L'écosystème RxJS/Vue.js se transforme rapidement

3. **Précautions d'Usage**
    - Ces patterns sont issus d'essais/erreurs sur un projet spécifique
    - Leur application nécessite une adaptation à votre contexte
    - La complexité de cette approche doit être justifiée par vos besoins réels

#### Message aux Lecteurs

Il est important de noter que la rédaction d'un tel article n'aurait pas été possible sans :

-   Une expérience concrète sur un projet complexe
-   Des situations de production exigeantes
-   Des échecs dont nous avons appris
-   Une équipe expérimentée pour challenger les approches

Les solutions présentées ici ne sont pas théoriques - elles ont été forgées par itérations sur un gros projet, affinées par l'expérience, et validées par la production.
Cependant, elles ne constituent pas un modèle universel, mais plutôt une source d'inspiration pour vos propres architectures.
</div>
