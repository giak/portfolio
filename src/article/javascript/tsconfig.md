---
title: Guide Complet de Configuration TypeScript avec tsconfig.json
description: Un guide détaillé pour configurer efficacement vos projets TypeScript via tsconfig.json, avec des recommandations pratiques pour différents types de projets.
date: 2024-01-15
tags: ['typescript', 'configuration', 'javascript', 'developpement']
---

<style>
/* Optimisation de la lisibilité */
article {
    max-width: 720px;
    margin: 0 auto;
    padding: 2rem;
    font-family: Inter, -apple-system, BlinkMacSystemFont, sans-serif;
    line-height: 1.7;
    color: #1f2937;
    background: linear-gradient(to bottom, #ffffff, #fafafa);
}

/* Titres */
h1, h2, h3 {
    font-weight: 600;
    letter-spacing: -0.02em;
}

h1 {
    font-size: 2.5rem;
    line-height: 1.2;
    margin: 2.5rem 0 1.5rem;
    background: linear-gradient(to right, #1f2937, #4b5563);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
}

h2 {
    font-size: 1.75rem;
    margin: 3rem 0 1.5rem;
    color: #1f2937;
}

h3 {
    font-size: 1.25rem;
    margin: 2rem 0 1rem;
    color: #374151;
}

/* Blocs de code */
pre {
    background: #ffffff;
    padding: 1.25rem;
    margin: 1.5rem 0;
    border: 1px solid #e5e7eb;
    border-radius: 8px;
    overflow-x: auto;
    box-shadow: 0 1px 2px rgba(0,0,0,0.05);
}

code {
    font-family: 'JetBrains Mono', 'Fira Code', monospace;
    font-size: 0.9em;
    color: #6366f1;
}

/* Liens */
a {
    color: #6366f1;
    text-decoration: none;
    border-bottom: 1px solid transparent;
    transition: border-color 0.2s ease;
}

a:hover {
    border-bottom-color: #6366f1;
}

/* Citations */
blockquote {
    margin: 2rem 0;
    padding: 1rem 1.5rem;
    border-left: 3px solid #6366f1;
    background: #ffffff;
    border-radius: 0 8px 8px 0;
    box-shadow: 0 1px 2px rgba(0,0,0,0.05);
}

/* Listes */
ul, ol {
    padding-left: 1.5rem;
    margin: 1rem 0;
}

li {
    margin: 0.5rem 0;
}

/* Espacement des paragraphes */
p {
    margin: 1.25rem 0;
    color: #4b5563;
}

/* Tableaux */
table {
    width: 100%;
    border-collapse: collapse;
    margin: 1.5rem 0;
}

th, td {
    padding: 0.75rem;
    border: 1px solid #ddd;
}

th {
    background: #f6f8fa;
}

/* Mise en évidence des emojis */
.emoji {
    display: inline-block;
    margin-right: 0.5rem;
}

/* Responsive design */
@media (max-width: 768px) {
    article {
        padding: 1.5rem;
        margin: 0;
    }

    h1 { font-size: 2rem; }
    h2 { font-size: 1.5rem; }
    h3 { font-size: 1.15rem; }
}
</style>

# Guide Complet de Configuration TypeScript avec tsconfig.json

[2025-01-15] dev, typescript

> Ce guide est inspiré de l'article original "[A checklist for your tsconfig.json](https://2ality.com/2025/01/tsconfig-json.html)" par Dr. Axel Rauschmayer, réorganisé et augmenté pour une approche progressive.

## 1. Introduction à TypeScript 🚀

### Pourquoi TypeScript ?

TypeScript améliore le développement JavaScript en apportant :

-   Un système de types statiques
-   Une détection précoce des erreurs
-   Un meilleur support IDE
-   Des fonctionnalités JavaScript modernes

### Nouveautés TypeScript 5.0-5.7 pour le Web

TypeScript 5.x apporte plusieurs améliorations significatives pour le développement web :

#### Gestion des Modules Améliorée

1. **verbatimModuleSyntax**

    ```json
    {
        "compilerOptions": {
            "verbatimModuleSyntax": true
        }
    }
    ```

    Remplace `importsNotUsedAsValues` et `preserveValueImports` avec une approche plus claire :

    - Les imports sans modificateur `type` sont conservés
    - Les imports avec `type` sont supprimés du JavaScript généré

    ```typescript
    // Supprimé dans le JS
    import type { UserType } from './types';
    // Conservé dans le JS
    import { User, type Admin } from './models';
    ```

2. **Réécriture des Chemins Relatifs**
    ```json
    {
        "compilerOptions": {
            "allowImportingTsExtensions": true,
            "rewriteRelativeImportExtensions": true
        }
    }
    ```
    Permet d'utiliser les extensions `.ts` dans les imports et les convertit automatiquement en `.js` lors de la compilation :
    ```typescript
    // Dans le code source
    import { utils } from './utils.ts';
    // Dans le JS généré
    import { utils } from './utils.js';
    ```

#### Nouvelles Options de Configuration

1. **Résolution des Modules**

    ```json
    {
        "compilerOptions": {
            "moduleResolution": "bundler",
            "resolvePackageJsonExports": true,
            "resolvePackageJsonImports": true
        }
    }
    ```

    - `bundler` : Nouveau mode optimisé pour les bundlers modernes
    - Support amélioré des champs `exports` et `imports` de package.json

2. **Sécurité et Cohérence**
    ```json
    {
        "compilerOptions": {
            "forceConsistentCasingInFileNames": true,
            "verbatimModuleSyntax": true,
            "isolatedModules": true
        }
    }
    ```

#### Optimisations pour le Développement

1. **Mode Développement**

    ```json
    {
        "compilerOptions": {
            "incremental": true,
            "tsBuildInfoFile": ".tsbuildinfo",
            "skipLibCheck": true
        }
    }
    ```

2. **Support des Assets Web**
    ```typescript
    // globals.d.ts
    declare module '*.css' {
        const styles: { [key: string]: string };
        export default styles;
    }
    declare module '*.svg' {
        const content: string;
        export default content;
    }
    ```

#### Options Dépréciées à Éviter

Les options suivantes sont dépréciées depuis TypeScript 5.0 :

```json
{
    "compilerOptions": {
        // ❌ Ne plus utiliser
        "target": "ES3",
        "out": "...",
        "keyofStringsOnly": true,
        "suppressExcessPropertyErrors": true,
        "noImplicitUseStrict": true,
        "importsNotUsedAsValues": "...",
        "preserveValueImports": true
    }
}
```

### Structure de Base

Un fichier `tsconfig.json` minimal :

```json
{
    "compilerOptions": {
        // Version ECMAScript cible pour la sortie JavaScript
        "target": "ES2020",
        // Système de modules à utiliser (ESNext pour les bundlers modernes)
        "module": "ESNext",
        // Active toutes les vérifications de type strictes
        "strict": true,
        // Dossier de sortie pour les fichiers compilés
        "outDir": "./dist"
    },
    // Motifs des fichiers à inclure dans la compilation
    "include": ["src/**/*"]
}
```

### Installation et Configuration Initiale

1. Initialisation du projet :

```bash
npm init -y
npm install typescript --save-dev
```

2. Création de la structure de base :

```bash
mkdir src dist
touch tsconfig.json
```

### Notation et conventions

Pour montrer les types inférés dans le code source, nous utilisons ts-expect. Par exemple :

```typescript
// Vérifie que le type inféré de `someVariable` est `boolean`
expectType<boolean>(someVariable);
```

Les exemples de configuration utilisent des virgules finales dans le JSON car c'est supporté pour tsconfig.json et cela facilite le réarrangement et la copie du code.

### Limitations et Fonctionnalités Non Couvertes

Ce guide se concentre sur la configuration de projets utilisant des modules ESM. Certains aspects ne sont pas couverts :

-   Configuration JSX (voir le Manuel TypeScript section "JSX")
-   JavaScript pur dans votre base de code (options `allowJs` et `checkJs`)
-   Projets/monorepos (option `composite` et références de projet)

Pour les monorepos, consultez :

-   Le chapitre "Project References" dans le Manuel TypeScript
-   L'article "Simple monorepos via npm workspaces and TypeScript project references"

## 2. Configuration de Base

### Fichiers d'Entrée

```json
{
    // Motifs des fichiers à inclure dans la compilation
    "include": ["src/**/*"],
    // Motifs des fichiers à exclure de la compilation
    "exclude": ["node_modules", "**/*.spec.ts"]
}
```

### Sortie Compilée

```json
{
    "compilerOptions": {
        // Dossier où seront générés les fichiers JavaScript
        "outDir": "./dist",
        // Dossier racine des fichiers source TypeScript
        "rootDir": "./src"
    }
}
```

### Extensions de Configuration

```json
{
    // Hérite des options d'un autre fichier tsconfig
    "extends": "./tsconfig.base.json",
    "compilerOptions": {
        // Options qui surchargent celles du fichier de base
    }
}
```

### Structure du Projet

Voici un exemple de structure de projet TypeScript typique :

```
mon-projet/
├── src/
│   ├── index.ts
│   ├── types/
│   │   └── declarations.d.ts
│   └── modules/
│       └── feature.ts
├── dist/
├── tests/
├── tsconfig.json
└── package.json
```

## 3. Configuration du Langage

### Version ECMAScript

```json
{
    "compilerOptions": {
        // Version ECMAScript cible pour la sortie JavaScript
        "target": "ES2020",
        // APIs disponibles dans l'environnement d'exécution
        "lib": ["ES2020", "DOM"]
    }
}
```

### Système de Modules

```json
{
    "compilerOptions": {
        // Format des modules en sortie (ESNext pour les bundlers modernes)
        "module": "ESNext",
        // Stratégie de résolution des modules (bundler pour les outils modernes)
        "moduleResolution": "bundler"
    }
}
```

### Exécution Directe de TypeScript

Pour exécuter TypeScript directement sans générer de fichiers JS :

```json
{
    "compilerOptions": {
        // Utilise la résolution de modules Node.js
        "module": "NodeNext",
        // Stratégie de résolution des modules Node.js
        "moduleResolution": "NodeNext",
        // Ne génère pas de fichiers JavaScript
        "noEmit": true
    }
}
```

### Importation de JSON

Pour importer des fichiers JSON :

```json
{
    "compilerOptions": {
        // Permet l'importation de fichiers JSON comme des modules
        "resolveJsonModule": true
    }
}
```

### Importation d'Autres Artefacts

Pour les fichiers avec des extensions inconnues de TypeScript, il existe deux approches :

1. **Déclarations de Modules Ambiants**

    ```typescript
    // ./src/globals.d.ts
    declare module '*.css' {}
    declare module '*.png' {}
    ```

2. **Option allowArbitraryExtensions**
    ```json
    {
        "compilerOptions": {
            // Permet l'importation de fichiers avec des extensions non standard
            "allowArbitraryExtensions": true
        }
    }
    ```

TypeScript cherchera un fichier `basename.d.ext.ts` pour chaque import `basename.ext`. Si ce fichier n'est pas trouvé :

-   Avec `allowArbitraryExtensions`: aucune erreur n'est générée
-   Sans l'option : une erreur est levée sauf si une déclaration de module ambiant existe

## 4. Vérification des Types

### Mode Strict ⚡

Le mode strict active plusieurs vérifications importantes :

```json
{
    "compilerOptions": {
        // Active toutes les vérifications strictes
        "strict": true,
        // Interdit l'utilisation implicite du type 'any'
        "noImplicitAny": true,
        // Active la vérification stricte des null/undefined
        "strictNullChecks": true,
        // Vérifie strictement les types de fonctions
        "strictFunctionTypes": true,
        // Vérifie les appels de bind/call/apply
        "strictBindCallApply": true,
        // Force l'initialisation des propriétés de classe
        "strictPropertyInitialization": true,
        // Interdit l'utilisation implicite de this
        "noImplicitThis": true,
        // Utilise 'unknown' pour les variables catch
        "useUnknownInCatchVariables": true
    }
}
```

### Vérifications Avancées

Options recommandées pour une vérification plus stricte :

```json
{
    "compilerOptions": {
        // Distinction stricte entre propriétés optionnelles et undefined
        "exactOptionalPropertyTypes": true,
        // Rend l'accès aux index de tableau plus sûr
        "noUncheckedIndexedAccess": true,
        // Force l'utilisation du mot-clé override
        "noImplicitOverride": true,
        // Force l'utilisation de la notation [] pour les signatures d'index
        "noPropertyAccessFromIndexSignature": true
    }
}
```

#### exactOptionalPropertyTypes

Cette option fait la distinction entre une propriété optionnelle non définie et une propriété définie avec la valeur `undefined` :

```typescript
interface Config {
    port?: number;
}

// Avec exactOptionalPropertyTypes: true
const config: Config = {
    port: undefined, // Erreur !
};

// Correct
const config: Config = {};
```

#### noPropertyAccessFromIndexSignature

Empêche l'accès aux propriétés via la notation point pour les signatures d'index :

```typescript
interface Dictionary {
    [key: string]: string;
}

const dict: Dictionary = {
    hello: 'world',
};

// Avec noPropertyAccessFromIndexSignature: true
dict.hello; // Erreur !
dict['hello']; // OK
```

#### noUncheckedIndexedAccess

Cette option rend l'accès aux index plus sûr :

```typescript
interface StringArray {
    [index: number]: string;
}
const arr: StringArray = ['hello'];
// Avec noUncheckedIndexedAccess: true
const item = arr[0]; // type: string | undefined
```

### Options Recommandées

Autres options de vérification recommandées :

```json
{
    "compilerOptions": {
        "noFallthroughCasesInSwitch": true,
        "noImplicitReturns": true,
        "noUnusedLocals": true,
        "noUnusedParameters": true
    }
}
```

## 5. Optimisation et Performance 🔥

### Optimisation du Temps de Compilation

```json
{
    "compilerOptions": {
        // Compilation incrémentale
        "incremental": true,
        "tsBuildInfoFile": ".tsbuildinfo",

        // Optimisations de performance
        "skipLibCheck": true,
        "isolatedModules": true,

        // Accélération de la résolution des modules
        "moduleResolution": "bundler",
        "verbatimModuleSyntax": true,

        // Limite les vérifications de types
        "types": [], // Ne charge que les @types explicitement listés
        "skipDefaultLibCheck": true
    },
    "watchOptions": {
        // Optimisation du mode watch
        "watchFile": "useFsEvents",
        "watchDirectory": "useFsEvents",
        "fallbackPolling": "dynamicPriority"
    },
    "include": ["src/**/*"],
    "exclude": ["node_modules", "build", "dist", "**/*.spec.ts"]
}
```

Stratégies d'optimisation :

1. **Compilation Sélective**

    - Utilisez `include` et `exclude` précisément
    - Évitez les patterns trop larges comme `"**/*"`
    - Excluez les fichiers de test en développement

2. **Cache et Incrémental**

    - Activez `incremental` pour réutiliser les résultats précédents
    - Configurez `tsBuildInfoFile` dans un dossier dédié
    - Utilisez le mode watch pour les mises à jour rapides

3. **Résolution des Modules**
    - Préférez `moduleResolution: "bundler"` pour les projets web
    - Utilisez `baseUrl` et `paths` pour accélérer les imports
    - Limitez l'utilisation des alias complexes

### Gestion de la Mémoire pour les Gros Projets 🔥

```json
{
    "compilerOptions": {
        // Optimisations mémoire
        "skipLibCheck": true,
        "types": ["node", "jest"], // Listez uniquement les types nécessaires

        // Réduction de la charge mémoire
        "preserveValueImports": true,
        "verbatimModuleSyntax": true,

        // Optimisations pour gros projets
        "noPropertyAccessFromIndexSignature": true,
        "exactOptionalPropertyTypes": true,

        // Évite les calculs redondants
        "importsNotUsedAsValues": "remove"
    }
}
```

Recommandations pour les gros projets :

1. **Gestion des Types**

    - Limitez les types chargés automatiquement
    - Utilisez `skipLibCheck` pour les fichiers `.d.ts`
    - Évitez les unions de types complexes

2. **Structure du Projet**

    - Divisez en sous-projets avec "references"
    - Utilisez des builds parallèles
    - Implémentez une stratégie de monorepo si nécessaire

3. **Monitoring**

    ```bash
    # Surveillance de la mémoire
    tsc --diagnostics

    # Analyse détaillée
    node --max-old-space-size=8192 ./node_modules/.bin/tsc
    ```

### Stratégies de Build Incrémental

```json
{
    "compilerOptions": {
        "incremental": true,
        "composite": true,
        "tsBuildInfoFile": "./cache/.tsbuildinfo"
    },
    "references": [{ "path": "../common" }, { "path": "../features" }],
    "watchOptions": {
        "synchronousWatchDirectory": true,
        "excludeDirectories": ["**/node_modules", "build"]
    }
}
```

Stratégies pour builds rapides :

1. **Build Incrémental**

    - Utilisez `composite: true` pour les projets référencés
    - Configurez correctement les "references"
    - Optimisez la structure des dépendances

2. **Parallélisation**

    ```json
    {
        "scripts": {
            "build": "tsc --build --verbose --pretty",
            "watch": "tsc --build --watch",
            "clean": "tsc --build --clean"
        }
    }
    ```

3. **Optimisations Avancées**
    - Utilisez `project references` pour les monorepos
    - Implémentez des builds partiels
    - Configurez le cache de build

Exemple de configuration pour un projet complexe :

```json
{
    "compilerOptions": {
        // Build optimisé
        "incremental": true,
        "tsBuildInfoFile": ".tsbuildinfo",
        "composite": true,

        // Optimisations mémoire
        "skipLibCheck": true,
        "types": ["node", "jest"],

        // Performance
        "moduleResolution": "bundler",
        "verbatimModuleSyntax": true,

        // Cache et optimisations
        "isolatedModules": true,
        "preserveValueImports": true,

        // Vérifications optimisées
        "noUnusedLocals": true,
        "noUnusedParameters": true
    },
    "watchOptions": {
        "watchFile": "useFsEvents",
        "watchDirectory": "useFsEvents",
        "fallbackPolling": "dynamicPriority",
        "synchronousWatchDirectory": true,
        "excludeDirectories": ["**/node_modules", "build", "dist"]
    },
    "include": ["src/**/*"],
    "exclude": ["node_modules", "build", "**/*.spec.ts"]
}
```

Ces optimisations peuvent réduire significativement le temps de compilation et la consommation mémoire, particulièrement pour les grands projets web.

## 6. Documentation et Maintenance

### Configuration TypeDoc

```json
{
    "typedocOptions": {
        // Point d'entrée pour la génération de la documentation
        "entryPoints": ["src/index.ts"],
        // Dossier de sortie de la documentation
        "out": "docs"
    }
}
```

### Bonnes Pratiques

-   Utilisation des commentaires TSDoc
-   Organisation claire des fichiers
-   Documentation des interfaces publiques

## 7. Configurations Spécialisées

### Application Web 💻

Configuration recommandée pour les applications web modernes :

```json
{
    "compilerOptions": {
        // Version ECMAScript cible
        "target": "ES2020",
        // APIs disponibles dans l'environnement
        "lib": ["ES2020", "DOM", "DOM.Iterable"],
        // Système de modules
        "module": "ESNext",
        // Résolution des modules optimisée pour les bundlers
        "moduleResolution": "bundler",
        // Préserve la syntaxe JSX pour les bundlers
        "jsx": "preserve",
        // Génère les source maps pour le debugging
        "sourceMap": true,
        // Permet l'import de fichiers JSON
        "resolveJsonModule": true,
        // Assure la compatibilité avec les bundlers
        "isolatedModules": true,
        // Améliore la compatibilité avec CommonJS
        "esModuleInterop": true,
        // Active toutes les vérifications strictes
        "strict": true,
        // Accélère la compilation en ignorant les vérifications des .d.ts
        "skipLibCheck": true
    }
}
```

### Application Node.js ⚙️

```json
{
    "compilerOptions": {
        // Version ECMAScript cible pour Node.js moderne
        "target": "ES2022",
        // Utilise le système de modules Node.js
        "module": "NodeNext",
        // Résolution des modules adaptée à Node.js
        "moduleResolution": "NodeNext",
        // Types Node.js à inclure
        "types": ["node"]
    }
}
```

### Bibliothèques npm 📦

Configuration pour publier une bibliothèque :

```json
{
    "compilerOptions": {
        // Version ECMAScript cible
        "target": "ES2020",
        // Format des modules
        "module": "ESNext",
        // Génère les fichiers de déclaration .d.ts
        "declaration": true,
        // Génère les source maps pour les fichiers de déclaration
        "declarationMap": true,
        // Génère les source maps pour le debugging
        "sourceMap": true,
        // Dossier de sortie
        "outDir": "./dist",
        // Active toutes les vérifications strictes
        "strict": true,
        // Résolution des modules pour les bundlers
        "moduleResolution": "bundler",
        // Compatibilité CommonJS
        "esModuleInterop": true,
        // Optimisation de la compilation
        "skipLibCheck": true
    },
    // Fichiers source à inclure
    "include": ["src"],
    // Fichiers à exclure
    "exclude": ["node_modules", "**/*.spec.ts"]
}
```

## 8. Interopérabilité

### JavaScript et TypeScript

#### verbatimModuleSyntax

Cette option clarifie la distinction entre les imports de types et de valeurs :

```typescript
// Entrée
import { type UserType, User } from './types';

// Sortie en JavaScript
import { User } from './types';
```

### CommonJS et ESM

#### Imports par Défaut Synthétiques

```json
{
    "compilerOptions": {
        // Permet l'import par défaut des modules CommonJS
        "allowSyntheticDefaultImports": true,
        // Améliore la compatibilité avec les modules CommonJS
        "esModuleInterop": true
    }
}
```

Permet d'importer des modules CommonJS comme s'ils avaient un export par défaut :

```typescript
// Avec allowSyntheticDefaultImports: true
import React from 'react';

// Sans l'option
import * as React from 'react';
```

#### Interopérabilité ESM

L'option `esModuleInterop` modifie la façon dont TypeScript compile les imports :

```typescript
// Avec esModuleInterop: true
import fs from 'fs';
import * as path from 'path';

// Sans l'option
import * as fs from 'fs';
const path = require('path');
```

### Déclarations Isolées

L'option `isolatedDeclarations` assure que les fichiers de déclaration (.d.ts) peuvent être utilisés indépendamment :

```json
{
    "compilerOptions": {
        // Génère les fichiers de déclaration .d.ts
        "declaration": true,
        // Assure que les fichiers .d.ts sont indépendants
        "isolatedDeclarations": true
    }
}
```

### Vérification de Types Uniquement 🔍

Pour utiliser TypeScript uniquement pour la vérification sans générer de code :

```json
{
    "compilerOptions": {
        // Désactive la génération de fichiers JavaScript
        "noEmit": true
    }
}
```

### Acquisition des Types

Configuration complète pour l'acquisition des types :

```json
{
    "typeAcquisition": {
        // Active l'acquisition automatique des types
        "enable": true,
        // Types spécifiques à inclure
        "include": ["jest", "mocha"],
        // Types à exclure
        "exclude": ["jquery"],
        // Désactive l'acquisition basée sur les noms de fichiers
        "disableFilenameBasedTypeAcquisition": false
    }
}
```

## 9. Outils et Intégration 🛠️

### Visual Studio Code

Configuration recommandée pour VS Code :

```json
{
    // Utilise TypeScript depuis node_modules
    "typescript.tsdk": "node_modules/typescript/lib",
    // Demande confirmation pour utiliser le TypeScript du workspace
    "typescript.enablePromptUseWorkspaceTsdk": true,
    // Format des extensions pour les imports TypeScript
    "typescript.preferences.importModuleSpecifierEnding": "js",
    // Format des extensions pour les imports JavaScript
    "javascript.preferences.importModuleSpecifierEnding": "js"
}
```

### Outils de Build

#### Compilation et Vérification

Scripts recommandés pour package.json :

```json
{
    "scripts": {
        "build": "tsc",
        "type-check": "tsc --noEmit",
        "watch": "tsc --watch",
        "test": "jest"
    }
}
```

### CI/CD 🔄

La configuration TypeScript joue un rôle crucial dans votre pipeline CI/CD. Voici une configuration complète pour l'intégration continue :

```yaml
# .github/workflows/typescript.yml
name: TypeScript Build & Test

on:
    push:
        branches: [main]
    pull_request:
        branches: [main]

jobs:
    build:
        runs-on: ubuntu-latest

        strategy:
            matrix:
                node-version: [18.x, 20.x]

        steps:
            - uses: actions/checkout@v4

            - name: Setup Node.js ${{ matrix.node-version }}
              uses: actions/setup-node@v4
              with:
                  node-version: ${{ matrix.node-version }}
                  cache: 'npm'

            - name: Installation des dépendances
              run: npm ci

            - name: Vérification des types
              run: |
                  # Vérifie la syntaxe et les types sans générer de fichiers
                  tsc --noEmit
                  # Vérifie les déclarations de types
                  tsc --declaration --emitDeclarationOnly
                  # Vérifie la compatibilité des imports
                  tsc --isolatedModules

            - name: Tests unitaires avec types
              run: |
                  # Exécute les tests avec la vérification des types
                  jest --typeCheck
                  # Vérifie la couverture des types
                  typescript-coverage-report

            - name: Build avec optimisations
              run: |
                  # Build avec les optimisations TypeScript
                  tsc --project tsconfig.prod.json
                  # Vérifie la taille des fichiers de types générés
                  npx type-coverage

            - name: Cache des builds TypeScript
              uses: actions/cache@v4
              with:
                  path: |
                      .tsbuildinfo
                      node_modules/.cache/typescript
                  key: ${{ runner.os }}-typescript-${{ hashFiles('**/tsconfig.json') }}

            - name: Analyse de la qualité du code
              run: |
                  npm run lint
                  npm run test:coverage
```

Cette configuration met l'accent sur :

-   La vérification approfondie des types TypeScript
-   La génération et validation des fichiers de déclaration
-   L'optimisation des builds TypeScript
-   La mise en cache spécifique aux artefacts TypeScript

### Package.json et TypeScript

Configuration package.json pour un projet TypeScript :

```json
{
    // Nom du package
    "name": "mon-projet",
    // Version du package
    "version": "1.0.0",
    // Définit le type de module (ESM)
    "type": "module",
    // Configuration des exports du package
    "exports": {
        ".": {
            // Fichiers de types TypeScript
            "types": "./dist/index.d.ts",
            // Point d'entrée ESM
            "import": "./dist/index.js"
        }
    },
    // Fichiers à inclure dans le package publié
    "files": ["dist"],
    // Scripts npm
    "scripts": {
        // Compilation TypeScript
        "build": "tsc",
        // Vérification des types
        "type-check": "tsc --noEmit",
        // Script exécuté avant la publication
        "prepublishOnly": "npm run build"
    }
}
```

### Paramètres package.json Considérés

TypeScript prend en compte plusieurs champs de package.json :

```json
{
    "type": "module", // Pour ESM
    "types": "./types.d.ts", // Fichier de types principal
    "exports": {
        ".": {
            "types": "./dist/index.d.ts",
            "import": "./dist/index.js"
        }
    }
}
```

## Résumé des Configurations Recommandées

### Pour une Application Web Moderne 💻

```json
{
    "compilerOptions": {
        "target": "ES2020",
        "lib": ["ES2020", "DOM", "DOM.Iterable"],
        "module": "ESNext",
        "moduleResolution": "bundler",
        "jsx": "preserve",
        "strict": true,
        "skipLibCheck": true,
        "esModuleInterop": true,
        "isolatedModules": true,
        "resolveJsonModule": true,
        "sourceMap": true,
        "outDir": "./dist"
    },
    "include": ["src"],
    "exclude": ["node_modules"]
}
```

### Pour une Bibliothèque npm 📦

```json
{
    "compilerOptions": {
        "target": "ES2020",
        "module": "ESNext",
        "moduleResolution": "bundler",
        "declaration": true,
        "declarationMap": true,
        "sourceMap": true,
        "outDir": "./dist",
        "strict": true,
        "skipLibCheck": true
    },
    "include": ["src"],
    "exclude": ["node_modules", "**/*.spec.ts"]
}
```

### Pour une Application Node.js ⚙️

```json
{
    "compilerOptions": {
        "target": "ES2022",
        "module": "NodeNext",
        "moduleResolution": "NodeNext",
        "outDir": "./dist",
        "rootDir": "./src",
        "strict": true,
        "skipLibCheck": true,
        "sourceMap": true,
        "types": ["node"],
        "esModuleInterop": true
    },
    "include": ["src"],
    "exclude": ["node_modules"]
}
```

### Pour un Projet PNPM

```json
{
    "compilerOptions": {
        // Version ECMAScript moderne pour la compatibilité
        "target": "ES2022",
        // Utilise le système de modules Node.js
        "module": "NodeNext",
        // Résolution des modules adaptée à PNPM
        "moduleResolution": "NodeNext",
        // Support des workspaces PNPM
        "baseUrl": ".",
        "paths": {
            "@/*": ["packages/*/src"]
        },
        // Configuration standard
        "strict": true,
        "skipLibCheck": true,
        "sourceMap": true,
        "declaration": true,
        "declarationMap": true,
        // Gestion des imports/exports
        "esModuleInterop": true,
        "isolatedModules": true,
        // Sortie
        "outDir": "./dist"
    },
    // Gestion des workspaces PNPM
    "include": ["packages/*/src"],
    "exclude": ["node_modules", "**/*.spec.ts", "**/dist"]
}
```

## Bonnes Pratiques et Conseils 💡

1. **Commencez Simple**

    - Activez d'abord `strict: true`
    - Ajoutez progressivement d'autres vérifications
    - Utilisez les configurations spécialisées comme base

2. **Performance**

    - Activez `skipLibCheck` pour les grands projets
    - Utilisez la compilation incrémentale
    - Configurez correctement les `include/exclude`

3. **Maintenabilité**

    - Documentez vos choix de configuration
    - Utilisez des fichiers de base via `extends`
    - Gardez la configuration cohérente entre projets

4. **Sécurité Type**
    - Activez toutes les vérifications strictes
    - Utilisez `exactOptionalPropertyTypes`
    - Configurez `noUncheckedIndexedAccess`

Cette structure progressive permet de comprendre et d'implémenter TypeScript étape par étape, en commençant par les bases essentielles jusqu'aux configurations avancées.

## Intégration avec les Frameworks Web 🌐

### Vue.js

```json
{
    "compilerOptions": {
        // Configuration de base
        "target": "ES2020",
        "module": "ESNext",
        "moduleResolution": "bundler",
        "strict": true,
        // Support Vue.js
        "jsx": "preserve",
        "jsxImportSource": "vue",
        // Types Vue.js
        "types": ["vue"],
        // Optimisations Vue.js
        "isolatedModules": true,
        "skipLibCheck": true,
        "verbatimModuleSyntax": true,
        // Support des SFC Vue
        "allowJs": true,
        "paths": {
            "@/*": ["./src/*"]
        },
        // Compilation
        "sourceMap": true,
        "outDir": "./dist",
        // Interopérabilité
        "esModuleInterop": true,
        "forceConsistentCasingInFileNames": true
    },
    "include": ["src/**/*.ts", "src/**/*.d.ts", "src/**/*.tsx", "src/**/*.vue"],
    "exclude": ["node_modules"]
}
```

### React

```json
{
    "compilerOptions": {
        // Configuration de base
        "target": "ES2020",
        "module": "ESNext",
        "lib": ["DOM", "DOM.Iterable", "ESNext"],
        "moduleResolution": "bundler",
        // Support React
        "jsx": "react-jsx",
        "types": ["react", "react-dom"],
        // Optimisations React
        "allowJs": true,
        "isolatedModules": true,
        "skipLibCheck": true,
        "noEmit": true,
        // Vérifications strictes
        "strict": true,
        "noFallthroughCasesInSwitch": true,
        "noUnusedLocals": true,
        "noUnusedParameters": true,
        // Résolution des modules
        "resolveJsonModule": true,
        "paths": {
            "@/*": ["./src/*"]
        },
        // Interopérabilité
        "esModuleInterop": true,
        "allowSyntheticDefaultImports": true,
        "forceConsistentCasingInFileNames": true
    },
    "include": ["src"],
    "exclude": ["node_modules"]
}
```

### Angular

```json
{
    "compilerOptions": {
        // Configuration de base
        "target": "ES2022",
        "module": "ES2022",
        "moduleResolution": "node",
        "experimentalDecorators": true,
        // Support Angular
        "lib": ["ES2022", "dom"],
        "types": ["node"],
        // Optimisations Angular
        "skipLibCheck": true,
        "sourceMap": true,
        "declaration": false,
        "downlevelIteration": true,
        "importHelpers": true,
        // Vérifications strictes
        "strict": true,
        "noImplicitOverride": true,
        "noPropertyAccessFromIndexSignature": true,
        "noImplicitReturns": true,
        "noFallthroughCasesInSwitch": true,
        // Résolution des modules
        "baseUrl": "./",
        "paths": {
            "@app/*": ["src/app/*"],
            "@env/*": ["src/environments/*"]
        },
        // Interopérabilité
        "esModuleInterop": true,
        "forceConsistentCasingInFileNames": true
    },
    "angularCompilerOptions": {
        "enableI18nLegacyMessageIdFormat": false,
        "strictInjectionParameters": true,
        "strictInputAccessModifiers": true,
        "strictTemplates": true
    },
    "include": ["src/**/*.ts"],
    "exclude": ["node_modules"]
}
```

Chaque framework a ses spécificités :

-   **Vue.js** nécessite le support des Single File Components (`.vue`), la préservation du JSX et des chemins d'alias.
-   **React** utilise une configuration JSX spécifique, des vérifications strictes et le support des imports par défaut.
-   **Angular** requiert le support des décorateurs, une configuration spéciale du compilateur et une structure de projet particulière.

Ces configurations peuvent être ajustées selon les besoins spécifiques du projet et les versions des frameworks utilisées.

### Vite 6

```json
{
    "compilerOptions": {
        // Configuration de base
        "target": "ESNext",
        "useDefineForClassFields": true,
        "module": "ESNext",
        "lib": ["ESNext", "DOM", "DOM.Iterable"],
        "skipLibCheck": true,

        // Bundling et modules
        "moduleResolution": "bundler",
        "allowImportingTsExtensions": true,
        "verbatimModuleSyntax": true,
        "noEmit": true,

        // Résolution des chemins
        "baseUrl": ".",
        "paths": {
            "@/*": ["./src/*"]
        },

        // Optimisations Vite
        "isolatedModules": true,
        "resolveJsonModule": true,
        "allowJs": true,

        // Vérifications strictes
        "strict": true,
        "noUnusedLocals": true,
        "noUnusedParameters": true,
        "noFallthroughCasesInSwitch": true,

        // Support JSX
        "jsx": "preserve",

        // Interopérabilité
        "esModuleInterop": true,
        "forceConsistentCasingInFileNames": true
    },
    "include": ["src/**/*.ts", "src/**/*.d.ts", "src/**/*.tsx", "src/**/*.vue", "vite.config.ts"],
    "exclude": ["node_modules", "dist"]
}
```

Cette configuration est optimisée pour Vite 6 avec :

-   Support complet d'ESM natif avec `module: "ESNext"`
-   Optimisations spécifiques pour le bundling avec `moduleResolution: "bundler"`
-   Support des extensions TypeScript dans les imports pendant le développement
-   Configuration optimisée pour le HMR avec `isolatedModules`
-   Support intégré pour les fichiers JSON et JSX
-   Vérifications strictes pour une meilleure qualité de code

Pour un projet Vite + Vue :

```json
{
    "extends": "./tsconfig.json",
    "compilerOptions": {
        "composite": true,
        "types": ["node", "vite/client", "vue/ref-macros"]
    }
}
```

Pour un projet Vite + React :

```json
{
    "extends": "./tsconfig.json",
    "compilerOptions": {
        "composite": true,
        "jsx": "react-jsx",
        "types": ["node", "vite/client", "@types/react", "@types/react-dom"]
    }
}
```

Pour un projet Vite + Vue.js :

```json
{
    "extends": "./tsconfig.json",
    "compilerOptions": {
        // Support Vue.js
        "composite": true,
        "types": ["node", "vite/client", "vue/ref-macros", "vue/runtime-core"],
        "jsx": "preserve",
        "jsxImportSource": "vue",
        // Optimisations Vue.js
        "preserveValueImports": true,
        // Support des SFC Vue
        "paths": {
            "@/*": ["./src/*"],
            "#components/*": ["./src/components/*"]
        }
    },
    "include": [
        "src/**/*.ts",
        "src/**/*.d.ts",
        "src/**/*.tsx",
        "src/**/*.vue",
        "vite.config.*",
        "vitest.config.*",
        "cypress.config.*",
        "playwright.config.*"
    ]
}
```

Pour un projet Vite + Angular 19 :

```json
{
    "extends": "./tsconfig.json",
    "compilerOptions": {
        // Support Angular 19
        "composite": true,
        "types": ["node", "vite/client", "@angular/core"],
        // Fonctionnalités Angular 19
        "experimentalDecorators": true,
        "useDefineForClassFields": false,
        "forceConsistentCasingInFileNames": true,
        // Support des templates
        "paths": {
            "@app/*": ["./src/app/*"],
            "@shared/*": ["./src/app/shared/*"],
            "@env/*": ["./src/environments/*"]
        }
    },
    "angularCompilerOptions": {
        // Options du compilateur Angular 19
        "enableI18nLegacyMessageIdFormat": false,
        "strictInjectionParameters": true,
        "strictInputAccessModifiers": true,
        "strictTemplates": true,
        "enableIvy": true,
        "allowEmptyInputs": false,
        "extendedDiagnostics": {
            "checks": {
                "optionalChainNotNullable": "error",
                "nullishCoalescingNotNullable": "error"
            }
        }
    },
    "include": ["src/**/*.ts", "src/**/*.d.ts", "vite.config.*", "src/**/*.spec.ts"]
}
```

## Conclusion 🎯

Ce guide de configuration TypeScript avec `tsconfig.json` vous a fourni une approche progressive pour configurer efficacement vos projets TypeScript.

La configuration TypeScript peut sembler complexe au premier abord, mais elle est essentielle pour :

-   Assurer la qualité et la maintenabilité du code
-   Optimiser les performances de développement
-   Faciliter la collaboration en équipe
-   Prévenir les erreurs courantes

N'oubliez pas que la configuration idéale dépend de votre contexte spécifique. Commencez avec les configurations recommandées, puis ajustez-les selon vos besoins.

Pour rester à jour :

-   Consultez régulièrement la documentation officielle TypeScript
-   Suivez les mises à jour des frameworks que vous utilisez
-   Adaptez votre configuration aux nouvelles bonnes pratiques
-   versionnez votre fichier de configs avec votre propre documentation dans un dépot spécifique

La puissance de TypeScript réside dans sa flexibilité et sa capacité à s'adapter à différents types de projets. Une configuration bien pensée est la clé d'une expérience de développement productive et agréable.
