---
title: Javascript > Pull contre Push état de l'art entre la programmation évènementielle versus réactive
date: 2020-03-21
index: true
description: 'Javascript pull versus push : reactivx - Analyse des paradigmes de programmation'
tag: ['Programming', 'reactivx']
category:
    - code
    - javascript
---

> Objet :
>
> -   Javascript : rappel historique et évolution
> -   Le pull versus push : analyse comparative
> -   Le paradigme de la programmation réactive : concepts fondamentaux
> -   ReactiveX -> RxJS : implémentation pratique

## Petit rappel historique sur le JavaScript

### Javascript natif (1995)

```javascript
var bozo = 'clown';
if (bozo == 'clown') {
    console.log('bozo est un clown');
} else {
    console.log("bozo n'est pas clown");
}

alert('Magnifique !');
```

Le code s'exécute de manière strictement séquentielle.
Si le `if` prend 5 minutes à s'exécuter, l'`alert` sera bloqué pendant toute cette durée.  
L'absence de typage peut rendre le code plus flexible mais aussi plus fragile.

### L'asynchrone : ES3 (1999)

```javascript
var getAjaxJSON = function (url, successHandler, errorHandler) {
    var xhr = typeof XMLHttpRequest != 'undefined' ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            var status = xhr.status;
            if (status == 200) {
                successHandler && successHandler(xhr.response);
            } else {
                errorHandler && errorHandler(status);
            }
        }
    };
    xhr.send();
};

getAjaxJSON(
    'https://jsonplaceholder.typicode.com/users',
    function (data) {},
    function (status) {},
);
```

`onreadystatechange` de `XMLHttpRequest` permet d'envoyer une requête et de gérer sa réponse de manière asynchrone.

### Simplification de l'écriture Ajax avec Jquery (2006) avec `callback` (fonctions anonymes)

```javascript
$.ajax({
    url: 'https://jsonplaceholder.typicode.com/users',
    type: 'GET',
    dataType: 'json',
    success: function (json, status) {},
    error: function (result, status, erreur) {},
    complete: function (result, status) {},
});
```

Problématique majeure : l'imbrication de multiples `callback` crée une complexité croissante dans la maintenance, la compréhension et la lisibilité du code : c'est le redoutable **CALLBACK HELL**.

```javascript
$.ajax({
    url: 'https://jsonplaceholder.typicode.com/users',
    type: 'GET',
    dataType: 'json',
    success: function (json, status) {
        $.ajax({
            url: 'https://jsonplaceholder.typicode.com/posts',
            type: 'GET',
            dataType: 'json',
            success: function (json, status) {
                $.ajax({
                    url: 'https://jsonplaceholder.typicode.com/comments',
                    type: 'GET',
                    dataType: 'json',
                    success: function (json, status) {
                        console.log('Nous avons tout les résultats.');
                    },
                    error: function (result, status, erreur) {},
                    complete: function (result, status) {},
                });
            },
            error: function (result, status, erreur) {},
            complete: function (result, status) {},
        });
    },
    error: function (result, status, erreur) {},
    complete: function (result, status) {},
});
```

### Les différentes façons d'écrire une fonction

```javascript
// fonction
function delayPromise(ms) {
    return new Promise(promiseAction);
    function promiseAction(resolve) {
        return setTimeout(resolve, ms);
    }
}

// closure (ES5)
const delayPromise = function (ms) {
    return new Promise(function (resolve) {
        return setTimeout(resolve, ms);
    });
};

// Nouveauté ES6 : fat arrow function
const delayPromise = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
```

### Les `Promise` - ES6 (ES2015 juin 2015 - version majeure - 6 ans pour sortir)

Définition d'une Promise : transforme une fonction en objet pour faire des **traitements asynchrone en parallèle** comprenant 3 états : attente, fini et erreur.

Exemple de paralléliser les traitements des Promises

```javascript
/**
 * @param ms - millisecondes
 * @returns {Promise<unknown>}
 */
const delayPromise = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

let promisedStep1 = () => {
    console.log('step 1 +3sec');
    return delayPromise(3000);
};
let promisedStep2 = () => {
    console.log('step 2 +2sec');
    return delayPromise(2000);
};
let promisedStep3 = () => {
    throw new Error('step 3 échoué.');
};

delayPromise(1000)
    .then(() => console.log('On attend 1 seconde.'))
    .then(promisedStep1)
    .then(promisedStep2)
    .then(promisedStep3)
    .catch((error) => {
        console.log('Erreur : ', error);
    });
console.log("Pendant l'execution d'une Promise... Nous pouvons aller faire autre chose.");
```

Exemple avec de l'Ajax

```javascript
const getAjaxJSON = (url) => {
    return new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.responseType = 'json';
        xhr.onload = () => {
            if (xhr.status === 200) {
                resolve(xhr.response);
            } else {
                reject(Error(xhr.status));
            }
        };
        xhr.send();
    });
};

// Cascade
getAjaxJSON('https://jsonplaceholder.typicode.com/users')
    .then(function (response) {
        return getAjaxJSON('https://jsonplaceholder.typicode.com/posts');
    })
    .then(function (response) {
        return getAjaxJSON('https://jsonplaceholder.typicode.com/comments');
    })
    .then(function (response) {
        console.log('Nous avons tout les résultats en cascade.');
        console.log(response);
    })
    .catch(function (errors) {});

// En parallèle
Promise.all([
    getAjaxJSON('https://jsonplaceholder.typicode.com/users'),
    getAjaxJSON('https://jsonplaceholder.typicode.com/posts'),
    getAjaxJSON('https://jsonplaceholder.typicode.com/comments'),
])
    .then(function (responses) {
        console.log('Nous avons tout les résultats en parallèle.');
        console.log(responses);
    })
    .catch(function (errors) {});
```

Même si cela est plus maintenable et lisible que les Callbacks, on tombe sur le même problème que les callbacks : promise pyramid of doom

Avantage :

-   cela ne bloque pas ce qui peut être déclaré après.
-   permet de paralléliser

Inconvénient :

-   On ne peut pas arrêter une Promise, elle doit se finir.
-   1 seul retour
-   la scénarisation est lourde
-   le code peut être verbeux
-   château d'IF pour vérifier les données

### `await` `async` - ES7 (ES2016) - mieux que de simples Promises

Facilite l'écrire du code asynchrone comme du code synchrone.

-   évite d'écrire `new Promise`

-   `async` renvoie systématiquement une Promise.
    -   erreur : la Promise est rejetée.
    -   résolue : retourne la valeur.
-   `await` attend la résolution d’une Promise et retourne sa valeur.

```javascript
const delayAsync = async () => {
    const simpleDelay = await delayPromise(3000);
    const step1 = await promisedStep1;
    const step2 = await promisedStep2;
    const step3 = await promisedStep3;
};
```

Exemple avec de l'Ajax

```javascript
async function JSONData() {
    const step1 = await getAjaxJSON('https://jsonplaceholder.typicode.com/users');
    const step2 = await getAjaxJSON('https://jsonplaceholder.typicode.com/posts');
    const step3 = await getAjaxJSON('https://jsonplaceholder.typicode.com/comments');

    return step1 + step2 + step3;
}

JSONData().then((result) => console.log(result));
```

Avantages

-   Le code est plus lisible (une fonction permet d'aggreger le comportement de plusieurs promise)
-   debug plus facile (utilisation de `try/catch`)
-   plus souple (on ne se fait pas coincer par un `then`)

Inconvénients

-   une fonction permet d'aggreger (chainer) le comportement de plusieurs promise (difficile de différencier des états par des évènements séparés)
-   ne pas regarder le code transpilé (très moche)
-   un `this` peut etre trimbalé sur un `async` (le garder en mémoire peut etre couteux)

## Le pull : "Your call is important to us. Please hold."

![Your call is important to us. Please hold.](./img/OnHoldcall-1.jpg)

`Every function is a pull system.`

Le système retourne une valeur uniquement sur demande explicite.

Défis principaux :

-   Comment synchroniser efficacement les mises à jour de données entre composants ?
-   Comment informer les composants indépendants des nouvelles données disponibles ?
-   Faut-il implémenter un système de polling pour les composants découplés ?
-   Comment tracer et comprendre les modifications des données partagées ?

## Cadrant

<div style="background-color: #fff;width:600px">

![Cadrant](./img/1455228358pillars.png)

</div>

## Le push : "don't call us, we call you"

![Inversion of controle](./img/1488266452_FXOflM_belongfinal.jpg)

`Inversion of controle`

Mécanisme d'envoi proactif d'événements et de valeurs vers les auditeurs.

-   simple : une promise émet un événement unique avec sa valeur résultante
-   multiple : les auditeurs reçoivent automatiquement les flux de données dès leur disponibilité

Cette approche constitue l'essence de la programmation réactive.

## La programmation réactive

[Reactivex](http://reactivex.io/) - [Ressource](https://www.freecodecamp.org/news/rxjs-and-node-8f4e0acebc7c/) -
[Vidéos fr](https://www.grafikart.fr/forum/topics/29945) - [rxmarbles](https://rxmarbles.com/)

### Pourquoi ?

1. Solution optimale pour la gestion de l'asynchrone
2. Stream : Flux de données disponibles progressivement, similaire au streaming vidéo
3. Observable : Émission automatique de flux de données vers les abonnés
4. Operators : Traitement déclaratif des flux asynchrones complexes

```javascript
// RxJS v6+
import { map, filter, reduce, tap } from 'rxjs/operators';

// observable (La convention de nommage est de suffixer les "Observables" avec $.)
const range$ = range(1, 10);

// subscription
range$
    .pipe(
        // combine de multiple opérateurs
        map((item) => item * 10), // similaire à `Array.map` ; applique un traitement à chaque valeur
        filter((item) => item % 20 === 0), // Retourne les valeurs qui passent la condition
        reduce((accumulator, item) => accumulator + item), // similaire à `Array.prototype.reduce()` ; réduit les valeurs à une seule
        tap((sum) => console.log(sum)), // faire quelque chose avec les valeurs (pas de transformation, pas d'effet de bord)
    )
    .subscribe(); // permet de souscrire à un "Observable" et être notifié des nouvelles valeurs, des erreurs ou de la fin du "Stream"

// output : 300
```

5. debouncing : Permet d'ignorer sous condition des valeurs. Ex : lancer une recherche dans un Input que si au moins 3 lettres sont saisies.

```javascript
// RxJS v6+
import { fromEvent } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

// element reference
const searchBox = document.getElementById('search');

// streams
const search$ = fromEvent(searchBox, 'keyup');

// subscription
search$
    .pipe(
        debounceTime(500), // attendre .5s
        distinctUntilChanged(), // emet seulement si la valeur courante est différente de la dernière
        filter((search) => search.length >= 3),
        tap((search) => console.log(search)),
    )
    .subscribe();
```

6. Replay : permet de ne pas relancer N requêtes lorsque l'on appelle plusieurs fois un "Observable".

```javascript
// RxJS v6+
import { of, shareReplay, tap } from 'rxjs/operators';

// observable (datas source request)
const observable$ = of('datas') // créer un Observable ; `of` = `Observable.create(function(observer) {});`
    .pipe(
        tap(() => console.log(`observable request`)),
        shareReplay(), // on ne l'appel qu'un fois
    );

// subscription #1
observable$.pipe(tap((item) => console.log(`Les données ${item} provenant de la subscription #1`))).subscribe();

// subscription #2
observable$.pipe(tap((item) => console.log(`Les données ${item} provenant de la  subscription #2`))).subscribe();

// output :
// observable request
// Les données datas provenant de la subscription #1
// Les données datas provenant de la subscription #2
```

7. Maitrise sur les Streams, Observables, Subscription. Nous pouvons stopper, relancer, créer...
8. Error catching : gestion des erreurs par une séquence d'`Observable`.

Pour ne pas se faire coincer dans un enchevêtrement de Promise, event, action, state, il faut penser différemment avec la programmation réactive.

C'est une façon de programmer qui concorde bien avec Javascript.  
Cela impose d'utiliser massivement des Design Pattern approuvés et éprouvés (Observer, Iterator).

Une source de données est appelé **`Observable`** en référence au Design Pattern "Observer".  
Une source de données ne doit pas faire, mais prévenir (coucou, j'ai les données que vous avez demandé).  
La source de données ne fait que 2 choses : mettre à disposition des données et envoyer un évènement.  
Le code qui a besoin de cette source de données doit "écouter" (**`subscription`**) l'évènement émis par celle-ci.  
Lorsqu'un évènement est émis par la source de données, ceux qui écoutent, peuvent aller récupérer les données.  
Au sein d'un `Observable`, les actions, évènements et données sont streamées.

### Example : dresser une table

on lance N fetch en parallèle qui forme un lot.  
Le lot, c'est un table de cuisine. Les fetch mettent la table.  
La contrainte, est de mettre la fourchette, le couteau, le dessous de table, l'assiete, le verre et la serviette en meme temps.  
L'observable va donc envoyé N lots qui doivent être correctement rempli.  
chaque Fetch (ici 6) s'occupe d'un ustensile de cuisine.  
La contrainte est d'avoir chaque fetch coordonné pour remplir un lot : la solution opérateur RxJS `zip`.

Exemple graphique de fonctionnement de quelques opérateurs : [rxmarbles](https://rxmarbles.com/#filter)  
Liste complète des outils : [operators](https://www.learnrxjs.io/operators/)

[//]: # '![](../../src/assets/img/rx-6.png =800x300)'

> par convention les `Observable` sont préfixés par un `$`

```javascript
import { delay } from 'rxjs/operators';
import { of, zip } from 'rxjs';

// `of` crée un Observable.
// Cela équivaut à :
// import { Observable } from 'rxjs';
// const helloObservable$ = Observable.create(function(observer) {});
const delayValue = 100;

const fork$ = of('fourchette');
const knife$ = of('couteau');
const towel$ = of('serviette');
const underside$ = of('dessous');
const dish$ = of('assiette');
const glass$ = of('verre');
// les résulats sont mis en cache.
const place$ = zip(
    fork$,
    knife$.pipe(delay(delayValue)),
    towel$.pipe(delay(delayValue)),
    underside$.pipe(delay(delayValue)),
    dish$.pipe(delay(delayValue)),
    glass$.pipe(delay(delayValue)),
);

const tabGuests = ['toi', 'eux', 'ils', 'vous', 'nous', 'moi'];

for (let guest of tabGuests) {
    // ont peut dresser une place
    place$.subscribe(
        (data) => setPlace(guest, data),
        (error) => console.error('error', error),
        () => console.log('complete'),
    );
}
```

Problématique de maintenance des Stores globaux d'évènements (redux, vuex...)

-   Trop d'effets de bord
-   Action, Mutation, State peut devenir lourd sur de grosses applications
-   Le changement d'état n'est géré que dans le Store et n'est pas communiqué. Ce sont les composants qui doivent interroger le store
-   la relation composant - Store peut devenir un code spaghetti
-   Pas de chaînage d'évènements dans la temps (une promise doit se finir)
-   L'échange des états peut devenir laborieux

Avantage de RxJs "store & more" versus "store state management" http://rudiyardley.com/redux-single-line-of-code-rxjs/

-   Résout les Callbacks Hell et promise pyramid
-   Les Observables sont intégrés dans ES7
-   tout est flux (stream)
    -   permet de fusionner les Observables et donc de souscrire à un lot d'Observables
    -   Meme dans un lot d'Observables, il est possible de souscrire unitairement par les Subjects
-   choix synchrone/asynchrone avec possibilité de renvoyer plusieurs retours (ce que ne font pas les Promise)
-   simplifie la gestion des états Observable Store
    -   1 seule source
    -   les états sont en lecture seule - non mutables
    -   renvoi un évènement à chaque changement à tout les souscripteurs
    -   moins de code à écrire
    -   fonctionne avec n'importe quel Framework FO
    -   fonctionne avec typeScript
-   les tendances vont très fortement vers RxJS (nouveau paradigme de programmation)

Il n'est pas nécessaire d'utiliser un Observable - subscription systématiquement. les questions à se poser :

-   une action déclenche plusieurs évènements
-   Plusieurs appel asynchrone et qu'ils doivent fonctionner ensemble
-   les mise à jour doivent réactives
-   la manipulations des données (voir opérateurs)

Pour afficher un message, il y a plusieurs moyens.

Afficher un message

```javascript
function displayMessage(message) {
    /*....... */
}
```

Evènement de l'utilisateur

```javascript
const sendMessageEl = document.getElementById('sendMessage');
sendMessageEl.addEventListener('submit', (event) => {
    displayMessage('Message envoyé!');
});
```

Données externe via Promise (API)

```javascript
fetch('/message/1')
    .then((response) => response.json())
    .then((message) => displayMessage(message))
    .catch((message) => displayMessage('Bug.'));
```

par WebSocket : ouverture de connexion, flux de messages

```javascript
const ws = new WebSocket('ws://socket.server');
const onMessageReceived = (event) => {
    const message = event.data;
    displayMessage(message);
};
ws.addEventListener('message', onMessageReceived, false);
```

D'une config (une array)

```javascript
['val1', 'val2', 'val3', 'val4'].forEach((item) => displayMessage(item));
```

et ...

Et si l'on veut faire tout ça en seul coup, réutilisable ?  
Comment compter le nombre de messages envoyé, reçu, depuis hier ?

Exemple :

```javascript
import { Observable } from 'rxjs'; //besoin que de l'observalbe
const source$ = new Observable((observer) => {
    //mise en place de l'observalbe
    observer.next('Bonjour!');
    observer.next('Tout le monde.');
    observer.error(new Error('Erreur. :/'));
    observer.complete();
});
```

Evènement de l'utilisateur

```javascript
const submit$ = new Observable((observer) => {
    sendMessageEl.addEventListener(
        'submit',
        (event) => {
            observer.next(event);
        },
        false,
    );
});
```

Réagir à la source de données

```javascript
submit$.subscribe(
    (data) => displayMessage(data),
    (error) => console.error('error', error),
    () => console.log('complete'),
);
```
