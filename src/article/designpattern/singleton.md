---
title: 🚀 Maîtrisez le Design Pattern "Singleton" en TypeScript 5 🌉
index: true
date: 2023-12-10
icon: spider
category:
    - code
    - JavaScript
    - design pattern
    - typescript
---

<img src="./dp-singleton.jpeg" alt="Design Pattern Singleton" title="Design Pattern Singleton"/>

# **🚀 Maîtrisez le Design Pattern "Singleton" en TypeScript 💻**

Bonjour à tous! Aujourd'hui, nous allons parler du design pattern Singleton en TypeScript. C'est un modèle de conception très populaire, mais aussi sujet à de nombreux débats. Alors, plongeons-nous dans le vif du sujet! 🏊‍♂️

## **👍 Les avantages du Singleton 👍**

Le Singleton est un design pattern qui garantit qu'une classe n'a qu'une seule instance, et fournit un point d'accès global à cette instance. C'est particulièrement utile pour les ressources partagées, comme les connexions à une base de données ou un fichier de log.

1. **Instance Unique 🦄 :** Garantit qu'une seule instance de la classe est créée, évitant les duplications inutiles et économisant des ressources.
2. **Accès Global 🌐 :** Permet un accès global à l'instance unique, simplifiant la communication entre différentes parties du code.
3. **Optimisation de la Mémoire 💾 :** Réduit la consommation de mémoire en maintenant une seule instance active.

## **⚠️ Les limites du Singleton ⚠️**

Cependant, le Singleton a aussi ses inconvénients. Il peut être difficile à tester en raison de son accès global, et il peut encourager l'utilisation excessive de variables globales. De plus, dans un contexte multithread, l'instanciation du Singleton peut causer des problèmes si plusieurs threads tentent de créer l'instance en même temps.

1. **Difficulté de Tests Unitaires 🧪 :** L'instance unique peut poser des défis lors des tests unitaires, car elle peut créer des dépendances indésirables.
2. **Couplage Fort 🤝 :** Peut entraîner un couplage fort entre différentes parties du code, rendant les modifications plus délicates.

## **🔄 Les évolutions du Singleton 🔄**

Avec l'évolution des langages de programmation et des paradigmes de programmation, le Singleton a également évolué. En TypeScript, nous pouvons utiliser les decorators pour créer des singletons de manière plus simple et plus sûre:

```tsx
export default function SingletonDecorator<T extends new (...args: any[]) => any>(ctr: T): T {
  let instance: T;

  return class {
    constructor(...args: any[]) {
      if (instance) {
        console.error('You cannot instantiate a singleton twice!');
        return instance;
      }

      instance = new ctr(...args);
      return instance;
    }
  } as T;
}

import { SingletonAdvancedDecorator } from './SingletonAdvancedDecorator';

/*
Add this to tsconfig.json to enable decorators in TS:
 "experimentalDecorators": true,
 "emitDecoratorMetadata": true,
*/
@SingletonAdvancedDecorator
class Store {
  private data: { id: number }[] = [];

  public add(item: { id: number }) {
    this.data.push(item);
  }

  public get(id: number) {
    return this.data.find((d) => d.id === id);
  }
}

const myStore = new Store();
myStore.add({ id: 1 });
myStore.add({ id: 2 });
myStore.add({ id: 3 });

// cant't reset the store
const anotherStore = new Store();
// This will return the first instance `myStore` of the store
anotherStore.get(2);

console.log(anotherStore.get(2));
```

Plusieurs exemples peuvent être consultés sur GitHub :

https://github.com/giak/design-patterns-typescript/tree/main/src/singleton

## **🔄 Peut-il être remplacé par un autre design pattern ? 🔄**

Dans certains cas, le Singleton peut être remplacé par le design pattern Factory ou Abstract Factory, qui peuvent fournir une instance unique tout en offrant plus de flexibilité et en facilitant les tests.
L’injection de dépendance permet aussi de limiter les instances de classes.

## **❓ Quand utiliser le Singleton ? ❓**

Avant d'utiliser le Singleton, posez-vous les questions suivantes:

1. Ai-je besoin d'un accès global à cette instance?
2. Ai-je besoin d'une seule instance de cette classe?
3. Le couplage fort entre les composants est-il acceptable?
4. Les avantages du Singleton l'emportent-ils sur ses inconvénients dans mon cas d'utilisation spécifique?

## **📚 Exemples de cas d'utilisation 📚**

Le Singleton est souvent utilisé pour les gestionnaires de connexion à une base de données, les fichiers de log, le cachen les Proxies entre deux serveurs et les gestionnaires de configuration.

En intégrant ces exemples étendus dans vos projets, le design pattern Singleton en TypeScript devient une arme polyvalente pour améliorer la structure, la maintenance et l'efficacité de votre code. 🚀✨

N'hésitez pas à partager vos pensées et vos questions dans les commentaires! 👋