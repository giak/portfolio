---
title: 🚀 Maîtrisez le Design Pattern "Prototype" en TypeScript 5 🌉
index: true
date: 2023-12-05
icon: spider
category:
    - code
    - JavaScript
    - design pattern
    - typescript
---

<img src="./dp-prototype.jpeg" alt="Design Pattern Prototype" title="Design Pattern Prototype"/>

# **🚀 Maîtrisez le Design Pattern "Prototype" en TypeScript 💻**

Salut les développeurs! 👋 Aujourd'hui, j'aimerais partager quelques réflexions sur le design pattern Prototype, un classique du Gang of Four. Nous allons explorer son utilisation en TypeScript, ses avantages, ses limites, ses évolutions potentielles et quand l'utiliser. Plongeons! 🏊‍♂️

## **🏗️ Qu'est-ce que le Design Pattern Prototype?**

Le design pattern Prototype permet de créer de nouveaux objets en clonant un objet existant, appelé le prototype. C'est particulièrement utile lorsque la création d'un nouvel objet qui est coûteuse en termes de ressources.

Voici quelques petits exemples en TypeScript :

```tsx
class CarPrototype {
  constructor(public model: string) {}

  getModel() {
    console.log(`The model of this vehicle is... ${this.model}`);
  }

  clone() {}
}

class Car extends CarPrototype {
  constructor(public model: string) {
    super(model);
  }

  clone() {
    return new Car(this.model);
  }
}

const carObject = new Car('Citroën 2 CV');
console.log(`carObject`, carObject);
carObject.getModel();

const carClone = carObject.clone();
console.log(`carClone`, carClone);
carClone.getModel();
```

Dans ce code, nous avons une classe `Car` avec une méthode `clone()`. Cette méthode crée un nouvel objet `Car` en clonant l'objet existant. 📰

Une autre façon plus concise sans passer par des classes est :

```tsx
const carPrototype = {
  drive() {
    console.log('drive');
  },
  panic() {
    console.log('panic');
  },
};

const createCar = (name: string) => {
  const car = Object.create(carPrototype);
  car.name = name;
  return car;
};

const myCar = createCar('Citroën 2 CV');
console.log(myCar.name);
myCar.drive();
myCar.panic();

const myCar2 = createCar('Renault 4L');
console.log(myCar2.name);
```

La différence notable est `Object.create` en JavaScript natif. 

encore un autre exemple qui permet d’étendre des objects littéraux à la JSON :

```tsx
const vehicle = {
  getModel() {
    // @ts-ignore
    console.log(`The model of this vehicle is... ${this.model}`);
  },
};

const car = Object.create(vehicle, {
  id: {
    value: Math.random().toString(36).substr(2, 9),
    enumerable: true,
  },
  model: {
    value: 'Renault 4L',
    enumerable: true,
  },
});

car.getModel();
console.log(carInstance);
```

Cette méthode permet de créer un nouvel object étendu.

Vous pouvez trouver ces exemple et d’autres sur Github :

https://github.com/giak/design-patterns-typescript/tree/main/src/prototype

## **🛠️ Exemples Pratiques**

- **Construction d'une Newsletter :** Cloner un modèle de newsletter avec des composants récurrents. 📧🔁
- **Configuration d'une Interface d'Administration :** dupliquer des configurations d'utilisateurs pour simplifier le processus de création. 🖥️🔄
- **Agréger des données :** regrouper Facture, Bon de commande, Devis. 📊🔄
- Dans certain cas, le **traitement en récursif** de données (algorithme). 🔄📂

## **🚀 Avantages du Design Pattern Prototype**

1. **Performance**: Le clonage d'objets est souvent plus rapide que leur instanciation à partir de zéro.
2. **Flexibilité**: Le pattern Prototype permet de créer des copies d'objets sans dépendre de leur classe concrète.
3. 🚨 **Mutabilité**: Utiliser un clonage dit en profondeur permet de limiter des effets de bords. 🔄

## **🚧 Limites du Design Pattern Prototype**

1. **Clonage complexe**: Le clonage d'objets peut devenir complexe lorsque les objets ont des références à d'autres objets.
2. **Maintenance difficile**: Le code peut devenir difficile à maintenir si le nombre de classes de prototypes est trop élevé.

## **🔄 Évolutions et Alternatives**

Le design pattern Prototype a résisté à l'épreuve du temps, mais cela ne signifie pas qu'il ne peut pas être remplacé. Le design pattern Factory est une alternative plus simple, idéale pour les situations où la création d'un seul type de produit est nécessaire. 🔄

## **❓ Quand utiliser le Design Pattern Prototype**

Avant d'implémenter le design pattern Prototype, posez-vous les questions suivantes:

1. La création d'objets est-elle une opération coûteuse en termes de ressources dans mon application? 🤔
2. Ai-je besoin de créer des copies d'objets à la volée sans connaître leur classe concrète? 🕵️‍♂️

Si vous avez répondu "oui" à ces questions, le design pattern Prototype pourrait être une bonne solution pour votre projet! 🎯

J'espère que vous avez trouvé cet article utile! Si vous avez des questions ou des réflexions, n'hésitez pas à laisser un commentaire ci-dessous. Bon codage! 💻🚀