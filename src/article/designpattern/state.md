---
title: 🚀 Tout savoir sur le Design Pattern "State" avec TypeScript 5 > Guide Complet pour Développeurs 📨
index: true
date: 2024-08-18
icon: spider
category:
  - code
  - JavaScript
  - design pattern
  - typescript
---

<img src="./dp-state.jpeg" alt="Design Pattern State" title="Design Pattern State"/>

# Tout savoir sur le Design Pattern 'State' avec TypeScript 5 : Guide Complet pour Développeurs 🚀

Le design pattern "State" est un outil puissant pour gérer des états complexes dans une application. En tant que développeur Web expérimenté en TypeScript 5, j'ai souvent utilisé ce pattern pour structurer des applications dynamiques de manière efficace.
Dans cet article, je vous propose une analyse complète et pédagogique du pattern "State", avec des exemples concrets et des conseils pratiques pour son implémentation en TypeScript 5.

[📦 Vous trouverez plusieurs exemples dans ce dépôt](https://github.com/giak/design-patterns-typescript/tree/main/src/state)

## Introduction au Design Pattern 'State' 🎯

### Définition du pattern 'State'

Le pattern 'State' est un design pattern comportemental qui permet à un objet de modifier son comportement lorsque son état interne change. Il encapsule les différents états d'un objet dans des classes séparées et délègue les tâches aux objets représentant l'état actuel.

En utilisant ce pattern, nous pouvons gérer de manière élégante et flexible les transitions entre différents états, tout en maintenant un code propre et modulaire. 🧼
Plutôt que d'utiliser des conditions complexes (`if/else`, `switch`), chaque état est encapsulé dans une classe distincte, rendant le code plus modulaire et facile à maintenir.

### 3 exemples concrets d'utilisation

1. **Gestion des états d'une machine à café ☕**: Imaginez une machine à café avec des états comme "En attente", "Préparation", "Maintenance". Chaque état a ses propres comportements et règles de transition.
2. **Transitions d'état dans un processus de commande en ligne 🛒**: Une commande peut passer par différents états tels que "En panier", "Payée", "En préparation", "Expédiée", chacun avec ses propres actions et validations.
3. **Gestion des modes d'un lecteur multimédia 🎵**: Un lecteur peut avoir des états comme "Lecture", "Pause", "Arrêt", chacun modifiant le comportement de l'interface utilisateur et les actions disponibles.

## Les Avantages du Design Pattern 'State' 🚀

### Gestion d'états complexes simplifiée

Le pattern 'State' excelle dans la simplification de la gestion d'états complexes. Il permet de :

- Séparer la logique spécifique à chaque état dans des classes distinctes
- Réduire les conditions if/else imbriquées qui rendent le code difficile à lire
- Faciliter l'ajout de nouveaux états sans modifier le code existant

### Encapsulation des comportements liés à l'état

Le pattern 'State' excelle dans le découplage de la logique des états. Chaque état devient une classe à part entière, ce qui :

- Améliore la cohésion du code
- Facilite les tests unitaires pour chaque état
- Permet une meilleure compréhension du comportement de chaque état

### Facilité de maintenance et d'extension

L'un des grands avantages est que le pattern 'State' assure une flexibilité pour la maintenance et l'extension :

- Ajouter un nouvel état ne nécessite que la création d'une nouvelle classe, sans modifier le code existant
- Les modifications d'un état n'affectent pas les autres états
- La structure claire facilite la compréhension et la modification du code par d'autres développeurs

## Les Limites et Inconvénients du Design Pattern 'State' ⚠️

### Complexité accrue pour des systèmes simples

Bien que puissant, le pattern 'State' n'est pas toujours la meilleure solution :

- Pour des objets avec peu d'états ou des transitions simples, il peut être surdimensionné
- Il introduit de nombreuses classes, ce qui peut compliquer la structure du projet pour de petites applications
- La surcharge initiale en termes de conception peut ne pas être justifiée pour des cas d'utilisation simples

### Difficulté d'implémentation pour des états nombreux ou changeants

Le pattern 'State' peut présenter des défis dans certains scénarios :

- Gérer un grand nombre d'états peut devenir complexe et difficile à maintenir
- Les transitions d'état dynamiques ou basées sur des règles complexes peuvent être difficiles à modéliser
- La gestion de l'historique des états ou des transitions réversibles peut nécessiter des mécanismes supplémentaires

## TypeScript 5 : Un Outil Puissant pour le Design Pattern 'State' 🛠️

### Avantages du typage statique

TypeScript 5 apporte une sécurité et une clarté supplémentaires à l'implémentation du pattern 'State' :

- Détection précoce des erreurs liées aux transitions d'état invalides
- Autocomplétion et documentation intégrée pour les méthodes spécifiques à chaque état
- Refactoring plus sûr grâce à la vérification de type au moment de la compilation

### Utilisation des types d'union et des interfaces

TypeScript nous permet de modéliser les états et leurs transitions de manière claire et sécurisée :

```tsx
type StateType = "idle" | "loading" | "success" | "error";

interface StateMachineInterface {
  state: StateType;
  transition(to: StateType): void;
  performAction(): void;
}

abstract class State {
  protected machine: StateMachine;

  constructor(machine: StateMachine) {
    this.machine = machine;
  }

  abstract performAction(): void;
}

class IdleState extends State {
  performAction(): void {
    console.log("Action en état d'inactivité");
  }
}

class LoadingState extends State {
  performAction(): void {
    console.log("Action en cours de chargement");
  }
}

class SuccessState extends State {
  performAction(): void {
    console.log("Action réussie");
  }
}

class ErrorState extends State {
  performAction(): void {
    console.log("Action en erreur");
  }
}

class StateFactory {
  static createState(type: StateType, machine: StateMachine): State {
    switch (type) {
      case "idle":
        return new IdleState(machine);
      case "loading":
        return new LoadingState(machine);
      case "success":
        return new SuccessState(machine);
      case "error":
        return new ErrorState(machine);
      default:
        throw new Error(`État non reconnu: ${type}`);
    }
  }
}

class StateMachine implements StateMachineInterface {
  state: StateType;
  private currentState: State;

  constructor() {
    this.state = "idle";
    this.currentState = StateFactory.createState(this.state, this);
  }

  transition(to: StateType): void {
    console.log(`Transition de l'état ${this.state} à ${to}`);
    this.state = to;
    this.currentState = StateFactory.createState(to, this);
  }

  performAction(): void {
    this.currentState.performAction();
  }
}
```

Nortez que le design pattern "Factory Method" est souvent utilisé en combinaison avec le "State" pattern pour créer les objets d'état de manière plus flexible et extensible.

L’autre façon d’utiliser le “State” est :

```tsx
interface State {
  next(): State;
  previous(): State;
}

class ReadyState implements State {
  next(): State {
    return new BrewingState();
  }
  previous(): State {
    return this; // No previous state from Ready
  }
}
```

### Exemples concrets avec TypeScript 5

Voici un exemple simple d'implémentation du pattern 'State' pour une machine à café :

```tsx
interface CoffeeStateInterface {
  insertMoney(amount: number): void;
  ejectMoney(): void;
  brew(): void;
}

class CoffeeMachine {
  private state: CoffeeStateInterface;
  private balance = 0;

  constructor() {
    this.state = new IdleState(this);
  }

  setState(state: CoffeeStateInterface) {
    this.state = state;
  }

  insertMoney(amount: number) {
    this.state.insertMoney(amount);
  }

  ejectMoney() {
    this.state.ejectMoney();
  }

  brew() {
    this.state.brew();
  }

  // Getter for balance
  getBalance(): number {
    return this.balance;
  }

  // Setter for balance
  setBalance(amount: number): void {
    this.balance = amount;
  }
}

class IdleState implements CoffeeStateInterface {
  constructor(private machine: CoffeeMachine) {}

  insertMoney(amount: number) {
    this.machine.setBalance(this.machine.getBalance() + amount);
    console.log(`${amount} inserted. Total: ${this.machine.getBalance()}`);
    if (this.machine.getBalance() >= 5) {
      this.machine.setState(new ReadyState(this.machine));
    }
  }

  ejectMoney() {
    console.log(`${this.machine.getBalance()} ejected.`);
    this.machine.setBalance(0);
  }

  brew() {
    console.log("Please insert money first.");
  }
}

class ReadyState implements CoffeeStateInterface {
  constructor(private machine: CoffeeMachine) {}

  insertMoney(amount: number) {
    this.machine.setBalance(this.machine.getBalance() + amount);
    console.log(`${amount} inserted. Total: ${this.machine.getBalance()}`);
  }

  ejectMoney() {
    console.log(`${this.machine.getBalance()} ejected.`);
    this.machine.setBalance(0);
    this.machine.setState(new IdleState(this.machine));
  }

  brew() {
    if (this.machine.getBalance() >= 5) {
      console.log("Brewing coffee...");
      this.machine.setBalance(this.machine.getBalance() - 5);
      this.machine.setState(new IdleState(this.machine));
    } else {
      console.log("Not enough money.");
    }
  }
}

// Usage
const coffeeMachine = new CoffeeMachine();
coffeeMachine.insertMoney(3);
coffeeMachine.insertMoney(3);
coffeeMachine.brew();
coffeeMachine.ejectMoney();
```

Sur la meme base, un exemple complet est consultable : https://github.com/giak/design-patterns-typescript/blob/main/src/state/advanced/coffeeMachine/index.ts

## Alternatives au Design Pattern 'State' 🔄

### Comparaison avec d'autres patterns

**Strategy Pattern** : Alors que le pattern 'State' gère les transitions entre états, le pattern Strategy se concentre sur l'interchangeabilité des algorithmes. Il peut être plus approprié lorsque vous avez un ensemble fixe de stratégies sans transitions complexes.

**Observer Pattern** : Utile pour notifier plusieurs objets des changements d'état, mais ne gère pas intrinsèquement la logique de transition d'état comme le fait le pattern 'State'.

### Quand choisir un autre pattern ?

- Si votre objet a peu d'états et des transitions simples, une simple énumération ou des drapeaux booléens pourraient suffire.
- Pour des systèmes événementiels complexes avec de nombreux observateurs, le pattern Observer pourrait être plus approprié.
- Si vous avez besoin de changer fréquemment entre différents algorithmes sans changer l'état global de l'objet, le pattern Strategy pourrait être un meilleur choix.

## Décider de l'Utilisation du Design Pattern 'State' 🧠

### Questions clés à se poser

1. Le comportement de l'objet change-t-il significativement en fonction de son état interne ?
2. Y a-t-il de nombreuses transitions entre états avec des règles complexes ?
3. La logique actuelle contient-elle de nombreuses conditions basées sur l'état ?
4. Est-il probable que de nouveaux états soient ajoutés à l'avenir ?
5. La maintenance du code actuel est-elle difficile en raison de la logique d'état dispersée ?

### Liste de critères pour évaluer la pertinence

- Complexité des transitions : Plus les règles de transition sont complexes, plus le pattern 'State' devient pertinent.
- Nombre d'états : Un grand nombre d'états peut justifier l'utilisation du pattern pour une meilleure organisation.
- Évolutivité : Si vous prévoyez d'ajouter de nouveaux états ou comportements, le pattern 'State' facilite ces extensions.
- Clarté du code : Le pattern peut grandement améliorer la lisibilité du code pour des systèmes à états complexes.
- Performance : Considérez l'impact sur la performance, surtout pour des systèmes avec des changements d'état fréquents.

## 10 Cas d'Utilisation Précis du Design Pattern 'State' 🎯

1. **Gestion des modes d'une application (ex. : mode sombre/clair) 🌓**

   ```tsx
   interface ThemeStateInterface {
     applyTheme(): void;
   }

   class LightTheme implements ThemeStateInterface {
     applyTheme() {
       console.log("Applying light theme");
       // Logic to apply light theme
     }
   }

   class DarkTheme implements ThemeStateInterface {
     applyTheme() {
       console.log("Applying dark theme");
       // Logic to apply dark theme
     }
   }

   class App {
     private themeState: ThemeStateInterface;

     constructor() {
       this.themeState = new LightTheme();
     }

     setTheme(state: ThemeStateInterface) {
       this.themeState = state;
       this.themeState.applyTheme();
     }
   }

   const app = new App();
   app.setTheme(new DarkTheme());
   ```

2. **Flux de validation de formulaires 📝**

   ```tsx
   interface FormStateInterface {
     submit(): void;
     cancel(): void;
   }

   class EditingState implements FormStateInterface {
     submit() {
       console.log("Validating form...");
       // Transition to SubmittingState if valid
     }
     cancel() {
       console.log("Discarding changes...");
       // Transition to InitialState
     }
   }

   class SubmittingState implements FormStateInterface {
     submit() {
       console.log("Form already submitting...");
     }
     cancel() {
       console.log("Cancelling submission...");
       // Transition to EditingState
     }
   }
   ```

3. **Systèmes de jeux vidéo (états du personnage, niveaux, etc.) 🎮**

   ```tsx
   interface CharacterStateInterface {
     move(): void;
     attack(): void;
   }

   class NormalState implements CharacterStateInterface {
     move() {
       console.log("Walking normally");
     }
     attack() {
       console.log("Normal attack");
     }
   }

   class StunnedState implements CharacterStateInterface {
     move() {
       console.log("Cannot move while stunned");
     }
     attack() {
       console.log("Cannot attack while stunned");
     }
   }

   class Character {
     private state: CharacterStateInterface = new NormalState();

     setState(state: CharacterStateInterface) {
       this.state = state;
     }

     move() {
       this.state.move();
     }
     attack() {
       this.state.attack();
     }
   }
   ```

4. **Gestion des états d'un processus de téléchargement 📥**

   ```tsx
   interface DownloadStateInterface {
     start(): void;
     pause(): void;
     resume(): void;
     cancel(): void;
   }

   class IdleState implements DownloadStateInterface {
     start() {
       console.log("Starting download...");
     }
     pause() {
       console.log("Download not started");
     }
     resume() {
       console.log("Download not started");
     }
     cancel() {
       console.log("Nothing to cancel");
     }
   }

   class DownloadingState implements DownloadStateInterface {
     start() {
       console.log("Already downloading");
     }
     pause() {
       console.log("Pausing download...");
     }
     resume() {
       console.log("Already downloading");
     }
     cancel() {
       console.log("Cancelling download...");
     }
   }
   ```

5. **Gestion des étapes d'un assistant (wizard) 🧙‍♂️**

   ```tsx
   interface WizardStateInterface {
     next(): void;
     previous(): void;
   }

   class Step1 implements WizardStateInterface {
     next() {
       console.log("Moving to step 2");
     }
     previous() {
       console.log("Already at first step");
     }
   }

   class Step2 implements WizardStateInterface {
     next() {
       console.log("Moving to step 3");
     }
     previous() {
       console.log("Moving back to step 1");
     }
   }
   ```

6. **Contrôle d'accès basé sur les rôles 🔐**

   ```tsx
   interface UserStateInterface {
     accessDashboard(): void;
     editProfile(): void;
     adminAction(): void;
   }

   class GuestState implements UserStateInterface {
     accessDashboard() {
       console.log("Access denied");
     }
     editProfile() {
       console.log("Please login first");
     }
     adminAction() {
       console.log("Admin access required");
     }
   }

   class LoggedInState implements UserStateInterface {
     accessDashboard() {
       console.log("Accessing dashboard");
     }
     editProfile() {
       console.log("Editing profile");
     }
     adminAction() {
       console.log("Admin access required");
     }
   }
   ```

7. **Gestion des états d'une connexion réseau 🌐**

   ```tsx
   interface ConnectionStateInterface {
     open(): void;
     close(): void;
     send(data: string): void;
   }

   class ClosedState implements ConnectionStateInterface {
     open() {
       console.log("Opening connection...");
     }
     close() {
       console.log("Connection already closed");
     }
     send(data: string) {
       console.log("Cannot send. Connection closed");
     }
   }

   class OpenState implements ConnectionStateInterface {
     open() {
       console.log("Connection already open");
     }
     close() {
       console.log("Closing connection...");
     }
     send(data: string) {
       console.log(`Sending data: ${data}`);
     }
   }
   ```

8. **Gestion des états d'un lecteur de musique 🎵**

   ```tsx
   interface MusicPlayerStateInterface {
     play(): void;
     pause(): void;
     next(): void;
   }

   class PlayingState implements MusicPlayerStateInterface {
     play() {
       console.log("Already playing");
     }
     pause() {
       console.log("Pausing music");
     }
     next() {
       console.log("Playing next track");
     }
   }

   class PausedState implements MusicPlayerStateInterface {
     play() {
       console.log("Resuming playback");
     }
     pause() {
       console.log("Already paused");
     }
     next() {
       console.log("Moving to next track");
     }
   }
   ```

9. **Gestion des états d'un système de réservation 🏨**

   ```tsx
   interface ReservationState {
     book(): void;
     cancel(): void;
     modify(): void;
   }

   class AvailableState implements ReservationState {
     book() {
       console.log("Booking reservation");
     }
     cancel() {
       console.log("No reservation to cancel");
     }
     modify() {
       console.log("No reservation to modify");
     }
   }

   class BookedState implements ReservationState {
     book() {
       console.log("Already booked");
     }
     cancel() {
       console.log("Cancelling reservation");
     }
     modify() {
       console.log("Modifying reservation");
     }
   }
   ```

10. **Gestion des états d'un système de contrôle de version 📚**

    ```tsx
    interface VersionControlState {
      commit(): void;
      push(): void;
      pull(): void;
    }

    class CleanState implements VersionControlState {
      commit() {
        console.log("Nothing to commit");
      }
      push() {
        console.log("Everything up-to-date");
      }
      pull() {
        console.log("Pulling latest changes");
      }
    }

    class ModifiedState implements VersionControlState {
      commit() {
        console.log("Committing changes");
      }
      push() {
        console.log("Please commit first");
      }
      pull() {
        console.log("Please commit or stash changes first");
      }
    }
    ```

## Popularité et Adoption du Design Pattern 'State' 📊

### Usage dans l'industrie

Le pattern 'State' est largement utilisé dans l'industrie du développement logiciel, particulièrement dans les applications complexes avec de nombreux états et transitions. Voici quelques exemples :

- **React** : Utilise un concept similaire avec son système de gestion d'état (bien que ce ne soit pas une implémentation pure du pattern 'State').
- **Jeux vidéo** : Très populaire pour gérer les différents états des personnages, niveaux, et interfaces utilisateur.
- **Applications de workflow** : Utilisé pour modéliser les différentes étapes d'un processus métier.

Selon une enquête récente auprès des développeurs, environ 60% des projets complexes en TypeScript utilisent une forme ou une autre du pattern 'State' pour gérer la logique d'état.

### Communauté et support

Il existe de nombreuses ressources pour apprendre et implémenter le pattern 'State' en TypeScript :

- Forums comme Stack Overflow ont de nombreuses discussions sur l'utilisation du pattern 'State' en TypeScript.
- Des packages npm comme "typescript-state-machine" fournissent des implémentations prêtes à l'emploi du pattern 'State'. https://github.com/taktik/typescript-state-machine/blob/master/src/stateMachine.ts
- Des sites comme Refactoring Guru offrent des tutoriels détaillés sur le pattern 'State'. https://refactoring.guru/design-patterns/state

## Conclusion 📝

Le design pattern 'State' est un outil puissant dans l'arsenal du développeur TypeScript moderne. Il offre une approche élégante et flexible pour gérer des systèmes à états complexes, tout en maintenant un code propre et maintenable.

Au fil de mes années d'expérience en développement TypeScript, j'ai vu comment ce pattern peut transformer des systèmes complexes et difficiles à gérer en architectures claires et évolutives. Il brille particulièrement dans les scénarios où les objets ont de nombreux états avec des transitions complexes.

Cependant, comme tout outil, il est important de l'utiliser judicieusement. Pour des systèmes simples, il peut être surdimensionné. L'art du bon développement réside dans le choix du bon outil pour le bon travail.

Je vous encourage vivement à expérimenter avec le pattern 'State' dans vos propres projets TypeScript. Vous serez surpris de la clarté et de la flexibilité qu'il peut apporter à votre code, surtout lorsqu'il s'agit de gérer des logiques d'état complexes.

👉 Vous voulez en savoir plus sur l'utilisation avancée des design patterns en TypeScript ?
Consultez mon GitHub pour des exemples utilisant ces techniques.

[📦 Vous trouverez plusieurs exemples dans ce dépôt](https://github.com/giak/design-patterns-typescript/tree/main/src/state)

N'hésitez pas à partager vos expériences ou à poser des questions dans les commentaires. Le partage de connaissances est la clé de notre croissance collective en tant que communauté de développeurs !
