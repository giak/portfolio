---
title: 🚀 Maîtrisez le Design Pattern "Factory" en TypeScript 5 🌉
index: true
date: 2023-11-25
icon: spider
category:
    - code
    - JavaScript
    - design pattern
    - typescript
---

<img src="./dp-factory.jpeg" alt="Design Pattern Factory" title="Design Pattern Factory"/>


# 🚀 **Maîtrisez le Design Pattern "Factory" en TypeScript! 🛠️**

**Pourquoi le Design Pattern "Factory" en TypeScript?**

En utilisant ce pattern en TypeScript, nous pouvons déléguer la responsabilité de l'instanciation des objets à une factory, évitant ainsi de coupler le code à des classes concrètes.🌐

**Comment ça marche?**

Imaginons une situation où la création d'objets peut varier en fonction de conditions ou de paramètres. La "Factory" intervient en centralisant cette logique, offrant ainsi une solution modulaire et flexible.

```tsx
/* Factory Design Pattern */

type WalletTypes = {
  hardware: string;
  web: string;
  mobile: string;
  pc: string;
};

const walletTypesNames: WalletTypes = {
  hardware: 'Hardware',
  web: 'Web',
  mobile: 'Mobile',
  pc: 'PC',
};

interface WalletInterface {
  getBalance(): number;
  name: string;
}

class Wallet implements WalletInterface {
  name = '';
  getBalance(): number {
    return 0;
  }
}

export class WalletHardware extends Wallet {
  constructor() {
    super();
    this.name = walletTypesNames.hardware;
  }

  getBalance(): number {
    return 100;
  }
}

export class WalletWeb extends Wallet {
  constructor() {
    super();
    this.name = walletTypesNames.web;
  }

  getBalance(): number {
    return 10;
  }
}

export class WalletMobile extends Wallet {
  constructor() {
    super();
    this.name = walletTypesNames.mobile;
  }

  getBalance(): number {
    return 50;
  }
}

export class WalletPC extends Wallet {
  constructor() {
    super();
    this.name = walletTypesNames.pc;
  }

  getBalance(): number {
    return 8;
  }
}

export class WalletFactory {
  static create(type: string) {
    switch (type) {
      case walletTypesNames.hardware:
        return new WalletHardware();
      case walletTypesNames.web:
        return new WalletWeb();
      case walletTypesNames.mobile:
        return new WalletMobile();
      case walletTypesNames.pc:
        return new WalletPC();
      default:
        throw new Error('Invalid type');
    }
  }
}

const wallet = WalletFactory.create(walletTypesNames.hardware);
console.log(wallet.name);
console.log(wallet.getBalance());

```

**Pourquoi devriez-vous intégrer le Design Pattern "Factory"?**

🚀 **Flexibilité:** Adaptez votre code aux évolutions sans tracas.

🌱 **Modularité:** Encouragez une structure de code propre et modulaire.

🚗 **Extensibilité:** Facilitez l'ajout de nouvelles fonctionnalités sans impacter le code existant.

**🛑Les limites :**

1. **Complexité avec de nombreuses sous-classes :** Lorsque le nombre de sous-classes augmente, la Factory peut devenir complexe à gérer, ce qui peut entraîner une perte de clarté dans le code. 🤯
2. **Rigidité dans la création d'objets :** Si la logique de création des objets est relativement simple et ne change pas souvent, l'utilisation d'une Factory peut introduire une surcharge inutile. 🤷‍♂️
3. **Difficulté à étendre certains produits :** Si la création d'objets nécessite des paramètres spécifiques ou des initialisations complexes, la Factory peut devenir moins flexible et nécessiter des ajustements fréquents. 🧩

### 🚀Évolutions possibles :

1. **Utilisation conjointe avec d'autres patterns :** La combinaison du design pattern "Factory" avec d'autres patterns tels que l'Abstract Factory peut étendre sa capacité à créer des familles d'objets connexes. 🔄
2. **Adaptation aux changements de besoins :** L'évolution du design pattern "Factory" peut impliquer l'ajout de mécanismes de configuration dynamique pour permettre des changements sans modification du code source. 🔄
3. **Utilisation de généricité en TypeScript :** Si vous utilisez TypeScript, l'introduction de généricité dans les Factory peut améliorer la flexibilité en fournissant une meilleure typisation des objets créés. 🧰
4. **Adoption de modèles de conception plus avancés :** Dans des systèmes plus complexes, l'utilisation de modèles tels que l'injection de dépendances peut offrir une flexibilité accrue dans la gestion des objets. 🔄
5. **Gestion dynamique des sous-classes :** Au lieu d'une hiérarchie statique de sous-classes, une gestion dynamique, peut-être avec une configuration externe, peut permettre une évolutivité plus facile. 🔄
6. **Utilisation de Factory paramétrée :** Permettant la création d'objets variés avec un seul mécanisme, cette approche peut rendre la Factory plus adaptable aux changements de configuration. 🎛️





# Evolution

En tant qu'expert développeur typescript, j'aimerais écrire un post "linkedin" sur le remplacement d’un `switch` un mapping avec un typage de générique y adjoignant un exemple.
Le texte doit être pragmatique et professionnel, mais en ne négligeant pas le côté marketing afin de promouvoir mes compétences.
Le post peut intégrer des émojis dans le but d'illustrer le texte.

ChatGPt

🚀 **Optimisation de Code avec TypeScript: Adieu `switch`, Bonjour Mapping Générique!** 🚀

En tant que développeur TypeScript passionné, je souhaite partager une astuce qui a transformé ma manière d'aborder la logique conditionnelle. Dites au revoir aux déclarations **`switch`** lourdes et bienvenue à l'élégance d'un mapping générique! 💡

**Pourquoi abandonner le `switch`?**
Le **`switch`** peut parfois sembler le choix évident, mais il peut rendre le code volumineux et moins lisible. Avec un mapping générique, vous pouvez atteindre le même résultat de manière plus propre et maintenable.

**💻 Changement dans le code base précédent**

```tsx
// genericFactory.ts
export interface FactoryInterface<T> {
  create(type: string): T;
}

export class GenericFactory<T> implements FactoryInterface<T> {
  private constructorFunc: Record<string, new () => T>;

  constructor(constructorFunc: Record<string, new () => T>) {
    this.constructorFunc = constructorFunc;
  }

  create(type: string): T {
    const constructorFunc = this.constructorFunc[type] ?? null;
    if (!constructorFunc) {
      throw new Error(`Invalid ${type}`);
    }
    return new constructorFunc();
  }
}
```

```jsx
import { FactoryInterface } from './genericFactory';

// en remplacement de notre exemple ci-joint
export const WalletFactory = new GenericFactory<WalletInterface>({
  [walletTypesNames.hardware]: WalletHardware,
  [walletTypesNames.web]: WalletWeb,
  [walletTypesNames.mobile]: WalletMobile,
  [walletTypesNames.pc]: WalletPC,
});
```

Vous trouverez plusieurs exemples sur Github :
https://github.com/giak/design-patterns-typescript/tree/main/src/factory

**✨ Pourquoi c'est génial:**

- **👍 Clarté**: Le code est épuré, facile à lire et à comprendre.
- **🚀 Extensibilité**: Ajouter de nouveaux types de traitement devient un jeu d'enfant.
- **🔒 Sécurité**: Les erreurs de typage sont détectées à la compilation.

**🌟 Mettez en pratique votre expertise TypeScript!**
Challengez-vous à adopter cette approche dans votre prochain projet. La simplicité et la robustesse de cette méthode parlent d'elles-mêmes. Partagez votre expérience et faites briller votre expertise dans les commentaires! 💬


**Concluons en beauté:**

En résumé, bien que le design pattern "Factory" soit extrêmement utile, il est important de l'utiliser judicieusement en fonction des exigences spécifiques de votre application. Comprendre ses limites et explorer des évolutions possibles peut contribuer à un design logiciel plus robuste et flexible.

💡 Partagez vos expériences avec ce pattern et découvrons ensemble comment il a amélioré vos projets !

Ensemble, faisons de TypeScript la force motrice derrière un code plus élégant et performant! 💻✨