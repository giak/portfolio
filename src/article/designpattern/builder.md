---
title: 🚀 Maîtrisez le Design Pattern "Builder" en TypeScript 5 🌉
index: true
date: 2023-12-01
icon: spider
category:
    - code
    - JavaScript
    - design pattern
    - typescript
---

<img src="./dp-builder.jpeg" alt="Design Pattern Builder" title="Design Pattern Builder"/>


# **🚀 Maîtrisez le Design Pattern "Builder" en TypeScript 💻**

Si vous êtes un développeur web, vous avez probablement croisé le chemin du pattern "Builder". Dans cet article, explorons les avantages, les limites, les évolutions, les alternatives et les cas d'utilisation pertinents de ce design pattern puissant.

## **Avantages du Design Pattern "Builder" 🏗️**

Le design pattern "Builder" permet de séparer la construction d'un objet de sa représentation.
Voici quelques avantages majeurs :

1. **Configuration Flexible** 🛠️ : Permet de construire des objets avec des configurations variées sans surcharger le constructeur. 
2. **Gestion de la complexité 🧩**:  en décomposant la construction d'un objet en plusieurs étapes.
3. **Clarté du Code** 🧹 : Améliore la lisibilité en isolant le processus de construction dans une classe distincte.
4. **Réutilisation du Code** 🔁 : Favorise la réutilisation en permettant la création de plusieurs "builders" pour des variations d'objets.
5. **Immutabilité des objets** 🔐: limite les effets de bords.

## **Limites du Design Pattern "Builder" ❌**

Bien que puissant, le pattern "Builder" a ses limites. Il peut devenir fastidieux pour des objets simples et peut générer un surcoût en termes de complexité. Les situations suivantes peuvent être des défis :

1. **Overhead Initial** 📈 : Le coût initial de configuration peut sembler excessif pour des objets simples.
2. **Maintenance Intensive** 🧑‍🔧 : Les changements fréquents dans la structure des objets peuvent nécessiter des ajustements continus du "builder".

## **Évolutions du Design Pattern "Builder" 🔄**

Le "Builder" évolue pour répondre aux besoins croissants des développeurs. Avec TypeScript et ses fonctionnalités de vérification de types permet une implémentation et syntaxe plus concise.

Une utilisation élégante est la méthode chaînée :

```tsx
class User {
  constructor(public name: string, public age: number) {}
}

class UserBuilder {
  private name: string | null = null;
  private age: number | null = null;

  public setName(name: string): UserBuilder {
    this.name = name;
    return this;
  }

  public setAge(age: number): UserBuilder {
    this.age = age;
    return this;
  }

  public build(): User {
    if (!this.name) {
      throw new Error('Name is required');
    }
    if (this.age === null || this.age < 0) {
      throw new Error('Valid age is required');
    }
    return new User(this.name, this.age);
  }
}

const user1 = new UserBuilder().setName('John Wick').setAge(39).build();
```

## **Remplacement par un autre design pattern❓**

Le "Builder" a sa place, mais d'autres patterns comme le "Factory" ou le "Prototype" peuvent être envisagés selon le contexte. Le choix dépend des besoins spécifiques du projet.

## **Pertinence du Design Pattern "Builder" ❓**

Avant d'opter pour le "Builder", posez-vous ces questions cruciales :

1. **Complexité de Construction** 🤔 : Est-ce que la construction de l'objet implique plusieurs étapes complexes ?
2. **Variabilité des Configurations** 🔄 : Les objets doivent-ils être créés avec différentes configurations ?
3. **Maintenabilité** 🧑‍🔧 : La structure des objets est-elle sujette à des modifications fréquentes ?

## **Cas d'Utilisation Pratiques 🌐**

Le "Builder" brille dans divers scénarios :

**Construction d'un Utilisateur** 👤

```tsx
const user = new UserBuilder()
  .withName("John Doe")
  .withEmail("john.doe@email.com")
  .build();
```

**Transformation de Texte vers JSON** 🔄

```tsx
const jsonBuilder = new JSONBuilder();
const jsonData = jsonBuilder
  .fromText("{'key': 'value'}")
  .build();
```

**Traitement d’une chaîne de caractères (Promise style)**

```jsx
const myName = 'vernon, earl of shipbrook  ';
const newString = new StringBuilder(myName)
.then((str) => str.trim()) // Anonymous function
.then(capitalizeWords) // Use a function
.finally(); // Get the new value
```

API

```tsx
const request = new SimpleApiRequestBuilder()
  .setUrl('https://jsonplaceholder.typicode.com/posts/')
  .setMethod('POST')
  .setHeader('Content-Type', 'application/json; charset=UTF-8')
  .setBody({
    title: 'foo',
    body: 'bar',
    userId: 1,
  })
  .build();

fetchData(request)
  .then((data) => console.log(data))
  .catch((error) => console.error(error));
```

Plusieurs exemples peuvent être consultés sur GitHub :

https://github.com/giak/design-patterns-typescript/tree/main/src/builder

Pour aller plus loin, je vous conseille un "Builder" par ES6 proxy (génial):

https://github.com/Vincent-Pang/builder-pattern

Le design pattern "Builder" est un outil puissant à avoir dans votre boîte à outils de développement. Explorez ses nuances, évaluez sa pertinence et maximisez votre maîtrise de la construction d'objets complexes en TypeScript.

N'hésitez pas à partager vos expériences avec le "Builder" et à discuter de vos propres astuces et approches ! 💡🚧