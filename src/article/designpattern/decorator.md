---
title: 🚀 Maîtrisez le Design Pattern "Decorator" en TypeScript 5 🌉
index: true
date: 2023-12-15
icon: spider
category:
    - code
    - JavaScript
    - design pattern
    - typescript
---

<img src="./dp-decorator.jpeg" alt="Design Pattern Decorator" title="Design Pattern Decorator"/>

# 🚀**Maîtrisez le** Design Pattern “Decorator” et les décorateurs de TypeScript 5 🎨

## Introduction 📚

Le design pattern “decorator” et les décorateurs en TypeScript 5 vous permettent d'ajouter dynamiquement des comportements à des objets sans les altérer. Imaginez la création d'un objet de base, puis l'enrichir progressivement en lui ajoutant des fonctionnalités spécifiques sans altérer son code source initial. Cela favorise une approche flexible et modulaire du développement
Dans cet article, nous plongerons dans les profondeurs du design pattern "Decorator", en explorant ses avantages, ses limites, évolutions, et en fournissant des exemples.

## Avantages du Design Pattern “Decorator” 🌟

1. **Flexibilité** : Le design pattern “Decorator” offre une grande flexibilité en permettant d’ajouter, de modifier ou de supprimer des comportements à des objets individuels, sans affecter d’autres objets de la même classe.
2. **Évolutivité** : Il facilite l’évolutivité en permettant d’ajouter de nouvelles fonctionnalités sans modifier le code source de la classe.
3. **Respect du principe de responsabilité unique** : Chaque décorateur a une responsabilité unique, ce qui rend le code plus facile à comprendre et à maintenir.

## Limites du Design Pattern “Decorator” 🚧

1. **Complexité** : L’utilisation excessive de décorateurs peut rendre le code difficile à comprendre et à maintenir.
2. **Performance** : L’ajout de plusieurs décorateurs peut affecter les performances en raison de l’overhead dû à la délégation.

## Évolutions du Design Pattern “Decorator” 🔄

Le design pattern "Decorator" est unique en ce sens qu'il permet d'ajouter dynamiquement des comportements à des objets. D'autres patterns comme "Strategy" ou "Composite" peuvent offrir une certaine flexibilité, mais ils ne permettent pas d'ajouter des comportements à la volée.

Avec l’introduction des décorateurs dans TypeScript 5, une confusion est née. 
Il est important de noter que les décorateurs en TypeScript ne sont pas exactement la même chose que le design pattern "Decorator".
Les décorateurs TypeScript permettent de modifier ou d’ajouter des comportements à des classes, des méthodes, des attributs ou des paramètres de manière déclarative.
Ils peuvent être utilisés pour implémenter le design pattern "Decorator", mais ils ont aussi d'autres utilisations.

## Remplacement par un autre Design Pattern ❓

Le design pattern "Decorator" reste inégalable pour la flexibilité qu'il offre, mais dans certains cas, un design pattern comme le "Chain of Responsibility" pourrait être envisagé.
Cependant, selon le contexte, d’autres design patterns comme “Strategy” ou “Composite” pourraient être utilisés.

## Questions à se poser avant d’utiliser le Design Pattern “Decorator” ❓

1. Est-ce que je veux éviter de modifier les classes existantes ?
2. Ai-je besoin d’ajouter des comportements à des objets de manière dynamique ?
3. Est-ce que l'héritage est trop rigide pour mon cas d'utilisation ?
4. Est-ce que je respecte le principe de responsabilité unique ?
5. Est-ce que l’ajout de décorateurs n’ajoute pas une complexité inutile à mon code ?

## Exemples de cas d’utilisation 🛠️

Voici un exemple de cas d’utilisation du design pattern “Decorator” avec TypeScript :

```tsx
interface CoffeeInterface {
  cost(): number;
  description(): string;
}

class SimpleCoffee implements CoffeeInterface {
  cost(): number {
    return 1;
  }

  description(): string {
    return 'Simple coffee';
  }
}

abstract class CoffeeDecorator implements CoffeeInterface {
  constructor(protected coffee: CoffeeInterface) {}

  abstract cost(): number;
  abstract description(): string;
}

class MilkDecorator extends CoffeeDecorator {
  cost(): number {
    return this.coffee.cost() + 0.5;
  }

  description(): string {
    return this.coffee.description() + ', milk';
  }
}

class SugarDecorator extends CoffeeDecorator {
  cost(): number {
    return this.coffee.cost() + 0.2;
  }

  description(): string {
    return this.coffee.description() + ', sugar';
  }
}

class WhippedCreamDecorator extends CoffeeDecorator {
  cost(): number {
    return this.coffee.cost() + 0.7;
  }

  description(): string {
    return this.coffee.description() + ', whipped cream';
  }
}

class CaramelDecorator extends CoffeeDecorator {
  cost(): number {
    return this.coffee.cost() + 0.6;
  }

  description(): string {
    return this.coffee.description() + ', caramel';
  }
}

let coffee: CoffeeInterface = new SimpleCoffee();
coffee = new MilkDecorator(coffee);
coffee = new SugarDecorator(coffee);
coffee = new WhippedCreamDecorator(coffee);
coffee = new CaramelDecorator(coffee);

console.log(`Cost: $${coffee.cost()}`);
console.log(`Description: ${coffee.description()}`);
```

Dans cet exemple, nous avons un décorateur `CoffeeDecorator` qui ajoute deux propriétés `cost` et `description` à la classe `SimpleCoffee`. Nous pouvons voir que le décorateur permet d’ajouter des comportements à des objets de manière dynamique et flexible.
Cela permet d’avoir de définir un café avec toutes ses variantes.

En Typescript 5 : 

```tsx
function addPrice(price: number) {
  return function (target: any, key: string, descriptor: PropertyDescriptor) {
    const originalMethod = descriptor.value;
    descriptor.value = function () {
      return originalMethod.call(this) + price;
    };
    return descriptor;
  };
}

function modifyDescription(description: string) {
  return function (target: any, key: string, descriptor: PropertyDescriptor) {
    const originalMethod = descriptor.value;
    descriptor.value = function () {
      return originalMethod.call(this) + description;
    };
    return descriptor;
  };
}

class SimpleCoffee {
  cost(): number {
    return 1;
  }

  description(): string {
    return 'Simple coffee';
  }
}

class MilkCoffee extends SimpleCoffee {
  @addPrice(0.5)
  cost() {
    return super.cost();
  }

  @modifyDescription(', milk')
  description() {
    return super.description();
  }
}

class SugarCoffee extends SimpleCoffee {
  @addPrice(0.1)
  cost() {
    return super.cost();
  }

  @modifyDescription(', sugar')
  description() {
    return super.description();
  }
}

class MilkAndSugarCoffee extends SimpleCoffee {
  @addPrice(0.5)
  @addPrice(0.1)
  cost() {
    return super.cost();
  }

  @modifyDescription(', milk')
  @modifyDescription(', sugar')
  description() {
    return super.description();
  }
}

let coffee = new SimpleCoffee();
console.log(`Price: ${coffee.cost()} €`);
console.log(`Description: ${coffee.description()}`);

coffee = new MilkCoffee();
console.log(`Price: ${coffee.cost()} €`);
console.log(`Description: ${coffee.description()}`);

coffee = new SugarCoffee();
console.log(`Price: ${coffee.cost()} €`);
console.log(`Description: ${coffee.description()}`);

coffee = new MilkAndSugarCoffee();
console.log(`Price: ${coffee.cost()} €`);
console.log(`Description: ${coffee.description()}`);

export {};
```

Ce n’est qu’un mauvais exemple afin d’utiliser les décorateurs de TypeScript 5 à la place du design Pattern “decorator”.
Vous voyez assez vite les limitations et nous permettant d'affirmer que malgré la similitude de noms, le "decorator" de TypeScript 5 n'est pas équivalent au  design pattern "decorator".

Vous trouverez plusieurs exemples sur Github, notamment celui-là qui est adaptation d’un vrai cas d’utilisation :
https://github.com/giak/design-patterns-typescript/tree/main/src/decorator/advanced/cache

## **Exemples de Cas d'Utilisation 🛠️**

1. **Construction d'un User** : Vous pouvez utiliser des décorateurs pour ajouter des rôles ou des permissions à un utilisateur.
2. **Construction d'un produit** : Vous pouvez utiliser des décorateurs pour ajouter des options ou des extras à un produit.
3. **Transformation de texte en JSON** : Vous pouvez utiliser des décorateurs pour ajouter des étapes de transformation à un convertisseur de texte en JSON.
4. **Méthodes chaînées** : Vous pouvez utiliser des décorateurs pour ajouter des méthodes chaînées à un objet.
5. **Proxy** : Vous pouvez utiliser des décorateurs pour ajouter des comportements de proxy à un objet.

## Conclusion 🎯

Le design pattern “Decorator” est un outil puissant pour étendre les fonctionnalités d’une classe de manière flexible et évolutive. Cependant, comme tout outil, il doit être utilisé avec discernement pour éviter d’ajouter une complexité inutile à votre code. Avec l’introduction des décorateurs dans TypeScript 5, nous avons maintenant encore plus de possibilités pour utiliser ce design pattern de manière efficace et élégante. 🚀

N’hésitez pas à me contacter si vous avez des questions ou besoin d’aide pour intégrer le design pattern “Decorator” dans vos projets TypeScript. Je serai ravi de vous aider ! 😊