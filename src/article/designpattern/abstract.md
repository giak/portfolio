---
title: 🚀 Maîtrisez le Design Pattern "Abstract" en TypeScript 5 🌉
index: true
date: 2023-11-30
icon: spider
category:
    - code
    - JavaScript
    - design pattern
    - typescript
---

<img src="./dp-abstract.jpeg" alt="Design Pattern Abstract" title="Design Pattern Abstract"/>

# **🚀 Maîtrisez le Design Pattern "Abstract Factory" en TypeScript! 🛠️**

Salut, chers développeurs! 👋 Aujourd'hui, je souhaite partager quelques idées sur le modèle de conception "Abstract Factory", un classique du GoF. Je vous propose une utilisation en TypeScript, ses limites, ses évolutions potentielles et quand l'utiliser. 🏊‍♂️

## **🏗️ Qu'est-ce que le design pattern "Abstract Factory"?**

Le modèle de conception "Abstract Factory" offre un moyen d'encapsuler un groupe de fabriques individuelles ayant un thème commun sans spécifier leurs classes concrètes. En d'autres termes, c'est une super-fabrique qui crée d'autres fabriques. 🏭

L'utilité est de cacher l'accès à une classe métier ou modèle en passant par des sous classes.

## **🏠 Analogie avec la construction d'une maison**

Pour mieux comprendre le concept, imaginons la construction d'une maison. Une entreprise de construction peut proposer différents styles de maisons : moderne, traditionnelle, ou écologique. Chaque style nécessite des éléments spécifiques (fenêtres, portes, toiture) qui doivent être cohérents entre eux.

Dans cette analogie :

-   La "Abstract Factory" serait l'entreprise de construction
-   Les "Concrete Factories" seraient les différentes lignes de production (moderne, traditionnelle, écologique)
-   Les "Abstract Products" seraient les composants génériques (fenêtre, porte, toit)
-   Les "Concrete Products" seraient les composants spécifiques (fenêtre moderne en aluminium, porte traditionnelle en bois, toit écologique végétalisé)

```typescript
interface HouseFactoryInterface {
    createWindow(): WindowInterface;
    createDoor(): DoorInterface;
    createRoof(): RoofInterface;
}

class ModernHouseFactory implements HouseFactoryInterface {
    createWindow() {
        return new AluminiumWindow();
    }
    createDoor() {
        return new SlidingGlassDoor();
    }
    createRoof() {
        return new FlatRoof();
    }
}

class TraditionalHouseFactory implements HouseFactoryInterface {
    createWindow() {
        return new WoodenWindow();
    }
    createDoor() {
        return new WoodenDoor();
    }
    createRoof() {
        return new TiledRoof();
    }
}
```

Cette approche garantit que tous les éléments d'une maison sont cohérents avec le style choisi, tout comme dans notre pattern qui assure la cohérence des objets créés. 🏗️

Voici un exemple pratique en TypeScript qui masque la classe métier principale `Airplane` :

```typescript
import { AirplanePassengerFactoryInterface } from './AirplanePassengerFactory';
import { AirplaneCargoFactoryInterface } from './AirplaneCargoFactory';

const airplanePassengerFactory = new AirplanePassengerFactory();
const airplaneCargoFactory = new AirplaneCargoFactory();

const A380 = airplanePassengerFactory.create('800', 'Airbus', 'A380', 853);
const A320 = airplaneCargoFactory.create('NEO', 'Airplane', 'A320', 180);

A380.buyTicket();
A320.loadCargo(160);
```

Dans ce code, nous avons deux fabriques: **`AirplanePassengerFactoryInterface`** et **`AirplaneCargoFactoryInterface`**. Chaque fabrique crée un type spécifique d'avion. 🛫

[📦 Vous trouverez plusieurs exemples dans ce dépôt](https://github.com/giak/design-patterns-typescript/tree/main/src/abstract-factory/plane)

## **🚀 Avantages du design pattern "Abstract Factory"**

Le design pattern "Abstract Factory" offre plusieurs avantages majeurs qui en font un choix privilégié pour la conception de systèmes complexes et évolutifs :

### 1. **Abstraction et Encapsulation Renforcées** 🎯
- **Isolation du code client** : Le code client n'a jamais besoin de connaître les classes concrètes, réduisant ainsi le couplage
- **Gestion centralisée** : Les modifications des implémentations peuvent être effectuées à un seul endroit
- **Sécurité accrue** : L'encapsulation empêche l'instanciation directe des objets, garantissant leur cohérence

```typescript
// Le client manipule uniquement des abstractions 
interface UIFactoryInterface {
    createButton(): ButtonInterface;
    createInput(): InputInterface;
}

// Il n'a pas besoin de connaître ces implémentations
class MaterialUIFactory implements UIFactoryInterface {
    createButton() { return new MaterialButton(); }
    createInput() { return new MaterialInput(); }
}
```

### 2. **Cohérence des Familles de Produits** 🔄
- **Garantie de compatibilité** : Tous les objets créés par une même fabrique sont compatibles entre eux
- **Prévention des erreurs** : Impossible de mélanger des éléments de différentes familles
- **Maintenance simplifiée** : Les modifications d'une famille de produits sont localisées

```typescript
// Exemple de cohérence garantie
const darkThemeFactory = new DarkThemeFactory();
const button = darkThemeFactory.createButton();    // Style sombre garanti
const input = darkThemeFactory.createInput();      // Assorti au bouton
```

### 3. **Flexibilité et Extensibilité** 🌱
- **Ajout facile** : Nouvelles familles de produits sans modifier le code existant
- **Configuration dynamique** : Changement de famille de produits à l'exécution
- **Tests simplifiés** : Possibilité de mocker facilement les fabriques pour les tests

```typescript
// Ajout d'une nouvelle famille sans modifier l'existant
class EcoThemeFactory implements UIFactoryInterface {
    createButton() { return new EcoButton(); }
    createInput() { return new EcoInput(); }
}
```

### 4. **Respect des Principes SOLID** 📐
- **Single Responsibility** : Chaque fabrique a une seule responsabilité
- **Open/Closed** : Extension facile sans modification du code existant
- **Dependency Inversion** : Dépendance vers les abstractions, non les implémentations

```typescript
// Injection de dépendances via l'abstraction
class Application {
    constructor(private factory: UIFactoryInterface) {}
    
    createUI() {
        const button = this.factory.createButton();
        const input = this.factory.createInput();
        // ...
    }
}
```

### 5. **Facilité de Maintenance** 🔧
- **Organisation claire** : Structure hiérarchique bien définie
- **Modifications localisées** : Les changements sont contenus dans les classes concrètes
- **Documentation implicite** : Le pattern lui-même documente l'intention du code

Ces avantages font du pattern "Abstract Factory" un choix particulièrement pertinent pour :
- Les systèmes nécessitant une forte modularité
- Les applications avec différentes variantes d'interface utilisateur
- Les frameworks devant supporter multiple plateformes
- Les systèmes où la cohérence des objets créés est critique

## **🚧 Limites du design pattern "Abstract Factory"**

Bien que puissant, le modèle "Abstract Factory" n'est pas sans limites:

1. **Complexité**: Il peut rendre le code plus compliqué que nécessaire, surtout pour des applications simples. 🤯
2. **Problèmes de modification**: Si les produits qu'une Factory crée doivent changer, cela peut nécessiter beaucoup de refactoring. 🔄

## **🔄 Évolutions et Alternatives**

Le modèle "Abstract Factory" a résisté à l'épreuve du temps, mais cela ne signifie pas qu'il ne peut pas être remplacé. Le design pattern "Factory" est une alternative plus simple, idéale pour les situations où la Factory ne doit créer qu'un seul type de produit. 🔄

### **Injection de dépendances 🛠️**

L'injection de dépendances consiste à fournir à un objet les dépendances nécessaires au lieu de les créer lui-même. Cette approche réduit la complexité en évitant la dépendance aux classes concrètes, tout en facilitant les tests unitaires. Elle peut être une alternative ou un complément au design pattern "Abstract Factory".

### **Le pattern "Builder" 🏗️**

Le pattern "Builder" est utilisé pour construire des objets complexes étape par étape. Il permet de séparer la création d'un objet de sa représentation, offrant ainsi une flexibilité accrue. Le "Builder" peut compléter le design pattern "Abstract Factory" dans des situations où la création d'objets est complexe et nécessite plusieurs étapes.

### **Le pattern "Prototype" 🔄**

Le pattern "Prototype" permet de créer de nouveaux objets en clonant des objets existants plutôt qu'en les instanciant de zéro. Utile lorsque la création d'objets est coûteuse en termes de performances ou de ressources, il peut être une alternative ou un complément au design pattern "Abstract Factory" dans certains cas.

## **❓ Quand utiliser le design pattern "Abstract Factory"?**

Avant d'implémenter "Abstract Factory", posez-vous ces questions:

1. Est-ce que le projet nécessite la création de familles d'objets apparentés ? 🤔
2. Est-ce que le code client a besoin de travailler avec les produits d'une famille sans connaître les détails de leur implémentation concrète ? 🤖
3. Est-ce que la flexibilité et la maintenance du code sont des priorités pour le projet ? 🧰
4. Est-ce que la complexité ajoutée par l'utilisation du design pattern "Abstract Factory" est justifiée par les avantages qu'il apporte ? 🤯

Si vous avez répondu "oui" à ces questions, "Abstract Factory" pourrait convenir à votre projet! 🎯

## Exemple d’utilisation

1. **Interface utilisateur (UI) lors de la création de composants web :**
   Utilisez le design pattern "Abstract Factory" 😎 pour créer des éléments d'interface utilisateur comme de boutons (boutons standard, boutons de style spécial, boutons d'alerte).
2. **Gestion de la persistance des données :**
   Simplifiez la prise en charge de multiples sources de données 🗃️ en utilisant une fabrique abstraite pour créer des objets de persistance adaptés à chaque source, assurant ainsi une flexibilité dans le stockage des données.
3. **Création de thèmes graphiques :**
   Personnalisez l'apparence de votre application avec différents thèmes visuels 🌈 en employant une fabrique abstraite pour créer des ensembles d'objets graphiques cohérents, indépendamment du thème choisi.
4. **Création de jeux vidéo :**
   Construisez des mondes de jeu variés 🎮 en utilisant le pattern "Abstract Factory" pour créer des personnages, ennemis et objets spécifiques à chaque niveau, facilitant ainsi le développement de jeux dynamiques et évolutifs.
5. **Traitement de documents dans différents formats :**
   Simplifiez le traitement de documents variés 📄 en utilisant une fabrique abstraite pour créer des analyseurs et des générateurs adaptés à chaque format, permettant une gestion facile de la diversité des documents.

J'espère que cet article vous a fourni des informations utiles et vous a aidé à approfondir vos connaissances. N'hésitez pas à le partager pour promouvoir vos compétences en TypeScript et en conception de design pattern. Ensemble, nous pouvons faire évoluer notre industrie et créer des solutions de qualité supérieure ! 🌟
