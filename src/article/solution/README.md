---
title: A propos
description: 'Une brève intronisation'
index: true
date: 2023-12-01
sticky: false
star: 3
icon: circle-info
category:
    - information
---

## Comment est réalisé ce site ?

Ayant déjà conçu une documentation technique dans le but de servir de support de formation avec [VuePress](https://v2.vuepress.vuejs.org), l'étendre pour réaliser un &laquo;portfolio&raquo; était une progression naturelle.  
Après m'être essayé à installer quelques plugins, j'ai décidé d'écourter cette phase en utilisant un `boilerplate` trouvé via [awesome-vuepress](https://github.com/vuepress/awesome-vuepress/blob/main/v2.md) : [VuePress Theme Hope](https://theme-hope.vuejs.press).  
Son adaptation est facile et déployable sur Gitlab.

## Quel environnement de travail ?

### Quels matériels ?

- Un bureau fabriqué par mes soins avec : 3 écrans 27" montés sur bras ; clavier mécanique ; trackball ; webcam Full HD 1080p ; enceinte Edifier ;
casque Sennheiser avec micro.  
- Un mini PC [MinisForum](https://store.minisforum.com/products/minisforum-um780-xtx) sous Debian/Mint.  
- Un PC sous Debian/openmediavault transformé en NAS délocalisé pour sécuriser les sauvegardes.  
- Un laptop Windows pour les déplacements permettant de faire des formations et des présentations.  
- 2 onduleurs APC 900 VA : régulation de la tension ; protection réseau Ethernet / surtensions / coupures.  
- La fibre avec un switch et CPL.

<img src="./work.jpg" alt="" title=""/>

### Quels outils logiciels ?

[Linux Mint 21.3 Cinnamon](https://www.linuxmint.com) : Un Os sous Debian qui sait se faire oublier afin que l'on se concentre sur nos outils de travail.  
[VSCode](https://code.visualstudio.com/) : Idéal pour le javascript et le typescript.  
[Cursor IDE](https://www.cursor.com/) : Fork de VSCode avec AI intégré.
[GitKraken](https://www.gitkraken.com/) : UI pour GIT (dommage pour [Fork](https://git-fork.com) qui ne fonctionne que sous Windows : À tester sous Wine).  
[Bruno](https://www.usebruno.com/): Pour tester tous types d'interrogations réseaux et API.  
[Docker](https://www.docker.com/) / [Portainer](https://www.portainer.io/) : gestion des `containers`.  
[Keepassxc](https://keepassxc.org/) : gestionnaire de mot de passes.  
[Figma.com](https://www.figma.com/) : Design UI.  
[Zsh/Oh My Zsh](https://ohmyz.sh) : terminal plus abouti.

Le tout est sauvegardé en 2 temps :

-   Une sauvegarde système hebdomadaire.
-   Une quotidienne pour les `data`.

[Github.com](https://github.com/giak) : Pour tout type de projets.  
[GitLab.com](https://gitlab.com/giak) : Pour des projets personnels.  
[Notion.so](https://www.notion.so) : Tout noter, tout documenter ; Leur API permet d'effectuer une sauvegarde.  
[Proton.me](https://proton.me/) : Messagerie en ligne ; VPN ; se degoogeliser ...  
[Grimp.io](https://www.grimp.io/) : pour gérer sa recherche de client, d'emploi (pas d'API, mais présence de GraphQL, héhé).  
[Wrike.com](https://www.wrike.com) : en test ; gestion clientèle / kanban / WBS, etc.

## Un portfolio, mais encore ?

Dans ma [présentation](../../presentation/), l'accent n'est pas mis sur les technologies, mais sur le partenariat et la confiance.  
Cependant, à la différence d'un artisan plombier dont les compétences ne sont généralement pas remises en question, le domaine de l'informatique, plus abstrait, nécessite des preuves tangibles.  
C'est ici qu'un portfolio et un dépôt Git jouent un rôle essentiel pour rassurer lors d'un premier contact.

## De nombreux centres d'intérêts.

### Un peu d'informatique

TO7, Amstrad, Amiga ... Les grosses calculatrices que sont les ordinateurs m'ont accompagné toute ma vie.  
Cette curiosité m'a amené à découvrir le monde de l'informatique et à en faire mon métier.

### Résilience

Disposer de son eau potable, de son énergie, de sa nourriture et surtout, de l'entraide et de la solidarité, permet d'aborder l'avenir avec sérénité.  
Ce terme à la mode, est pourtant la norme de traditions millénaires.  
Est-il possible de l'adapter au développement informatique ... ?

### Jardinage et élevage

Une bonne partie de mon temps est consacré au jardin.

### Conserverie & Co

La transformation et la conservation des produits sont des étapes incontournables.

### Bricolage

Pompe à eau solaire, séchoir solaire, électricité, maçonnerie, ébénisterie, une ferme nécessite d’être débrouillard, ingénieux et pragmatique.

<img src="./home.jpg" alt="" title=""/>

### Le djing et la musique

[Traktor Kontrol S8](https://fr.audiofanzine.com/surface-de-controle-dj/native-instruments/traktor-kontrol-s8/) : Une surface de Contrôle MIDI DJ pour piloter le logiciel Traktor.  
[Akai Professional Force](https://fr.audiofanzine.com/sequenceur-sampleur/akai/apc-live/) : Sequenceur sampleur autonome.  
[Korg Kaoss Pad 3 KP3+](https://fr.audiofanzine.com/effet-dj/korg/kaoss-pad-3-kp3plus/) : Multi-effets DJ.  
[Mackie Big Knob Studio+](https://fr.audiofanzine.com/controleur-monitoring/mackie/big-knob-studioplus/) : Contrôleurs de monitoring.  
[Behringer Xenyx 1002FX](https://fr.audiofanzine.com/console-analogique/behringer/xenyx-1002fx/) : petite console de mixage.  
[Behringer TD-3-MO](https://fr.audiofanzine.com/synthe-analogique/behringer/td-3-mo/) : un clone boosté de la fameuse TB303.  
[Behringer Nekkst K8](https://fr.audiofanzine.com/enceinte-active/behringer/nekkstk8/) : retour de studio.  
[Sennheiser HD 215-II](https://fr.audiofanzine.com/casque-dj/sennheiser/hd-215-ii/) : casque de monitoring

<img src="./music.jpg" alt="" title=""/>
