---
home: true
title: Accueil
heroText: 'Développeur Web Sénior'
tagline: 'Architecte de solutions innovantes pour la croissance de votre entreprise'
sitemap:
    priority: 1

footer: Portfolio et CV d'un Développeur Web Full Stack JavaScript Senior | Expert Adonis.js / Nest.js / Vue.js / Angular

head:
  - - link
    - rel: canonical
      href: https://votre-domaine.com
---

<PortfolioPresentation />
