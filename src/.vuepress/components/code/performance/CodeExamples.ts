export interface CodeExample {
  name: string;
  description: string;
  codeA: string;
  codeB: string;
}

export const codeExamples: CodeExample[] = [
  {
    name: "Array Manipulation",
    description: "Comparaison entre .map() et une boucle for classique pour la transformation d'un tableau",
    codeA: "// Utilisation de .map()\n" +
           "const array = Array.from({ length: 1000 }, (_, i) => i);\n" +
           "const result = array.map(x => x * 2);",
    codeB: "// Utilisation d'une boucle for\n" +
           "const array = Array.from({ length: 1000 }, (_, i) => i);\n" +
           "const result = [];\n" +
           "for (let i = 0; i < array.length; i++) {\n" +
           "  result.push(array[i] * 2);\n" +
           "}"
  },
  {
    name: "String Concatenation",
    description: "Comparaison entre l'utilisation de template literals et l'opérateur + pour la concaténation de chaînes",
    codeA: "// Template literals\n" +
           "const items = Array.from({ length: 1000 }, (_, i) => `item${i}`);\n" +
           "let result = '';\n" +
           "for (const item of items) {\n" +
           "  result += `${item}, `;\n" +
           "}",
    codeB: "// Join method\n" +
           "const items = Array.from({ length: 1000 }, (_, i) => 'item' + i);\n" +
           "const result = items.join(', ');"
  },
  {
    name: "Object Creation",
    description: "Comparaison entre Object.assign() et la syntaxe spread pour la fusion d'objets",
    codeA: "// Object.assign()\n" +
           "const base = { a: 1, b: 2 };\n" +
           "const result = Array.from({ length: 1000 }, () => \n" +
           "  Object.assign({}, base, { c: 3, d: 4 })\n" +
           ");",
    codeB: "// Spread operator\n" +
           "const base = { a: 1, b: 2 };\n" +
           "const result = Array.from({ length: 1000 }, () => \n" +
           "  ({ ...base, c: 3, d: 4 })\n" +
           ");"
  },
  {
    name: "Array Filtering",
    description: "Comparaison entre .filter() et une boucle for...of pour le filtrage d'éléments",
    codeA: "// Méthode filter()\n" +
           "const numbers = Array.from({ length: 1000 }, (_, i) => i);\n" +
           "const result = numbers.filter(n => n % 2 === 0);",
    codeB: "// Boucle for...of\n" +
           "const numbers = Array.from({ length: 1000 }, (_, i) => i);\n" +
           "const result = [];\n" +
           "for (const n of numbers) {\n" +
           "  if (n % 2 === 0) result.push(n);\n" +
           "}"
  },
   {
    name: "Set vs Array",
    description: "Comparaison entre Set et Array pour la recherche d'éléments uniques",
    codeA: "// Utilisation d'un Set\n" +
           "const numbers = Array.from({ length: 1000 }, () => \n" +
           "  Math.floor(Math.random() * 100)\n" +
           ");\n" +
           "const uniqueSet = new Set(numbers);\n" +
           "const result = Array.from(uniqueSet);",
    codeB: "// Utilisation d'un Array\n" +
           "const numbers = Array.from({ length: 1000 }, () => \n" +
           "  Math.floor(Math.random() * 100)\n" +
           ");\n" +
           "const result = numbers.filter((v, i, a) => \n" +
           "  a.indexOf(v) === i\n" +
           ");"
  },
  {
    name: "Async Operations",
    description: "Comparaison entre Promise.all() et les appels séquentiels",
    codeA: "// Promise.all()\n" +
           "const items = Array.from({ length: 10 }, (_, i) => i);\n" +
           "const promises = items.map(i => \n" +
           "  new Promise(resolve => \n" +
           "    setTimeout(() => resolve(i), 100)\n" +
           "  )\n" +
           ");\n" +
           "Promise.all(promises).then(results => results);",
    codeB: "// Sequential promises\n" +
           "const items = Array.from({ length: 10 }, (_, i) => i);\n" +
           "let results = [];\n" +
           "items.reduce((promise, i) => \n" +
           "  promise.then(() => \n" +
           "    new Promise(resolve => \n" +
           "      setTimeout(() => {\n" +
           "        results.push(i);\n" +
           "        resolve();\n" +
           "      }, 100)\n" +
           "    )\n" +
           "  ), Promise.resolve());"
  },
  {
    name: "Object Property Access",
    description: "Comparaison entre la déstructuration et l'accès direct aux propriétés",
    codeA: "// Destructuring\n" +
           "const items = Array.from({ length: 1000 }, (_, i) => ({\n" +
           "  id: i, value: i * 2, name: 'item' + i\n" +
           "}));\n" +
           "const result = items.map(({ id, value, name }) => \n" +
           "  id + value + name.length\n" +
           ");",
    codeB: "// Direct property access\n" +
           "const items = Array.from({ length: 1000 }, (_, i) => ({\n" +
           "  id: i, value: i * 2, name: 'item' + i\n" +
           "}));\n" +
           "const result = items.map(item => \n" +
           "  item.id + item.value + item.name.length\n" +
           ");"
  },

  {
    name: "Modern Array Methods",
    description: "Comparaison entre reduce() et les méthodes impératives",
    codeA: "// Using reduce\n" +
           "const numbers = Array.from({ length: 1000 }, (_, i) => i);\n" +
           "const result = numbers.reduce((acc, curr) => {\n" +
           "  if (curr % 2 === 0) acc.push(curr * 2);\n" +
           "  return acc;\n" +
           "}, []);",
    codeB: "// Using imperative approach\n" +
           "const numbers = Array.from({ length: 1000 }, (_, i) => i);\n" +
           "const result = [];\n" +
           "for (let i = 0; i < numbers.length; i++) {\n" +
           "  if (numbers[i] % 2 === 0) {\n" +
           "    result.push(numbers[i] * 2);\n" +
           "  }\n" +
           "}"
  }
]; 