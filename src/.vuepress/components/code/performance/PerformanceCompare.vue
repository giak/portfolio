<script setup lang="ts">
import { computed, onMounted, onUnmounted, ref } from 'vue'
import { codeExamples } from './CodeExamples'

interface TestResult {
  executionTime: number
  operationsPerSecond: number
  memoryUsage: number
  memoryPerIteration: number
  error?: string
}

const MAX_CODE_LENGTH = 2000; // Limite de 2000 caractères

const leftCode = ref('')
const rightCode = ref('')
const isRunning = ref(false)
const iterations = ref(1000)
const selectedExample = ref('')
const results = ref<{ left: TestResult | null; right: TestResult | null }>({
  left: null,
  right: null
})

let leftWorker: Worker | null = null
let rightWorker: Worker | null = null

const leftCharCount = computed(() => leftCode.value.length)
const rightCharCount = computed(() => rightCode.value.length)

const isLeftValid = computed(() => leftCharCount.value <= MAX_CODE_LENGTH)
const isRightValid = computed(() => rightCharCount.value <= MAX_CODE_LENGTH)

onMounted(() => {
  leftWorker = new Worker(new URL('./performance.worker.ts', import.meta.url), { type: 'module' })
  rightWorker = new Worker(new URL('./performance.worker.ts', import.meta.url), { type: 'module' })

  leftWorker.onmessage = (e: MessageEvent<TestResult>) => {
    results.value.left = e.data
    checkCompletion()
  }

  leftWorker.onerror = (e) => {
    results.value.left = {
      executionTime: 0,
      operationsPerSecond: 0,
      memoryUsage: 0,
      memoryPerIteration: 0,
      error: e.message
    }
    checkCompletion()
  }

  rightWorker.onmessage = (e: MessageEvent<TestResult>) => {
    results.value.right = e.data
    checkCompletion()
  }

  rightWorker.onerror = (e) => {
    results.value.right = {
      executionTime: 0,
      operationsPerSecond: 0,
      memoryUsage: 0,
      memoryPerIteration: 0,
      error: e.message
    }
    checkCompletion()
  }
})

onUnmounted(() => {
  leftWorker?.terminate()
  rightWorker?.terminate()
})

const checkCompletion = () => {
  if (results.value.left && results.value.right) {
    isRunning.value = false
  }
}

const runComparison = () => {
  if (!leftWorker || !rightWorker || !leftCode.value || !rightCode.value) return
  if (!isLeftValid.value || !isRightValid.value) return

  isRunning.value = true
  results.value = { left: null, right: null }

  leftWorker.postMessage({ code: leftCode.value, iterations: iterations.value })
  rightWorker.postMessage({ code: rightCode.value, iterations: iterations.value })
}

const loadExample = (exampleName: string) => {
  const example = codeExamples.find(ex => ex.name === exampleName)
  if (example) {
    leftCode.value = example.codeA
    rightCode.value = example.codeB
    selectedExample.value = exampleName
  }
}

const currentExample = computed(() => {
  return codeExamples.find(ex => ex.name === selectedExample.value)
})

const formatNumber = (num: number) => {
  return new Intl.NumberFormat().format(Math.round(num))
}

const formatBytes = (bytes: number) => {
  if (bytes === 0) return '0 B'
  const k = 1024
  const sizes = ['B', 'KB', 'MB', 'GB']
  const i = Math.floor(Math.log(Math.abs(bytes)) / Math.log(k))
  return `${(bytes / Math.pow(k, i)).toFixed(2)} ${sizes[i]}`
}

const getPerformanceComparison = computed(() => {
  if (!results.value.left || !results.value.right || 
      results.value.left.error || results.value.right.error) {
    return null
  }

  const leftOps = results.value.left.operationsPerSecond
  const rightOps = results.value.right.operationsPerSecond
  const diff = ((rightOps - leftOps) / leftOps) * 100

  return {
    diff,
    faster: diff > 0 ? 'B' : 'A',
    percentage: Math.abs(diff).toFixed(2)
  }
})
</script>

<template> 
  <div class="tw-min-h-screen tw-bg-gradient-to-br tw-from-gray-900 tw-to-gray-800 tw-text-gray-100 tw-antialiased">
    <div class="tw-max-w-7xl tw-mx-auto tw-px-4 sm:tw-px-6 lg:tw-px-8 tw-py-12">
      <h1 class="tw-text-4xl tw-font-bold tw-text-center tw-mb-12 tw-bg-gradient-to-r tw-from-blue-400 tw-to-green-400 tw-bg-clip-text tw-text-transparent tw-drop-shadow-lg">
        Comparateur de Performance
      </h1>
      
      <!-- Examples Selector -->
      <div class="tw-flex tw-flex-col sm:tw-flex-row tw-items-center tw-justify-center tw-gap-4 tw-mb-12">
        <label class="tw-text-gray-300 tw-font-medium">Charger un exemple :</label>
        <select
          v-model="selectedExample"
          @change="loadExample(selectedExample)"
          class="tw-bg-gray-800/50 tw-border tw-border-gray-700 tw-rounded-lg tw-px-4 tw-py-2 tw-text-gray-100 tw-transition-all hover:tw-border-blue-500 focus:tw-border-blue-500 focus:tw-ring-2 focus:tw-ring-blue-500/20 focus:tw-outline-none tw-backdrop-blur-sm tw-w-full sm:tw-w-auto"
        >
          <option value="">Sélectionnez un exemple...</option>
          <option v-for="example in codeExamples" :key="example.name" :value="example.name">
            {{ example.name }}
          </option>
        </select>
      </div>

      <!-- Example Description -->
      <div v-if="currentExample" class="tw-text-center tw-text-lg tw-text-gray-300 tw-mb-12 tw-max-w-3xl tw-mx-auto tw-leading-relaxed">
        {{ currentExample.description }}
      </div>
      
      <!-- Code Editors -->
      <div class="tw-grid tw-grid-cols-1 md:tw-grid-cols-2 tw-gap-8 tw-mb-12">
        <!-- Code A -->
        <div class="tw-flex tw-flex-col tw-gap-4">
          <h2 class="tw-text-xl tw-font-semibold tw-text-blue-400 tw-flex tw-items-center tw-gap-2">
            <span class="tw-w-2 tw-h-2 tw-rounded-full tw-bg-blue-400"></span>
            Code A
          </h2>
          <div class="tw-relative">
            <textarea
              v-model="leftCode"
              :maxlength="MAX_CODE_LENGTH"
              class="tw-w-full tw-h-96 tw-p-4 tw-font-mono tw-text-base tw-bg-gray-800/50 tw-border tw-border-gray-700 tw-rounded-lg tw-transition-all hover:tw-border-blue-500 focus:tw-border-blue-500 focus:tw-ring-2 focus:tw-ring-blue-500/20 focus:tw-outline-none tw-text-gray-100 tw-resize-none tw-backdrop-blur-sm"
              :class="{ 'tw-border-red-500': !isLeftValid }"
              placeholder="Entrez votre premier extrait de code ici..."
              spellcheck="false"
            />
            <div class="tw-absolute tw-bottom-2 tw-right-2 tw-text-sm" 
                 :class="{ 'tw-text-red-400': !isLeftValid, 'tw-text-gray-400': isLeftValid }">
              {{ leftCharCount }}/{{ MAX_CODE_LENGTH }}
            </div>
          </div>
        </div>

        <!-- Code B -->
        <div class="tw-flex tw-flex-col tw-gap-4">
          <h2 class="tw-text-xl tw-font-semibold tw-text-green-400 tw-flex tw-items-center tw-gap-2">
            <span class="tw-w-2 tw-h-2 tw-rounded-full tw-bg-green-400"></span>
            Code B
          </h2>
          <div class="tw-relative">
            <textarea
              v-model="rightCode"
              :maxlength="MAX_CODE_LENGTH"
              class="tw-w-full tw-h-96 tw-p-4 tw-font-mono tw-text-base tw-bg-gray-800/50 tw-border tw-border-gray-700 tw-rounded-lg tw-transition-all hover:tw-border-blue-500 focus:tw-border-blue-500 focus:tw-ring-2 focus:tw-ring-blue-500/20 focus:tw-outline-none tw-text-gray-100 tw-resize-none tw-backdrop-blur-sm"
              :class="{ 'tw-border-red-500': !isRightValid }"
              placeholder="Entrez votre second extrait de code ici..."
              spellcheck="false"
            />
            <div class="tw-absolute tw-bottom-2 tw-right-2 tw-text-sm"
                 :class="{ 'tw-text-red-400': !isRightValid, 'tw-text-gray-400': isRightValid }">
              {{ rightCharCount }}/{{ MAX_CODE_LENGTH }}
            </div>
          </div>
        </div>
      </div>

      <!-- Controls -->
      <div class="tw-flex tw-flex-col sm:tw-flex-row tw-items-center tw-justify-center tw-gap-6 tw-mb-12">
        <label class="tw-flex tw-items-center tw-gap-3 tw-px-4 tw-py-2 tw-bg-gray-800/30 tw-border tw-border-gray-700 tw-rounded-lg tw-backdrop-blur-sm">
          <span class="tw-font-medium">Itérations :</span>
          <input
            v-model="iterations"
            type="number"
            min="1"
            max="1000000"
            class="tw-w-28 tw-px-2 tw-py-1 tw-bg-gray-800/50 tw-border tw-border-gray-700 tw-rounded tw-transition-all focus:tw-border-blue-500 focus:tw-ring-2 focus:tw-ring-blue-500/20 focus:tw-outline-none tw-text-gray-100"
          />
        </label>
        <button
          @click="runComparison"
          :disabled="isRunning || !leftCode || !rightCode"
          class="tw-px-6 tw-py-2 tw-font-medium tw-bg-gradient-to-r tw-from-blue-500 tw-to-green-500 tw-text-white tw-rounded-lg tw-transition-all tw-duration-300 hover:tw-opacity-90 disabled:tw-opacity-50 disabled:tw-cursor-not-allowed tw-shadow-lg hover:tw-shadow-xl tw-w-full sm:tw-w-auto"
        >
          {{ isRunning ? 'En cours...' : 'Comparer les Performances' }}
        </button>
      </div>

      <!-- Results -->
      <div v-if="results.left || results.right" class="tw-grid tw-grid-cols-1 md:tw-grid-cols-2 tw-gap-8 tw-mb-12">
        <!-- Results A -->
        <div v-if="results.left" class="tw-p-6 tw-bg-gray-800/30 tw-border tw-border-blue-500/30 tw-rounded-lg tw-backdrop-blur-sm tw-shadow-lg hover:tw-shadow-xl tw-transition-all">
          <h3 class="tw-text-xl tw-font-semibold tw-mb-4 tw-flex tw-items-center tw-gap-2">
            <span class="tw-w-2 tw-h-2 tw-rounded-full tw-bg-blue-400"></span>
            Résultats A
          </h3>
          <div v-if="results.left.error" class="tw-text-red-400 tw-p-3 tw-bg-red-500/10 tw-border tw-border-red-500/30 tw-rounded-lg">
            Erreur : {{ results.left.error }}
          </div>
          <div v-else class="tw-space-y-4">
            <div class="tw-grid tw-grid-cols-2 tw-gap-4">
              <div class="tw-p-3 tw-bg-gray-800/30 tw-rounded-lg">
                <p class="tw-text-gray-400 tw-text-sm">Temps d'exécution</p>
                <p class="tw-text-blue-400 tw-text-lg tw-font-semibold">{{ results.left.executionTime.toFixed(2) }}ms</p>
              </div>
              <div class="tw-p-3 tw-bg-gray-800/30 tw-rounded-lg">
                <p class="tw-text-gray-400 tw-text-sm">Opérations/sec</p>
                <p class="tw-text-blue-400 tw-text-lg tw-font-semibold">{{ formatNumber(results.left.operationsPerSecond) }}</p>
              </div>
            </div>
            <div class="tw-mt-4 tw-pt-4 tw-border-t tw-border-gray-700">
              <h4 class="tw-font-medium tw-mb-3 tw-text-gray-300">Utilisation Mémoire</h4>
              <div class="tw-grid tw-grid-cols-2 tw-gap-4">
                <div class="tw-p-3 tw-bg-gray-800/30 tw-rounded-lg">
                  <p class="tw-text-gray-400 tw-text-sm">Mémoire Totale</p>
                  <p class="tw-text-blue-400 tw-text-lg tw-font-semibold">{{ formatBytes(results.left.memoryUsage) }}</p>
                </div>
                <div class="tw-p-3 tw-bg-gray-800/30 tw-rounded-lg">
                  <p class="tw-text-gray-400 tw-text-sm">Par Itération</p>
                  <p class="tw-text-blue-400 tw-text-lg tw-font-semibold">{{ formatBytes(results.left.memoryPerIteration) }}</p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Results B -->
        <div v-if="results.right" class="tw-p-6 tw-bg-gray-800/30 tw-border tw-border-green-500/30 tw-rounded-lg tw-backdrop-blur-sm tw-shadow-lg hover:tw-shadow-xl tw-transition-all">
          <h3 class="tw-text-xl tw-font-semibold tw-mb-4 tw-flex tw-items-center tw-gap-2">
            <span class="tw-w-2 tw-h-2 tw-rounded-full tw-bg-green-400"></span>
            Résultats B
          </h3>
          <div v-if="results.right.error" class="tw-text-red-400 tw-p-3 tw-bg-red-500/10 tw-border tw-border-red-500/30 tw-rounded-lg">
            Erreur : {{ results.right.error }}
          </div>
          <div v-else class="tw-space-y-4">
            <div class="tw-grid tw-grid-cols-2 tw-gap-4">
              <div class="tw-p-3 tw-bg-gray-800/30 tw-rounded-lg">
                <p class="tw-text-gray-400 tw-text-sm">Temps d'exécution</p>
                <p class="tw-text-green-400 tw-text-lg tw-font-semibold">{{ results.right.executionTime.toFixed(2) }}ms</p>
              </div>
              <div class="tw-p-3 tw-bg-gray-800/30 tw-rounded-lg">
                <p class="tw-text-gray-400 tw-text-sm">Opérations/sec</p>
                <p class="tw-text-green-400 tw-text-lg tw-font-semibold">{{ formatNumber(results.right.operationsPerSecond) }}</p>
              </div>
            </div>
            <div class="tw-mt-4 tw-pt-4 tw-border-t tw-border-gray-700">
              <h4 class="tw-font-medium tw-mb-3 tw-text-gray-300">Utilisation Mémoire</h4>
              <div class="tw-grid tw-grid-cols-2 tw-gap-4">
                <div class="tw-p-3 tw-bg-gray-800/30 tw-rounded-lg">
                  <p class="tw-text-gray-400 tw-text-sm">Mémoire Totale</p>
                  <p class="tw-text-green-400 tw-text-lg tw-font-semibold">{{ formatBytes(results.right.memoryUsage) }}</p>
                </div>
                <div class="tw-p-3 tw-bg-gray-800/30 tw-rounded-lg">
                  <p class="tw-text-gray-400 tw-text-sm">Par Itération</p>
                  <p class="tw-text-green-400 tw-text-lg tw-font-semibold">{{ formatBytes(results.right.memoryPerIteration) }}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Performance Summary -->
      <div 
        v-if="getPerformanceComparison" 
        class="tw-max-w-2xl tw-mx-auto tw-p-8 tw-bg-gray-800/30 tw-border tw-border-gray-700 tw-rounded-lg tw-text-center tw-backdrop-blur-sm tw-shadow-lg"
      >
        <h3 class="tw-text-2xl tw-font-bold tw-mb-6 tw-bg-gradient-to-r tw-from-blue-400 tw-to-green-400 tw-bg-clip-text tw-text-transparent tw-drop-shadow-lg">
          Résumé des Résultats
        </h3>
        <div class="tw-space-y-4">
          <p class="tw-text-2xl tw-font-medium">
            Le Code <span class="tw-font-bold tw-text-3xl">{{ getPerformanceComparison.faster }}</span> est
            <span class="tw-font-bold tw-text-3xl tw-bg-gradient-to-r tw-from-blue-400 tw-to-green-400 tw-bg-clip-text tw-text-transparent">
              {{ getPerformanceComparison.percentage }}%
            </span>
            plus rapide
          </p>
          <p class="tw-text-xl" v-if="results.left && results.right">
            Différence de mémoire : 
            <span :class="results.left.memoryUsage < results.right.memoryUsage ? 'tw-text-blue-400' : 'tw-text-green-400'">
              Le Code {{ results.left.memoryUsage < results.right.memoryUsage ? 'A' : 'B' }} utilise moins de mémoire
              <br>
              <span class="tw-font-semibold">({{ formatBytes(Math.abs(results.left.memoryUsage - results.right.memoryUsage)) }} de moins)</span>
            </span>
          </p>
        </div>
      </div>
    </div>
  </div>
</template> 