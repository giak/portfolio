interface TestResult {
  executionTime: number
  operationsPerSecond: number
  memoryUsage: number
  memoryPerIteration: number
  error?: string | undefined
}

interface TestMessage {
  code: string
  iterations: number
}

function createMemoryTracker() {
  return {
    allocations: new WeakMap(),
    totalMemory: 0,
    
    track(obj: any): void {
      if (obj === null || obj === undefined) return;
      if (typeof obj !== 'object') return;
      if (this.allocations.has(obj)) return;
      
      const size = this.calculateSize(obj);
      this.allocations.set(obj, size);
      this.totalMemory += size;
      
      // Track nested objects
      if (Array.isArray(obj)) {
        obj.forEach(item => this.track(item));
      } else {
        Object.values(obj).forEach(value => this.track(value));
      }
    },
    
    calculateSize(value: any): number {
      if (value === null || value === undefined) return 0;
      if (typeof value === 'boolean') return 4;
      if (typeof value === 'number') return 8;
      if (typeof value === 'string') return value.length * 2;
      if (Array.isArray(value)) return 40 + (value.length * 8); // Array overhead + pointers
      if (value instanceof Map || value instanceof Set) return 40 + (value.size * 8);
      if (typeof value === 'object') return 40 + Object.keys(value).length * 8; // Object overhead + property pointers
      return 8; // Default size for other types
    }
  };
}

const MAX_ITERATIONS = 100000;
const MAX_EXECUTION_TIME = 5000; // 5 seconds
const MAX_MEMORY_USAGE = 100 * 1024 * 1024; // 100MB

const runTest = async (code: string, iterations: number): Promise<TestResult> => {
  try {
    // Limit iterations for security
    if (iterations > MAX_ITERATIONS) {
      throw new Error(`Nombre d'itérations limité à ${MAX_ITERATIONS} pour des raisons de sécurité`);
    }

    // Create a new memory tracker for this test
    const memoryTracker = createMemoryTracker();
    
    // Create a proxy to intercept object creations
    const handler = {
      construct(target: any, args: any[]): any {
        const obj = new target(...args);
        memoryTracker.track(obj);
        
        // Check memory usage
        if (memoryTracker.totalMemory > MAX_MEMORY_USAGE) {
          throw new Error(`Limite de mémoire dépassée (${formatBytes(MAX_MEMORY_USAGE)})`);
        }
        
        return obj;
      },
      
      set(target: any, prop: string, value: any): boolean {
        if (typeof value === 'object' && value !== null) {
          memoryTracker.track(value);
        }
        target[prop] = value;
        return true;
      },

      get(target: any, prop: string): any {
        const value = target[prop];
        if (typeof value === 'function') {
          return function(...args: any[]) {
            const result = value.apply(target, args);
            if (typeof result === 'object' && result !== null) {
              memoryTracker.track(result);
            }
            return result;
          };
        }
        return value;
      }
    };

    // Create proxied globals with enhanced tracking
    const globals = {
      Array: new Proxy(Array, handler),
      Object: new Proxy(Object, handler),
      Map: new Proxy(Map, handler),
      Set: new Proxy(Set, handler),
      JSON: new Proxy(JSON, {
        get(target: any, prop: string): any {
          const value = target[prop];
          if (typeof value === 'function') {
            return function(...args: any[]) {
              const result = value.apply(target, args);
              if (prop === 'parse' && typeof result === 'object' && result !== null) {
                memoryTracker.track(result);
                
                // Check memory usage
                if (memoryTracker.totalMemory > MAX_MEMORY_USAGE) {
                  throw new Error(`Limite de mémoire dépassée (${formatBytes(MAX_MEMORY_USAGE)})`);
                }
              }
              return result;
            };
          }
          return value;
        }
      })
    };
    
    // Create function with proxied globals
    const testFn = new Function(...Object.keys(globals), code);
    
    const start = performance.now();
    
    // Run the code for the specified number of iterations
    for (let i = 0; i < iterations; i++) {
      // Check execution time
      if (performance.now() - start > MAX_EXECUTION_TIME) {
        throw new Error(`Temps d'exécution maximum dépassé (${MAX_EXECUTION_TIME}ms)`);
      }

      const result = testFn(...Object.values(globals));
      if (typeof result === 'object' && result !== null) {
        memoryTracker.track(result);
        
        // Check memory usage
        if (memoryTracker.totalMemory > MAX_MEMORY_USAGE) {
          throw new Error(`Limite de mémoire dépassée (${formatBytes(MAX_MEMORY_USAGE)})`);
        }
      }
    }
    
    const end = performance.now();
    
    const executionTime = end - start;
    const operationsPerSecond = Math.floor((iterations / executionTime) * 1000);
    const totalMemory = memoryTracker.totalMemory;
    const memoryPerIteration = totalMemory / iterations;

    return {
      executionTime,
      operationsPerSecond,
      memoryUsage: totalMemory,
      memoryPerIteration,
      error: undefined
    };
  } catch (error) {
    return {
      executionTime: 0,
      operationsPerSecond: 0,
      memoryUsage: 0,
      memoryPerIteration: 0,
      error: error instanceof Error ? error.message : 'Unknown error'
    };
  }
};

function formatBytes(bytes: number): string {
  if (bytes === 0) return '0 B';
  const k = 1024;
  const sizes = ['B', 'KB', 'MB', 'GB'];
  const i = Math.floor(Math.log(Math.abs(bytes)) / Math.log(k));
  return `${(bytes / Math.pow(k, i)).toFixed(2)} ${sizes[i]}`;
}

self.addEventListener('message', async (event: MessageEvent<TestMessage>) => {
  const { code, iterations } = event.data;
  const result = await runTest(code, iterations);
  self.postMessage(result);
});

export { }

