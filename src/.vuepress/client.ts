import { defineClientConfig } from "vuepress/client";
import PerformanceCompare from "./components/code/performance/PerformanceCompare.vue";
import ContactForm from "./components/ContactForm.vue";
import PortfolioPresentation from "./components/PortfolioPresentation.vue";
import PortfolioRealisations from "./components/PortfolioRealisations.vue";
import PortfolioSolution from "./components/PortfolioSolution.vue";

export default defineClientConfig({
  enhance(context) {
    const { app } = context;
    app.component("PerformanceCompare", PerformanceCompare);
    app.component("ContactForm", ContactForm);
    app.component("PortfolioRealisations", PortfolioRealisations);
    app.component("PortfolioPresentation", PortfolioPresentation);
    app.component("PortfolioSolution", PortfolioSolution);

    // Configuration globale pour les icônes
    if (typeof window !== "undefined") {
      // S'assurer que le code s'exécute uniquement côté client
      app.mixin({
        mounted() {
          // Forcer la mise à jour après le montage
          this.$nextTick(() => {
            this.$forceUpdate();
          });
        },
      });
    }
  },
}); 