import type { Plugin } from 'vuepress'

export const canonicalUrlPlugin: Plugin = {
  name: 'vuepress-plugin-canonical-url',
  extendsPage: (page) => {
    const baseUrl = 'hhttps://giak.gitlab.io/portfolio/'
    const canonicalUrl = `${baseUrl}${page.path}`
    
    if (!page.frontmatter.head) {
      page.frontmatter.head = []
    }

    page.frontmatter.head.push([
      'link',
      {
        rel: 'canonical',
        href: canonicalUrl
      }
    ])
  }
} 