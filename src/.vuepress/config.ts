import { viteBundler } from "@vuepress/bundler-vite";
import { dirname, resolve } from 'path';
import { fileURLToPath } from 'url';
import { defineUserConfig } from "vuepress";
import { canonicalUrlPlugin } from './plugins/canonicalUrlPlugin';
import theme from "./theme.js";

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

export default defineUserConfig({
  lang: "fr-FR",
  title: "Christophe Giacomel - développeur Web JavaScript",
  description: "Développeur Web Javascript FullStack Adonis.js / Nest.js / Vue.js / Angular. Votre solution pour les projets Web.",

  bundler: viteBundler({
    viteOptions: {
      css: {
        postcss: './src/.vuepress/styles/postcss.config.cjs',
      },
    },
  }),

  head: [
    ["link", { rel: "preconnect", href: "https://fonts.googleapis.com" }],
    [
      "link",
      { rel: "preconnect", href: "https://fonts.gstatic.com", crossorigin: "" },
    ],
    [
      "link",
      {
        href: "https://fonts.googleapis.com/css2?family=Noto+Sans:ital,wght@0,100..900;1,100..900&display=swap",
        rel: "stylesheet",
      },
    ],
    [
      "script",
      {},
      `
var sc_project=12854158;
var sc_invisible=1;
var sc_security="1ebfe487";
      `,
    ],
    [
      "script",
      {
        async: true,
        src: "https://www.statcounter.com/counter/counter.js",
      },
    ],
    ['link', { 
      rel: 'canonical', 
      href: 'https://giak.gitlab.io/portfolio/' 
    }],
    [
      "meta",
      {
        name: "google-site-verification",
        content: "glFGExJZTE_d0JFXQyDklHzXxvWgzV40FVzGgHWbbVY"
      }
    ]
  ],

  base: "/portfolio/",

  dest: "public",

  locales: {
    "/": {
      lang: "fr-FR",
      title: "Christophe Giacomel - développeur Web JavaScript",
      description:
        "Développeur Web Javascript FullStack Adonis.js / Nest.js / Vue.js / Angular. Votre solution pour les projets Web.",
    },
  },

  theme,
  
  clientConfigFile: resolve(__dirname, './client.ts'),
  plugins: [
    canonicalUrlPlugin
  ],
  shouldPrefetch: (file) => {
    const criticalPaths = [
      'README',
      'contact',
      'portfolio'
    ]
    return criticalPaths.some(path => file.includes(path))
  }
});
