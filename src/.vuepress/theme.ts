import { hopeTheme } from "vuepress-theme-hope";
import { enNavbar as navbar } from "./navbar/index.js";
import sidebar from "./sidebar.js";

export default hopeTheme({
  hostname: "https://giak.gitlab.io",

  author: {
    name: "Christophe Giacomel",
    url: "https://giak.gitlab.io/cv/",
    email: "christophe.giacomel@proton.me",
  },

  iconAssets: "fontawesome-with-brands",

  logo: "/logo.svg",

  favicon: "/favicon.ico",

  repo: "https://gitlab.com/giak/portfolio",
  repoLabel: "portfolio",

  docsDir: "src",

  navbar,
  sidebar,

  displayFooter: true,

  encrypt: {
    config: {},
  },

  blog: {
    articlePerPage: 5,
    description: "A FullStack programmer",
    intro: "/intro.html",
    medias: {
      Gitlab: "https://gitlab.com/giak",
      Github: "https://github.com/giak",
      Linkedin: "https://www.linkedin.com/in/christophe-giacomel/",
      Twitter: "https://twitter.com/ChrisGiacomel",
      Email: "mailto:christophe.giacomel@proton.me",
    },
  },

  metaLocales: {},

  plugins: {
    blog: true,    
    sitemap: {
      changefreq: "daily",
      excludeUrls: ["/404.html"],
      extraUrls: ["/"],
      sitemapFilename: "sitemap.xml",
      priority: 0.7,
      modifyTimeGetter: (page): string => {
        const gitData = page.data?.git as { updatedTime?: number } | undefined;
        return gitData?.updatedTime 
          ? new Date(gitData.updatedTime).toISOString() 
          : new Date().toISOString();
      }
    },

    components: {
      components: ["Badge", "VPCard"],
    },

    markdownImage: {
      figure: true,
      lazyload: true,
      size: true,
    },

    mdEnhance: {
      align: true,
      attrs: true,
      component: true,
      demo: true,
      echarts: true,
      include: true,
      mark: true,
      mermaid: true,
      plantuml: true,
      spoiler: true,
      stylize: [
        {
          matcher: "Recommended",
          replacer: ({ tag }) => {
            if (tag === "em")
              return {
                tag: "Badge",
                attrs: { type: "tip" },
                content: "Recommended",
              };
          },
        },
      ],
      sub: true,
      sup: true,
      tasklist: true,
      vPre: true,
    },
  },

  hotReload: true,

  pure: false,
});
