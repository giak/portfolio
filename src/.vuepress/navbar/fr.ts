import { navbar } from 'vuepress-theme-hope';

export const enNavbar = navbar([
    '/',
    { text: 'Solutions', icon: 'lightbulb', link: '/solution/' },
    { text: 'Réalisations', icon: 'laptop-code', link: '/realisations/' },
    {
        text: 'Articles',
        icon: 'book',
        link: '/article/',
        activeMatch: '/article/',
        children: [
            // { text: 'Tout les articles', icon: 'newspaper', link: '/article/' },
            { text: 'Design Patterns', icon: 'layer-group', link: '/article/designpattern/' },
            { text: 'JavaScript', icon: 'fa-brands fa-square-js', link: '/article/javascript/' },
            { text: 'Principes SOLID', icon: 'cubes', link: '/article/solid/' },
            { text: 'Clean Architecture', icon: 'sitemap', link: '/article/cleanarchitecture/' },
            { text: 'Vue.js', icon: 'fa-brands fa-vuejs', link: '/article/javascript/vuejs/' },
            { text: 'Gestion de Projet', icon: 'diagram-project', link: '/article/gestion-projet/' },
            { text: 'Clean Code', icon: 'code-branch', link: '/article/clean-code/' },
        ],
    },
    {
        text: 'Code',
        icon: 'terminal',
        children: [
            {
                text: 'Outils',
                icon: 'screwdriver-wrench',
                children: [{ text: 'Comparateur de Performance', icon: 'gauge-high', link: '/code/toolling/' }],
            },
        ],
    },
    {
        text: 'CV',
        icon: 'user-tie',
        link: 'https://giak.gitlab.io/cv/',
    },
    {
        text: 'Gitlab',
        icon: 'fa-brands fa-square-gitlab',
        link: 'https://gitlab.com/giak/portfolio',
    },
    {
        text: 'Contact',
        icon: 'envelope',
        link: '/contact/',
    },
]);
