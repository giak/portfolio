import { sidebar } from 'vuepress-theme-hope';

export default sidebar({
    sidebarSorter: ['readme', 'order', 'title', 'filename'],
    '/': [
        '',
        {
            text: 'Articles',
            icon: 'book',
            prefix: 'article/',
            children: 'structure',
        },
    ],
});
